var classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite =
[
    [ "MyTopLevelComposite", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a9fbbfb4765a283e912ddb2fd11d154ae", null ],
    [ "checkSubclass", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#ae7bebae87432d464696041f99ccc68e9", null ],
    [ "getMessageTextWindow", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#ac689b0fb6a3d3637420f096951c53500", null ],
    [ "getMyTabbedPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a05d5dd5f776015d1b8c9c203237ccb21", null ],
    [ "getProjectPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a27de06dbfec83c5f0256f3d7d7dff31d", null ],
    [ "getSashFormLeftRight", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#aa9aa1ac209487df4cf3e618125de6064", null ],
    [ "getSashFormUpDw", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a87355b58ea3abcc8d682645f1297f72e", null ],
    [ "isConnectedToMatlab", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#adc5ce88d248d1cdfa1c7d0b390874468", null ],
    [ "setConnectedToMatlab", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a2ed00ba8b53b42b712adbfdf3f80ec95", null ],
    [ "setProjectPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#abea2b97f7db6a1d05eb94fd8cd87c914", null ],
    [ "toggleConnectionStatus", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a1151ad1e3d11f0eb482f9d71d4e2ed49", null ],
    [ "_connectedToMatlab", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#ac24ae6ef121a6e160e10d479fd3a15f9", null ],
    [ "_projectPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a5e05cfc38416712d4d05d1abd41e77ea", null ],
    [ "_sashFormLeftRight", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a45d38e215188dcacf2e60909bfb573b8", null ],
    [ "_sashFormUpDw", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#ac2c68e8e5b01ce4427251730457af9c4", null ],
    [ "_tabbedPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#ae46f9fbce6e26ba45951e5f966321be9", null ],
    [ "_tabFolderLog", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a17d3216b788653d0488f2a883e90d656", null ],
    [ "_tabItemLog", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a9beb1084266c52a45646b4e3d4a240ab", null ],
    [ "_textLogger", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#aa38da6401a2be79ae940be9efc9e8279", null ],
    [ "_theAircraft", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a32ea4b2a48b11ba5355fbf8a86dd0c2b", null ],
    [ "_theAnalysis", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html#a82f64c376bef435149bae4f8620405b7", null ]
];