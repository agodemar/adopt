var classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z =
[
    [ "MyLineMeshYZ", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#ae28f6ec3240e5decd1156d3c058abc69", null ],
    [ "get_CurvesSection", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#a411c7d3441ca963f41133f5227977460", null ],
    [ "get_Fuselage", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#affa9c79b60f0600784c7046133fffd9c", null ],
    [ "get_meshPoints", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#ac15ef11d453abc8d58254269ab312274", null ],
    [ "get_ni_Sec", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#a5ba90f9268f84e8fa14c1d1795719211", null ],
    [ "set_CurvesSection", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#a3984752ec6a794a8b102edc5ba795d15", null ],
    [ "set_Fuselage", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#aad06066ff7370174477f9dcc634cc80e", null ],
    [ "set_ni_Sec", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#a85315385047a5157f6887eb40b6dc42c", null ],
    [ "_meshPoints", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#ae650e506c4c66b3b36a0f11199592501", null ],
    [ "_ni_Sec", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#ae987ed858d578d5c3dbb304867554552", null ],
    [ "_theCurvesSection", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#a5a3a2e8f5c870cc23e5688b17897e394", null ],
    [ "_theFuselage", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#a1f5b1f8ac50ef036fe82bb71a458f8fb", null ],
    [ "_x", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html#ad6f8912126adb297fe8305dfc3e600b6", null ]
];