var hierarchy =
[
    [ "aircraft.calculators.ACAnalysisManager", "classaircraft_1_1calculators_1_1_a_c_analysis_manager.html", null ],
    [ "aircraft.calculators.ACCalculatorManager", "classaircraft_1_1calculators_1_1_a_c_calculator_manager.html", [
      [ "aircraft.calculators.ACAerodynamicsManager", "classaircraft_1_1calculators_1_1_a_c_aerodynamics_manager.html", null ],
      [ "aircraft.calculators.ACBalanceManager", "classaircraft_1_1calculators_1_1_a_c_balance_manager.html", null ],
      [ "aircraft.calculators.ACWeightsManager", "classaircraft_1_1calculators_1_1_a_c_weights_manager.html", null ],
      [ "aircraft.calculators.costs.MyCosts", "classaircraft_1_1calculators_1_1costs_1_1_my_costs.html", null ]
    ] ],
    [ "aircraft.calculators.ACDynamicsManager", "classaircraft_1_1calculators_1_1_a_c_dynamics_manager.html", null ],
    [ "aircraft.calculators.ACPerformanceManager", "classaircraft_1_1calculators_1_1_a_c_performance_manager.html", null ],
    [ "aircraft.calculators.ACStabilityManager", "classaircraft_1_1calculators_1_1_a_c_stability_manager.html", null ],
    [ "aircraft.calculators.ACStructuralCalculatorManager", "classaircraft_1_1calculators_1_1_a_c_structural_calculator_manager.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.OldTestViewer.Action", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_old_test_viewer_1_1_action.html", null ],
    [ "configuration.enumerations.AeroConfigurationTypeEnum", "enumconfiguration_1_1enumerations_1_1_aero_configuration_type_enum.html", null ],
    [ "calculators.aerodynamics.AerodynamicCalc", "classcalculators_1_1aerodynamics_1_1_aerodynamic_calc.html", null ],
    [ "database.databasefunctions.aerodynamics.AerodynamicDatabaseReader", "classdatabase_1_1databasefunctions_1_1aerodynamics_1_1_aerodynamic_database_reader.html", null ],
    [ "database.databasefunctions.aerodynamics.AerodynamicsDatabaseManager", "classdatabase_1_1databasefunctions_1_1aerodynamics_1_1_aerodynamics_database_manager.html", null ],
    [ "aircraft.components.Aircraft", "classaircraft_1_1components_1_1_aircraft.html", null ],
    [ "aircraft.components.AircraftCreator", "classaircraft_1_1components_1_1_aircraft_creator.html", null ],
    [ "configuration.enumerations.AircraftTypeEnum", "enumconfiguration_1_1enumerations_1_1_aircraft_type_enum.html", null ],
    [ "configuration.enumerations.AirfoilFamilyEnum", "enumconfiguration_1_1enumerations_1_1_airfoil_family_enum.html", null ],
    [ "configuration.enumerations.AirfoilTypeEnum", "enumconfiguration_1_1enumerations_1_1_airfoil_type_enum.html", null ],
    [ "configuration.enumerations.AnalysisTypeEnum", "enumconfiguration_1_1enumerations_1_1_analysis_type_enum.html", null ],
    [ "calculators.aerodynamics.AnglesCalc", "classcalculators_1_1aerodynamics_1_1_angles_calc.html", null ],
    [ "standaloneutils.atmosphere.AtmosphereCalc", "classstandaloneutils_1_1atmosphere_1_1_atmosphere_calc.html", null ],
    [ "aircraft.auxiliary.AuxiliaryComponentCalculator", "classaircraft_1_1auxiliary_1_1_auxiliary_component_calculator.html", [
      [ "aircraft.auxiliary.airfoil.Aerodynamics", "classaircraft_1_1auxiliary_1_1airfoil_1_1_aerodynamics.html", null ],
      [ "aircraft.auxiliary.airfoil.Geometry", "classaircraft_1_1auxiliary_1_1airfoil_1_1_geometry.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.SubdivisionMesh.BoundaryMode", "enumit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1_subdivision_mesh_1_1_boundary_mode.html", null ],
    [ "cad.jcae.CADExplorer", "interfacecad_1_1jcae_1_1_c_a_d_explorer.html", [
      [ "cad.occ.OCCExplorer", "classcad_1_1occ_1_1_o_c_c_explorer.html", null ]
    ] ],
    [ "cad.jcae.CADGeomCurve2D", "interfacecad_1_1jcae_1_1_c_a_d_geom_curve2_d.html", [
      [ "cad.occ.OCCGeomCurve2D", "classcad_1_1occ_1_1_o_c_c_geom_curve2_d.html", null ]
    ] ],
    [ "cad.jcae.CADGeomCurve3D", "interfacecad_1_1jcae_1_1_c_a_d_geom_curve3_d.html", [
      [ "cad.occ.OCCGeomCurve3D", "classcad_1_1occ_1_1_o_c_c_geom_curve3_d.html", null ]
    ] ],
    [ "cad.jcae.CADGeomSurface", "interfacecad_1_1jcae_1_1_c_a_d_geom_surface.html", [
      [ "cad.occ.OCCGeomSurface", "classcad_1_1occ_1_1_o_c_c_geom_surface.html", null ]
    ] ],
    [ "cad.jcae.CADIterator", "interfacecad_1_1jcae_1_1_c_a_d_iterator.html", [
      [ "cad.occ.OCCIterator", "classcad_1_1occ_1_1_o_c_c_iterator.html", null ]
    ] ],
    [ "cad.jcae.CADShape", "interfacecad_1_1jcae_1_1_c_a_d_shape.html", [
      [ "cad.jcae.CADCompound", "interfacecad_1_1jcae_1_1_c_a_d_compound.html", [
        [ "cad.occ.OCCCompound", "classcad_1_1occ_1_1_o_c_c_compound.html", null ]
      ] ],
      [ "cad.jcae.CADCompSolid", "interfacecad_1_1jcae_1_1_c_a_d_comp_solid.html", [
        [ "cad.occ.OCCCompSolid", "classcad_1_1occ_1_1_o_c_c_comp_solid.html", null ]
      ] ],
      [ "cad.jcae.CADEdge", "interfacecad_1_1jcae_1_1_c_a_d_edge.html", [
        [ "cad.occ.OCCEdge", "classcad_1_1occ_1_1_o_c_c_edge.html", null ]
      ] ],
      [ "cad.jcae.CADFace", "interfacecad_1_1jcae_1_1_c_a_d_face.html", [
        [ "cad.occ.OCCFace", "classcad_1_1occ_1_1_o_c_c_face.html", null ]
      ] ],
      [ "cad.jcae.CADShell", "interfacecad_1_1jcae_1_1_c_a_d_shell.html", [
        [ "cad.occ.OCCShell", "classcad_1_1occ_1_1_o_c_c_shell.html", null ]
      ] ],
      [ "cad.jcae.CADSolid", "interfacecad_1_1jcae_1_1_c_a_d_solid.html", [
        [ "cad.occ.OCCSolid", "classcad_1_1occ_1_1_o_c_c_solid.html", null ]
      ] ],
      [ "cad.jcae.CADVertex", "interfacecad_1_1jcae_1_1_c_a_d_vertex.html", [
        [ "cad.occ.OCCVertex", "classcad_1_1occ_1_1_o_c_c_vertex.html", null ]
      ] ],
      [ "cad.jcae.CADWire", "interfacecad_1_1jcae_1_1_c_a_d_wire.html", [
        [ "cad.occ.OCCWire", "classcad_1_1occ_1_1_o_c_c_wire.html", null ]
      ] ],
      [ "cad.occ.OCCShape", "classcad_1_1occ_1_1_o_c_c_shape.html", [
        [ "cad.occ.OCCCompound", "classcad_1_1occ_1_1_o_c_c_compound.html", null ],
        [ "cad.occ.OCCCompSolid", "classcad_1_1occ_1_1_o_c_c_comp_solid.html", null ],
        [ "cad.occ.OCCEdge", "classcad_1_1occ_1_1_o_c_c_edge.html", null ],
        [ "cad.occ.OCCFace", "classcad_1_1occ_1_1_o_c_c_face.html", null ],
        [ "cad.occ.OCCShell", "classcad_1_1occ_1_1_o_c_c_shell.html", null ],
        [ "cad.occ.OCCSolid", "classcad_1_1occ_1_1_o_c_c_solid.html", null ],
        [ "cad.occ.OCCVertex", "classcad_1_1occ_1_1_o_c_c_vertex.html", null ],
        [ "cad.occ.OCCWire", "classcad_1_1occ_1_1_o_c_c_wire.html", null ]
      ] ]
    ] ],
    [ "cad.jcae.CADShapeFactory", "classcad_1_1jcae_1_1_c_a_d_shape_factory.html", [
      [ "cad.occ.OCCShapeFactory", "classcad_1_1occ_1_1_o_c_c_shape_factory.html", null ]
    ] ],
    [ "cad.jcae.CADShapeTypes", "classcad_1_1jcae_1_1_c_a_d_shape_types.html", [
      [ "cad.occ.OCCShapeTypes", "classcad_1_1occ_1_1_o_c_c_shape_types.html", null ]
    ] ],
    [ "cad.jcae.CADWireExplorer", "interfacecad_1_1jcae_1_1_c_a_d_wire_explorer.html", [
      [ "cad.occ.OCCWireExplorer", "classcad_1_1occ_1_1_o_c_c_wire_explorer.html", null ]
    ] ],
    [ "aircraft.auxiliary.airfoil.Aerodynamics.CalculateCd", "classaircraft_1_1auxiliary_1_1airfoil_1_1_aerodynamics_1_1_calculate_cd.html", null ],
    [ "aircraft.auxiliary.airfoil.Aerodynamics.CalculateCdWaveDrag", "classaircraft_1_1auxiliary_1_1airfoil_1_1_aerodynamics_1_1_calculate_cd_wave_drag.html", null ],
    [ "aircraft.auxiliary.airfoil.Aerodynamics.CalculateCl", "classaircraft_1_1auxiliary_1_1airfoil_1_1_aerodynamics_1_1_calculate_cl.html", null ],
    [ "aircraft.auxiliary.airfoil.Aerodynamics.CalculateClAlpha", "classaircraft_1_1auxiliary_1_1airfoil_1_1_aerodynamics_1_1_calculate_cl_alpha.html", null ],
    [ "aircraft.components.fuselage.FusAerodynamicsManager.CalculateCm0", "classaircraft_1_1components_1_1fuselage_1_1_fus_aerodynamics_manager_1_1_calculate_cm0.html", null ],
    [ "aircraft.components.fuselage.FusAerodynamicsManager.CalculateCmAlpha", "classaircraft_1_1components_1_1fuselage_1_1_fus_aerodynamics_manager_1_1_calculate_cm_alpha.html", null ],
    [ "aircraft.components.fuselage.FusAerodynamicsManager.CalculateCmCL", "classaircraft_1_1components_1_1fuselage_1_1_fus_aerodynamics_manager_1_1_calculate_cm_c_l.html", null ],
    [ "aircraft.auxiliary.airfoil.Aerodynamics.CalculateMachCr", "classaircraft_1_1auxiliary_1_1airfoil_1_1_aerodynamics_1_1_calculate_mach_cr.html", null ],
    [ "aircraft.components.liftingSurface.LSGeometryManager.CalculateThickness", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_geometry_manager_1_1_calculate_thickness.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseTokenizer.Callback", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_tokenizer_1_1_callback.html", [
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.FileParserCallback", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_file_parser_callback.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.MaterialListParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_material_list_parser.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.MeshParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_mesh_parser.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.MeshParser.MeshFaceList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_mesh_parser_1_1_mesh_face_list.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.MeshParser.MeshTFaceList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_16adf9f9016d50fec13edfdefb27ec016.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.MeshParser.MeshTVertexList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1110b400518175acd65c28c34d08b606f.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.MeshParser.MeshVertexList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_17fa1d9b70744b7e0a585ccb7988e018f.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.NodeParserBase", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_node_parser_base.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.CameraNodeParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_camera_node_parser.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.GeomNodeParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_geom_node_parser.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.LightNodeParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_light_node_parser.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.NodeParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_node_parser.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser.NodeTMParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser_1_1_node_t_m_parser.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseTokenizer.CallbackNOP", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_tokenizer_1_1_callback_n_o_p.html", null ]
    ] ],
    [ "standaloneutils.customdata.CenterOfGravity", "classstandaloneutils_1_1customdata_1_1_center_of_gravity.html", null ],
    [ "aircraft.auxiliary.SeatsBlock.CGboarding", "classaircraft_1_1auxiliary_1_1_seats_block_1_1_c_gboarding.html", null ],
    [ "cad.occ.OCCDiscretizeCurve3D.CheckRatio", "interfacecad_1_1occ_1_1_o_c_c_discretize_curve3_d_1_1_check_ratio.html", [
      [ "cad.occ.OCCDiscretizeCurve3D.CheckRatioDeflection", "classcad_1_1occ_1_1_o_c_c_discretize_curve3_d_1_1_check_ratio_deflection.html", null ],
      [ "cad.occ.OCCDiscretizeCurve3D.CheckRatioLength", "classcad_1_1occ_1_1_o_c_c_discretize_curve3_d_1_1_check_ratio_length.html", null ]
    ] ],
    [ "configuration.enumerations.ClassTypeEnum", "enumconfiguration_1_1enumerations_1_1_class_type_enum.html", null ],
    [ "Cloneable", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList.SubList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list_1_1_sub_list.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList.SubList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list_1_1_sub_list.html", null ]
      ] ]
    ] ],
    [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CoefficientWrapper", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_coefficient_wrapper.html", null ],
    [ "Comparable", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.MPath", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_path.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.MPath.Component", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_path_1_1_component.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MPath.Index", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_path_1_1_index.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MPath.Select", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_path_1_1_select.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MPath.Slice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_path_1_1_slice.html", null ]
      ] ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MComponentList.Component", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_component_list_1_1_component.html", null ],
    [ "aircraft.componentmodel.Component", "classaircraft_1_1componentmodel_1_1_component.html", [
      [ "aircraft.componentmodel.AeroComponent", "classaircraft_1_1componentmodel_1_1_aero_component.html", [
        [ "aircraft.components.fuselage.Fuselage", "classaircraft_1_1components_1_1fuselage_1_1_fuselage.html", null ],
        [ "aircraft.components.liftingSurface.LiftingSurface", "classaircraft_1_1components_1_1lifting_surface_1_1_lifting_surface.html", [
          [ "aircraft.components.liftingSurface.Canard", "classaircraft_1_1components_1_1lifting_surface_1_1_canard.html", null ],
          [ "aircraft.components.liftingSurface.HTail", "classaircraft_1_1components_1_1lifting_surface_1_1_h_tail.html", null ],
          [ "aircraft.components.liftingSurface.VTail", "classaircraft_1_1components_1_1lifting_surface_1_1_v_tail.html", null ],
          [ "aircraft.components.liftingSurface.Wing", "classaircraft_1_1components_1_1lifting_surface_1_1_wing.html", null ]
        ] ]
      ] ],
      [ "aircraft.components.FuelTank", "classaircraft_1_1components_1_1_fuel_tank.html", null ],
      [ "aircraft.components.LandingGear", "classaircraft_1_1components_1_1_landing_gear.html", null ],
      [ "aircraft.components.nacelles.Nacelle", "classaircraft_1_1components_1_1nacelles_1_1_nacelle.html", null ],
      [ "aircraft.components.powerPlant.Engine", "classaircraft_1_1components_1_1power_plant_1_1_engine.html", null ],
      [ "aircraft.components.powerPlant.PowerPlant", "classaircraft_1_1components_1_1power_plant_1_1_power_plant.html", null ],
      [ "aircraft.components.Systems", "classaircraft_1_1components_1_1_systems.html", null ]
    ] ],
    [ "aircraft.componentmodel.componentcalcmanager.ComponentCalculator", "classaircraft_1_1componentmodel_1_1componentcalcmanager_1_1_component_calculator.html", [
      [ "aircraft.componentmodel.componentcalcmanager.AerodynamicsManager", "classaircraft_1_1componentmodel_1_1componentcalcmanager_1_1_aerodynamics_manager.html", [
        [ "aircraft.components.fuselage.FusAerodynamicsManager", "classaircraft_1_1components_1_1fuselage_1_1_fus_aerodynamics_manager.html", null ],
        [ "aircraft.components.liftingSurface.LSAerodynamicsManager", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager.html", null ],
        [ "aircraft.components.nacelles.NacAerodynamicsManager", "classaircraft_1_1components_1_1nacelles_1_1_nac_aerodynamics_manager.html", null ]
      ] ],
      [ "aircraft.componentmodel.componentcalcmanager.BalanceManager", "classaircraft_1_1componentmodel_1_1componentcalcmanager_1_1_balance_manager.html", [
        [ "aircraft.components.nacelles.NacBalanceManager", "classaircraft_1_1components_1_1nacelles_1_1_nac_balance_manager.html", null ],
        [ "aircraft.components.powerPlant.EngBalanceManager", "classaircraft_1_1components_1_1power_plant_1_1_eng_balance_manager.html", null ]
      ] ],
      [ "aircraft.componentmodel.componentcalcmanager.GeometryManager", "classaircraft_1_1componentmodel_1_1componentcalcmanager_1_1_geometry_manager.html", [
        [ "aircraft.components.liftingSurface.LSGeometryManager", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_geometry_manager.html", null ]
      ] ],
      [ "aircraft.componentmodel.componentcalcmanager.StructuresManager", "classaircraft_1_1componentmodel_1_1componentcalcmanager_1_1_structures_manager.html", null ],
      [ "aircraft.componentmodel.componentcalcmanager.WeightsManager", "classaircraft_1_1componentmodel_1_1componentcalcmanager_1_1_weights_manager.html", [
        [ "aircraft.components.liftingSurface.LSWeightsManager", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_weights_manager.html", null ],
        [ "aircraft.components.nacelles.NacWeightsManager", "classaircraft_1_1components_1_1nacelles_1_1_nac_weights_manager.html", null ],
        [ "aircraft.components.powerPlant.EngWeightsManager", "classaircraft_1_1components_1_1power_plant_1_1_eng_weights_manager.html", null ]
      ] ],
      [ "aircraft.components.fuselage.FusGeometryManager", "classaircraft_1_1components_1_1fuselage_1_1_fus_geometry_manager.html", null ]
    ] ],
    [ "configuration.enumerations.ComponentEnum", "enumconfiguration_1_1enumerations_1_1_component_enum.html", null ],
    [ "aircraft.components.Configuration", "classaircraft_1_1components_1_1_configuration.html", null ],
    [ "calculators.costs.CostsCalcUtils", "classcalculators_1_1costs_1_1_costs_calc_utils.html", null ],
    [ "it.unina.adopt.utilities.cpacs.CPACSConfiguration", "classit_1_1unina_1_1adopt_1_1utilities_1_1cpacs_1_1_c_p_a_c_s_configuration.html", null ],
    [ "it.unina.adopt.utilities.cpacs.CPACSWingProfile", "classit_1_1unina_1_1adopt_1_1utilities_1_1cpacs_1_1_c_p_a_c_s_wing_profile.html", null ],
    [ "it.unina.adopt.utilities.gui.CurveOnXYGraph", "classit_1_1unina_1_1adopt_1_1utilities_1_1gui_1_1_curve_on_x_y_graph.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.dae.DaeImporter", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1dae_1_1_dae_importer.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.dae.DaeImporter.DaeNode", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1dae_1_1_dae_importer_1_1_dae_node.html", null ],
    [ "standaloneutils.database.io.DatabaseFileReader", "classstandaloneutils_1_1database_1_1io_1_1_database_file_reader.html", null ],
    [ "standaloneutils.database.io.DatabaseFileWriter< E extends Enum< E >", "classstandaloneutils_1_1database_1_1io_1_1_database_file_writer.html", null ],
    [ "standaloneutils.database.io.DatabaseIOmanager< E extends Enum< E >", "classstandaloneutils_1_1database_1_1io_1_1_database_i_omanager.html", null ],
    [ "database.databasefunctions.DatabaseReader", "classdatabase_1_1databasefunctions_1_1_database_reader.html", [
      [ "database.databasefunctions.aerodynamics.vedsc.VeDSCDatabaseReader", "classdatabase_1_1databasefunctions_1_1aerodynamics_1_1vedsc_1_1_ve_d_s_c_database_reader.html", null ],
      [ "database.databasefunctions.engine.EngineDatabaseReader", "classdatabase_1_1databasefunctions_1_1engine_1_1_engine_database_reader.html", [
        [ "database.databasefunctions.engine.TurbofanEngineDatabaseReader", "classdatabase_1_1databasefunctions_1_1engine_1_1_turbofan_engine_database_reader.html", null ],
        [ "database.databasefunctions.engine.TurbopropEngineDatabaseReader", "classdatabase_1_1databasefunctions_1_1engine_1_1_turboprop_engine_database_reader.html", null ]
      ] ]
    ] ],
    [ "it.unina.adopt.viewer3d.cad.occ.DataProvider", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1_data_provider.html", [
      [ "it.unina.adopt.viewer3d.cad.occ.OCCMeshExtractor.EdgeData", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1_o_c_c_mesh_extractor_1_1_edge_data.html", null ],
      [ "it.unina.adopt.viewer3d.cad.occ.OCCMeshExtractor.FaceData", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1_o_c_c_mesh_extractor_1_1_face_data.html", null ],
      [ "it.unina.adopt.viewer3d.cad.occ.OCCMeshExtractor.VertexData", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1_o_c_c_mesh_extractor_1_1_vertex_data.html", null ]
    ] ],
    [ "it.unina.adopt.test.la.MyTest_LA_04.Department", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_test___l_a__04_1_1_department.html", null ],
    [ "calculators.aerodynamics.DragCalc", "classcalculators_1_1aerodynamics_1_1_drag_calc.html", null ],
    [ "standaloneutils.customdata.DragPolarPoint", "classstandaloneutils_1_1customdata_1_1_drag_polar_point.html", null ],
    [ "standaloneutils.customdata.DragPolarPoint.DragPolarPointEnum", "enumstandaloneutils_1_1customdata_1_1_drag_polar_point_1_1_drag_polar_point_enum.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.utils3d.DragSupport", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1utils3d_1_1_drag_support.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.SmoothingGroups.Edge", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_smoothing_groups_1_1_edge.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SymbolicSubdivisionBuilder.Edge", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_symbolic_subdivision_builder_1_1_edge.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SymbolicSubdivisionBuilder.EdgeInfo", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_symbolic_subdivision_builder_1_1_edge_info.html", null ],
    [ "calculators.performance.EnduranceCalc", "classcalculators_1_1performance_1_1_endurance_calc.html", null ],
    [ "database.databasefunctions.engine.EngineDatabaseManager", "classdatabase_1_1databasefunctions_1_1engine_1_1_engine_database_manager.html", null ],
    [ "configuration.enumerations.EngineMountingPositionEnum", "enumconfiguration_1_1enumerations_1_1_engine_mounting_position_enum.html", null ],
    [ "configuration.enumerations.EngineOperatingConditionEnum", "enumconfiguration_1_1enumerations_1_1_engine_operating_condition_enum.html", null ],
    [ "configuration.enumerations.EngineTypeEnum", "enumconfiguration_1_1enumerations_1_1_engine_type_enum.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.exporters.fxml.FXMLExporter.FXML.Entry", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1exporters_1_1fxml_1_1_f_x_m_l_exporter_1_1_f_x_m_l_1_1_entry.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MCharacterMapping.Entry", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_character_mapping_1_1_entry.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MPolyFace.FaceData", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_poly_face_1_1_face_data.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SymbolicSubdivisionBuilder.FaceInfo", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_symbolic_subdivision_builder_1_1_face_info.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MCompoundType.Field", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_compound_type_1_1_field.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.FourWayNavControl.FourWayListener", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_four_way_nav_control_1_1_four_way_listener.html", null ],
    [ "database.databasefunctions.aerodynamics.FusDesDatabaseReader", "classdatabase_1_1databasefunctions_1_1aerodynamics_1_1_fus_des_database_reader.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.exporters.fxml.FXMLExporter.FXML", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1exporters_1_1fxml_1_1_f_x_m_l_exporter_1_1_f_x_m_l.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.exporters.fxml.FXMLExporter", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1exporters_1_1fxml_1_1_f_x_m_l_exporter.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_00.GenericOperator< T >", "interfacesandbox_1_1adm_1_1_my_test___lambdas__00_1_1_generic_operator.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_01.GenericOperator< T >", "interfacesandbox_1_1adm_1_1_my_test___lambdas__01_1_1_generic_operator.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_00.GenericOperator< Integer >", "interfacesandbox_1_1adm_1_1_my_test___lambdas__00_1_1_generic_operator.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_01.GenericOperator< Integer >", "interfacesandbox_1_1adm_1_1_my_test___lambdas__01_1_1_generic_operator.html", null ],
    [ "standaloneutils.GeometryCalc", "classstandaloneutils_1_1_geometry_calc.html", null ],
    [ "it.unina.adopt.core.GlobalData", "classit_1_1unina_1_1adopt_1_1core_1_1_global_data.html", null ],
    [ "it.unina.adopt.utilities.gui.GridDataUtil", "classit_1_1unina_1_1adopt_1_1utilities_1_1gui_1_1_grid_data_util.html", null ],
    [ "it.unina.adopt.utilities.gui.GridLayoutUtil", "classit_1_1unina_1_1adopt_1_1utilities_1_1gui_1_1_grid_layout_util.html", null ],
    [ "examples.groups.H5Ex_G_Iterate", "classexamples_1_1groups_1_1_h5_ex___g___iterate.html", null ],
    [ "examples.groups.H5Ex_G_Traverse", "classexamples_1_1groups_1_1_h5_ex___g___traverse.html", null ],
    [ "standaloneutils.database.hdf.MyHDFReader.H5O_type", "enumstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader_1_1_h5_o__type.html", null ],
    [ "examples.groups.H5Ex_G_Iterate.H5O_type", "enumexamples_1_1groups_1_1_h5_ex___g___iterate_1_1_h5_o__type.html", null ],
    [ "it.unina.adopt.main.ADOPT_GUI.IExternalFunctionsHandler", "interfaceit_1_1unina_1_1adopt_1_1main_1_1_a_d_o_p_t___g_u_i_1_1_i_external_functions_handler.html", [
      [ "it.unina.adopt.externalFunctions.MyAnalysisExternalFunctions", "classit_1_1unina_1_1adopt_1_1external_functions_1_1_my_analysis_external_functions.html", null ],
      [ "it.unina.adopt.externalFunctions.MyBalanceExternalFunctions", "classit_1_1unina_1_1adopt_1_1external_functions_1_1_my_balance_external_functions.html", null ],
      [ "it.unina.adopt.externalFunctions.MyFuselageExternalFunctions", "classit_1_1unina_1_1adopt_1_1external_functions_1_1_my_fuselage_external_functions.html", null ],
      [ "it.unina.adopt.externalFunctions.MyLiftingSurfaceExternalFunctions", "classit_1_1unina_1_1adopt_1_1external_functions_1_1_my_lifting_surface_external_functions.html", null ],
      [ "it.unina.adopt.externalFunctions.MyWeightsExternalFunctions", "classit_1_1unina_1_1adopt_1_1external_functions_1_1_my_weights_external_functions.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.importers.Importer3D", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_importer3_d.html", null ],
    [ "it.unina.adopt.GUI.tree.IMyProjectTreeDeltaListener", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_i_my_project_tree_delta_listener.html", [
      [ "it.unina.adopt.GUI.tree.MyProjectContentProvider", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_my_project_content_provider.html", null ],
      [ "it.unina.adopt.main.MyNullAircraftDeltaListener", "classit_1_1unina_1_1adopt_1_1main_1_1_my_null_aircraft_delta_listener.html", null ]
    ] ],
    [ "aircraft.componentmodel.InnerCalculator", "classaircraft_1_1componentmodel_1_1_inner_calculator.html", [
      [ "aircraft.calculators.costs.MyFixedCharges.CalcCrewCosts", "classaircraft_1_1calculators_1_1costs_1_1_my_fixed_charges_1_1_calc_crew_costs.html", null ],
      [ "aircraft.calculators.costs.MyFixedCharges.CalcDepreciation", "classaircraft_1_1calculators_1_1costs_1_1_my_fixed_charges_1_1_calc_depreciation.html", null ],
      [ "aircraft.calculators.costs.MyFixedCharges.CalcInsurance", "classaircraft_1_1calculators_1_1costs_1_1_my_fixed_charges_1_1_calc_insurance.html", null ],
      [ "aircraft.calculators.costs.MyFixedCharges.CalcInterest", "classaircraft_1_1calculators_1_1costs_1_1_my_fixed_charges_1_1_calc_interest.html", null ],
      [ "aircraft.calculators.costs.MyTripCharges.CalcFuelAndOilCharges", "classaircraft_1_1calculators_1_1costs_1_1_my_trip_charges_1_1_calc_fuel_and_oil_charges.html", null ],
      [ "aircraft.calculators.costs.MyTripCharges.CalcGroundHandlingCharges", "classaircraft_1_1calculators_1_1costs_1_1_my_trip_charges_1_1_calc_ground_handling_charges.html", null ],
      [ "aircraft.calculators.costs.MyTripCharges.CalcLandingFees", "classaircraft_1_1calculators_1_1costs_1_1_my_trip_charges_1_1_calc_landing_fees.html", null ],
      [ "aircraft.calculators.costs.MyTripCharges.CalcMaintenanceCosts", "classaircraft_1_1calculators_1_1costs_1_1_my_trip_charges_1_1_calc_maintenance_costs.html", null ],
      [ "aircraft.calculators.costs.MyTripCharges.CalcNavigationalCharges", "classaircraft_1_1calculators_1_1costs_1_1_my_trip_charges_1_1_calc_navigational_charges.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcAlpha0L", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_alpha0_l.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcBasicLoad", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_basic_load.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCdWaveDrag", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_cd_wave_drag.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCL0", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_c_l0.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCLAlpha", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_c_l_alpha.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCLAtAlpha", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_c_l_at_alpha.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCLMaxClean", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_c_l_max_clean.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCLMaxFlapSlat", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_c_l_max_flap_slat.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCLvsAlphaCurve", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_c_lvs_alpha_curve.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCm0", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_cm0.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCmAC", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_cm_a_c.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcCmAlpha", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_cm_alpha.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcLiftDistribution", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_lift_distribution.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcMachCr", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_mach_cr.html", null ],
      [ "aircraft.components.liftingSurface.LSAerodynamicsManager.CalcXAC", "classaircraft_1_1components_1_1lifting_surface_1_1_l_s_aerodynamics_manager_1_1_calc_x_a_c.html", null ],
      [ "aircraft.components.nacelles.NacWeightsManager.Piston", "classaircraft_1_1components_1_1nacelles_1_1_nac_weights_manager_1_1_piston.html", null ],
      [ "aircraft.components.nacelles.NacWeightsManager.Turbofan", "classaircraft_1_1components_1_1nacelles_1_1_nac_weights_manager_1_1_turbofan.html", null ],
      [ "aircraft.components.nacelles.NacWeightsManager.Turboprop", "classaircraft_1_1components_1_1nacelles_1_1_nac_weights_manager_1_1_turboprop.html", null ],
      [ "aircraft.components.powerPlant.EngWeightsManager.Piston", "classaircraft_1_1components_1_1power_plant_1_1_eng_weights_manager_1_1_piston.html", null ],
      [ "aircraft.components.powerPlant.EngWeightsManager.Turbofan", "classaircraft_1_1components_1_1power_plant_1_1_eng_weights_manager_1_1_turbofan.html", null ],
      [ "aircraft.components.powerPlant.EngWeightsManager.Turboprop", "classaircraft_1_1components_1_1power_plant_1_1_eng_weights_manager_1_1_turboprop.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.importers.dae.DaeImporter.Input", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1dae_1_1_dae_importer_1_1_input.html", null ],
    [ "it.unina.adopt.GUI.tree.MyProjectTreePane.IopenInitiator", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_my_project_tree_pane_1_1_iopen_initiator.html", null ],
    [ "JavaDemo", "class_java_demo.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.exporters.javasource.JavaSourceExporter", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1exporters_1_1javasource_1_1_java_source_exporter.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.SkinningMesh.JointIndex", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1_skinning_mesh_1_1_joint_index.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.Optimizer.KeyInfo", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_optimizer_1_1_key_info.html", null ],
    [ "calculators.aerodynamics.LiftCalc", "classcalculators_1_1aerodynamics_1_1_lift_calc.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.Loader", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_loader.html", null ],
    [ "calculators.geometry.LSGeometryCalc", "classcalculators_1_1geometry_1_1_l_s_geometry_calc.html", null ],
    [ "databasesIO.vedscdatabase.Main", "classdatabases_i_o_1_1vedscdatabase_1_1_main.html", null ],
    [ "it.unina.adopt.test.la.Main", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_main.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.SubdivisionMesh.MapBorderMode", "enumit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1_subdivision_mesh_1_1_map_border_mode.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.MappingChannel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_mapping_channel.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.Material", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_material.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseTokenizer", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_tokenizer.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxLoader", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_loader.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.MayaImporter", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_maya_importer.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.MConnection", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_connection.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MData", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_data.html", [
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MDataImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_data_impl.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MArrayImpl.MArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_array_impl_1_1_m_array_slice.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MAttributeAliasImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_attribute_alias_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MBoolImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_bool_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MCharacterMappingImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_character_mapping_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MComponentListImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_component_list_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MCompoundImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_compound_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2ArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float2_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2ArrayImpl.MFloat2ArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_182d87129e50592cf42cf177f430d2d3d.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2Impl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float2_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2Impl.MFloat2Component", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1e4c04bf07fff3b3f2055bfb84fd47ed6.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3ArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float3_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3ArrayImpl.MFloat3ArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1c636c10b2e9989afe54e90aead1b7bfc.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3Impl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float3_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3Impl.MFloat3Component", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_16f13f0952d07f361b864a36fba1f081e.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloatArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloatArrayImpl.MFloatArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_11e322608665de9fa21c70dcb8680b4bf.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloatImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MInt3ArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int3_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MInt3ArrayImpl.MInt3ArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_128357e7b7503d627037d5a78f0dd9ed4.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MIntArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MIntArrayImpl.MIntArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1d3c326d95e2f25e4ee049b7a7205372a.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MIntImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MNurbsCurveImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_nurbs_curve_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MPointerImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_pointer_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MPolyFaceImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_poly_face_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MStringImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_string_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MArray", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_array.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MArrayImpl.MArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_array_impl_1_1_m_array_slice.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MAttributeAlias", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_attribute_alias.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MAttributeAliasImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_attribute_alias_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MBool", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_bool.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MBoolImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_bool_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MCharacterMapping", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_character_mapping.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MCharacterMappingImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_character_mapping_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MComponentList", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_component_list.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MComponentListImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_component_list_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MCompound", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_compound.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MCompoundImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_compound_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MFloat", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_float.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2Impl.MFloat2Component", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1e4c04bf07fff3b3f2055bfb84fd47ed6.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3Impl.MFloat3Component", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_16f13f0952d07f361b864a36fba1f081e.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloatImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MFloat2", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_float2.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2Impl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float2_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MFloat2Array", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_float2_array.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2ArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float2_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2ArrayImpl.MFloat2ArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_182d87129e50592cf42cf177f430d2d3d.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MFloat3", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_float3.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3Impl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float3_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MFloat3Array", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_float3_array.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3ArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float3_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3ArrayImpl.MFloat3ArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1c636c10b2e9989afe54e90aead1b7bfc.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MFloatArray", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_float_array.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloatArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloatArrayImpl.MFloatArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_11e322608665de9fa21c70dcb8680b4bf.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MInt", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_int.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MIntImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MInt3Array", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_int3_array.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MInt3ArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int3_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MInt3ArrayImpl.MInt3ArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_128357e7b7503d627037d5a78f0dd9ed4.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MIntArray", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_int_array.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MIntArrayImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int_array_impl.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MIntArrayImpl.MIntArraySlice", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1d3c326d95e2f25e4ee049b7a7205372a.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MNurbsCurve", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_nurbs_curve.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MNurbsCurveImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_nurbs_curve_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MPointer", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_pointer.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MPointerImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_pointer_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MPolyFace", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_poly_face.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MPolyFaceImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_poly_face_impl.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.MString", "interfaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1_m_string.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MStringImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_string_impl.html", null ]
      ] ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.Mesh", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_mesh.html", null ],
    [ "configuration.enumerations.MethodEnum", "enumconfiguration_1_1enumerations_1_1_method_enum.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.MObject", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_object.html", [
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.MAttribute", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_attribute.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.MNode", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_node.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.MNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_node_type.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.animClipType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1anim_clip_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.characterType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1character_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.clipLibraryType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1clip_library_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.clipSchedulerType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1clip_scheduler_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.dCollisionShape", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1d_collision_shape.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.dHingeConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1d_hinge_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.dNailConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1d_nail_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.dRigidBody", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1d_rigid_body.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.AbstractBaseCreateType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_abstract_base_create_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.AddDoubleLinearType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_add_double_linear_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.AimConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_aim_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurve", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveTA", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_t_a.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveTL", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_t_l.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveTT", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_t_t.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveTU", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_t_u.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveUA", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_u_a.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveUL", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_u_l.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveUT", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_u_t.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurveUU", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_u_u.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.BlendColorsType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_blend_colors_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.BlendShapeNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_blend_shape_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.BlendType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_blend_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.BlendWeightedType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_blend_weighted_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.BlinnNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_blinn_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.Bump2dNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_bump2d_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.CameraType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_camera_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.cgfxShaderType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1cgfx_shader_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.cgfxVectorType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1cgfx_vector_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.CharacterType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_character_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ChoiceNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_choice_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ClampType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_clamp_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ConditionType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_condition_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.Constraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ControlPointType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_control_point_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.CurveShapeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_curve_shape_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DagNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_dag_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DagPoseType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_dag_pose_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DeformableShapeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_deformable_shape_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DependNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_depend_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DirectionalLightType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_directional_light_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DynBase", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_dyn_base.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.EntityType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_entity_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.Field", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_field.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.FileType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_file_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.GeometryFilterNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_geometry_filter_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.GeometryShapeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_geometry_shape_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.GravityField", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_gravity_field.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.GroupIdType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_group_id_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.GroupPartsType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_group_parts_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.hwShaderType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1hw_shader_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.IKEffectorType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_i_k_effector_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.IKHandle", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_i_k_handle.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.JointNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_joint_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.LambertType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_lambert_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.LayeredTexture", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_layered_texture.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.LightLinkerNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_light_linker_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.LightType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_light_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.MaterialInfoNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_material_info_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.MeshNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_mesh_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.MotionPathType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_motion_path_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.MultiplyDivideType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_multiply_divide_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.NonAmbientLightShapeNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_non_ambient_light_shape_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.NonExtendedLightShapeNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_non_extended_light_shape_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.NurbsCurveType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_nurbs_curve_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ObjectSetType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_object_set_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.OrientConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_orient_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ParentConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_parent_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ParticleType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_particle_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PhongENodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_phong_e_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PhongNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_phong_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.Place2dTexture", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_place2d_texture.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PlusMinusAverageType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_plus_minus_average_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PointConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_point_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PointEmitter", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_point_emitter.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PointLightType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_point_light_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PoleVectorConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_pole_vector_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolyBaseType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_base_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolyCreatorType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_creator_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolyCubeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_cube_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolyCylinderType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_cylinder_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolyModifierType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_modifier_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolyNormalType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_normal_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolyPrimitiveType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_primitive_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PolySphereType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_poly_sphere_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.PsdFileTex", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_psd_file_tex.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.RadialField", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_radial_field.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ReflectType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_reflect_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.RenderLightType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_render_light_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ReverseType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_reverse_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.RigidBodyType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_rigid_body_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.RigidConstraint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_rigid_constraint.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ShadingEngineNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_shading_engine_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ShapeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_shape_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.SkinClusterNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_skin_cluster_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.SpotLightType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_spot_light_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.SurfaceShaderType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_surface_shader_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.SurfaceShapeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_surface_shape_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.Texture2DType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_texture2_d_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.TransformGeometryType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_transform_geometry_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.TransformNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_transform_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.TweakNodeType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_tweak_node_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.UnitConversionType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_unit_conversion_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.UvChooser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_uv_chooser.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.VortexField", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_vortex_field.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.nxRigidBody", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1nx_rigid_body.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MDataType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_data_type.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MArrayType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_array_type.html", [
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ControlPointType.UVSet", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_control_point_type_1_1_u_v_set.html", null ],
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DagNodeType.iogType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_dag_node_type_1_1iog_type.html", null ],
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DagNodeType.iogType.ogArrayType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_dag_noddc0b0e6433d9a73f7422b067442586b0.html", null ]
        ] ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MAttributeAliasType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_attribute_alias_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MBoolType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_bool_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MCharacterMappingType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_character_mapping_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MComponentListType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_component_list_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MCompoundType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_compound_type.html", [
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.animCurve.ktv", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1anim_curve_1_1ktv.html", null ],
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.ControlPointType.UVSet.UVSetElement", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_control906efac99284049f014ac9785605dd7d.html", null ],
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DagNodeType.iogType.ogsType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_dag_node_type_1_1iog_type_1_1ogs_type.html", null ],
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.DagNodeType.iogType.ogType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_dag_node_type_1_1iog_type_1_1og_type.html", null ]
        ] ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MFloat2ArrayType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_float2_array_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MFloat2Type", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_float2_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MFloat3ArrayType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_float3_array_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MFloat3Type", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_float3_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MFloatArrayType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_float_array_type.html", [
          [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MMatrixType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_matrix_type.html", null ]
        ] ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MFloatType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_float_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MInt3ArrayType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_int3_array_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MIntArrayType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_int_array_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MIntType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_int_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MNurbsCurveType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_nurbs_curve_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MPointerType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_pointer_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MPolyFaceType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_poly_face_type.html", null ],
        [ "it.unina.adopt.GUI.test.javafx.importers.maya.types.MStringType", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1types_1_1_m_string_type.html", null ]
      ] ]
    ] ],
    [ "calculators.aerodynamics.MomentCalc", "classcalculators_1_1aerodynamics_1_1_moment_calc.html", null ],
    [ "aircraft.components.nacelles.Nacelle.MountingPosition", "enumaircraft_1_1components_1_1nacelles_1_1_nacelle_1_1_mounting_position.html", null ],
    [ "aircraft.components.LandingGear.MountingPosition", "enumaircraft_1_1components_1_1_landing_gear_1_1_mounting_position.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.parser.MParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1parser_1_1_m_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.obj.MtlReader", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_mtl_reader.html", null ],
    [ "cad.aircraft.MyAircraftBuilder", "classcad_1_1aircraft_1_1_my_aircraft_builder.html", null ],
    [ "aircraft.auxiliary.airfoil.MyAirfoil", "classaircraft_1_1auxiliary_1_1airfoil_1_1_my_airfoil.html", null ],
    [ "standaloneutils.customdata.MyArray", "classstandaloneutils_1_1customdata_1_1_my_array.html", null ],
    [ "standaloneutils.MyArrayUtils", "classstandaloneutils_1_1_my_array_utils.html", null ],
    [ "standaloneutils.MyChartToFileUtils", "classstandaloneutils_1_1_my_chart_to_file_utils.html", null ],
    [ "it.unina.adopt.utilities.write.MyChartWriter", "classit_1_1unina_1_1adopt_1_1utilities_1_1write_1_1_my_chart_writer.html", null ],
    [ "sandbox.adm.MyClass", "classsandbox_1_1adm_1_1_my_class.html", null ],
    [ "it.unina.adopt.main.MyCommandLineOptions", "classit_1_1unina_1_1adopt_1_1main_1_1_my_command_line_options.html", null ],
    [ "configuration.MyConfiguration", "classconfiguration_1_1_my_configuration.html", null ],
    [ "it.unina.adopt.utilities.cpacs.MyCPACSWriter", "classit_1_1unina_1_1adopt_1_1utilities_1_1cpacs_1_1_my_c_p_a_c_s_writer.html", null ],
    [ "it.unina.adopt.utilities.write.MyDataWriter", "classit_1_1unina_1_1adopt_1_1utilities_1_1write_1_1_my_data_writer.html", null ],
    [ "it.unina.adopt.GUI.dialogs.MyDialogFuselageSection", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1dialogs_1_1_my_dialog_fuselage_section.html", null ],
    [ "it.unina.adopt.GUI.dialogs.MyDialogHDFInterpolationCheck", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1dialogs_1_1_my_dialog_h_d_f_interpolation_check.html", null ],
    [ "it.unina.adopt.main.MyExecutionManager", "classit_1_1unina_1_1adopt_1_1main_1_1_my_execution_manager.html", null ],
    [ "aircraft.calculators.costs.MyFixedCharges", "classaircraft_1_1calculators_1_1costs_1_1_my_fixed_charges.html", null ],
    [ "aircraft.components.fuselage.MyFuselageAdjustCriteria", "enumaircraft_1_1components_1_1fuselage_1_1_my_fuselage_adjust_criteria.html", null ],
    [ "cad.aircraft.MyFuselageBuilder", "classcad_1_1aircraft_1_1_my_fuselage_builder.html", null ],
    [ "aircraft.components.fuselage.MyFuselageCurvesSection", "classaircraft_1_1components_1_1fuselage_1_1_my_fuselage_curves_section.html", null ],
    [ "aircraft.components.fuselage.MyFuselageCurvesSideView", "classaircraft_1_1components_1_1fuselage_1_1_my_fuselage_curves_side_view.html", null ],
    [ "aircraft.components.fuselage.MyFuselageCurvesUpperView", "classaircraft_1_1components_1_1fuselage_1_1_my_fuselage_curves_upper_view.html", null ],
    [ "it.unina.adopt.viewer3d.MyFuselageSection", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1_my_fuselage_section.html", null ],
    [ "aircraft.components.fuselage.MyFuselageSectionData", "enumaircraft_1_1components_1_1fuselage_1_1_my_fuselage_section_data.html", null ],
    [ "aircraft.components.fuselage.MyFuselageSurfaceMesh", "classaircraft_1_1components_1_1fuselage_1_1_my_fuselage_surface_mesh.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.MyFX3DContentModel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_f_x3_d_content_model.html", null ],
    [ "it.unina.adopt.viewer3d.MyFXAircraft3DView", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1_my_f_x_aircraft3_d_view.html", null ],
    [ "it.unina.adopt.viewer3d.MyFXAxes", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1_my_f_x_axes.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.MyFXSubScene", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_f_x_sub_scene.html", null ],
    [ "it.unina.adopt.utilities.gui.MyGuiUtils", "classit_1_1unina_1_1adopt_1_1utilities_1_1gui_1_1_my_gui_utils.html", null ],
    [ "standaloneutils.database.hdf.MyHDFReader", "classstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader.html", null ],
    [ "it.unina.adopt.GUI.actions.MyImportAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_action.html", null ],
    [ "standaloneutils.MyInterpolatingFunction", "classstandaloneutils_1_1_my_interpolating_function.html", null ],
    [ "standaloneutils.database.hdf.MyHDFReader.MyIterateData_FindDatasetByName", "classstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader_1_1_my_iterate_data___find_dataset_by_name.html", null ],
    [ "standaloneutils.database.hdf.MyHDFReader.MyIterateData_FindGroupByName", "classstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader_1_1_my_iterate_data___find_group_by_name.html", null ],
    [ "cad.aircraft.MyLiftingSurfaceBuilder", "classcad_1_1aircraft_1_1_my_lifting_surface_builder.html", null ],
    [ "aircraft.components.fuselage.MyLineMeshYZ", "classaircraft_1_1components_1_1fuselage_1_1_my_line_mesh_y_z.html", null ],
    [ "it.unina.adopt.main.MyLineMeshYZ", "classit_1_1unina_1_1adopt_1_1main_1_1_my_line_mesh_y_z.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.MyMainController", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_main_controller.html", null ],
    [ "standaloneutils.MyMapUtils", "classstandaloneutils_1_1_my_map_utils.html", null ],
    [ "standaloneutils.MyMathUtils", "classstandaloneutils_1_1_my_math_utils.html", null ],
    [ "it.unina.adopt.GUI.javafxaddons.MyMeshUtils", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_mesh_utils.html", null ],
    [ "standaloneutils.MyMiscUtils", "classstandaloneutils_1_1_my_misc_utils.html", null ],
    [ "it.unina.adopt.main.MyNomenclatureImageDialog", "classit_1_1unina_1_1adopt_1_1main_1_1_my_nomenclature_image_dialog.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_01.MyOperator", "interfacesandbox_1_1adm_1_1_my_test___lambdas__01_1_1_my_operator.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_01.MyOperator2", "interfacesandbox_1_1adm_1_1_my_test___lambdas__01_1_1_my_operator2.html", null ],
    [ "standaloneutils.customdata.MyPoint", "classstandaloneutils_1_1customdata_1_1_my_point.html", null ],
    [ "it.unina.adopt.GUI.javafxaddons.MyPoint3D", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_point3_d.html", null ],
    [ "it.unina.adopt.GUI.tree.MyProjectTree", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_my_project_tree.html", null ],
    [ "it.unina.adopt.GUI.tree.MyProjectTreeDeltaEvent", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_my_project_tree_delta_event.html", null ],
    [ "sandbox.adm.MySandbox_ADM", "classsandbox_1_1adm_1_1_my_sandbox___a_d_m.html", null ],
    [ "it.unina.adopt.test.la.MySandbox_LA", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_sandbox___l_a.html", null ],
    [ "it.unina.adopt.GUI.actions.MySaveAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_save_action.html", null ],
    [ "standaloneutils.MyStaticWriteUtils", "classstandaloneutils_1_1_my_static_write_utils.html", null ],
    [ "sandbox.ac.MyTest_AC_costs", "classsandbox_1_1ac_1_1_my_test___a_c__costs.html", null ],
    [ "sandbox.ac.MyTest_AC_eval", "classsandbox_1_1ac_1_1_my_test___a_c__eval.html", null ],
    [ "sandbox.ac.MyTest_AC_GenericAmount", "classsandbox_1_1ac_1_1_my_test___a_c___generic_amount.html", null ],
    [ "sandbox.ac.MyTest_AC_HDF", "classsandbox_1_1ac_1_1_my_test___a_c___h_d_f.html", null ],
    [ "sandbox.adm.MyTest_ADM_00", "classsandbox_1_1adm_1_1_my_test___a_d_m__00.html", null ],
    [ "sandbox.adm.MyTest_ADM_01", "classsandbox_1_1adm_1_1_my_test___a_d_m__01.html", null ],
    [ "sandbox.adm.MyTest_ADM_02", "classsandbox_1_1adm_1_1_my_test___a_d_m__02.html", null ],
    [ "sandbox.adm.MyTest_ADM_03aSplines", "classsandbox_1_1adm_1_1_my_test___a_d_m__03a_splines.html", null ],
    [ "sandbox.adm.MyTest_ADM_03bCAD", "classsandbox_1_1adm_1_1_my_test___a_d_m__03b_c_a_d.html", null ],
    [ "sandbox.adm.MyTest_ADM_03cCAD", "classsandbox_1_1adm_1_1_my_test___a_d_m__03c_c_a_d.html", null ],
    [ "sandbox.adm.MyTest_ADM_03Splines", "classsandbox_1_1adm_1_1_my_test___a_d_m__03_splines.html", null ],
    [ "sandbox.adm.MyTest_ADM_04AeroLSLibrary", "classsandbox_1_1adm_1_1_my_test___a_d_m__04_aero_l_s_library.html", null ],
    [ "sandbox.adm.MyTest_ADM_05aDatabase", "classsandbox_1_1adm_1_1_my_test___a_d_m__05a_database.html", null ],
    [ "sandbox.adm.MyTest_ADM_05bDatabase", "classsandbox_1_1adm_1_1_my_test___a_d_m__05b_database.html", null ],
    [ "sandbox.adm.MyTest_ADM_05c_HDF_Interpolation", "classsandbox_1_1adm_1_1_my_test___a_d_m__05c___h_d_f___interpolation.html", null ],
    [ "it.unina.adopt.test.la.MyTest_LA_01", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_test___l_a__01.html", null ],
    [ "it.unina.adopt.test.la.MyTest_LA_02", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_test___l_a__02.html", null ],
    [ "it.unina.adopt.test.la.MyTest_LA_03", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_test___l_a__03.html", null ],
    [ "it.unina.adopt.test.la.MyTest_LA_04", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_test___l_a__04.html", null ],
    [ "it.unina.adopt.test.la.MyTest_LA_05", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_test___l_a__05.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_00", "classsandbox_1_1adm_1_1_my_test___lambdas__00.html", null ],
    [ "sandbox.adm.MyTest_Lambdas_01", "classsandbox_1_1adm_1_1_my_test___lambdas__01.html", null ],
    [ "sandbox.adm.MyTest_Tigl_00", "classsandbox_1_1adm_1_1_my_test___tigl__00.html", null ],
    [ "aircraft.calculators.costs.MyTripCharges", "classaircraft_1_1calculators_1_1costs_1_1_my_trip_charges.html", null ],
    [ "standaloneutils.MyUnits", "classstandaloneutils_1_1_my_units.html", null ],
    [ "standaloneutils.MyVariableToWrite", "classstandaloneutils_1_1_my_variable_to_write.html", null ],
    [ "it.unina.adopt.GUI.javafxaddons.MyVector3D", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_vector3_d.html", null ],
    [ "it.unina.adopt.core.MyWorkSession", "classit_1_1unina_1_1adopt_1_1core_1_1_my_work_session.html", null ],
    [ "it.unina.adopt.test.la.MyWriteClass", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_write_class.html", null ],
    [ "it.unina.adopt.utilities.write.MyWriteInputUtils", "classit_1_1unina_1_1adopt_1_1utilities_1_1write_1_1_my_write_input_utils.html", null ],
    [ "it.unina.adopt.utilities.write.MyWriteMap", "classit_1_1unina_1_1adopt_1_1utilities_1_1write_1_1_my_write_map.html", null ],
    [ "it.unina.adopt.utilities.write.MyWriteOutputUtils", "classit_1_1unina_1_1adopt_1_1utilities_1_1write_1_1_my_write_output_utils.html", null ],
    [ "it.unina.adopt.utilities.write.MyWriteUtils", "classit_1_1unina_1_1adopt_1_1utilities_1_1write_1_1_my_write_utils.html", null ],
    [ "standaloneutils.MyXLSWriteUtils", "classstandaloneutils_1_1_my_x_l_s_write_utils.html", null ],
    [ "it.unina.adopt.utilities.MyXMLReader", "classit_1_1unina_1_1adopt_1_1utilities_1_1_my_x_m_l_reader.html", null ],
    [ "standaloneutils.MyXMLReaderUtils", "classstandaloneutils_1_1_my_x_m_l_reader_utils.html", null ],
    [ "standaloneutils.customdata.MyXmlTree", "classstandaloneutils_1_1customdata_1_1_my_xml_tree.html", null ],
    [ "aircraft.components.nacelles.NacellesManager", "classaircraft_1_1components_1_1nacelles_1_1_nacelles_manager.html", null ],
    [ "calculators.aerodynamics.NasaBlackwell", "classcalculators_1_1aerodynamics_1_1_nasa_blackwell.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.Node", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_node.html", [
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.CameraNode", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_camera_node.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.GeomNode", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_geom_node.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.LightNode", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_light_node.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxData.NodeTM", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_data_1_1_node_t_m.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.obj.ObjImporter", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_obj_importer.html", null ],
    [ "cad.occ.OCCDiscretizeCurve3D", "classcad_1_1occ_1_1_o_c_c_discretize_curve3_d.html", null ],
    [ "it.unina.adopt.viewer3d.cad.occ.OCCMeshExtractor", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1_o_c_c_mesh_extractor.html", null ],
    [ "aircraft.OperatingConditions", "classaircraft_1_1_operating_conditions.html", null ],
    [ "aircraft.OperatingConditionsManager", "classaircraft_1_1_operating_conditions_manager.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.Optimizer", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_optimizer.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseTokenizer.Callback.ParamList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_tokenizer_1_1_callback_1_1_param_list.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MIntArrayImpl.Parser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int_array_impl_1_1_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MInt3ArrayImpl.Parser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_int3_array_impl_1_1_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat2ArrayImpl.Parser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float2_array_impl_1_1_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MArrayImpl.Parser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_array_impl_1_1_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloat3ArrayImpl.Parser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float3_array_impl_1_1_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MFloatArrayImpl.Parser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_float_array_impl_1_1_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MPolyFaceImpl.Parser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1_m_poly_face_impl_1_1_parser.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxAseTokenizer.ParserImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_ase_tokenizer_1_1_parser_impl.html", null ],
    [ "calculators.performance.PerformanceCalcManager", "classcalculators_1_1performance_1_1_performance_calc_manager.html", null ],
    [ "calculators.performance.PerformanceCalcUtils", "classcalculators_1_1performance_1_1_performance_calc_utils.html", null ],
    [ "calculators.performance.PerformanceDataManager", "classcalculators_1_1performance_1_1_performance_data_manager.html", null ],
    [ "calculators.performance.customdata.PerformanceMap", "classcalculators_1_1performance_1_1customdata_1_1_performance_map.html", [
      [ "calculators.performance.customdata.CeilingMap", "classcalculators_1_1performance_1_1customdata_1_1_ceiling_map.html", null ],
      [ "calculators.performance.customdata.DragMap", "classcalculators_1_1performance_1_1customdata_1_1_drag_map.html", null ],
      [ "calculators.performance.customdata.DragThrustIntersectionMap", "classcalculators_1_1performance_1_1customdata_1_1_drag_thrust_intersection_map.html", null ],
      [ "calculators.performance.customdata.FlightEnvelopeMap", "classcalculators_1_1performance_1_1customdata_1_1_flight_envelope_map.html", null ],
      [ "calculators.performance.customdata.RCMap", "classcalculators_1_1performance_1_1customdata_1_1_r_c_map.html", null ],
      [ "calculators.performance.customdata.ThrustMap", "classcalculators_1_1performance_1_1customdata_1_1_thrust_map.html", null ]
    ] ],
    [ "it.unina.adopt.utilities.gui.MyGuiUtils.PlotFactory", "classit_1_1unina_1_1adopt_1_1utilities_1_1gui_1_1_my_gui_utils_1_1_plot_factory.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SymbolicSubdivisionBuilder.PointInfo", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_symbolic_subdivision_builder_1_1_point_info.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.PolygonMesh", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1_polygon_mesh.html", [
      [ "it.unina.adopt.GUI.test.javafx.shape3d.SkinningMesh", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1_skinning_mesh.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.shape3d.SubdivisionMesh", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1_subdivision_mesh.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.importers.obj.PolyObjImporter", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_poly_obj_importer.html", null ],
    [ "standaloneutils.atmosphere.PressureCalc", "classstandaloneutils_1_1atmosphere_1_1_pressure_calc.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.exporters.fxml.FXMLExporter.Property", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1exporters_1_1fxml_1_1_f_x_m_l_exporter_1_1_property.html", null ],
    [ "calculators.performance.RangeCalc", "classcalculators_1_1performance_1_1_range_calc.html", null ],
    [ "calculators.performance.RateOfClimbCalc", "classcalculators_1_1performance_1_1_rate_of_climb_calc.html", null ],
    [ "configuration.enumerations.RelativePositionEnum", "enumconfiguration_1_1enumerations_1_1_relative_position_enum.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.Xform.RotateOrder", "enumit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_xform_1_1_rotate_order.html", null ],
    [ "it.unina.adopt.GUI.javafxaddons.Xform.RotateOrder", "enumit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_xform_1_1_rotate_order.html", null ],
    [ "aircraft.auxiliary.SeatsBlock.RowColumnCoordinate", "classaircraft_1_1auxiliary_1_1_seats_block_1_1_row_column_coordinate.html", null ],
    [ "aircraft.auxiliary.SeatsBlock", "classaircraft_1_1auxiliary_1_1_seats_block.html", null ],
    [ "it.unina.adopt.viewer3d.Viewable.SelectionType", "enumit_1_1unina_1_1adopt_1_1viewer3d_1_1_viewable_1_1_selection_type.html", null ],
    [ "Serializable", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.SessionManager", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_session_manager.html", null ],
    [ "Shape", null, [
      [ "sandbox.adm.MyTest_ADM_01.Shape", "classsandbox_1_1adm_1_1_my_test___a_d_m__01_1_1_shape.html", null ]
    ] ],
    [ "it.unina.adopt.viewer3d.cad.ViewableCAD.ShapeType", "enumit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1_viewable_c_a_d_1_1_shape_type.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.SmoothingGroups", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_smoothing_groups.html", null ],
    [ "standaloneutils.atmosphere.SpeedCalc", "classstandaloneutils_1_1atmosphere_1_1_speed_calc.html", null ],
    [ "calculators.stability.StabilityDerivatives", "classcalculators_1_1stability_1_1_stability_derivatives.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.dae.DaeImporter.State", "enumit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1dae_1_1_dae_importer_1_1_state.html", null ],
    [ "standaloneutils.MyStaticWriteUtils.StoreResults< T >", "classstandaloneutils_1_1_my_static_write_utils_1_1_store_results.html", null ],
    [ "org.eclipse.wb.swt.SWTResourceManager", "classorg_1_1eclipse_1_1wb_1_1swt_1_1_s_w_t_resource_manager.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SymbolicPointArray", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_symbolic_point_array.html", [
      [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.OriginalPointArray", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_original_point_array.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SubdividedPointArray", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_subdivided_point_array.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SymbolicPolygonMesh", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_symbolic_polygon_mesh.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.shape3d.symbolic.SymbolicSubdivisionBuilder", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1symbolic_1_1_symbolic_subdivision_builder.html", null ],
    [ "standaloneutils.atmosphere.TemperatureCalc", "classstandaloneutils_1_1atmosphere_1_1_temperature_calc.html", null ],
    [ "it.unina.adopt.GUI.test.TestApp", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1_test_app.html", null ],
    [ "it.unina.adopt.viewer3d.cad.occ.test.TestOCCMeshExtractor", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1test_1_1_test_o_c_c_mesh_extractor.html", null ],
    [ "it.unina.adopt.viewer3d.cad.occ.test.TestViewableCAD", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1test_1_1_test_viewable_c_a_d.html", null ],
    [ "it.unina.adopt.viewer3d.cad.occ.test.TestViewableCAD2", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1occ_1_1test_1_1_test_viewable_c_a_d2.html", null ],
    [ "calculators.performance.ThrustCalc", "classcalculators_1_1performance_1_1_thrust_calc.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.TimelineController", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_timeline_controller.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.parser.MParser.Tokenizer", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1parser_1_1_m_parser_1_1_tokenizer.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.Validator", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_validator.html", null ],
    [ "databasesIO.vedscdatabase.VeDSCDatabaseCalc", "classdatabases_i_o_1_1vedscdatabase_1_1_ve_d_s_c_database_calc.html", null ],
    [ "configuration.enumerations.VeDSCDatabaseEnum", "enumconfiguration_1_1enumerations_1_1_ve_d_s_c_database_enum.html", null ],
    [ "database.databasefunctions.aerodynamics.vedsc.VeDSCDatabaseResults", "classdatabase_1_1databasefunctions_1_1aerodynamics_1_1vedsc_1_1_ve_d_s_c_database_results.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.Loader.VertexHash", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_loader_1_1_vertex_hash.html", null ],
    [ "it.unina.adopt.viewer3d.Viewable", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1_viewable.html", [
      [ "it.unina.adopt.viewer3d.cad.ViewableCAD", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1_viewable_c_a_d.html", null ]
    ] ],
    [ "it.unina.adopt.GUI.test.javafx.importers.maya.MEnv.Visitor", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_m_env_1_1_visitor.html", null ],
    [ "it.unina.adopt.GUI.test.javafx.MyFXSubScene.VP", "enumit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_f_x_sub_scene_1_1_v_p.html", null ],
    [ "it.unina.adopt.viewer3d.MyFXAircraft3DView.VP", "enumit_1_1unina_1_1adopt_1_1viewer3d_1_1_my_f_x_aircraft3_d_view_1_1_v_p.html", null ],
    [ "configuration.enumerations.WindshieldType", "enumconfiguration_1_1enumerations_1_1_windshield_type.html", null ],
    [ "AbstractList", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list.html", null ]
    ] ],
    [ "Action", null, [
      [ "it.unina.adopt.GUI.actions.My3DViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my3_d_view_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyExportAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_export_aircraft_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyExportCadAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_export_cad_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyHelpAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_help_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyImportAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_aircraft_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyImportSerializedAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_serialized_aircraft_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyImportSerializedConditionsAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_serialized_conditions_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyNewAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_new_aircraft_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyNewAnalysisAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_new_analysis_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyOptionsAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_options_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MySelectAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_select_aircraft_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MySerializeAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_serialize_aircraft_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyStatusAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_status_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyToggleFuselageInitiatorViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_fuselage_initiator_view_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyToggleMessageViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_message_view_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyToggleProjectViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_project_view_action.html", null ],
      [ "it.unina.adopt.GUI.actions.MyWorkingDirectoryAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_working_directory_action.html", null ]
    ] ],
    [ "AnimationTimer", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.Loader.SkinningMeshTimer", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_loader_1_1_skinning_mesh_timer.html", null ]
    ] ],
    [ "Application", null, [
      [ "it.unina.adopt.GUI.test.javafx.OldTestViewer", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_old_test_viewer.html", null ]
    ] ],
    [ "ApplicationWindow", null, [
      [ "it.unina.adopt.main.ADOPT_GUI", "classit_1_1unina_1_1adopt_1_1main_1_1_a_d_o_p_t___g_u_i.html", null ]
    ] ],
    [ "Canvas", null, [
      [ "it.unina.adopt.GUI.test.javafx.TestFXLineChartPane.MyCanvas", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_test_f_x_line_chart_pane_1_1_my_canvas.html", null ]
    ] ],
    [ "Comparator", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.Optimizer.KeyFrameComparator", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_optimizer_1_1_key_frame_comparator.html", null ]
    ] ],
    [ "Composite", null, [
      [ "it.unina.adopt.GUI.dialogs.MyAirfoilsDialog", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1dialogs_1_1_my_airfoils_dialog.html", null ],
      [ "it.unina.adopt.GUI.MyTopLevelComposite", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_top_level_composite.html", null ],
      [ "it.unina.adopt.GUI.tabpanels.liftingSurface.MyActualLiftingSurface", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1lifting_surface_1_1_my_actual_lifting_surface.html", null ],
      [ "it.unina.adopt.GUI.tabpanels.liftingSurface.MyCommonData", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1lifting_surface_1_1_my_common_data.html", null ],
      [ "it.unina.adopt.GUI.tabpanels.liftingSurface.MyEquivalentLiftingSurface", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1lifting_surface_1_1_my_equivalent_lifting_surface.html", null ],
      [ "it.unina.adopt.GUI.tabpanels.liftingSurface.MyLiftingSurfaceNewPanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1lifting_surface_1_1_my_lifting_surface_new_panel.html", null ],
      [ "it.unina.adopt.GUI.tabpanels.MyHDFInterpolationCheckPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1_my_h_d_f_interpolation_check_pane.html", null ],
      [ "it.unina.adopt.GUI.tabpanels.MyPanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1_my_panel.html", [
        [ "it.unina.adopt.GUI.tabpanels.analysis.MyAnalysisPanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1analysis_1_1_my_analysis_panel.html", null ],
        [ "it.unina.adopt.GUI.tabpanels.analysis.MyBalancePanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1analysis_1_1_my_balance_panel.html", null ],
        [ "it.unina.adopt.GUI.tabpanels.analysis.MyWeightsPanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1analysis_1_1_my_weights_panel.html", null ],
        [ "it.unina.adopt.GUI.tabpanels.fuselage.MyFuselagePanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1fuselage_1_1_my_fuselage_panel.html", null ],
        [ "it.unina.adopt.GUI.tabpanels.liftingSurface.MyAirfoilAerodynamicsPanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1lifting_surface_1_1_my_airfoil_aerodynamics_panel.html", null ],
        [ "it.unina.adopt.GUI.tabpanels.liftingSurface.MyAirfoilGeometryPanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1lifting_surface_1_1_my_airfoil_geometry_panel.html", null ],
        [ "it.unina.adopt.GUI.tabpanels.liftingSurface.MyLiftingSurfacePanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1lifting_surface_1_1_my_lifting_surface_panel.html", null ],
        [ "it.unina.adopt.GUI.tabpanels.MyConfigurationPanel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1_my_configuration_panel.html", null ],
        [ "it.unina.adopt.test.la.MySWTTest", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_s_w_t_test.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.tabpanels.MyTabbedPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tabpanels_1_1_my_tabbed_pane.html", null ],
      [ "it.unina.adopt.GUI.test.gef.MyTestGEFPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1gef_1_1_my_test_g_e_f_pane.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.MyTestJavaFXPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_test_java_f_x_pane.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.MyTestJavaFXPane2", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_test_java_f_x_pane2.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.MyTestJavaFXPane3", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_test_java_f_x_pane3.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.TestFXLineChartPane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_test_f_x_line_chart_pane.html", null ],
      [ "it.unina.adopt.GUI.tree.MyProjectTreePane", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_my_project_tree_pane.html", null ],
      [ "it.unina.adopt.main.MyCompositeProcessing", "classit_1_1unina_1_1adopt_1_1main_1_1_my_composite_processing.html", null ],
      [ "it.unina.adopt.test.la.MyTestComposite", "classit_1_1unina_1_1adopt_1_1test_1_1la_1_1_my_test_composite.html", null ],
      [ "it.unina.adopt.viewer3d.MyPane3D", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1_my_pane3_d.html", null ]
    ] ],
    [ "Cylinder", null, [
      [ "it.unina.adopt.GUI.javafxaddons.MyCylinder", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_cylinder.html", null ]
    ] ],
    [ "DefaultHandler", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.dae.DaeImporter.DaeSaxParser", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1dae_1_1_dae_importer_1_1_dae_sax_parser.html", null ]
    ] ],
    [ "DoubleBinding", null, [
      [ "it.unina.adopt.GUI.test.javafx.SettingsController.Power10DoubleBinding", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_settings_controller_1_1_power10_double_binding.html", null ]
    ] ],
    [ "Duration", null, [
      [ "it.unina.adopt.GUI.test.javafx.Frame", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_frame.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.Frame", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_frame.html", null ]
    ] ],
    [ "Entry", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.values.impl.MCharacterMappingImpl.EntryImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1values_1_1impl_1_1fcf645579e3eb02ff7b97724e6ab7e35.html", null ]
    ] ],
    [ "FigureListener", null, [
      [ "it.unina.adopt.GUI.MyListener", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_listener.html", null ]
    ] ],
    [ "FirstOrderDifferentialEquations", null, [
      [ "calculators.stability.DynamicsEquations", "classcalculators_1_1stability_1_1_dynamics_equations.html", null ]
    ] ],
    [ "GridPane", null, [
      [ "it.unina.adopt.GUI.test.javafx.FourWayNavControl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_four_way_nav_control.html", null ]
    ] ],
    [ "Group", null, [
      [ "it.unina.adopt.GUI.javafxaddons.MyCurveExtrusion", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_curve_extrusion.html", null ],
      [ "it.unina.adopt.GUI.javafxaddons.MyPolyLine3D", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_poly_line3_d.html", null ],
      [ "it.unina.adopt.GUI.javafxaddons.Xform", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_xform.html", [
        [ "it.unina.adopt.viewer3d.cad.LeafNode", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1cad_1_1_leaf_node.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.AutoScalingGroup", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_auto_scaling_group.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.max.MaxLoader.MaxScene", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1max_1_1_max_loader_1_1_max_scene.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.Joint", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_joint.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.MayaGroup", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_maya_group.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.Xform", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_xform.html", null ]
    ] ],
    [ "H5L_iterate_cb", null, [
      [ "examples.groups.H5L_iter_callbackT", "classexamples_1_1groups_1_1_h5_l__iter__callback_t.html", null ]
    ] ],
    [ "H5L_iterate_t", null, [
      [ "examples.groups.opdata", "classexamples_1_1groups_1_1opdata.html", null ]
    ] ],
    [ "H5O_iterate_cb", null, [
      [ "standaloneutils.database.hdf.MyHDFReader.H5O_IterCallback_FindDatasetByName", "classstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader_1_1_h5_o___iter_callback___find_dataset_by_name.html", null ],
      [ "standaloneutils.database.hdf.MyHDFReader.H5O_IterCallback_FindGroupByName", "classstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader_1_1_h5_o___iter_callback___find_group_by_name.html", null ]
    ] ],
    [ "H5O_iterate_t", null, [
      [ "standaloneutils.database.hdf.MyHDFReader.H5O_MyIterData_FindDatasetByName", "classstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader_1_1_h5_o___my_iter_data___find_dataset_by_name.html", null ],
      [ "standaloneutils.database.hdf.MyHDFReader.H5O_MyIterData_FindGroupByName", "classstandaloneutils_1_1database_1_1hdf_1_1_my_h_d_f_reader_1_1_h5_o___my_iter_data___find_group_by_name.html", null ]
    ] ],
    [ "HashMap", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.Optimizer.MapOfLists< K, V >", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1_optimizer_1_1_map_of_lists.html", null ]
    ] ],
    [ "IEntityParameter", null, [
      [ "it.unina.adopt.test.igeoaddons.MyReferenceFrame", "classit_1_1unina_1_1adopt_1_1test_1_1igeoaddons_1_1_my_reference_frame.html", null ]
    ] ],
    [ "IInputValidator", null, [
      [ "it.unina.adopt.GUI.actions.LengthValidator", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_length_validator.html", null ]
    ] ],
    [ "ILabelProvider", null, [
      [ "it.unina.adopt.GUI.tree.MyProjectTreeLabelProvider", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_my_project_tree_label_provider.html", null ]
    ] ],
    [ "Initializable", null, [
      [ "it.unina.adopt.GUI.test.javafx.MainController", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_main_controller.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.NavigationController", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_navigation_controller.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.SettingsController", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_settings_controller.html", null ]
    ] ],
    [ "Interpolator", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.maya.MayaAnimationCurveInterpolator", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1maya_1_1_maya_animation_curve_interpolator.html", null ]
    ] ],
    [ "IParameterObject", null, [
      [ "it.unina.adopt.test.igeoaddons.MyReferenceFrame", "classit_1_1unina_1_1adopt_1_1test_1_1igeoaddons_1_1_my_reference_frame.html", null ]
    ] ],
    [ "Iterator", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList.Itr", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list_1_1_itr.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList.ListItr", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list_1_1_list_itr.html", null ]
      ] ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList.Itr", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list_1_1_itr.html", [
        [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList.ListItr", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list_1_1_list_itr.html", null ]
      ] ]
    ] ],
    [ "ITraceListener", null, [
      [ "it.unina.adopt.GUI.MyListener", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_listener.html", null ]
    ] ],
    [ "ITreeContentProvider", null, [
      [ "it.unina.adopt.GUI.tree.MyProjectContentProvider", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1tree_1_1_my_project_content_provider.html", null ]
    ] ],
    [ "Label", null, [
      [ "it.unina.adopt.GUI.test.javafx.MyTestJavaFXPane3.HUDLabel", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_my_test_java_f_x_pane3_1_1_h_u_d_label.html", null ],
      [ "it.unina.adopt.viewer3d.MyPane3D.HUDLabel", "classit_1_1unina_1_1adopt_1_1viewer3d_1_1_my_pane3_d_1_1_h_u_d_label.html", null ]
    ] ],
    [ "List", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list.html", null ]
    ] ],
    [ "ListIterator", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList.ListItr", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list_1_1_list_itr.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList.ListItr", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list_1_1_list_itr.html", null ]
    ] ],
    [ "MeshView", null, [
      [ "it.unina.adopt.GUI.javafxaddons.MySimpleQuad", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_simple_quad.html", null ],
      [ "it.unina.adopt.GUI.javafxaddons.MySimpleTriangle", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_simple_triangle.html", null ],
      [ "it.unina.adopt.GUI.javafxaddons.MyTorusMesh", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1javafxaddons_1_1_my_torus_mesh.html", null ]
    ] ],
    [ "MouseListener", null, [
      [ "it.unina.adopt.GUI.MyListener", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_listener.html", null ]
    ] ],
    [ "MouseMotionListener", null, [
      [ "it.unina.adopt.GUI.MyListener", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1_my_listener.html", null ]
    ] ],
    [ "Pane", null, [
      [ "it.unina.adopt.GUI.test.javafx.SubSceneResizer", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_sub_scene_resizer.html", null ]
    ] ],
    [ "PApplet", null, [
      [ "it.unina.adopt.test.igeoaddons.MyPAppletIGeo", "classit_1_1unina_1_1adopt_1_1test_1_1igeoaddons_1_1_my_p_applet_i_geo.html", null ]
    ] ],
    [ "Parent", null, [
      [ "it.unina.adopt.GUI.test.javafx.shape3d.PolygonMeshView", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1shape3d_1_1_polygon_mesh_view.html", null ]
    ] ],
    [ "RandomAccess", null, [
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.FloatArrayList.SubList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_float_array_list_1_1_sub_list.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list.html", null ],
      [ "it.unina.adopt.GUI.test.javafx.importers.obj.IntegerArrayList.SubList", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1importers_1_1obj_1_1_integer_array_list_1_1_sub_list.html", null ]
    ] ],
    [ "Region", null, [
      [ "it.unina.adopt.GUI.test.javafx.TimelineDisplay", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_timeline_display.html", null ]
    ] ],
    [ "SelectionListener", null, [
      [ "it.unina.adopt.GUI.actions.MySelectAircraftAction.MySelection", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_select_aircraft_action_1_1_my_selection.html", null ]
    ] ],
    [ "Serializable", null, [
      [ "it.unina.adopt.core.MyProperty", "classit_1_1unina_1_1adopt_1_1core_1_1_my_property.html", null ]
    ] ],
    [ "Shell", null, [
      [ "it.unina.adopt.GUI.shells.MySaveShell", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1shells_1_1_my_save_shell.html", null ]
    ] ],
    [ "StackPane", null, [
      [ "it.unina.adopt.GUI.test.javafx.TestFXLineChartPane.HoveredThresholdNode", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_test_f_x_line_chart_pane_1_1_hovered_threshold_node.html", null ]
    ] ],
    [ "TreeItem", null, [
      [ "it.unina.adopt.GUI.test.javafx.SettingsController.TreeItemImpl", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1test_1_1javafx_1_1_settings_controller_1_1_tree_item_impl.html", null ]
    ] ]
];