var namespaceit_1_1unina_1_1adopt =
[
    [ "core", "namespaceit_1_1unina_1_1adopt_1_1core.html", "namespaceit_1_1unina_1_1adopt_1_1core" ],
    [ "externalFunctions", "namespaceit_1_1unina_1_1adopt_1_1external_functions.html", "namespaceit_1_1unina_1_1adopt_1_1external_functions" ],
    [ "GUI", "namespaceit_1_1unina_1_1adopt_1_1_g_u_i.html", "namespaceit_1_1unina_1_1adopt_1_1_g_u_i" ],
    [ "main", "namespaceit_1_1unina_1_1adopt_1_1main.html", "namespaceit_1_1unina_1_1adopt_1_1main" ],
    [ "test", "namespaceit_1_1unina_1_1adopt_1_1test.html", "namespaceit_1_1unina_1_1adopt_1_1test" ],
    [ "utilities", "namespaceit_1_1unina_1_1adopt_1_1utilities.html", "namespaceit_1_1unina_1_1adopt_1_1utilities" ],
    [ "viewer3d", "namespaceit_1_1unina_1_1adopt_1_1viewer3d.html", "namespaceit_1_1unina_1_1adopt_1_1viewer3d" ]
];