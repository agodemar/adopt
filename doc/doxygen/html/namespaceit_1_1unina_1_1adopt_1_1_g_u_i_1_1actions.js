var namespaceit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions =
[
    [ "LengthValidator", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_length_validator.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_length_validator" ],
    [ "My3DViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my3_d_view_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my3_d_view_action" ],
    [ "MyExportAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_export_aircraft_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_export_aircraft_action" ],
    [ "MyExportCadAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_export_cad_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_export_cad_action" ],
    [ "MyHelpAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_help_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_help_action" ],
    [ "MyImportAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_action.html", null ],
    [ "MyImportAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_aircraft_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_aircraft_action" ],
    [ "MyImportSerializedAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_serialized_aircraft_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_serialized_aircraft_action" ],
    [ "MyImportSerializedConditionsAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_serialized_conditions_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_import_serialized_conditions_action" ],
    [ "MyNewAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_new_aircraft_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_new_aircraft_action" ],
    [ "MyNewAnalysisAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_new_analysis_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_new_analysis_action" ],
    [ "MyOptionsAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_options_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_options_action" ],
    [ "MySaveAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_save_action.html", null ],
    [ "MySelectAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_select_aircraft_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_select_aircraft_action" ],
    [ "MySerializeAircraftAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_serialize_aircraft_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_serialize_aircraft_action" ],
    [ "MyStatusAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_status_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_status_action" ],
    [ "MyToggleFuselageInitiatorViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_fuselage_initiator_view_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_fuselage_initiator_view_action" ],
    [ "MyToggleMessageViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_message_view_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_message_view_action" ],
    [ "MyToggleProjectViewAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_project_view_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_toggle_project_view_action" ],
    [ "MyWorkingDirectoryAction", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_working_directory_action.html", "classit_1_1unina_1_1adopt_1_1_g_u_i_1_1actions_1_1_my_working_directory_action" ]
];