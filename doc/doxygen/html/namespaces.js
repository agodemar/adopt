var namespaces =
[
    [ "aircraft", "namespaceaircraft.html", "namespaceaircraft" ],
    [ "cad", "namespacecad.html", "namespacecad" ],
    [ "calculators", "namespacecalculators.html", "namespacecalculators" ],
    [ "configuration", "namespaceconfiguration.html", "namespaceconfiguration" ],
    [ "database", "namespacedatabase.html", "namespacedatabase" ],
    [ "databasesIO", "namespacedatabases_i_o.html", "namespacedatabases_i_o" ],
    [ "examples", "namespaceexamples.html", "namespaceexamples" ],
    [ "it", "namespaceit.html", "namespaceit" ],
    [ "org", "namespaceorg.html", "namespaceorg" ],
    [ "sandbox", "namespacesandbox.html", "namespacesandbox" ],
    [ "standaloneutils", "namespacestandaloneutils.html", "namespacestandaloneutils" ]
];