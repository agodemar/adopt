clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';
h5disp(fileName,'/(C_m_q_w)_k_q_vs_AR/data')
data = h5read(fileName,'/(C_m_q_w)_k_q_vs_AR/data');
var_0 = h5read(fileName,'/(C_m_q_w)_k_q_vs_AR/var_0');
plot(var_0,data)
xlabel('AR');
ylabel('k_q');
axis([0 12 0 1]);
