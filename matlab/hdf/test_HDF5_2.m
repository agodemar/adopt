clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';
h5disp(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/data')
data = h5read(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/data');
var_0 = h5read(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/var_0');
var_0 = double(var_0);
var_1 = h5read(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/var_1');
var_1 = double(var_1);
var_2 = h5read(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/var_2');
var_2 = double(var_2);

data_1(:,:) = data(1,:,:);
data_1 = data_1';
data_2(:,:) = data(2,:,:);
data_2 = data_2';
data_3(:,:) = data(3,:,:);
data_3 = data_3';
data_4(:,:) = data(4,:,:);
data_4 = data_4';
data_5(:,:) = data(5,:,:);
data_5 = data_5';
data_6(:,:) = data(6,:,:);
data_6 = data_6';

D(:,:,1) = data_1;
D(:,:,2) = data_2;
D(:,:,3) = data_3;
D(:,:,4) = data_4;
D(:,:,5) = data_5;
D(:,:,6) = data_6;
D=double(D);

v0=0.2; %lambda
v1=8; %AR
v2=28;  %Lambda_LE (deg)


s(1) = subplot(3,2,1);
plot( ...
    var_2',data_1 ...
    );
title(s(1),['\lambda = ',num2str(var_0(1))]);
legend(s(1), ...
    ['AR = ',num2str(var_1(1))], ...
    ['AR = ',num2str(var_1(2))], ...
    ['AR = ',num2str(var_1(3))], ...
    ['AR = ',num2str(var_1(4))], ...
    ['AR = ',num2str(var_1(5))], ...
    ['AR = ',num2str(var_1(6))], ...
    'Location','NorthEastOutside' ...
    );

%set(gca,'ydir','reverse')
xlim([0 45]);
ylim([0 2]);
xlabel('\Lambda_{\mathcal{LE}}');
ylabel('K_2');
hold on
%z1 = sTables.lambda_2_tab(1);

s(2) = subplot(3,2,2);
plot( ...
    var_2',data_2 ...
    );
title(s(2),['\lambda = ',num2str(var_0(2))]);
legend(s(2), ...
    ['AR = ',num2str(var_1(1))], ...
    ['AR = ',num2str(var_1(2))], ...
    ['AR = ',num2str(var_1(3))], ...
    ['AR = ',num2str(var_1(4))], ...
    ['AR = ',num2str(var_1(5))], ...
    ['AR = ',num2str(var_1(6))], ...
    'Location','NorthEastOutside' ...
    );

%set(gca,'ydir','reverse')
xlim([0 45]);
ylim([0 2]);
xlabel('\Lambda_{\mathcal{LE}}');
ylabel('K_2');
hold on
%z1 = sTables.lambda_2_tab(1);

f1 = ...
    interlim3( ...
        var_1', ... % the various curves of each plot
        var_2', ... % the x values
        var_0', ... % the plots
        D, ...      % f_tab(v1,x2,v0)
        v1, ...
        v2, ...
        v0 ...
    )

% plot([x5 x5],[0 1],'--k');
% plot([0 2], [f1 f1], '--k');
% hold on
% plot( [x5], [f1], 'ok' );



% plot(var_0,data)
% xlabel('AR');
% ylabel('k_q');
% axis([0 12 0 1]);


