clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';

groupName = '/(x_bar_ac_w)_k1_vs_lambda';
datasetName = 'data';
pathToData = sprintf('%s/%s', groupName, datasetName);
h5disp(fileName,pathToData);
data = h5read(fileName,pathToData);
var0Name = 'var_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);

data_1(:,:) = data(:,:,:);
data_1 = double(data_1);

v0=0.2; 

% y=[0 .2 .3 .4 .5 .6 .7 .8 .9 1 1.1];
% 
% plot( ...
%     var_0, y);

plot( ...
    var_0, data_1);

% xlim([0 1]);
% ylim([1 1.5]);
xlabel('\lambda');
ylabel('K_{1}');
hold on
