clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';

groupName = '/(alpha0_L)_alpha0_lift_over_alpha0_lift_(M_=_0.3)_vs_mach_times_cos_(L_c4)_(t_over_c)';
datasetName = 'data';
pathToData = sprintf('%s/%s', groupName, datasetName);
h5disp(fileName,pathToData);
data = h5read(fileName,pathToData);
var0Name = 'var_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);
var1Name = 'var_1';
pathToVar1 = sprintf('%s/%s', groupName, var1Name);
var_1 = h5read(fileName,pathToVar1);
var_1 = double(var_1);

data_1(:,:) = data(1,:);
data_1 = data_1';
data_2(:,:) = data(2,:);
data_2 = data_2';
data_3(:,:) = data(3,:);
data_3 = data_3';
data_4(:,:) = data(4,:);
data_4 = data_4';
data_5(:,:) = data(5,:);
data_5 = data_5';
data_6(:,:) = data(6,:);
data_6 = data_6';
data_7(:,:) = data(7,:);
data_7 = data_7';

D(:,1) = data_1;
D(:,2) = data_2;
D(:,3) = data_3;
D(:,4) = data_4;
D(:,5) = data_5;
D(:,6) = data_6;
D(:,7) = data_7;
D=double(D);

% fixed value
v0 = 0.1;
v1 = 0.8;

% plot
plot( ...
    var_1, data(:,:));
legend(...
    ['t/c = ',num2str(var_0(1))], ...
    ['t/c = ',num2str(var_0(2))], ...
    ['t/c = ',num2str(var_0(3))], ...
    ['t/c = ',num2str(var_0(4))], ...5
    ['t/c = ',num2str(var_0(5))], ...
    ['t/c = ',num2str(var_0(6))], ...
    ['t/c = ',num2str(var_0(7))], ...
    'Location','NorthEastOutside' ...
    );

xlim([.3 1]);
ylim([-1.5 1.2]);
xlabel('M cos \Lambda_{c/4}');
ylabel('(\alpha_{0L})_{M=0}/(\alpha_{0L})_{M=0.3}');
hold on

f = ...
     interlim2( ...
        var_0, ...                
        var_1, ...       % rows (x1)
        D, ... % f_tab
        v0, ...
        v1 ...    
     );
 
plot([v1 v1],[min(ylim) max(ylim)],'--k');
plot([min(xlim)  max(xlim)], [f f], '--k');
hold on
plot( [v1], [f], 'ok' );

