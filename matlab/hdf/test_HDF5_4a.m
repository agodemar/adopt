clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';

groupName = '/(C_m0_w)_DC_m0_over_twist_vs_Lc4_(AR)_(lambda)';
datasetName = 'data';
pathToData = sprintf('%s/%s', groupName, datasetName);
h5disp(fileName,pathToData);
data = h5read(fileName,pathToData);
var0Name = 'var_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);
var1Name = 'var_1';
pathToVar1 = sprintf('%s/%s', groupName, var1Name);
var_1 = h5read(fileName,pathToVar1);
var_1 = double(var_1);

var2Name = 'var_2';
pathToVar2 = sprintf('%s/%s', groupName, var2Name);
var_2 = h5read(fileName,pathToVar2);
var_2 = double(var_2);

data_1(:,:) = data(1,:,:);
data_1 = data_1';
data_2(:,:) = data(2,:,:);
data_2 = data_2';
data_3(:,:) = data(3,:,:);
data_3 = data_3';

D(:,:,1) = data_1;
D(:,:,2) = data_2;
D(:,:,3) = data_3;
D=double(D);

% fixed value
v0 = 0.0;
v1 = 8.5;
v2 = 30.0;

% plot
plot( ...
    var_2, D(:,:,1));
legend(...
    ['AR = ',num2str(var_1(1))], ...
    ['AR = ',num2str(var_1(2))], ...
    ['AR = ',num2str(var_1(3))], ...
    ['AR = ',num2str(var_1(4))], ...
    ['AR = ',num2str(var_1(5))], ...
    'Location','NorthEastOutside' ...
    );

xlim([0 60]);
ylim([-0.016 0.0]);
xlabel('\Lambda_{c/4} (deg)');
ylabel('(\Delta C_{m0} / \epsilon_{g,tip} (1/deg)');
set(gca,'YDir','Reverse');
hold on

f = ...
     interlim3( ...
        var_1, ...                
        var_2, ...       % rows (x1)
        var_0, ...       %
        D, ... % f_tab
        v1, ...
        v2, ...    
        v0 ...
     );
 
plot([v2 v2],[min(ylim) max(ylim)],'--k');
plot([min(xlim)  max(xlim)], [f f], '--k');
hold on
plot( [v2], [f], 'ok' );

