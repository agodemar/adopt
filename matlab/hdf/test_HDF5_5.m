clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';

groupName = '/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)';
datasetName = 'data';
pathToData = sprintf('%s/%s', groupName, datasetName);
%h5disp(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/data');
h5disp(fileName,pathToData);
%data = h5read(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/data');
data = h5read(fileName,pathToData);
var0Name = 'var_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
%var_0 = h5read(fileName,'/(x_bar_ac_w)_k2_vs_L_LE_(AR)_(lambda)/var_0');
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);
var1Name = 'var_1';
pathToVar1 = sprintf('%s/%s', groupName, var1Name);
var_1 = h5read(fileName,pathToVar1);
var_1 = double(var_1);
var2Name = 'var_2';
pathToVar2 = sprintf('%s/%s', groupName, var2Name);
var_2 = h5read(fileName,pathToVar2);
var_2 = double(var_2);

data_1(:,:) = data(1,:,:);
data_1 = data_1';
data_2(:,:) = data(2,:,:);
data_2 = data_2';
data_3(:,:) = data(3,:,:);
data_3 = data_3';
data_4(:,:) = data(4,:,:);
data_4 = data_4';
data_5(:,:) = data(5,:,:);
data_5 = data_5';
data_6(:,:) = data(6,:,:);
data_6 = data_6';

D(:,:,1) = data_1;
D(:,:,2) = data_2;
D(:,:,3) = data_3;
D(:,:,4) = data_4;
D(:,:,5) = data_5;
D(:,:,6) = data_6;
D=double(D);

v0=0.2; 
v1=7; 
v2=30; % degree

plot( ...
    var_2',data_1 ...
    );
title(['\lambda = ',num2str(var_0(1))]);
legend(...
    ['AR = ',num2str(var_1(1))], ...
    ['AR = ',num2str(var_1(2))], ...
    ['AR = ',num2str(var_1(3))], ...
    ['AR = ',num2str(var_1(4))], ...
    ['AR = ',num2str(var_1(5))], ...
    ['AR = ',num2str(var_1(6))], ...
    'Location','NorthEastOutside' ...
    );

xlim([0 45]);
ylim([0 2]);
xlabel('\Lambda_{LE}');
ylabel('K_2');
hold on

f1 = ...
    interlim3( ...
        var_1', ... % the various curves of each plot
        var_2', ... % the x values
        var_0', ... % the plots
        D, ...      % f_tab(v1,x2,v0)
        v1, ...
        v2, ...
        v0 ...
    );

plot([v2 v2],[min(xlim) max(ylim)],'--k');
plot([min(ylim)  max(xlim)], [f1 f1], '--k');
hold on
plot( [v2], [f1], 'ok' );

