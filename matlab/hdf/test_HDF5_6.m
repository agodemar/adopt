clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';

h5disp(fileName,'/(x_bar_ac_w)_k1_vs_lambda/data')
data = h5read(fileName,'/(x_bar_ac_w)_k1_vs_lambda/data');
var_0 = h5read(fileName,'/(x_bar_ac_w)_k1_vs_lambda/var_0');
lambda_tab_K1 = double(var_0');

K1_tab = data(:,:);

lambda= 0.4;

plot( ...
    lambda_tab_K1',K1_tab ...
    );

%xlim([0 45]);
%ylim([0 2]);
xlabel('\Lambda_{LE}');
ylabel('K_2');
hold on

val_K1 = ...
        interlim1( ...
            lambda_tab_K1, ...
            K1_tab, ...
            lambda ...
        );
