clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';

h5disp(fileName,'/(x_bar_ac_w)_k1_vs_lambda/data')
data = h5read(fileName,'/(x_bar_ac_w)_k1_vs_lambda/data');
var_0 = h5read(fileName,'/(x_bar_ac_w)_k1_vs_lambda/var_0');
lambda_tab_K1 = double(var_0');

K1_tab = data(:,:);

lambda= 0.4;

plot( ...
    lambda_tab_K1',K1_tab ...
    );

%xlim([0 45]);
%ylim([0 2]);
xlabel('\Lambda_{LE}');
ylabel('K_2');
hold on

val_K1 = ...
        interlim1( ...
            lambda_tab_K1, ...
            K1_tab, ...
            lambda ...
        );
%% write
h5create( ...
    'test01.h5', ... % file name
    '/pippo/data', ... % dataset name
    size(data'), ... % unlimited or sized data
    'Datatype', 'single', ...
    'ChunkSize', size(data'), ...
    'Deflate', 9 ...
    );
h5write('test01.h5','/pippo/data',data')

h5create( ...
    'test01.h5', ... % file name
    '/pippo/var_0', ... % dataset name
    size(var_0'), ... % unlimited or sized data
    'Datatype', 'single', ...
    'ChunkSize', size(var_0'), ...
    'Deflate', 9 ...
    );
h5write('test01.h5','/pippo/var_0',var_0')


