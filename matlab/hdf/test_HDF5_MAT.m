clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';


% fixed values
x1 = 0.5;
x2 = 0.7;
x3 = 20; %(deg)

clear D data data_0 data_1 data_2 data_3 data_4 data_5 data_6
clear var_0 var_1 var_2

groupName = '/(C_m0_w)_DC_m0_over_twist_vs_Lc4_(AR)_(lambda)';
datasetName = 'data';
pathToData = sprintf('%s/%s', groupName, datasetName);
h5disp(fileName,pathToData);
data = h5read(fileName,pathToData);
var0Name = 'var_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);
var1Name = 'var_1';
pathToVar1 = sprintf('%s/%s', groupName, var1Name);
var_1 = h5read(fileName,pathToVar1);
var_1 = double(var_1);
var2Name = 'var_2';
pathToVar2 = sprintf('%s/%s', groupName, var2Name);
var_2 = h5read(fileName,pathToVar2);
var_2 = double(var_2);

data_1(:,:) = data(1,:,:);
data_1 = data_1';
data_2(:,:) = data(2,:,:);
data_2 = data_2';
data_3(:,:) = data(3,:,:);
data_3 = data_3';
 
D(:,:,1) = data_1;
D(:,:,2) = data_2;
D(:,:,3) = data_3;
D=double(D);

plot( ...
    var_2, data_1...
    );
