clear all; clear classes; clc; close all;

fileName = '../test/Aerodynamic_Database_Ultimate.h5';

% fixed values

x1 = 0.5; 
x9 = 2;
x10 = 0;

clear D data data_0 data_1 data_2 data_3 data_4 data_5 data_6
clear var_0 var_1 var_2

groupName = '/(C_l_delta_a)_RME_vs_eta_(Lambda_beta)_(beta_times_AR_over_k)_(lambda)';
datasetName = 'data_0';
pathToData = sprintf('%s/%s', groupName, datasetName);
h5disp(fileName,pathToData);
data = h5read(fileName,pathToData);
var0Name = 'var_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);
var1Name = 'var_1';
pathToVar1 = sprintf('%s/%s', groupName, var1Name);
var_1 = h5read(fileName,pathToVar1);
var_1 = double(var_1);
var2Name = 'var_2';
pathToVar2 = sprintf('%s/%s', groupName, var2Name);
var_2 = h5read(fileName,pathToVar2);
var_2 = double(var_2);
var3Name = 'var_3';
pathToVar3 = sprintf('%s/%s', groupName, var3Name);
var_3 = h5read(fileName,pathToVar3);
var_3 = double(var_3);

data_1(:,:) = data(1,:,:);
data_1 = data_1';
data_2(:,:) = data(2,:,:);
data_2 = data_2';
data_3(:,:) = data(3,:,:);
data_3 = data_3';

D(:,:,1) = data_1;
D(:,:,2) = data_2;
D(:,:,3) = data_3;
D=double(D);

plot( ...
    var_3,data_1 ...
    );

xlim([0 1]);
ylim([0 0.3]);
xlabel('\eta');
ylabel('RME');
hold on


f1 = ...
    interlim3( ...
        var_0, var_3, var_1, ...
        D, ...
        x10, x1, x9 ...
    );

% plot([x1 x1],[0 0.7],'--k');
% plot([0 1], [f1 f1], '--k');
% hold on
% plot( [x1], [f1], 'ok' );




