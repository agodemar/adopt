clear all; clear classes; clc; close all;

fileName = '../Aerodynamic_Database_Ultimate.h5';

% fixed values
Lambda_c4          = obj.Wing.sweep_quarter_chord.value;
x2                 = obj.Wing.aspect_ratio.value;
lambda             = obj.Wing.taper_ratio.value;
xbar_AC            = 0; % obj.Wing.X_ac_from_apex.value;     %%% to check
xbar_CG            = obj.Xcg_From_MACLE_To_MAC_Ratio.value;  %%% to check
diff_Xac_Xag = xbar_AC - xbar_CG;

clear D data data_0 data_1 data_2 data_3 data_4 data_5 data_6
clear var_0 var_1 var_2

groupName = '/(C_n_delta_a)_k_n_a_vs_eta_(AR)_(lambda)';
datasetName = 'data';
pathToData = sprintf('%s/%s', groupName, datasetName);
h5disp(fileName,pathToData);
data = h5read(fileName,pathToData);
var0Name = 'var_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);
var1Name = 'var_1';
pathToVar1 = sprintf('%s/%s', groupName, var1Name);
var_1 = h5read(fileName,pathToVar1);
var_1 = double(var_1);
var2Name = 'var_2';
pathToVar2 = sprintf('%s/%s', groupName, var2Name);
var_2 = h5read(fileName,pathToVar2);
var_2 = double(var_2);


data_1(:,:) = data(1,:,:);
data_1 = data_1';
data_2(:,:) = data(2,:,:);
data_2 = data_2';
data_3(:,:) = data(3,:,:);
data_3 = data_3';
data_4(:,:) = data(4,:,:);
data_4 = data_4';
 
D(:,:,1) = data_1;
D(:,:,2) = data_2;
D(:,:,3) = data_3;
D(:,:,4) = data_4;
D=double(D);

s(4) = subplot(2,2,4);
plot( ...
    var_2, data_1...
    );
title(s(4),['\lambda = ',num2str(var_0(1))]);
legend(s(4), ...
    ['AR = ',num2str(var_1(1))], ...
    ['AR = ',num2str(var_1(2))], ...
    ['AR = ',num2str(var_1(3))], ...
    ['AR = ',num2str(var_1(4))], ...
    'Location','NorthEastOutside' ...
    );
xlim([0 1]);
ylim([0 0.4]);
xlabel('\eta');
ylabel('K_{n_{A}}');
hold on

f1 = ...
    interlim3( ...
        var_1, ...
        var_2, ...
        var_0, ...
        D, ...
        AR, x1, lambda ...
    );

plot([x1 x1],[min(ylim) max(ylim)],'--k');
plot([min(xlim) max(xlim)], [f1 f1], '--k');
hold on
plot( [x1], [f1], 'ok' );

