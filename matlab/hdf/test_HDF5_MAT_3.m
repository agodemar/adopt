clear all; clear classes; clc; close all;

fileName = '../Aerodynamic_Database_Ultimate.h5';

clear D data data_0 data_1 data_2 data_3 data_4 data_5 data_6
clear var_0 var_1 var_2

groupName = '/(C_n_r_w)_C_n_r_over_squared_(C_Lift1)_vs_AR_(lambda)_(L_c4)_(x_bar_ac_minus_x_bar_cg)';
datasetName = 'data_1';
pathToData = sprintf('%s/%s', groupName, datasetName);
h5disp(fileName,pathToData);
data = h5read(fileName,pathToData);
var0Name = 'var_1_0';
pathToVar0 = sprintf('%s/%s', groupName, var0Name);
var_0 = h5read(fileName,pathToVar0);
var_0 = double(var_0);
var1Name = 'var_1_1';
pathToVar1 = sprintf('%s/%s', groupName, var1Name);
var_1 = h5read(fileName,pathToVar1);
var_1 = double(var_1);

data_1(:,:) = data(1,:);
data_1 = data_1';
data_2(:,:) = data(2,:);
data_2 = data_2';
data_3(:,:) = data(3,:);
data_3 = data_3';

D(:,1) = data_1;
D(:,2) = data_2;
D(:,3) = data_3;
D=double(D);


plot( ...
    var_1, data(:,:));
% legend(s(4), ...
%     ['\Lambda_{V} = ',num2str(var_0(1))], ...
%     ['\Lambda_{V} = ',num2str(var_0(2))], ...
%     'Location','NorthEastOutside' ...
%     );
