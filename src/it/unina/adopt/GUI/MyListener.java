package it.unina.adopt.GUI;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.nebula.visualization.xygraph.figures.Axis;
import org.eclipse.nebula.visualization.xygraph.figures.ITraceListener;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.TraceType;
import org.eclipse.swt.graphics.Color;

import it.unina.adopt.main.ADOPT_GUI;

/** This Listener implements drag and drop for Draw2D Figure */
public class MyListener 
implements MouseListener, MouseMotionListener, FigureListener, ITraceListener {

	Figure figure;
	org.eclipse.draw2d.geometry.Point location;

	/** constructor save reference to figure, then add listeners */
	public MyListener(Figure figure) {
		this.figure = figure;
		figure.addMouseListener(this);
		figure.addMouseMotionListener(this);
	}

	@Override
	public void mousePressed(MouseEvent me) {
		location = me.getLocation();
		me.consume();

		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The ADOpT | MOUSE ON TRACE EVENT \n"
				);
		
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		org.eclipse.draw2d.geometry.Point newLocation = me.getLocation();
		if( location==null || newLocation == null)
			return;
		// calculate offset wrt last location
		Dimension offset = newLocation.getDifference( location );
		if( offset.width==0 && offset.height==0 )
			return;
		// exchange location
		location = newLocation;

		// old Bounds are dirty
		figure.getUpdateManager()
		.addDirtyRegion(figure.getParent(), figure.getBounds()); 

		// translate figure  
		figure.translate( offset.width, offset.height );

		// new Bounds are dirty
		figure.getUpdateManager()
		.addDirtyRegion( figure.getParent(), figure.getBounds() );

		// new Bounds: set _parent constraint
		figure.getParent().getLayoutManager()
		.setConstraint(figure, figure.getBounds() );
		//
		me.consume();
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		if( location==null )
			return;
		location = null;
		me.consume();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseHover(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDoubleClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void figureMoved(IFigure arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void traceColorChanged(Trace arg0, Color arg1, Color arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void traceNameChanged(Trace arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void traceTypeChanged(Trace arg0, TraceType arg1, TraceType arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void traceYAxisChanged(Trace arg0, Axis arg1, Axis arg2) {
		// TODO Auto-generated method stub
		
	}

}// end-of-class MyListener