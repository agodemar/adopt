package it.unina.adopt.GUI;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import it.unina.adopt.GUI.tabpanels.MyTabbedPane;
import it.unina.adopt.GUI.tree.MyProjectTreePane;
import it.unina.adopt.main.ADOPT_GUI;

public class MyTopLevelComposite extends Composite {
	
	// private static final Logger LOGGER = Logger.getLogger(MyTopLevelComposite.class);
	
	private Aircraft _theAircraft;
	private ACAnalysisManager _theAnalysis;
	private MyProjectTreePane _projectPane = null;
	
	private MyTabbedPane _tabbedPane = null;
	public MyTabbedPane getMyTabbedPane() { return _tabbedPane;};
	
	private TabFolder _tabFolderLog = null;
	private TabItem _tabItemLog = null;
	private Text _textLogger = null;
	private SashForm _sashFormUpDw = null;
	private SashForm _sashFormLeftRight = null;
	
	private static boolean _connectedToMatlab = false;

	public MyTopLevelComposite(
			Composite parent, 
			int style, 
			Aircraft aircraft, 
			ACAnalysisManager analysis) {
		
		super(parent, style);
		
		_theAircraft = aircraft;
		_theAnalysis = analysis;
		
		// Title of the GUI
		getShell().setText("ADOpT");
		
		this.setLayout(new FillLayout());

		//--------------------------------------------------------------------
		// Horizontal division (Up/Down)
		
		_sashFormUpDw = new SashForm(this, SWT.VERTICAL);
		
		// Change the width of the sashes
		// sashFormUpDw.SASH_WIDTH = 10;
		
		// Change the color used to paint the sashes
		_sashFormUpDw.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));

		//--------------------------------------------------------------------
		// Upper sash for Left/Right division
		
		_sashFormLeftRight = new SashForm(_sashFormUpDw, SWT.HORIZONTAL|SWT.BORDER);
		
		// Change the color used to paint the sashes
		_sashFormLeftRight.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));
		
		// Left pane --> project view
		_projectPane = new MyProjectTreePane(_sashFormLeftRight, SWT.NONE);
		
		ADOPT_GUI.getApp().setTheProjectPane(_projectPane);
		
		// Right pane --> tabs
		_tabbedPane = new MyTabbedPane(
				_sashFormLeftRight, 
				SWT.NONE, 
				_theAnalysis, 
				_theAircraft);
		
		//--------------------------------------------------------------------
		// The text window for message logs

		_tabFolderLog = new TabFolder(_sashFormUpDw, SWT.NONE);
		_tabItemLog = new TabItem(_tabFolderLog, SWT.NULL);
		_tabItemLog.setText("Log Messages");
		_textLogger = 
				new Text(_tabFolderLog, 
						SWT.MULTI
						| SWT.BORDER
						| SWT.H_SCROLL
						| SWT.V_SCROLL
						);
		_tabItemLog.setControl(_textLogger);

		// Initial message

		DateTime dateTimeGUI = new DateTime(parent, SWT.DATE | SWT.TIME);
		Calendar instance = Calendar.getInstance();
		instance.set(Calendar.DAY_OF_MONTH, dateTimeGUI.getDay());
		instance.set(Calendar.MONTH, dateTimeGUI.getMonth());
		instance.set(Calendar.YEAR, dateTimeGUI.getYear());
		String dateString = 
				new SimpleDateFormat("dd MMM yyyy -- kk:mm:ss (zzz)").format(instance.getTime());
        
		_textLogger.setText(
				"ADOPT_GUI -- " + dateString + "\n***\n");
		
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// Set the relative weights
		// NOTE: set them here when layout is done
		_sashFormUpDw.setWeights(new int[] { 5, 1});
		
		// TODO: check this
		// collapse the project view panel initially --> { 0, 4} 
		_sashFormLeftRight.setWeights(new int[] {1, 4});
		
		// or show the project view panel initially
		// _sashFormLeftRight.setWeights(new int[] { 1, 4});
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//		sashFormLeftRight.pack();
//		sashFormUpDw.pack();

	}

	public Text getMessageTextWindow() {
		return _textLogger;
	}
	
	public SashForm getSashFormUpDw() {
		return _sashFormUpDw;
	}
	
	public SashForm getSashFormLeftRight() {
		return _sashFormLeftRight;
	}

	public boolean isConnectedToMatlab() { return _connectedToMatlab; }
	public void setConnectedToMatlab(boolean yesOrNot) { _connectedToMatlab = yesOrNot; }
	public void toggleConnectionStatus() { _connectedToMatlab = !_connectedToMatlab; }

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public MyProjectTreePane getProjectPane() {
		return _projectPane;
	}

	public void setProjectPane(MyProjectTreePane _projectPane) {
		this._projectPane = _projectPane;
	}

}