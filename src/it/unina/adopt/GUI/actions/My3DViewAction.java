package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import configuration.MyConfiguration;
import it.unina.adopt.GUI.tabpanels.MyTabbedPane;
import it.unina.adopt.main.ADOPT_GUI;

public class My3DViewAction extends Action 
{
	StatusLineManager statusLineManager;

	public My3DViewAction(StatusLineManager sm)
	{
		super("&Update 3D View of Active Aircraft@Ctrl+d", AS_PUSH_BUTTON);
		
		statusLineManager = sm;
		
		setToolTipText("Update 3D View of Active Aircraft");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/3DView_32x32.png");
		
		setImageDescriptor(ImageDescriptor.createFromImage(img));
		
	}
	
	
	public void run()
	{
		statusLineManager.setMessage(
				"3D view of selected aircraft is being updated ...\n"
				);
		
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"\n3D view of selected aircraft is being updated ...\n"
				);
		
//		MyAircraft theAircraft = GlobalData.getTheCurrentAircraft();
		
		// check if view is already there, open otherwise
		int idx = ADOPT_GUI.getApp().getTopLevelComposite()
				.getMyTabbedPane()
				.findTabItem(MyTabbedPane.TAB_NAME_3DVIEW);
		
//		System.out.println("idx = " + idx);
//		System.out.println("Tabbed pane = " + (ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()!=null));
		
		ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().updateFX3DPane();
	
	}
	
	//TODO: remove
	
//	if ( idx == -1 ) {
//	
//	// do open the view
//	
////	ADOPT_GUI.getApp().theSketch = null;
////	ADOPT_GUI.getApp().getTopLevelComposite()
////		.getMyTabbedPane().open3DView();
//	
//	// TODO: notify events to someone?
////	ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().applyDisposeEventListeners();
//	
//	// select & focus the tab
////	ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().get_tabFolder().setSelection(idx);
////	ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().get_tabFolder().forceFocus();
//	
//} else {
//	
//	ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().get_tabFolder().setSelection(idx);
//	ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().get_tabFolder().forceFocus();
//	
//	ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
//			"\n3D already there.\n"
//			);
//	statusLineManager.setMessage(
//			"3D already there\n"
//			);			
//}
	
}