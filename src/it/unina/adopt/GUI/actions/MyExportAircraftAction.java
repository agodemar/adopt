package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import aircraft.OperatingConditions;
import aircraft.calculators.ACAnalysisManager;
import aircraft.calculators.ACPerformanceManager;
import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.utilities.write.MyDataWriter;

public class MyExportAircraftAction extends Action 
{
	StatusLineManager statusLineManager;
	private OperatingConditions _theOperatingConditions;
	private MyDataWriter _theUtilities;
	private ACAnalysisManager _theAnalysis;
	private ACPerformanceManager _thePerformances;

	public MyExportAircraftAction(StatusLineManager sm) {

		super("&Export Aircraft@Ctrl+E", AS_PUSH_BUTTON);
		statusLineManager = sm;

		setToolTipText("Export aircraft to xml and xls file");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/Export_32x32.png");
		
		setImageDescriptor(ImageDescriptor.createFromImage(img));

		_theOperatingConditions = new OperatingConditions();
		_theOperatingConditions.calculate();
		_thePerformances = new ACPerformanceManager();
		_theAnalysis = new ACAnalysisManager(_theOperatingConditions);

	}


	public void run()
	{
		statusLineManager.setMessage(
				"The aircraft is being exported ..."
				);

		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The aircraft is being exported ..."
				);

		final Label label = new Label(ADOPT_GUI.getApp().getTopLevelComposite(), SWT.NONE);

		InputDialog dialog = new InputDialog(ADOPT_GUI.get_shell(), 
				"Enter File Name", 
				"The file will be exported in xml and xls formats", 
				label.getText(), 
				new LengthValidator());

		if (dialog.open() == Window.OK) {
			// User clicked OK; update the label with the input
			label.setText(dialog.getValue());
		}

		Aircraft aircraft = GlobalData.getTheCurrentAircraft();
		_theUtilities = new MyDataWriter(_theOperatingConditions, aircraft);

		// Export everything to file
		if (GlobalData.getTheCurrentAircraft() != null) {
			_theUtilities.exportToXMLfile(
					MyConfiguration.currentDirectoryString 
					+ "/" + label.getText() + ".xml");
			
			_theUtilities.exportToXLSfile(
					MyConfiguration.currentDirectoryString 
					+ "/" + label.getText() + ".xls");
		}

		if (GlobalData.getTheCurrentAircraft() != null) {
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"done\n"
					);
			statusLineManager.setMessage(
					"Aircraft exported"
					);
		} else {
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"\nUnable to export the aircraft.\n"
					);
			statusLineManager.setMessage(
					"Aircraft NOT exported"
					);
		}
	}

}

/**
 * This class validates a String. It makes sure that the String is between 5 and 8
 * characters
 */
class LengthValidator implements IInputValidator {

	/**
	 * Validates the String. Returns null for no error, or an error message
	 * 
	 * @param newText the String to validate
	 * @return String
	 */
	public String isValid(String newText) {
		int len = newText.length();

		// Determine if input is too short or too long
		if (len < 1) return "Too short";
		if (len > 50) return "Too long";

		// Input must be OK
		return null;
	}

}


