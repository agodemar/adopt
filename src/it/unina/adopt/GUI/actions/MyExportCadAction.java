package it.unina.adopt.GUI.actions;

import java.io.File;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import aircraft.components.Aircraft;
import cad.aircraft.MyAircraftBuilder;
import configuration.MyConfiguration;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;


public class MyExportCadAction extends Action{

	private StatusLineManager statusLineManager;

	public MyExportCadAction(StatusLineManager sm) {

		super("&Export CAD@Ctrl+M", AS_PUSH_BUTTON);
		statusLineManager = sm;

		setToolTipText("Export aircraft CAD model");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/cad_32x32.png");
		setImageDescriptor(ImageDescriptor.createFromImage(img));

	}


	public void run() {
		
		Aircraft aircraft = GlobalData.getTheCurrentAircraft();

		final Label label = new Label(ADOPT_GUI.getApp().getTopLevelComposite(), SWT.NONE);

		InputDialog dialog = new InputDialog(ADOPT_GUI.get_shell(), 
				"Enter File Name", 
				"The file will be exported in brep and step formats", 
				label.getText(), 
				new LengthValidator());

		if (dialog.open() == Window.OK) {

			// User clicked OK; update the label with the input
			label.setText(dialog.getValue());

			statusLineManager.setMessage(
					"The aircraft CAD is being exported ..."
					);

			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"The aircraft CAD is being exported ..."
					);

			MyAircraftBuilder aircraftBuilder = new MyAircraftBuilder();
			aircraftBuilder.buildAndWriteCAD(
					aircraft, 
					MyConfiguration.currentDirectory.getAbsolutePath() + File.separator + 
					"cad");
		}

		if (GlobalData.getTheCurrentAircraft() != null)
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"done\n"
					);
			statusLineManager.setMessage(
					"Aircraft CAD created"
					);
		}
		else
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"\nUnable to create CAD model.\n"
					);
			statusLineManager.setMessage(
					"CAD model NOT created"
					);
		}
	}

}
