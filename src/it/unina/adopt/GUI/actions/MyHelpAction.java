package it.unina.adopt.GUI.actions;

import java.text.MessageFormat;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import configuration.MyConfiguration;

public class MyHelpAction extends Action
{
	StatusLineManager statman;
	short triggercount = 0;
	
	public MyHelpAction(StatusLineManager sm)
	{
		super("&About@Ctrl+H", AS_PUSH_BUTTON);
		
		statman = sm;
		setToolTipText("Get some help");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/Help_32x32.png");
		setImageDescriptor(ImageDescriptor.createFromImage(img));

	}
	
	
	public void run()
	{
		triggercount++;
		statman.setMessage(
				"The help action has fired. Count: " +
				triggercount
				);
		
		// display a modal message box
		MessageBox box = new MessageBox(				
				Display.getCurrent().getActiveShell(), 
				SWT.ICON_INFORMATION | SWT.OK);
		box.setText("About");
		String msg = MessageFormat.format(
				"ADOpT, Aircraft Design and Optimization Tool"
				+ "\n\nAgostino De Marco\nFabrizio Nicolosi\nLorenzo Attanasio\nDaniele Gambardella\n\nRunning on {0}",
				new Object[] { System.getProperty("os.name") }
				);
		box.setMessage(msg);
		box.open();

	}
}