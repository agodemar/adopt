package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

import aircraft.OperatingConditions;
import aircraft.calculators.ACAnalysisManager;
import aircraft.calculators.ACPerformanceManager;
import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import configuration.enumerations.AnalysisTypeEnum;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.utilities.MyXMLReader;
import it.unina.adopt.utilities.write.MyDataWriter;
import it.unina.adopt.utilities.write.MyWriteUtils;

public class MyImportAircraftAction extends Action {

	StatusLineManager statusLineManager;
	private OperatingConditions _theOperatingConditions;
	private MyDataWriter _theUtilities;
	private Aircraft _theAircraft;
	private ACAnalysisManager _theAnalysis;
	private ACPerformanceManager _thePerformances;
	private String IMPORT_FILE;

	public MyImportAircraftAction(StatusLineManager sm) {

		super("&Import Aircraft@Ctrl+I", AS_PUSH_BUTTON);
		statusLineManager = sm;

		setToolTipText("Import aircraft from xml file");

		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/XML_32x32.png");

		setImageDescriptor(ImageDescriptor.createFromImage(img));

		_theOperatingConditions = new OperatingConditions();
		_theOperatingConditions.calculate();
		_thePerformances = new ACPerformanceManager();
		_theAnalysis = new ACAnalysisManager(_theOperatingConditions);

	}


	public void run()
	{
		statusLineManager.setMessage(
				"The aircraft is being imported ..."
				);

		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The aircraft is being imported ..."
				);

		//		if (GlobalData.getTheCurrentAircraft() != null) {
		//			ADOPT_GUI.getApp().deleteTheCurrentAircraft();
		//		}

		_theOperatingConditions = new OperatingConditions();
		GlobalData.set_theCurrentOperatingConditions(_theOperatingConditions);

		// Initialize Aircraft with default parameters
		_theAircraft = Aircraft.createDefaultAircraft();

		_theAircraft.set_name("AIRCRAFT");
		GlobalData.setTheCurrentAircraftInMemory(_theAircraft);
		GlobalData.get_theAircraftList().add(_theAircraft);

		// +++++++++++++++++++++++++++++++++++++++++++++++
		// STATIC FUNCTIONS - TO BE CALLED BEFORE EVERYTHING ELSE
		MyWriteUtils.buildXmlTree();

		// Initialize utilities
		//		_theWriteUtilities = new MyWriteUtils(_theAircraft, _theOperatingConditions);

		// Enables the user to select the xml file which has to be imported
		FileDialog dialog = new FileDialog(ADOPT_GUI.get_shell(), SWT.OPEN);
		dialog.setFilterExtensions(new String [] {"*.xml"});
		//		   dialog.setFilterPath("c:\\");
		IMPORT_FILE = dialog.open();

		MyXMLReader _theReadUtilities = new MyXMLReader(_theAircraft, _theOperatingConditions, IMPORT_FILE);
		_theReadUtilities.importAircraft(_theAircraft, IMPORT_FILE);

		// Create dummy analysis to show the tree
		GlobalData.setTheCurrentAnalysis(
				new ACAnalysisManager(_theAircraft, 
						AnalysisTypeEnum.AERODYNAMIC, 
						AnalysisTypeEnum.WEIGHTS,
						AnalysisTypeEnum.BALANCE
						));
		GlobalData.getTheCurrentAnalysis().updateGeometry(_theAircraft);

		ADOPT_GUI.getApp().getTheProjectPane().populateTreeViewer();

		// TODO: can treat this action with a dialog box
		//       with toggle buttons to select the desired components in configuration

		if (GlobalData.getTheCurrentAircraft() != null)
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"done\n"
					);
			statusLineManager.setMessage(
					"Aircraft created"
					);
		}
		else
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"\nUnable to create a new aircraft.\n"
					);
			statusLineManager.setMessage(
					"Aircraft NOT created"
					);
		}
	}

}
