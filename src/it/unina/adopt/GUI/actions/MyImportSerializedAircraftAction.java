package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import standaloneutils.MyXMLReaderUtils;

public class MyImportSerializedAircraftAction extends Action {

	private static String menuMessage = "Import serialized aircraft";
	StatusLineManager statusLineManager;

	public MyImportSerializedAircraftAction (StatusLineManager sm) {
		
		super("&" + menuMessage + "@Ctrl+d", AS_PUSH_BUTTON);

		statusLineManager = sm;
		setToolTipText(menuMessage);

		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/XMLser_32x32.png");

		setImageDescriptor(ImageDescriptor.createFromImage(img));

	}


	public void run() {
		
		String runMessage = "Importing serialized aircraft ...\n",
				endMessage = "...done\n";
		
		statusLineManager.setMessage(runMessage);
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(runMessage);

		FileDialog dialog = new FileDialog(ADOPT_GUI.get_shell(), SWT.OPEN);
		dialog.setFilterExtensions(new String [] {"*.xml"});
		//		   dialog.setFilterPath("c:\\");
		String IMPORT_FILE = dialog.open();
		
		Aircraft aircraft = new Aircraft();
		aircraft = (Aircraft) MyXMLReaderUtils.deserializeObject(GlobalData.getTheCurrentAircraft(), IMPORT_FILE);
		GlobalData.setTheCurrentAircraftInMemory(aircraft);
		GlobalData.getTheCurrentAnalysis().updateGeometry(aircraft);
		GlobalData.get_theAircraftList().add(aircraft);

		ADOPT_GUI.getApp().getTheProjectPane().populateTreeViewer();
		
		statusLineManager.setMessage(endMessage);
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(endMessage);
	}

}


