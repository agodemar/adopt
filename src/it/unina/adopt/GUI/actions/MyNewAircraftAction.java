package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import configuration.enumerations.AnalysisTypeEnum;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;

public class MyNewAircraftAction extends Action 
{
	StatusLineManager statusLineManager;

	public MyNewAircraftAction(StatusLineManager sm)
	{
		super("&New Aircraft@Ctrl+A", AS_PUSH_BUTTON);
		
		statusLineManager = sm;
		
		setToolTipText("Create a new default aircraft");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/FolderAirplane_32x32.png");
		
		setImageDescriptor(ImageDescriptor.createFromImage(img));
		
	}
	
	
	public void run()
	{
		statusLineManager.setMessage(
				"The aircraft is being created ..."
				);
		
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"A new aircraft is being created ..."
				);
		
		if (GlobalData.getTheCurrentAircraft() != null) {
			GlobalData.deleteTheCurrentAircraft();
		}
		
		Aircraft theAircraft = Aircraft.createDefaultAircraft();

		theAircraft.setName("AIRCRAFT");
		GlobalData.setTheCurrentAircraftInMemory(theAircraft);
		GlobalData.get_theAircraftList().add(theAircraft);

		// Create dummy analysis to show the tree
		// TODO: check this
		GlobalData.setTheCurrentAnalysis(
				new ACAnalysisManager(theAircraft, 
						AnalysisTypeEnum.AERODYNAMIC, 
						AnalysisTypeEnum.WEIGHTS,
						AnalysisTypeEnum.BALANCE
						));
		GlobalData.getTheCurrentAnalysis().updateGeometry(theAircraft);
		
		ADOPT_GUI.getApp().getTheProjectPane().populateTreeViewer();
		ADOPT_GUI.getApp().getTheProjectPane().get_theTreeViewer().expandAll();
		
		// TODO: can treat this action with a dialog box
		//       with toggle buttons to select the desired components in configuration

		if (GlobalData.getTheCurrentAircraft() != null)
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"done\n"
					);
			statusLineManager.setMessage(
					"Aircraft created"
					);
		}
		else
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"\nUnable to create a new aircraft.\n"
					);
			statusLineManager.setMessage(
					"Aircraft NOT created"
					);
		}
	}
	
}
