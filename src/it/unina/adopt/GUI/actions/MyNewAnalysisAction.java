package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import aircraft.OperatingConditions;
import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import configuration.enumerations.AnalysisTypeEnum;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;


public class MyNewAnalysisAction extends Action{

	private StatusLineManager statusLineManager;

	public MyNewAnalysisAction(StatusLineManager sm) {

		super("&Create an analysis@Ctrl+M", AS_PUSH_BUTTON);
		statusLineManager = sm;

		Device dev = Display.getCurrent();
		Image img = new Image(dev, 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/analysis_32x32.png");

		setToolTipText("Create an analysis");
		setImageDescriptor(ImageDescriptor.createFromImage(img));

	}


	public void run() {

		statusLineManager.setMessage(
				"The Analysis is being created ..."
				);
		
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The Analysis is being created  ..."
				);
		
		Aircraft aircraft = GlobalData.getTheCurrentAircraft();

		GlobalData.setTheCurrentAnalysis(
				new ACAnalysisManager(
						new OperatingConditions(),
						aircraft, 
						AnalysisTypeEnum.AERODYNAMIC, 
						AnalysisTypeEnum.WEIGHTS,
						AnalysisTypeEnum.BALANCE
						));
		
		GlobalData.getTheCurrentAnalysis().setName("ANALYSIS");
		GlobalData.getTheCurrentAnalysis().doAnalysis(
				aircraft, 
				AnalysisTypeEnum.AERODYNAMIC, 
				AnalysisTypeEnum.WEIGHTS,
				AnalysisTypeEnum.BALANCE);
		
		ADOPT_GUI.getApp().getTheProjectPane().populateTreeViewer();
//		ADOPT_GUI.getApp().getTheProjectPane().get_theTreeViewer().expandAll();
		
		if (GlobalData.getTheCurrentAnalysis() != null) {
			
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"done\n"
					);
			statusLineManager.setMessage(
					"Analysis created"
					);
			
		} else {
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"\nUnable to create Analysis.\n"
					);
			statusLineManager.setMessage(
					"Analysis NOT created"
					);
		}
	}

}
