package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import configuration.MyConfiguration;
import it.unina.adopt.main.ADOPT_GUI;

public class MyOptionsAction extends Action
{

	StatusLineManager statman;
	short triggercount = 0;
	
	public MyOptionsAction(StatusLineManager sm)
	{
		super("&Options@Ctrl+O", AS_PUSH_BUTTON);
		
		statman = sm;
		setToolTipText("Get some options");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/Options_32x32.png");
		setImageDescriptor(ImageDescriptor.createFromImage(img));
		
	}
	
	public void run()
	{
		triggercount++;
		statman.setMessage(
				"The option action has fired. Count: " +
				triggercount
				);
		
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The ADAS|options action has fired. Count: " +
				triggercount + "\n"
				);

	}// end of run method
	
}// end of class definition 