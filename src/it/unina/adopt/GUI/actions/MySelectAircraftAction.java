package it.unina.adopt.GUI.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;

public class MySelectAircraftAction extends Action {

	private Display display;
	private Shell shell;
	private Button btnAircraft = null;
	private StatusLineManager statusLineManager;
	private List<Button> buttonList = new ArrayList<Button>();
	private int idxSelection = 0, idx = 0;


	public MySelectAircraftAction(StatusLineManager sm) {

		super("&Select Aircraft@Ctrl+P", AS_PUSH_BUTTON);
		statusLineManager = sm;

		setToolTipText("Select aircraft");

		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/Select32x32.png");

		setImageDescriptor(ImageDescriptor.createFromImage(img));

	}

	public void run() {

		idxSelection = 0; 
		idx = 0;
		buttonList.clear();
		display = ADOPT_GUI.getApp().getTopLevelComposite().getDisplay();
		shell = new Shell(display);

		statusLineManager.setMessage("Aircraft selected...");
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append("\nAircraft selected...");

		//		FormLayout layout = new FormLayout();
		//		layout.marginWidth = 3;
		//		layout.marginHeight = 3;

		shell.setText("Select the aircraft");
		shell.setLocation(display.getBounds().x + (int)(display.getBounds().width/2), 
				display.getBounds().y + (int)(display.getBounds().height/2));
		shell.setLayout(new FillLayout(SWT.VERTICAL));
		shell.setSize(250, 200);

		//		// Create a List with a vertical ScrollBar
		//		List list = new List(shell, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		//
		//		// Add a bunch of items to it
		//		for (int i = 0; i < ADOPT_GUI.getApp().get_theAircraftList().size(); i++) {
		//			list.add((i+1) +" - " + ADOPT_GUI.getApp().get_theAircraftList().get(i).get_name());
		//		}

		for (int i= 0; i < GlobalData.get_theAircraftList().size(); i++) {
			//			btnAircraft.setParent(shell);
			btnAircraft = new Button(shell, SWT.NONE);
			btnAircraft.setBounds(0, 25*i, 50, 20);
			btnAircraft.setText((i+1) +" - " +  GlobalData.get_theAircraftList().get(i).get_name());
			btnAircraft.setEnabled(true);
			btnAircraft.setSelection(false);
			buttonList.add(btnAircraft);
			System.out.println("number of aircrafts: " + GlobalData.get_theAircraftList().size());
			//			System.out.println("idx: " + idxSelection);

		}

		for (idx = 0; idx < GlobalData.get_theAircraftList().size(); idx++) {
			MySelection selection = new MySelection(GlobalData.get_theAircraftList().get(idx));
			buttonList.get(idx).addSelectionListener(selection);
		}

		//			idxSelection = i;

		//		for (int j=0; j < ADOPT_GUI.getApp().get_theAircraftList().size(); j++) {
		//			
		//			idxSelection = j;
		//			System.out.println("idxSelection " + idxSelection);
		//			buttonList.get(j).addSelectionListener(new SelectionAdapter() {
		//
		//				@Override
		//				public void widgetSelected(SelectionEvent e) {
		//
		//					if (buttonList.get(idxSelection).getSelection())
		//						ADOPT_GUI.getApp().setTheCurrentAircraft(
		//								ADOPT_GUI.getApp().get_theAircraftList().get(idxSelection));
		//
		//					System.out.println("Aircraft " + idxSelection + " selected.");
		//				}
		//			});
		//		}

		//		list.addListener(SWT.Selection, new Listener() {
		//			public void handleEvent(Event e) {
		//
		//				String string = "";
		//				int[] selection = list.getSelectionIndices();
		//				
		//				for (int i = 0; i < selection.length; i++) {
		//					idxSelection = i;
		//					string += selection[i] + " ";
		//				}
		//
		//				System.out.println("Aircraft " + idxSelection + " selected.");
		//			}
		//		});

		//		// Get the ScrollBar
		//		ScrollBar sb = list.getVerticalBar();
		//
		//		// Show the selection value
		//		System.out.println("Selection: " + sb.getSelection());
		//		ADOPT_GUI.getApp().setTheCurrentAircraft(ADOPT_GUI.getApp().get_theAircraftList().get(idxSelection));

		//		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private class MySelection implements SelectionListener {

		private Aircraft aircraft;

		public MySelection(Aircraft ac) {
			aircraft = ac;
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void widgetSelected(SelectionEvent arg0) {

			System.out.println("Aircraft " + aircraft.get_name() + " selected.");
			GlobalData.setTheCurrentAircraftInMemory(aircraft);
			shell.dispose();

		}

	}

}

