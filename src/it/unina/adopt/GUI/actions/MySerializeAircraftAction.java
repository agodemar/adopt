package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import configuration.MyConfiguration;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import writers.JPADStaticWriteUtils;

public class MySerializeAircraftAction extends Action {

	private static String menuMessage = "Serialize aircraft";
	StatusLineManager statusLineManager;

	public MySerializeAircraftAction (StatusLineManager sm) {

		super("&" + menuMessage + "@Ctrl+d", AS_PUSH_BUTTON);

		statusLineManager = sm;
		setToolTipText(menuMessage);

		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/ExportSer_32x32.png");

		setImageDescriptor(ImageDescriptor.createFromImage(img));

	}


	public void run() {

		String runMessage = "Serializing aircraft ...\n",
				endMessage = "...done\n";

		statusLineManager.setMessage(runMessage);
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(runMessage);

		final Label label = new Label(ADOPT_GUI.getApp().getTopLevelComposite(), SWT.NONE);

		InputDialog dialog = new InputDialog(ADOPT_GUI.get_shell(), 
				"Input File Name", 
				"The file will be serialized in an xml file", 
				label.getText(), 
				new LengthValidator());

		if (dialog.open() == Window.OK) {
			// User clicked OK; update the label with the input
			label.setText(dialog.getValue());
		}

		JPADStaticWriteUtils.serializeObject(
				GlobalData.getTheCurrentAircraft(), MyConfiguration.databaseDirectory, label.getText());

		statusLineManager.setMessage(endMessage);
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(endMessage);

	}

}

