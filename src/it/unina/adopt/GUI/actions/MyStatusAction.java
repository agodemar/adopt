package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import configuration.MyConfiguration;
import it.unina.adopt.main.ADOPT_GUI;

public class MyStatusAction extends Action
{
	StatusLineManager statman;
	short triggercount = 0;
	
	public MyStatusAction(StatusLineManager sm)
	{
		super("&Trigger@Ctrl+T", AS_PUSH_BUTTON);

		statman = sm;
		setToolTipText("Trigger the Action");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse32.png");
		setImageDescriptor(ImageDescriptor.createFromImage(img));

	}
	
	
	public void run()
	{
		triggercount++;
		statman.setMessage(
				"The status action has fired. Count: " +
				triggercount
				);
		
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The status action has fired. Count: " +
				triggercount + "\n"
				);
	}
}