package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import configuration.MyConfiguration;
import it.unina.adopt.main.ADOPT_GUI;

public class MyToggleFuselageInitiatorViewAction extends Action {

	public MyToggleFuselageInitiatorViewAction(StatusLineManager sm)
	{
		super("&Toggle fuselage initiator tab@Ctrl+F", AS_PUSH_BUTTON);
		
		setToolTipText("Toggle fuselage initiator tab");

		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_32x32.png");
		
		setImageDescriptor(ImageDescriptor.createFromImage(img));
		
	}
	
	public void run()
	{
		if ( ADOPT_GUI.getApp().theFuselage == null )
		{
			
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
			"The ADOpT | fuselage initiator: null"
			+ "\n"
			);			
			
		}
		else
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
			"The ADOpT | fuselage initiator: NOT null"
			+ "\n"
			);
			
			int nTabs = 
					ADOPT_GUI.getApp()
					.getTopLevelComposite()
					.getMyTabbedPane().get_tabFolder().getItemCount();
			
			System.out.println("nTabs="+ nTabs);

			boolean bFound = false;
			for (int t = 0; t < nTabs; t++)
			{
				System.out.println("t="+ t);
				
				System.out.println(
						ADOPT_GUI.getApp()
						.getTopLevelComposite()
						.getMyTabbedPane().get_tabFolder().getItem(t).getText()
						);

				System.out.println(
						"("+
						ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
							.tabItemFuselageInitiator.getText()
						+")"
						);
				
				// TO DO: check this: "(CTabItem {Fuselage Initiator})"
				bFound =
					(
					ADOPT_GUI.getApp()
					.getTopLevelComposite()
					.getMyTabbedPane().get_tabFolder().getItem(t).getText()
						.equals(
							ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
								.tabItemFuselageInitiator.getText()				
						)
					);
				System.out.println("found ------> " + bFound);

				if ( bFound )
					break;
			}

			
			// detect if tab is there
			if (bFound) {
				
				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The ADOpT | fuselage initiator: FOUND"
						+ "\n"
						);

				// _tabItemFuselageInitiator = 
				
			}

			
		}
		
	}// end of run method

}