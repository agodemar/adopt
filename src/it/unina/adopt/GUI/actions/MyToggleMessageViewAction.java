package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import configuration.MyConfiguration;
import it.unina.adopt.main.ADOPT_GUI;

public class MyToggleMessageViewAction extends Action
{
	public MyToggleMessageViewAction(StatusLineManager sm)
	{
		super("&Toggle log message view@Ctrl+M", AS_PUSH_BUTTON);
		
		setToolTipText("Toggle log message window view");
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/TextPlain_32x32.png");
		
		setImageDescriptor(ImageDescriptor.createFromImage(img));
		
	}
	
	
	public void run()
	{
		// get sash form weights and resize accordingly
		int [] weights = ADOPT_GUI.getApp().getTopLevelComposite().getSashFormUpDw().getWeights();
		if ( weights[1] == 0 )
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getSashFormUpDw().setWeights(new int[] { 4, 1});			
		}
		else
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getSashFormUpDw().setWeights(new int[] { 4, 0});			
		}
		
	}// end of run method

}