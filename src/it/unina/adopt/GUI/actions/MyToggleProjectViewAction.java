package it.unina.adopt.GUI.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import configuration.MyConfiguration;
import it.unina.adopt.main.ADOPT_GUI;

public class MyToggleProjectViewAction extends Action
{
	public MyToggleProjectViewAction(StatusLineManager sm)
	{
		super("&Toggle project view@Ctrl+P", AS_PUSH_BUTTON);
		
		setToolTipText("Toggle project pannel view on left side");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/TreeView_32x32.png");
		
		setImageDescriptor(ImageDescriptor.createFromImage(img));
		
	}
	
	
	public void run()
	{
		// get sash form weights and resize accordingly
		int [] weights = ADOPT_GUI.getApp().getTopLevelComposite().getSashFormLeftRight().getWeights();
		if ( weights[0] == 0 )
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getSashFormLeftRight().setWeights(new int[] { 1, 5});
		}
		else
		{
			ADOPT_GUI.getApp().getTopLevelComposite().getSashFormLeftRight().setWeights(new int[] { 0, 4});			
		}
		
	}// end of run method

}
