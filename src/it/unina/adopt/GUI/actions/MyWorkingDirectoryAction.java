package it.unina.adopt.GUI.actions;

import java.io.File;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;

import configuration.MyConfiguration;
import it.unina.adopt.main.ADOPT_GUI;

public class MyWorkingDirectoryAction extends Action
{

	StatusLineManager statman;
	short triggercount = 0;
	
	public MyWorkingDirectoryAction(StatusLineManager sm)
	{
		super("&Current directory@Ctrl+W", AS_PUSH_BUTTON);
		
		statman = sm;
		setToolTipText("Set working directory");
		
		Image img = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/Folder_32x32.png");
		setImageDescriptor(ImageDescriptor.createFromImage(img));
		
	}
	
	public void run()
	{
		triggercount++;
		statman.setMessage(
				"The option action has fired. Count: " +
				triggercount
				);
		
		DirectoryDialog dialog = new DirectoryDialog(ADOPT_GUI.getApp().getShell());
	    dialog.setFilterPath( MyConfiguration.currentDirectory.toString() ); // Windows specific
	    String sDir = dialog.open();
	    if ( sDir != null ) {
			File dir = new File(sDir);
			ADOPT_GUI.set_CurrentDirectory(dir);
	    }
	    
		ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The ADOpT|working directory: " +
				MyConfiguration.currentDirectory.toString() + "\n"
				);

	}// end of run method
	
}// end of class definition 