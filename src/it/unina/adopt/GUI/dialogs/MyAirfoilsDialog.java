package it.unina.adopt.GUI.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import it.unina.adopt.GUI.tabpanels.liftingSurface.MyAirfoilAerodynamicsPanel;

public class MyAirfoilsDialog extends Composite {
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	private Text text;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyAirfoilsDialog(Composite parent, int style) {
		super(parent, style);
		
		CTabFolder tabFolder = new CTabFolder(this, SWT.BORDER);
		tabFolder.setBounds(10, 10, 457, 473);
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		
		CTabItem tbtmItem = new CTabItem(tabFolder, SWT.NONE);
		tbtmItem.setShowClose(true);
		tbtmItem.setText("item1");
		
		MyAirfoilAerodynamicsPanel composite = new MyAirfoilAerodynamicsPanel(tabFolder, SWT.NONE);
		tbtmItem.setControl(composite);
		formToolkit.paintBordersFor(composite);
		
		CTabItem tbtmItem_1 = new CTabItem(tabFolder, SWT.NONE);
		tbtmItem_1.setShowClose(true);
		tbtmItem_1.setText("item2");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
