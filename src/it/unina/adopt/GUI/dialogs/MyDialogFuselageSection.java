package it.unina.adopt.GUI.dialogs;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.measure.DecimalMeasure;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.jscience.physics.amount.Amount;

import aircraft.components.fuselage.Fuselage;
import it.unina.adopt.GUI.tabpanels.fuselage.MyFuselagePanel;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.MyNomenclatureImageDialog;
import it.unina.adopt.utilities.gui.MyGuiUtils;


public class MyDialogFuselageSection {

	private Fuselage _theFuselage = null;
	private MyFuselagePanel _theInitiatorPaneFuselage = null;

	private MyNomenclatureImageDialog _nomenclatureFuselageSection = null;

	private Text _text_SectionX;
	private Text _text_SectionWidth;
	private Text _text_SectionHeight;
	private Text _text_UpperSectionControlPoints;
	private Text _text_LowerSectionControlPoints;
	private Text _text_LowerSectionAPoint;
	private final int TEXT_WIDTH_HINT = 80;
	private final int LATEX_FORMULA_WIDTH_HINT = 70;

	Button _button_Nomenclature;

	public MyDialogFuselageSection(Display display, MyFuselagePanel initiatorPaneFuselage, Fuselage fuselage){

		_theFuselage = fuselage;
		_theInitiatorPaneFuselage = initiatorPaneFuselage;

		final Shell dialog = new Shell(
				display,
				SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL
				| SWT.RESIZE
				);
		dialog.setText("Section Shape");

//		dialog.setSize(450, 410);
		Monitor monitor = Display.getDefault().getMonitors()[0];
		dialog.setSize(
				(int)( 0.55*monitor.getBounds().width  ),
				(int)( 0.55*monitor.getBounds().height )
			);

		// Fill the main dialog appropriately
		dialog.setLayout(new FillLayout());
		
		// Create the ScrolledComposite to scroll horizontally and vertically
		ScrolledComposite scrolledComposite = new ScrolledComposite(
				dialog, SWT.H_SCROLL | SWT.V_SCROLL
				);
		
		// Create a child composite to hold the controls
	    Composite container = new Composite(scrolledComposite, SWT.NONE);
	    
		// Fill the container (dialog's child) appropriately
		GridLayout gridLayout = new GridLayout(2,false);
		container.setLayout(gridLayout);

		// Fuselage X Section

		GridData gd_sectionX_Label = new GridData();
		gd_sectionX_Label.horizontalSpan = 2;

		Label labelX = new Label(container, SWT.NONE);
		labelX.setText(
				"Section X-coordinate"
						+ " (" + _theInitiatorPaneFuselage.get_selectedSectionData().toString() +")"
				);
		labelX.setLayoutData(gd_sectionX_Label);

		GridData gd_sectionX = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_sectionX.widthHint=TEXT_WIDTH_HINT;

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_00 = "X";
		Image formulaImage_00 = MyGuiUtils.renderLatexFormula(
				container,	latex_text_00, 22
				);
		Label label_00a = new Label(container, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_00, formulaImage_00);
		label_00a.setImage (formulaImage_00);

		GridData gd_Label_00a = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_Label_00a.horizontalSpan = 1;
		gd_Label_00a.widthHint = LATEX_FORMULA_WIDTH_HINT;
		label_00a.setLayoutData(gd_Label_00a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		_text_SectionX = new Text(container, SWT.BORDER);
		Amount<Length> _len_X = null;
		switch ( _theInitiatorPaneFuselage.get_selectedSectionData() )
		{
		case NOSE_TIP:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_NOSE_TIP);
			break;
		case NOSE_CAP:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_NOSE_CAP);
			break;
		case MID_NOSE:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_MID_NOSE);
			break;
		case CYLINDRICAL_BODY_1:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_1);
			break;
		case CYLINDRICAL_BODY_2:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_2);
			break;
		case MID_TAIL:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_MID_TAIL);
			break;
		case TAIL_CAP:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_TAIL_CAP);
			break;
		case TAIL_TIP:
			_len_X = _theFuselage.get_sectionsYZStations().get(_theFuselage.IDX_SECTION_YZ_TAIL_TIP);
			break;
		default:
			// do nothing
			break;
		}
		BigDecimal bd_str_X= BigDecimal.valueOf(_len_X.getEstimatedValue());
		bd_str_X = bd_str_X.setScale(3, RoundingMode.DOWN);
		_text_SectionX.setText( " " +bd_str_X+ " "+_len_X.getUnit().toString());
		_text_SectionX.setLayoutData(gd_sectionX);	

		// Fuselage Height at X section

		GridData gd_sectionHeight_Label = new GridData();
		gd_sectionHeight_Label.horizontalSpan = 2;

		Label labelHeight = new Label(container, SWT.NONE);
		labelHeight.setText("Section Height");
		labelHeight.setLayoutData(gd_sectionHeight_Label);

		GridData gd_sectionHeight = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_sectionHeight.widthHint=TEXT_WIDTH_HINT;

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_001 = "h_\\mathrm{f}(X)";
		Image formulaImage_001 = MyGuiUtils.renderLatexFormula(
				container,	latex_text_001, 22
				);
		Label label_001a = new Label(container, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_001, formulaImage_001);
		label_001a.setImage (formulaImage_001);

		GridData gd_Label_001a = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_Label_001a.horizontalSpan = 1;
		gd_Label_001a.widthHint = LATEX_FORMULA_WIDTH_HINT;
		label_001a.setLayoutData(gd_Label_001a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		_text_SectionHeight= new Text(container, SWT.BORDER);

		Amount<Length>  _len_hf = null;
		switch ( _theInitiatorPaneFuselage.get_selectedSectionData() )
		{
		case NOSE_TIP:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_NOSE_TIP).get_Len_Height();
			break;
		case NOSE_CAP:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_NOSE_CAP).get_Len_Height();
			break;
		case MID_NOSE:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_NOSE).get_Len_Height();
			break;
		case CYLINDRICAL_BODY_1:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_1).get_Len_Height();
			break;
		case CYLINDRICAL_BODY_2:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_2).get_Len_Height();
			break;
		case MID_TAIL:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_TAIL).get_Len_Height();
			break;
		case TAIL_CAP:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_CAP).get_Len_Height();
			break;
		case TAIL_TIP:
			_len_hf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_TIP).get_Len_Height();
			break;
		default:
			// do nothing
			break;
		}
		
		BigDecimal bd_str_hf= BigDecimal.valueOf(_len_hf.getEstimatedValue());
		bd_str_hf = bd_str_hf.setScale(3, RoundingMode.DOWN);
		_text_SectionHeight.setText( " " +bd_str_hf+ " "+_len_hf.getUnit().toString());
		_text_SectionHeight.setLayoutData(gd_sectionHeight);	
		
		// Fuselage Width at X Section

		GridData gd_sectionWidth_Label = new GridData();
		gd_sectionWidth_Label.horizontalSpan = 2;

		Label labelWidth = new Label(container, SWT.NONE);
		labelWidth.setText("Section Width");
		labelWidth.setLayoutData(gd_sectionWidth_Label);

		GridData gd_sectionWidth = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_sectionWidth.widthHint=TEXT_WIDTH_HINT;

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_01 = "w_\\mathrm{f}(X)";
		Image formulaImage_01 = MyGuiUtils.renderLatexFormula(
				container,	latex_text_01, 22
				);
		Label label_01a = new Label(container, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_01, formulaImage_01);
		label_01a.setImage (formulaImage_01);

		GridData gd_Label_01a = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_Label_01a.horizontalSpan = 1;
		gd_Label_01a.widthHint = LATEX_FORMULA_WIDTH_HINT;
		label_01a.setLayoutData(gd_Label_01a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		_text_SectionWidth = new Text(container, SWT.BORDER);

		Amount<Length> _len_wf = null;
		switch ( _theInitiatorPaneFuselage.get_selectedSectionData() )
		{
		case NOSE_TIP:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_NOSE_TIP).get_w_f();
			break;
		case NOSE_CAP:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_NOSE_CAP).get_w_f();
			break;
		case MID_NOSE:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_NOSE).get_w_f();
			break;
		case CYLINDRICAL_BODY_1:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_1).get_w_f();
			break;
		case CYLINDRICAL_BODY_2:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_2).get_w_f();
			break;
		case MID_TAIL:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_TAIL).get_w_f();
			break;
		case TAIL_CAP:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_CAP).get_w_f();
			break;
		case TAIL_TIP:
			_len_wf = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_TIP).get_w_f();
			break;
		default:
			// do nothing
			break;
		}
		BigDecimal bd_str_wf= BigDecimal.valueOf(_len_wf.getEstimatedValue());
		bd_str_wf = bd_str_wf.setScale(3, RoundingMode.DOWN);
		_text_SectionWidth.setText( " " +bd_str_wf+ " "+_len_wf.getUnit().toString());
		_text_SectionWidth.setLayoutData(gd_sectionWidth);	
		
		
		// Fuselage Weight Factor Upper Section Control Points at X Section

		GridData gd_uSectionControlPoints_Label = new GridData();
		gd_uSectionControlPoints_Label.horizontalSpan = 2;

		Label upperSectionControlPoints = new Label(container, SWT.NONE);
		upperSectionControlPoints.setText("Upper Section Control Point Weigth Factor");
		upperSectionControlPoints.setLayoutData(gd_uSectionControlPoints_Label);

		GridData gd_uSectionControlPoints = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_uSectionControlPoints.widthHint = TEXT_WIDTH_HINT;

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_02 = "\\rho_\\mathrm{u}(X)";
		Image formulaImage_02 = MyGuiUtils.renderLatexFormula(
				container,	latex_text_02, 22
				);
		Label label_02a = new Label(container, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_02, formulaImage_02);
		label_02a.setImage (formulaImage_02);

		GridData gd_Label_02a = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_Label_02a.horizontalSpan = 1;
		gd_Label_02a.widthHint = LATEX_FORMULA_WIDTH_HINT;
		label_02a.setLayoutData(gd_Label_02a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		_text_UpperSectionControlPoints= new Text(container, SWT.BORDER);

		String str_rhoU = "";
		switch ( _theInitiatorPaneFuselage.get_selectedSectionData() )
		{
		case NOSE_TIP:
			str_rhoU =(_theFuselage.get_sectionsYZ()).get(_theFuselage.IDX_SECTION_YZ_NOSE_TIP).get_RhoUpper().toString() ;
			break;
		case NOSE_CAP:
			str_rhoU = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_NOSE_CAP).get_RhoUpper().toString();
			break;
		case MID_NOSE:
			str_rhoU = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_NOSE).get_RhoUpper().toString();
			break;
		case CYLINDRICAL_BODY_1:
			str_rhoU = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_1).get_RhoUpper().toString();
			break;
		case CYLINDRICAL_BODY_2:
			str_rhoU = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_2).get_RhoUpper().toString();
			break;
		case MID_TAIL:
			str_rhoU = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_TAIL).get_RhoUpper().toString();
			break;
		case TAIL_CAP:
			str_rhoU = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_CAP).get_RhoUpper().toString();
			break;
		case TAIL_TIP:
			str_rhoU = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_TIP).get_RhoUpper().toString();
			break;
		default:
			// do nothing
			break;
		}
		
		_text_UpperSectionControlPoints.setText( "  " + str_rhoU);
		_text_UpperSectionControlPoints.setLayoutData(gd_uSectionControlPoints);	

		// Fuselage Weight Factor Lower Section Control Points at X Section

		GridData gd_lSectionControlPoints_Label = new GridData();
		gd_lSectionControlPoints_Label.horizontalSpan = 2;

		Label lowerSectionControlPoints = new Label(container, SWT.NONE);
		lowerSectionControlPoints.setText("Lower Section Control Point Weigth Factor");
		lowerSectionControlPoints.setLayoutData(gd_lSectionControlPoints_Label);

		GridData gd_lSectionControlPoints = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_lSectionControlPoints.widthHint = TEXT_WIDTH_HINT;

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_03 = "\\rho_\\mathrm{l}(X)";
		Image formulaImage_03 = MyGuiUtils.renderLatexFormula(
				container,	latex_text_03, 22
				);
		Label label_03a = new Label(container, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_03, formulaImage_03);
		label_03a.setImage (formulaImage_03);

		GridData gd_Label_03a = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_Label_03a.horizontalSpan = 1;
		gd_Label_03a.widthHint = LATEX_FORMULA_WIDTH_HINT;
		label_03a.setLayoutData(gd_Label_03a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		_text_LowerSectionControlPoints= new Text(container, SWT.BORDER);


		String str_rhoL = "";
		switch ( _theInitiatorPaneFuselage.get_selectedSectionData() )
		{
		case NOSE_TIP:
			str_rhoL =(_theFuselage.get_sectionsYZ()).get(_theFuselage.IDX_SECTION_YZ_NOSE_TIP).get_RhoLower().toString() ;
			break;
		case NOSE_CAP:
			str_rhoL = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_NOSE_CAP).get_RhoLower().toString();
			break;
		case MID_NOSE:
			str_rhoL = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_NOSE).get_RhoLower().toString();
			break;
		case CYLINDRICAL_BODY_1:
			str_rhoL = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_1).get_RhoLower().toString();
			break;
		case CYLINDRICAL_BODY_2:
			str_rhoL = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_2).get_RhoLower().toString();
			break;
		case MID_TAIL:
			str_rhoL = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_TAIL).get_RhoLower().toString();
			break;
		case TAIL_CAP:
			str_rhoL = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_CAP).get_RhoLower().toString();
			break;
		case TAIL_TIP:
			str_rhoL = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_TIP).get_RhoLower().toString();
			break;
		default:
			// do nothing
			break;
		}
		_text_LowerSectionControlPoints.setText( "  " + str_rhoL);
		_text_LowerSectionControlPoints.setLayoutData(gd_lSectionControlPoints);	

		// Fuselage Weight Factor A section point at X Section

		GridData gd_aLowerPoint_Label = new GridData();
		gd_aLowerPoint_Label .horizontalSpan = 2;

		Label loweraControlPoint = new Label(container, SWT.NONE);
		loweraControlPoint.setText("Z-location of the cross-section lower section point");
		loweraControlPoint.setLayoutData(gd_aLowerPoint_Label);

		GridData gd_aSectionControlPoint = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_aSectionControlPoint .widthHint = TEXT_WIDTH_HINT;

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_04 = "a(X)";
		Image formulaImage_04 = MyGuiUtils.renderLatexFormula(
				container,	latex_text_04, 22
				);
		Label label_04a = new Label(container, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_04, formulaImage_04);
		label_04a.setImage (formulaImage_04);

		GridData gd_Label_04a = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_Label_04a.horizontalSpan = 1;
		gd_Label_04a.widthHint = LATEX_FORMULA_WIDTH_HINT;
		label_04a.setLayoutData(gd_Label_04a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		_text_LowerSectionAPoint = new Text(container, SWT.BORDER);

		String str_a = "";
		switch ( _theInitiatorPaneFuselage.get_selectedSectionData() )
		{
		case NOSE_TIP:
			str_a =(_theFuselage.get_sectionsYZ()).get(_theFuselage.IDX_SECTION_YZ_NOSE_TIP).get_LowerToTotalHeightRatio().toString() ;
			break;
		case NOSE_CAP:
			str_a = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_NOSE_CAP).get_LowerToTotalHeightRatio().toString();
			break;
		case MID_NOSE:
			str_a = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_NOSE).get_LowerToTotalHeightRatio().toString();
			break;
		case CYLINDRICAL_BODY_1:
			str_a = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_1).get_LowerToTotalHeightRatio().toString();
			break;
		case CYLINDRICAL_BODY_2:
			str_a = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_CYLINDER_2).get_LowerToTotalHeightRatio().toString();
			break;
		case MID_TAIL:
			str_a = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_MID_TAIL).get_LowerToTotalHeightRatio().toString();
			break;
		case TAIL_CAP:
			str_a = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_CAP).get_LowerToTotalHeightRatio().toString();
			break;
		case TAIL_TIP:
			str_a = _theFuselage.get_sectionsYZ().get(_theFuselage.IDX_SECTION_YZ_TAIL_TIP).get_LowerToTotalHeightRatio().toString();
			break;
		default:
			// do nothing
			break;
		}

		_text_LowerSectionAPoint.setText( "  " + str_a);
		_text_LowerSectionAPoint.setLayoutData(gd_aSectionControlPoint);


		ShowSectionParameters();

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// Nomenclature

		GridData gd_button_Nomenclature = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_button_Nomenclature.horizontalSpan = 2;
		_button_Nomenclature = new Button(container, SWT.NONE);
		_button_Nomenclature.setText("Section Nomenclature");
		_button_Nomenclature.setLayoutData(gd_button_Nomenclature);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		
		  // Set the child as the scrolled content of the ScrolledComposite
	    scrolledComposite.setContent(container);

	    // Set the minimum size
	    // to make scroll bars visible when container is resized
	    scrolledComposite.setMinSize(
	    		(int)( 0.45*monitor.getBounds().width  ),
	    		(int)( 0.45*monitor.getBounds().height )
	    		);

	    // Expand both horizontally and vertically
	    scrolledComposite.setExpandHorizontal(true);
	    scrolledComposite.setExpandVertical(true);
		
	    
	    container.pack();
	    scrolledComposite.pack();
		
		
		
		// EVENTS

		_button_Nomenclature.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {

						Shell dialogNomenclature = new Shell(Display.getCurrent(),
								SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.RESIZE
								);



						dialogNomenclature.setLayout(new FillLayout());
						dialogNomenclature.setText(" Section Nomenclature ");						
						_nomenclatureFuselageSection = new MyNomenclatureImageDialog(dialogNomenclature,"images/Fuselage_Nomenclature_Sectionview.png");


					}
				}
				); // end of button_Nomenclature listener

		_text_SectionWidth.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {

				// NOTE: this text box will be editable only if CYLINDRICAL_BODY_1 is selected
				
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_01.getText();

				s_value = s_value.trim();

				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {

					// check for illegal values
					try {  

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						 DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						 len =len.to(SI.METER);

						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER));
						if ( 
								(value < _theFuselage.get_sectionWidth_MIN().getEstimatedValue()) 
								|| 
								(value > _theFuselage.get_sectionWidthMAX().getEstimatedValue()) 
								){
							e.doit = false;
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: bad width. Insert new value!\n");
						}

						else {

							_theFuselage.adjustCylinderSectionWidth(
									Amount.valueOf(value.doubleValue(),SI.METRE)
									);

						}

						// recalculateCurves(); // TO DO: make only the section recalculation
						_theFuselage.calculateOutlines();
						_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
						_theInitiatorPaneFuselage.redrawFuselageXY();

					}
					catch( Exception ex ){  
						if(!s_value.equals("")) {
							e.doit = false;
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
						}
					}		
				}
			}
		}); // end of SectionWidth listener

		
			
		_text_UpperSectionControlPoints.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_01.getText();
				s_value = s_value.trim();
				Double value = Double.valueOf(s_value);
				if (e.detail == SWT.TRAVERSE_RETURN) {
					
					try{

						if ( 
								(value < _theFuselage.get_sectionRhoUpper_MIN()) 
								|| 
								(value > _theFuselage.get_sectionRhoUpper_MAX()) 
								) {  
							e.doit = false;
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: bad rho_upper. Insert new value!\n");
						}
						
						else{
							
							switch( _theInitiatorPaneFuselage.get_selectedSectionData())
							{
							
							
							
							case MID_NOSE:
								
								_theFuselage.adjustMidNoseSectionRhoUpper(value);
								int idx_Mid_Nose = _theInitiatorPaneFuselage.get_selectedSectionIdx();
								Double a_Mid_Nose = _theFuselage.get_sectionNoseMidLowerToTotalHeightRatio();
								Double rhoUpper_Mid_Nose  = value;
								Double rhoLower_Mid_Nose  = _theFuselage.get_sectionMidNoseRhoLower();
								Double x_Mid_Nose = _theFuselage.get_sectionsYZStations().get(idx_Mid_Nose)
										.doubleValue(SI.METRE);
								Double hf_Mid_Nose = _theFuselage.get_sectionsYZ().get(idx_Mid_Nose).get_h_f();
								Double zlf_Mid_Nose = _theFuselage.getZOutlineXZLowerAtX(x_Mid_Nose);
								Double dZ_Mid_Nose = zlf_Mid_Nose + 0.5*hf_Mid_Nose; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx_Mid_Nose,  // section IDX
										a_Mid_Nose, rhoUpper_Mid_Nose , rhoLower_Mid_Nose
										);

								_theFuselage.get_sectionsYZ().get(idx_Mid_Nose).translateZ(dZ_Mid_Nose);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx_Mid_Nose);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();

								break;

							case MID_TAIL:
								
								_theFuselage.adjustMidTailSectionRhoUpper(value);
								int idx_Mid_Tail = _theInitiatorPaneFuselage.get_selectedSectionIdx();
								Double a_Mid_Tail = _theFuselage.get_sectionTailMidLowerToTotalHeightRatio();
								Double rhoUpper_Mid_Tail = value;
								Double rhoLower_Mid_Tail = _theFuselage.get_sectionMidTailRhoLower();
								Double x_Mid_Tail = _theFuselage.get_sectionsYZStations().get(idx_Mid_Tail)
										.doubleValue(SI.METRE);
								 Double hf_Mid_Tail = _theFuselage.get_sectionsYZ().get(idx_Mid_Tail).get_h_f();
								 Double zlf_Mid_Tail = _theFuselage.getZOutlineXZLowerAtX(x_Mid_Tail);
								 Double dZ_Mid_Tail = zlf_Mid_Tail + 0.5*hf_Mid_Tail; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx_Mid_Tail,  // section IDX
										a_Mid_Tail, rhoUpper_Mid_Tail, rhoLower_Mid_Tail
										);

								_theFuselage.get_sectionsYZ().get(idx_Mid_Tail).translateZ(dZ_Mid_Tail);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx_Mid_Tail);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
								break;	


							case CYLINDRICAL_BODY_1:
								_theFuselage.adjustCylinderSectionRhoUpper(value);			
								int [] idx = new int[2]; 
								idx[0]= _theInitiatorPaneFuselage.get_selectedSectionIdx();
								idx[1]= _theInitiatorPaneFuselage.get_selectedSectionIdx()+1;
								Double a = _theFuselage.get_sectionLowerToTotalHeightRatio();
								Double rhoUpper = value;
								Double rhoLower = _theFuselage.get_sectionCylinderRhoLower();	
								for (int i=0;i<idx.length;i++){
									Double x = _theFuselage.get_sectionsYZStations().get(idx[i])
											.doubleValue(SI.METRE);
									Double hf = _theFuselage.get_sectionsYZ().get(idx[i]).get_h_f();
									Double zlf = _theFuselage.getZOutlineXZLowerAtX(x);
									Double dZ = zlf + 0.5*hf; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx[i],  // section IDX
										a, rhoUpper, rhoLower
										);

								_theFuselage.get_sectionsYZ().get(idx[i]).translateZ(dZ);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx[i]);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
								}

								break;
								 default:
									 break;
							}
						
							//_theFuselage.adjustCylinderSectionRhoUpper(value);							
							//int idx = _theInitiatorPaneFuselage.get_selectedSectionIdx();
//							Double a = _theFuselage.get_sectionLowerToTotalHeightRatio();
//							Double rhoUpper = value;
//							Double rhoLower = _theFuselage.get_sectionRhoLower();
//							Double x = _theFuselage.get_sectionsYZStations().get(idx)
//											.doubleValue(SI.METRE);
//							Double hf = _theFuselage.get_sectionsYZ().get(idx).get_Height();
//							Double zlf = _theFuselage.getZOutlineXZLowerAtX(x);
//							Double dZ = zlf + 0.5*hf; 
							
//							_theFuselage
//								.adjustSectionShapeParameters(
//									idx,  // section IDX
//									a, rhoUpper, rhoLower
//									);
//
//							_theFuselage.get_sectionsYZ().get(idx).translateZ(dZ);
//							_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx);
//							_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
							
							
						}
					}
					catch( Exception ex ){
						e.doit=false;
					}
				}
			}
		});     // end of UpperSectionControlPoints listener

		_text_LowerSectionControlPoints.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_01.getText();

				s_value = s_value.trim();

				Double value = Double.valueOf(s_value);
				if (e.detail == SWT.TRAVERSE_RETURN) {
					try{

						if ( 
								(value < _theFuselage.get_sectionCylinderRhoLower_MIN()) 
								|| 
								(value > _theFuselage.get_sectionCylinderRhoLower_MAX()) 
								) 
						{  
							e.doit = false;
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: bad rho_lower. Insert new value!\n");
						}
						
						else{

							switch( _theInitiatorPaneFuselage.get_selectedSectionData())
							{

							
							
							case MID_NOSE:
								
								_theFuselage.adjustMidNoseSectionRhoLower(value);
								int idx_Mid_Nose = _theInitiatorPaneFuselage.get_selectedSectionIdx();
								Double a_Mid_Nose = _theFuselage.get_sectionNoseMidLowerToTotalHeightRatio();
								Double rhoUpper_Mid_Nose  = _theFuselage.get_sectionMidNoseRhoUpper();
								Double rhoLower_Mid_Nose  = value;
								Double x_Mid_Nose = _theFuselage.get_sectionsYZStations().get(idx_Mid_Nose)
										.doubleValue(SI.METRE);
								Double hf_Mid_Nose = _theFuselage.get_sectionsYZ().get(idx_Mid_Nose).get_h_f();
								Double zlf_Mid_Nose = _theFuselage.getZOutlineXZLowerAtX(x_Mid_Nose);
								Double dZ_Mid_Nose = zlf_Mid_Nose + 0.5*hf_Mid_Nose; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx_Mid_Nose,  // section IDX
										a_Mid_Nose, rhoUpper_Mid_Nose , rhoLower_Mid_Nose
										);

								_theFuselage.get_sectionsYZ().get(idx_Mid_Nose).translateZ(dZ_Mid_Nose);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx_Mid_Nose);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();

								break;

							case MID_TAIL:
								
								_theFuselage.adjustMidTailSectionRhoLower(value);
								int idx_Mid_Tail = _theInitiatorPaneFuselage.get_selectedSectionIdx();
								Double a_Mid_Tail = _theFuselage.get_sectionTailMidLowerToTotalHeightRatio();
								Double rhoUpper_Mid_Tail = _theFuselage.get_sectionMidTailRhoUpper();;
								Double rhoLower_Mid_Tail = value;
								Double x_Mid_Tail = _theFuselage.get_sectionsYZStations().get(idx_Mid_Tail)
										.doubleValue(SI.METRE);
								 Double hf_Mid_Tail = _theFuselage.get_sectionsYZ().get(idx_Mid_Tail).get_h_f();
								 Double zlf_Mid_Tail = _theFuselage.getZOutlineXZLowerAtX(x_Mid_Tail);
								 Double dZ_Mid_Tail = zlf_Mid_Tail + 0.5*hf_Mid_Tail; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx_Mid_Tail,  // section IDX
										a_Mid_Tail, rhoUpper_Mid_Tail, rhoLower_Mid_Tail
										);

								_theFuselage.get_sectionsYZ().get(idx_Mid_Tail).translateZ(dZ_Mid_Tail);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx_Mid_Tail);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
								break;	


							case CYLINDRICAL_BODY_1:
								
								_theFuselage.adjustCylinderSectionRhoLower(value);			
								int [] idx = new int[2]; 
								idx[0]= _theInitiatorPaneFuselage.get_selectedSectionIdx();
								idx[1]= _theInitiatorPaneFuselage.get_selectedSectionIdx()+1;
								Double a = _theFuselage.get_sectionLowerToTotalHeightRatio();
								Double rhoUpper = _theFuselage.get_sectionCylinderRhoUpper();
								Double rhoLower = value;	
								for (int i=0;i<idx.length;i++){
									Double x = _theFuselage.get_sectionsYZStations().get(idx[i])
											.doubleValue(SI.METRE);
									Double hf = _theFuselage.get_sectionsYZ().get(idx[i]).get_h_f();
									Double zlf = _theFuselage.getZOutlineXZLowerAtX(x);
									Double dZ = zlf + 0.5*hf; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx[i],  // section IDX
										a, rhoUpper, rhoLower
										);

								_theFuselage.get_sectionsYZ().get(idx[i]).translateZ(dZ);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx[i]);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
								}

								break;
								
								default:
									break;
							}
							
//						_theFuselage.adjustCylinderSectionRhoLower(value);
//						int idx = _theInitiatorPaneFuselage.get_selectedSectionIdx();
//						Double a = _theFuselage.get_sectionLowerToTotalHeightRatio();
//						Double rhoUpper =_theFuselage.get_sectionRhoUpper();
//						Double rhoLower = value;
//						System.out.println(rhoUpper);
//						Double x = _theFuselage.get_sectionsYZStations().get(idx)
//										.doubleValue(SI.METRE);
//						Double hf = _theFuselage.get_sectionsYZ().get(idx).get_Height();
//						Double zlf = _theFuselage.getZOutlineXZLowerAtX(x);
//						Double dZ = zlf + 0.5*hf; 
//						
//						_theFuselage
//							.adjustSectionShapeParameters(
//								idx,  // section IDX
//								a, rhoUpper, rhoLower
//								);
//
//						_theFuselage.get_sectionsYZ().get(idx).translateZ(dZ);
//						_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx);
//						_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
						}
					}
					catch( Exception ex ){
						e.doit=false;
					}
				}
			}
		}); // end of LowerSectionControlPoints listener

		// Fuselage lower section point

		_text_LowerSectionAPoint.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_01.getText();

				s_value = s_value.trim();

				Double value = Double.valueOf(s_value);

				if (e.detail == SWT.TRAVERSE_RETURN) {
					try{
						if ( 
								(value < _theFuselage.get_sectionLowerToTotalHeightRatio_MIN()) 
								|| 
								(value > _theFuselage.get_sectionLowerToTotalHeightRatio_MAX())
								)
						{  
							e.doit = false;
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: bad a. Insert new value!\n");
						}
						else{

							switch( _theInitiatorPaneFuselage.get_selectedSectionData())
							
							{
							
							case MID_NOSE:
								
								_theFuselage.adjustNoseMidSectionLowerToTotalHeightRatio(value);
								int idx_Mid_Nose = _theInitiatorPaneFuselage.get_selectedSectionIdx();
								Double a_Mid_Nose =value;
								Double rhoUpper_Mid_Nose  = _theFuselage.get_sectionMidNoseRhoUpper();
								Double rhoLower_Mid_Nose  = _theFuselage.get_sectionMidNoseRhoLower();
								Double x_Mid_Nose = _theFuselage.get_sectionsYZStations().get(idx_Mid_Nose)
										.doubleValue(SI.METRE);
								Double hf_Mid_Nose = _theFuselage.get_sectionsYZ().get(idx_Mid_Nose).get_h_f();
								Double zlf_Mid_Nose = _theFuselage.getZOutlineXZLowerAtX(x_Mid_Nose);
								Double dZ_Mid_Nose = zlf_Mid_Nose + 0.5*hf_Mid_Nose; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx_Mid_Nose,  // section IDX
										a_Mid_Nose, rhoUpper_Mid_Nose , rhoLower_Mid_Nose
										);

								_theFuselage.get_sectionsYZ().get(idx_Mid_Nose).translateZ(dZ_Mid_Nose);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx_Mid_Nose);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();

								break;

							case MID_TAIL:
								
								_theFuselage.adjustTailMidSectionLowerToTotalHeightRatio(value);
								int idx_Mid_Tail = _theInitiatorPaneFuselage.get_selectedSectionIdx();
								Double a_Mid_Tail = value;
								Double rhoUpper_Mid_Tail = _theFuselage.get_sectionMidTailRhoUpper();
								Double rhoLower_Mid_Tail =  _theFuselage.get_sectionMidTailRhoLower();;
								Double x_Mid_Tail = _theFuselage.get_sectionsYZStations().get(idx_Mid_Tail)
										.doubleValue(SI.METRE);
								 Double hf_Mid_Tail = _theFuselage.get_sectionsYZ().get(idx_Mid_Tail).get_h_f();
								 Double zlf_Mid_Tail = _theFuselage.getZOutlineXZLowerAtX(x_Mid_Tail);
								 Double dZ_Mid_Tail = zlf_Mid_Tail + 0.5*hf_Mid_Tail; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx_Mid_Tail,  // section IDX
										a_Mid_Tail, rhoUpper_Mid_Tail, rhoLower_Mid_Tail
										);

								_theFuselage.get_sectionsYZ().get(idx_Mid_Tail).translateZ(dZ_Mid_Tail);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx_Mid_Tail);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
								break;	


							case CYLINDRICAL_BODY_1:
								
								_theFuselage.adjustCylinderSectionLowerToTotalHeightRatio(value);	
								int [] idx = new int[2]; 
								idx[0]= _theInitiatorPaneFuselage.get_selectedSectionIdx();
								idx[1]= _theInitiatorPaneFuselage.get_selectedSectionIdx()+1;
								Double a = value;
								Double rhoUpper =  _theFuselage.get_sectionCylinderRhoUpper();
								Double rhoLower =  _theFuselage.get_sectionCylinderRhoLower();
								for (int i=0;i<idx.length;i++){
									Double x = _theFuselage.get_sectionsYZStations().get(idx[i])
											.doubleValue(SI.METRE);
									Double hf = _theFuselage.get_sectionsYZ().get(idx[i]).get_h_f();
									Double zlf = _theFuselage.getZOutlineXZLowerAtX(x);
									Double dZ = zlf + 0.5*hf; 
								_theFuselage
								.adjustSectionShapeParameters(
										idx[i],  // section IDX
										a, rhoUpper, rhoLower
										);

								_theFuselage.get_sectionsYZ().get(idx[i]).translateZ(dZ);
								_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx[i]);
								_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
								}
								break;
								
								default:
									break;
							}
							
//							_theFuselage.adjustCylinderSectionLowerToTotalHeightRatio(value);
//							int idx = _theInitiatorPaneFuselage.get_selectedSectionIdx();
//							Double a = value;
//							Double rhoUpper =_theFuselage.get_sectionRhoUpper();
//							Double rhoLower =_theFuselage.get_sectionRhoLower();
//							Double x = _theFuselage.get_sectionsYZStations().get(idx)
//											.doubleValue(SI.METRE);
//							Double hf = _theFuselage.get_sectionsYZ().get(idx).get_Height();
//							Double zlf = _theFuselage.getZOutlineXZLowerAtX(x);
//							Double dZ = zlf + 0.5*hf; 
//							
//							_theFuselage
//								.adjustSectionShapeParameters(
//									idx,  // section IDX
//									a, rhoUpper, rhoLower
//									);
//
//							_theFuselage.get_sectionsYZ().get(idx).translateZ(dZ);
//							_theFuselage.calculateOutlinesUpperLowerSectionYZ(idx);
//							_theInitiatorPaneFuselage.redrawFuselageSectionYZ();
						}
					}
					catch( Exception ex ){
						e.doit=false;
					}
				}
			}
		}); // end of LowerSectionaPoint listener
		
		
		// DIALOG lyfe-cycle
		dialog.open();
		// Set up the event loop.
		while (dialog.isDisposed()) {
			if (!display.readAndDispatch()) {
				// If no more entries in event queue
				display.sleep();
			}
		}

	} // end of constructor



	private void ShowSectionParameters (){

		_text_SectionX.setEditable(false);
		_text_SectionWidth.setEditable(true);
		_text_SectionHeight.setEditable(true);
		_text_UpperSectionControlPoints.setEditable(true);
		_text_LowerSectionControlPoints.setEditable(true);
		_text_LowerSectionAPoint.setEditable(true);


		switch (_theInitiatorPaneFuselage.get_selectedSectionData()){

		case NOSE_TIP: 

			_text_SectionWidth.setEditable(false);
			_text_SectionHeight.setEditable(false);
			_text_UpperSectionControlPoints.setEditable(false);
			_text_LowerSectionControlPoints.setEditable(false);
			_text_LowerSectionAPoint.setEditable(false);
			break;

		case NOSE_CAP:

			_text_SectionWidth.setEditable(false);
			_text_SectionHeight.setEditable(false);
			_text_UpperSectionControlPoints.setEditable(false);
			_text_LowerSectionControlPoints.setEditable(false);
			_text_LowerSectionAPoint.setEditable(false);
			break;

		case MID_NOSE:

			_text_SectionWidth.setEditable(false);
			_text_SectionHeight.setEditable(false);
			break;

		case CYLINDRICAL_BODY_1:

			_text_SectionWidth.setEditable(true);
			_text_SectionHeight.setEditable(false);
			_text_UpperSectionControlPoints.setEditable(true);
			_text_LowerSectionControlPoints.setEditable(true);
			_text_LowerSectionAPoint.setEditable(true);
			break;

		case CYLINDRICAL_BODY_2:

			_text_SectionWidth.setEditable(true);
			_text_SectionHeight.setEditable(false);
			_text_UpperSectionControlPoints.setEditable(true);
			_text_LowerSectionControlPoints.setEditable(true);
			_text_LowerSectionAPoint.setEditable(true);
			break;

		case MID_TAIL:	

			_text_SectionWidth.setEditable(false);
			_text_SectionHeight.setEditable(false);
			break;

		case TAIL_CAP:

			_text_SectionWidth.setEditable(false);
			_text_SectionHeight.setEditable(false);
			_text_UpperSectionControlPoints.setEditable(false);
			_text_LowerSectionControlPoints.setEditable(false);
			_text_LowerSectionAPoint.setEditable(false);
			break;

		case TAIL_TIP:

			_text_SectionWidth.setEditable(false);
			_text_SectionHeight.setEditable(false);
			_text_UpperSectionControlPoints.setEditable(false);
			_text_LowerSectionControlPoints.setEditable(false);
			_text_LowerSectionAPoint.setEditable(false);
			break;

		default:
			// do nothing
			break;
		}		

	}// end  ShowSectionParameters


	public Text get_text_SectionWidth() {
		return _text_SectionWidth;
	}



	public void set_text_SectionWidth(Text _text_SectionWidth) {
		this._text_SectionWidth = _text_SectionWidth;
	}



	public Text get_text_UpperSectionControlPoints() {
		return _text_UpperSectionControlPoints;
	}



	public void set_text_UpperSectionControlPoints(
			Text _text_UpperSectionControlPoints) {
		this._text_UpperSectionControlPoints = _text_UpperSectionControlPoints;
	}



	public Text get_text_LowerSectionControlPoints() {
		return _text_LowerSectionControlPoints;
	}



	public void set_text_LowerSectionControlPoints(
			Text _text_LowerSectionControlPoints) {
		this._text_LowerSectionControlPoints = _text_LowerSectionControlPoints;
	}



	public Text get_text_LowerSectionaPoint() {
		return _text_LowerSectionAPoint;
	}



	public void set_text_LowerSectionaPoint(Text _text_LowerSectionaPoint) {
		this._text_LowerSectionAPoint = _text_LowerSectionaPoint;
	}

}  // end of class 
