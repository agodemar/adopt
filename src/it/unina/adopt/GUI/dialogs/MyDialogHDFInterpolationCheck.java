package it.unina.adopt.GUI.dialogs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import it.unina.adopt.utilities.gui.MyGuiUtils;
import it.unina.adopt.utilities.gui.MyGuiUtils.PlotFactory;
import ncsa.hdf.hdf5lib.exceptions.HDF5LibraryException;
import standaloneutils.database.hdf.MyHDFReader;

public class MyDialogHDFInterpolationCheck {

	private MyHDFReader _hdfReader;
	String _groupFullName;
	String _datasetFullName;
	private Double _var0, _var1, _var2;
	private Double _fvalue;
	private int _rank;
	
	private CTabFolder _tabFolderPlots = null;
	private PlotFactory _plotFactory;
	
	public MyDialogHDFInterpolationCheck(
			Display display, 
			MyHDFReader hdfReader,
			String groupFullName,
			int rank,
			List<Double> varsValues, 
			List<Double> fValues
			) {
		
		_hdfReader = hdfReader;
		_groupFullName = groupFullName;
		_datasetFullName = _groupFullName + "/data";
		_rank = rank;

		System.out.println(
				"MyDialogHDFInterpolationCheck :: \n"
				+ "\tFile: " + _hdfReader.getDatabaseAbsolutePath() + "\n"
				+ "\tGroup: " + _groupFullName + "\n"
				+ "\tRank: " + _rank + "\n"
				+ "\tVars: " + varsValues + "\n"
				+ "\tF value: " + fValues + "\n"
				);
		
		
		final Shell dialog = new Shell(
				display,
				SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL
				| SWT.RESIZE
				);
		dialog.setText("HDF Interpolation Check");

		// force size only if you really need it
		
//		Monitor monitor = Display.getDefault().getMonitors()[0];
//		dialog.setSize(
//				(int)( 0.55*monitor.getBounds().width  ),
//				(int)( 0.55*monitor.getBounds().height )
//			);

		// Fill the main dialog appropriately
		dialog.setLayout(new FillLayout());

		// Create the ScrolledComposite to scroll horizontally and vertically
		ScrolledComposite scrolledComposite = 
				new ScrolledComposite(
						dialog,
						SWT.BORDER |SWT.H_SCROLL | SWT.V_SCROLL
				);
		
		// necessary settings
		scrolledComposite.setMinHeight(700);
		scrolledComposite.setMinWidth(400);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
	
		// Create a child composite to hold the controls
	    Composite container = new Composite(scrolledComposite, SWT.NONE);

		// Fill the container (dialog's child) appropriately
		container.setLayout(new FillLayout(SWT.VERTICAL));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		//-------------------------------------
	    // Populate dialog
		//-------------------------------------
		
		// the tab folder
		_tabFolderPlots = new CTabFolder(container, SWT.NONE);
	    
		// get arrays var_0 [, var_1, var_2 ] from HDF file
		
		List<Double[]> varArrays = new ArrayList<Double[]>();
		try {
			
			varArrays.addAll(_hdfReader.getVarsByGroupName(groupFullName));
		
		} catch (HDF5LibraryException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} 

//		System.out.println(
//				"MyDialogHDFInterpolationCheck :: \n"
//				+ "\tvarArrays size: " + varArrays.size()
//				);

		// sanity check then create tab items as appropriate
		
		if (varArrays.size() == rank) {
			
			List<Double[]> xList = new ArrayList<Double[]>();
			List<Double[]> yList = new ArrayList<Double[]>();
			List<String> legendList = new ArrayList<String>();
			List<String> tabItemTitleList = new ArrayList<String>();
			List<String> titleList = new ArrayList<String>();

//			xList.add(varArrays);
			
			// build lists as appropriate
			if (rank == 1) {

				xList.add(
						varArrays.get(0)
						);
				try {
					yList.add(
							ArrayUtils.toObject(
									_hdfReader.getDataset1DFloatByName(_datasetFullName)
									)							
							);
				} catch (HDF5LibraryException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

				// interpolation trace
				xList.add(
						new Double[] { varsValues.get(0), varsValues.get(0), 0.0}
						);
				yList.add(
						new Double[] { 0.0, fValues.get(0), fValues.get(0)}
						);

				_plotFactory = new MyGuiUtils.PlotFactory();
				_plotFactory.newTabPlot(xList, yList,
						"Loading", // trace title
						"AHhh", // graph title (also tab heading text, TODO: make separate)
						"x", "y", // axis labels
						"m", "kg",  // units to be displayed
						_tabFolderPlots);
				
			} else if (rank == 2) {

				String var0_2DFullName = _groupFullName + "/var_0";
				String var1_2DFullName = _groupFullName + "/var_1";
				double[] var0_2D = null;
				double[] var1_2D = null;
				try {
					var0_2D = _hdfReader.getDataset1DFloatByName(var0_2DFullName);
					var1_2D = _hdfReader.getDataset1DFloatByName(var1_2DFullName);
				} catch (HDF5LibraryException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

				double[][] dset2D = null;
				try {
					dset2D = _hdfReader.getDataset2DFloatByName(_datasetFullName);
				} catch (HDF5LibraryException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				// convert into a "matrix"
				RealMatrix dataMatrix2D = new Array2DRowRealMatrix(dset2D);
				
				// loop on columns
				for (int kCol = 0; kCol < var0_2D.length; kCol++ ) {
					// var_1 on x-axis
					xList.add(
							ArrayUtils.toObject(
									var1_2D
									)
							);
					// var_0 values associated to /data columns
					yList.add( 
							ArrayUtils.toObject(
									dataMatrix2D.getColumn(kCol)
									)
							);
					
					legendList.add(
							"v0 = " + var0_2D[kCol]
							);
					
				}
				//-------------------------------------------
				// interpolation trace
				xList.add(
						new Double[] { varsValues.get(1), varsValues.get(1), 0.0}
						);
				yList.add(
						new Double[] { 0.0, fValues.get(1), fValues.get(1)}
						);
				
				legendList.add(
						fValues.get(1) 
						+ " = f(" + varsValues.get(0) + ", " + varsValues.get(1) + ")"
						);
				//-------------------------------------------
				

				tabItemTitleList.add(
						"v0 - v1"
						);

				titleList.add(
						"Interpolation: "
						+ "v0 = " + varsValues.get(0) + "   ,   "
						+ "v1 = " + varsValues.get(1) + "   ,   "
						+ "f(v0, v1) = " + 
							String.valueOf(BigDecimal.valueOf((
									fValues.get(1)
							)).setScale(4, RoundingMode.HALF_UP))
							// TODO   .toEngineeringString()  //
						);				
				
				_plotFactory = new MyGuiUtils.PlotFactory();
				_plotFactory.newTabPlot(
						xList, 
						yList,
						legendList,
						tabItemTitleList.get(0),
						titleList.get(0),
						"x", "y", // axis labels
						"tbd", "tbd",  // units to be displayed
						_tabFolderPlots);
				
			} else if (rank == 3) {

			}

//				String annotation =
//						"File: " + _hdfReader.getFileName() + " --- "
//								+ "Group: " + _groupFullName + "\n"
//								+ "Rank: " + _rank + " --- "
//								+ "Vars: " + varsValues + " --- "
//								+ "F value: " + fValues;



			// force first tab selection to get canvas painted
			_tabFolderPlots.setSelection(0);
			_tabFolderPlots.forceFocus();

		}
		
	    // TODO: add event listeners  
	    
		
		// layout finishing stuff
		scrolledComposite.setContent(container);

//		container.pack();
//		scrolledComposite.pack();
		
		// DIALOG lyfe-cycle
		dialog.open();
		// Set up the event loop.
		while (dialog.isDisposed()) {
			if (!display.readAndDispatch()) {
				// If no more entries in event queue
				display.sleep();
			}
		}

	    
	}

}
