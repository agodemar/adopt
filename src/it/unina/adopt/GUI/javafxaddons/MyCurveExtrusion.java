package it.unina.adopt.GUI.javafxaddons;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;

public class MyCurveExtrusion extends Group {

	private MeshView _meshView;

	private TriangleMesh _theTriangleMesh = new TriangleMesh();
	private List<Point3D> _points3D = new ArrayList<Point3D>(); 
	private Point3D _p1, _p2;
	private boolean _doubleFace = false;

	public MyCurveExtrusion(
			List<Point3D> pts, 
			Point3D p1, Point3D p2, 
			boolean doubleFace) {
		_points3D.addAll(pts);
		_p1 = p1;
		_p2 = p2;
		_doubleFace = doubleFace;

		if (pts.size() >= 2) {
			Point3D q0 = _points3D.get(0);
			Point3D q1 = _points3D.get(2);
			Point3D dq = _p2.add(_p1.multiply(-1.0)); // p2 - p1
			Point3D q2 = q1.add(dq);
			Point3D q3 = q0.add(dq);
			MySimpleQuad qdA = new MySimpleQuad(q0,q1,q2,q3,_doubleFace);
			_theTriangleMesh = qdA.getTriangleMesh();
			for (int k = 1; k < _points3D.size() - 1; k++) {
				q0 = _points3D.get(k);
				q1 = _points3D.get(k+1);
				dq = _p2.add(_p1.multiply(-1.0)); // p2 - p1
				q2 = q1.add(dq);
				q3 = q0.add(dq);
				MySimpleQuad qdB = new MySimpleQuad(q0,q1,q2,q3,_doubleFace);
				TriangleMesh tmB = MyMeshUtils.MergeTriangleMeshes(
						_theTriangleMesh, qdB.getTriangleMesh());
				_theTriangleMesh = tmB;
			}
			
			_meshView = new MeshView(_theTriangleMesh);
			_meshView.setDrawMode(DrawMode.FILL);
			getChildren().add(_meshView);
		}
	}

	public MeshView getMeshView() {
		return _meshView;
	}
}
