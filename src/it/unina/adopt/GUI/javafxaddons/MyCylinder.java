package it.unina.adopt.GUI.javafxaddons;

import javafx.scene.shape.Cylinder;

public class MyCylinder extends Cylinder {
	
	private Cylinder _theCylinder;
	private MyVector3D _v1, _v2;
	private MyVector3D _theAxisVersor;
	private Double _height;
	private Double _radius;
	private Double _angle;
	
	public MyCylinder(MyPoint3D p1, MyPoint3D p2, double radius) {
		
		_radius = radius;
		
		_v1 = new MyVector3D(p1.x, p1.y, p1.z);
		_v2 = new MyVector3D(p2.x, p2.y, p2.z);

		_height = ( _v2.add( _v1.multiply(-1.0) ) ).magnitude(); 
		
		_theAxisVersor = new MyVector3D(
				p2.x - p1.x,  
				p2.y - p1.y, 
				p2.z - p1.z
				);
		
		MyVector3D unitZ = new MyVector3D(0,0,1);
		_angle = unitZ.angle( _v2.add( _v1.multiply(-1.0) ) );
		
		_theCylinder = new Cylinder(_radius, _height);
		_theCylinder.rotateProperty().setValue(_angle);

		// _theCylinder.rotationAxisProperty().set(new Point3D(p2.x -p1.x, p2.y - p1.y, p2.z - p1.z));
		
		_theCylinder.translateXProperty().set(p1.x);
		_theCylinder.translateYProperty().set(p1.y);
		_theCylinder.translateZProperty().set(p1.z);
		
	}

}
