package it.unina.adopt.GUI.javafxaddons;

import javafx.collections.ObservableFloatArray;
import javafx.scene.shape.ObservableFaceArray;
import javafx.scene.shape.TriangleMesh;

public class MyMeshUtils {

	public static TriangleMesh MergeTriangleMeshes(TriangleMesh tm1, TriangleMesh tm2) {
		
		TriangleMesh m = new TriangleMesh();
		
		if ((tm1.getFaceElementSize() == 0) && (tm2.getFaceElementSize() == 0))
			return m;
		
		if (tm1.getFaceElementSize() == 0)
			return tm2;
		
		if (tm2.getFaceElementSize() == 0)
			return tm1;
		
		ObservableFloatArray ofaPoints =  tm1.getPoints();
		ofaPoints.addAll(tm2.getPoints());		
		m.getPoints().addAll(ofaPoints);

		ObservableFloatArray ofaTexCoords =  tm1.getTexCoords();
		ofaTexCoords.addAll(tm2.getTexCoords());
		m.getTexCoords().addAll(ofaTexCoords);

		ObservableFaceArray ofcaFaces =  tm1.getFaces();
		ObservableFaceArray ofcaFaces2 =  tm2.getFaces();
		// increase face indices of 2nd triangle mesh before merging
		for (int k = 0; k < ofcaFaces2.size(); k++) {
			if ( (k % 2) == 0 ) {
				ofcaFaces2.set(
						k, ofcaFaces2.get(k) + ofcaFaces.size()/2
						);  
			}
		}
		ofcaFaces.addAll(ofcaFaces2);
		m.getFaces().addAll(ofcaFaces);

		return m;

	}

}
