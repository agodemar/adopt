package it.unina.adopt.GUI.javafxaddons;

import javafx.collections.ObservableFloatArray;
import javafx.geometry.Point3D;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.ObservableFaceArray;
import javafx.scene.shape.TriangleMesh;

public class MySimpleQuad extends MeshView {

	private TriangleMesh _theTriangleMesh = new TriangleMesh();
	private Point3D _p0, _p1, _p2, _p3; 
	private boolean _doubleFace = false;

	public MySimpleQuad() {
		_p0 = new Point3D(0,0,0);
		_p1 = new Point3D(1,0,0);
		_p2 = new Point3D(1,1,0);
		_p3 = new Point3D(0,1,0);
		buildMesh();
		this.setDrawMode(DrawMode.FILL);
	}

	public MySimpleQuad(Point3D p0, Point3D p1, Point3D p2, Point3D p3) {
		_p0 = p0;
		_p1 = p1;
		_p2 = p2;
		_p3 = p3;
		buildMesh();
		this.setDrawMode(DrawMode.FILL);
	}

	public MySimpleQuad(Point3D p0, Point3D p1, Point3D p2, Point3D p3, boolean doubleFace) {
		_doubleFace = doubleFace;
		_p0 = p0;
		_p1 = p1;
		_p2 = p2;
		_p3 = p3;
		buildMesh();
		this.setDrawMode(DrawMode.FILL);
	}
	
	public void buildMesh() {
		
		MySimpleTriangle tr1 = new MySimpleTriangle(_p0, _p1, _p2, _doubleFace);
		MySimpleTriangle tr2 = new MySimpleTriangle(_p0, _p2, _p3, _doubleFace);
		
		ObservableFloatArray ofaPoints =  tr1.getTriangleMesh().getPoints();
		ofaPoints.addAll(tr2.getTriangleMesh().getPoints());		
		_theTriangleMesh.getPoints().addAll(ofaPoints);

		ObservableFloatArray ofaTexCoords =  tr1.getTriangleMesh().getTexCoords();
		ofaTexCoords.addAll(tr2.getTriangleMesh().getTexCoords());
		_theTriangleMesh.getTexCoords().addAll(ofaTexCoords);

		ObservableFaceArray ofcaFaces =  tr1.getTriangleMesh().getFaces();
		ObservableFaceArray ofcaFaces2 =  tr2.getTriangleMesh().getFaces();
		// increase face indices of 2nd triangle before merging
		for (int k = 0; k < ofcaFaces2.size(); k++) {
			if ( (k % 2) == 0 ) {
				ofcaFaces2.set(
						k, ofcaFaces2.get(k) + ofcaFaces.size()/2
						);  
			}
		}
		// ofcaFaces.addAll(tr2.getTriangleMesh().getFaces());
		ofcaFaces.addAll(ofcaFaces2);
		_theTriangleMesh.getFaces().addAll(ofcaFaces);
		
		this.setMesh(_theTriangleMesh);
		
	}
	
	public TriangleMesh getTriangleMesh() {
		return _theTriangleMesh;
	}

}
