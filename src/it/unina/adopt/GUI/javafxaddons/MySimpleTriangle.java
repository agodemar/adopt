package it.unina.adopt.GUI.javafxaddons;

import java.util.Arrays;
import java.util.List;

import javafx.geometry.Point3D;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;

public class MySimpleTriangle extends MeshView {

	private TriangleMesh _theTriangleMesh = new TriangleMesh();
	private Point3D _p0, _p1, _p2;
	private boolean _doubleFace = false;

	public MySimpleTriangle() {
		_p0 = new Point3D(0,0,0);
		_p1 = new Point3D(1,0,0);
		_p2 = new Point3D(0,1,0);
		buildMesh();
		this.setDrawMode(DrawMode.FILL);
	}

	public MySimpleTriangle(Point3D p0, Point3D p1, Point3D p2) {
		_p0 = p0;
		_p1 = p1;
		_p2 = p2;
		buildMesh();
		this.setDrawMode(DrawMode.FILL);
	}

	public MySimpleTriangle(Point3D p0, Point3D p1, Point3D p2, boolean doubleFace) {
		_doubleFace = doubleFace;
		_p0 = p0;
		_p1 = p1;
		_p2 = p2;
		buildMesh();
		this.setDrawMode(DrawMode.FILL);
	}

	
	public TriangleMesh getTriangleMesh() {
		return _theTriangleMesh;
	}

	public void buildMesh() {
		
		_theTriangleMesh.getPoints().addAll(
				(float)_p0.getX(), (float)_p0.getY(), (float)_p0.getZ(),
				(float)_p1.getX(), (float)_p1.getY(), (float)_p1.getZ(),
				(float)_p2.getX(), (float)_p2.getY(), (float)_p2.getZ()
				);
		
		_theTriangleMesh.getTexCoords().addAll(
				0.0f, 0.0f,
				0.0f, 1.0f,
				1.0f, 0.0f
				);
		
		_theTriangleMesh.getFaces().addAll(
				0,0, 1,0, 2,0
				);

		if (_doubleFace) {
			_theTriangleMesh.getPoints().addAll(
					(float)_p0.getX(), (float)_p0.getY(), (float)_p0.getZ(),
					(float)_p2.getX(), (float)_p2.getY(), (float)_p2.getZ(),
					(float)_p1.getX(), (float)_p1.getY(), (float)_p1.getZ()
					);			
			_theTriangleMesh.getTexCoords().addAll(
					0.0f, 0.0f,
					0.0f, 1.0f,
					1.0f, 0.0f
					);
			_theTriangleMesh.getFaces().addAll(
					3,0, 4,0, 5,0
					);
		}
		
		this.setMesh(_theTriangleMesh);
		
	}

	public List<Point3D> getVertices() {
		return (List<Point3D>) Arrays.asList(_p0, _p1, _p2);
	}

	public boolean is_DoubleFace() {
		return _doubleFace;
	}

	public void set_DoubleFace(boolean doubleFace) {
		this._doubleFace = doubleFace;
	}

}
