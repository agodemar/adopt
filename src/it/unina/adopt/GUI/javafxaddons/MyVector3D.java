package it.unina.adopt.GUI.javafxaddons;

/**
 *
 * @author Agodemar
 */
public final class MyVector3D {

	public static final MyVector3D// Homogenious      public static final MyVector3D// Homogenious      
	FORWARD = new MyVector3D(0, 0, 1),
	BACK = new MyVector3D(0, 0, -1),
	LEFT = new MyVector3D(-1, 0, 0),
	RIGHT = new MyVector3D(1, 0, 0),
	UP = new MyVector3D(0, 1, 0),
	DOWN = new MyVector3D(0, -1, 0),
	ONE = new MyVector3D(1, 1, 1),
	ZERO = new MyVector3D(0, 0, 0),
	NAN = new MyVector3D(Double.NaN, Double.NaN, Double.NaN);

	public double x;
	public double y;
	public double z;

	public MyVector3D() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}

	public MyVector3D(MyVector3D source) {
		this.x = source.x;
		this.y = source.y;
		this.z = source.z;
	}

	public MyVector3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public MyVector3D(double... values) {
		if (values.length != size()) {
			throw new IllegalArgumentException();
		}
		this.x = values[0];
		this.y = values[1];
		this.z = values[2];
	}

	public double angle(MyVector3D v) {
		double mag2 = (x * x) + (y * y) + (z * z);
		double vmag2 = (v.x * v.x) + (v.y * v.y) + (v.z * v.z);
		double dot = (x * v.x) + (y * v.y) + (z * v.z);
		return Math.acos(dot / Math.sqrt(mag2 * vmag2));
	}

	public MyVector3D add(double constant) {
		double nx = x + constant,
				ny = y + constant,
				nz = z + constant;
		return new MyVector3D(nx, ny, nz);
	}

	public MyVector3D add(double dx, double dy, double dz) {
		double nx = x + dx,
				ny = y + dy,
				nz = z + dz;
		return new MyVector3D(nx, ny, nz);
	}

	public MyVector3D add(MyVector3D v) {
		double nx = x + v.x,
				ny = y += v.y,
				nz = z += v.z;
		return new MyVector3D(nx, ny, nz);
	}

	public MyVector3D addMultiply(double dx, double dy, double dz, double factor) {
		double nx = x + dx * factor,
				ny = y + dy * factor,
				nz = z + dz * factor;
		return new MyVector3D(nx, ny, nz);
	}

	public MyVector3D addMultiply(MyVector3D v, double factor) {
		double nx = x + v.x * factor,
				ny = y + v.y * factor,
				nz = z + v.z * factor;
		return new MyVector3D(nx, ny, nz);
	}

	public MyVector3D addProduct(MyVector3D a, MyVector3D b) {
		x += a.x * b.x;
		y += a.y * b.y;
		z += a.z * b.z;
		return new MyVector3D(x, y, z);
	}

	public MyVector3D addProduct(MyVector3D a, MyVector3D b, double factor) {
		x += a.x * b.x * factor;
		y += a.y * b.y * factor;
		z += a.z * b.z * factor;
		return new MyVector3D(x, y, z);
	}

	public double magnitudeSquared() {
		return (x * x) + (y * y) + (z * z);
	}

	public double distanceSquared(MyVector3D v) {
		double dx = x - v.x, dy = y - v.y, dz = z - v.z;
		return (dx * dx) + (dy * dy) + (dz * dz);
	}

	public double distance(MyVector3D v) {
		return Math.sqrt(distanceSquared(v));
	}

	public double inverseMagnitude() {
		return 1.0 / Math.sqrt(magnitudeSquared());
	}

	public double magnitude() {
		return Math.sqrt(magnitudeSquared());
	}

	public void normalize() {
		double d = magnitude();
		if (d == 0.0) {
			x = 0;
			y = 0;
			z = 0;
		}
		x /= d;
		y /= d;
		z /= d;
	}

	public void setAs(MyVector3D a) {
		this.x = a.x;
		this.y = a.y;
		this.z = a.z;
	}

	public MyVector3D multiply(double d) {
		return new MyVector3D(
				x * d,
				y * d,
				z * d);
	}

	public MyVector3D subtractMultiple(MyVector3D v, double factor) {        
		return new MyVector3D(
				x - v.x * factor,
				y - v.y * factor,
				z - v.z * factor
				);
	}

	public MyVector3D sub(MyVector3D v) {
		double nx  = x - v.x,
				ny  = y - v.y,
				nz  = z - v.z;
		return new MyVector3D(nx, ny, nz);
	}

	public MyVector3D subMultiple(MyVector3D v, double factor) {
		return addMultiply(v, -factor);
	}

	public double dotProduct(MyVector3D a) {
		return (x * a.x) + (y * a.y) + (z * a.z);
	}

	public double dotProduct(double[] data, int offset) {
		return x * data[offset + 0] + y * data[offset + 1] + z * data[offset + 2];
	}

	public MyVector3D crossProduct(MyVector3D a) {
		double tx = y * a.z - z * a.y;
		double ty = z * a.x - x * a.z;
		double tz = x * a.y - y * a.x;

		return new MyVector3D(tx, ty, tz);
	}

	public MyVector3D orthogonalTo(MyVector3D v1, MyVector3D v2){        
		return v1.crossProduct(v2);
	}

	public MyVector3D projectToPlane(MyVector3D normal, double distance) {
		double d = dotProduct(normal);
		return addMultiply(normal, distance - d);
	}

	public int size() {
		return 3;
	}

	public double getSum() {
		return x + y + z;
	}

	public double getProduct() {
		return x * y * z;
	}

	public MyVector3D scaleAdd(double factor, double constant) {
		x = (x * factor) + constant;
		y = (y * factor) + constant;
		z = (z * factor) + constant;
		return new MyVector3D(x, y, z);
	}

	public MyVector3D scaleAdd(double factor, MyVector3D constant) {
		x = (x * factor) + constant.x;
		y = (y * factor) + constant.y;
		z = (z * factor) + constant.z;
		return new MyVector3D(x, y, z);
	}

	public void setValues(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public MyVector3D negate() {
		return new MyVector3D(-x, -y, -z);
	}

	public void getElements(double[] data, int offset) {
		data[offset] = x;
		data[offset + 1] = y;
		data[offset + 2] = z;
	}

	public double[] toDoubleArray() {
		return new double[]{x, y, z};
	}

	public MyVector3D toNormal() {
		double d = this.magnitude();
		return (d == 0) ? ZERO : new MyVector3D(x / d, y / d, z / d);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	public boolean equals(MyVector3D v) {
		return (x == v.x) && (y == v.y) && (z == v.z);
	}

	@Override
	public String toString() {
		return "Vector3: {X: " + x + ", Y: " + y + ", Z: " + z + "}";
	}

}
