package it.unina.adopt.GUI.shells;

import java.io.File;

import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

import aircraft.OperatingConditions;
import aircraft.components.Aircraft;
import cad.aircraft.MyAircraftBuilder;
import configuration.MyConfiguration;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.utilities.write.MyDataWriter;
import writers.JPADStaticWriteUtils;

public class MySaveShell extends Shell {

	private StatusLineManager statusLineManager;
	private MyDataWriter _theUtilities;
	private OperatingConditions _theOperatingConditions;
	private Aircraft _theAircraft;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			MySaveShell shell = new MySaveShell(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	public MySaveShell(Display display) {

		super(display, SWT.SHELL_TRIM);
		GridLayout gridLayout = new GridLayout(1, true);
		gridLayout.horizontalSpacing = 10;
		gridLayout.verticalSpacing = 8;
		setLayout(gridLayout);

		Combo combo = new Combo(this, SWT.NONE);
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				// when an item is selected get the string
				String s = combo.getText();

				if (GlobalData.get_theAircraftList().size() != 0
						&& GlobalData.get_theAircraftList() != null) {
					for (Aircraft ac : GlobalData.get_theAircraftList()) {
						if (ac.get_name().equals(s))
							GlobalData.setTheCurrentAircraftInMemory(ac);
					}
				}
			}
		});

		combo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		combo.setText("Select aircraft");
		if (GlobalData.get_theAircraftList().size() != 0
				&& GlobalData.get_theAircraftList() != null) {
			for (Aircraft ac : GlobalData.get_theAircraftList()) {
				combo.add(ac.get_name());
			}
		}

		Group group_1 = new Group(this, SWT.NONE);
		group_1.setLayout(new GridLayout(3, false));
		group_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		CLabel label_1 = new CLabel(group_1, SWT.NONE);
		label_1.setText("");
		label_1.setImage(SWTResourceManager.getImage(MySaveShell.class, "/it/unina/adopt/images/XML_64x64.png"));

		Button btnSaveToXml = new Button(group_1, SWT.NONE);
		btnSaveToXml.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				statusLineManager.setMessage(
						"The aircraft is being exported ..."
						);

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The aircraft is being exported ..."
						);

				_theUtilities = new MyDataWriter(
						_theOperatingConditions,
						GlobalData.getTheCurrentAircraft());

				// Export everything to file
				if (GlobalData.getTheCurrentAircraft() != null) {
					_theUtilities.exportToXMLfile(
							MyConfiguration.currentDirectoryString 
							+ File.separator + GlobalData.getTheCurrentAircraft().get_name() + ".xml");
				}

				if (GlobalData.getTheCurrentAircraft() != null) {
					ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
							"done\n"
							);
					statusLineManager.setMessage(
							"Aircraft exported"
							);
				} else {
					ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
							"\nUnable to export the aircraft.\n"
							);
					statusLineManager.setMessage(
							"Aircraft NOT exported"
							);
				}
			}
		}
				);

		btnSaveToXml.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnSaveToXml.setImage(null);
		btnSaveToXml.setText("Selected aircraft");

		Button btnSaveAllAircrafts = new Button(group_1, SWT.NONE);
		btnSaveAllAircrafts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnSaveAllAircrafts.setText("All aircrafts");

		Group group = new Group(this, SWT.NONE);
		group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		group.setLayout(new GridLayout(3, false));

		CLabel lblNewLabel = new CLabel(group, SWT.NONE);
		lblNewLabel.setImage(SWTResourceManager.getImage(MySaveShell.class, "/it/unina/adopt/images/XLS64x64.png"));
		lblNewLabel.setText("");

		Button btnSaveAircraftTo = new Button(group, SWT.NONE);
		btnSaveAircraftTo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				statusLineManager.setMessage(
						"The aircraft is being saved to xls ...");

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The aircraft is being saved to xls ...");

				_theUtilities = new MyDataWriter(_theOperatingConditions, _theAircraft);

				// Export everything to file
				if (GlobalData.getTheCurrentAircraft() != null) {

					_theUtilities.exportToXLSfile(
							MyConfiguration.currentDirectoryString 
							+ File.separator + _theAircraft.get_name() + ".xls");
				}

				if (GlobalData.getTheCurrentAircraft() != null) {
					ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append("done\n");
					statusLineManager.setMessage("done");

				} else {
					ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
							"\nUnable to export the aircraft.\n");
					statusLineManager.setMessage(
							"Aircraft NOT exported");
				}
			}
		}
				);

		btnSaveAircraftTo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnSaveAircraftTo.setImage(null);
		btnSaveAircraftTo.setText("Selected aircraft");

		Button btnSaveToXls = new Button(group, SWT.NONE);
		btnSaveToXls.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		btnSaveToXls.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnSaveToXls.setText("All aircrafts");

		Group group_3 = new Group(this, SWT.NONE);
		group_3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		group_3.setLayout(new GridLayout(3, false));

		CLabel label = new CLabel(group_3, SWT.NONE);
		label.setImage(SWTResourceManager.getImage(MySaveShell.class, "/it/unina/adopt/images/cad64x64.png"));
		label.setText("");

		Button btnSaveSelectedAircraft = new Button(group_3, SWT.NONE);
		btnSaveSelectedAircraft.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				statusLineManager.setMessage(
						"The aircraft CAD is being exported ...");

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The aircraft CAD is being exported ...");

				MyAircraftBuilder aircraftBuilder = new MyAircraftBuilder();
				aircraftBuilder.buildAndWriteCAD(
						GlobalData.getTheCurrentAircraft(), 
						MyConfiguration.currentDirectory.getAbsolutePath() + File.separator + 
						"cad");

				statusLineManager.setMessage("... done");

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append("... done");
			}
		});
		btnSaveSelectedAircraft.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		btnSaveSelectedAircraft.setImage(null);
		btnSaveSelectedAircraft.setText("Selected aircraft");

		Button btnSaveAllAircrafts_1 = new Button(group_3, SWT.NONE);
		btnSaveAllAircrafts_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnSaveAllAircrafts_1.setText("All aircrafts");

		Group group_2 = new Group(this, SWT.NONE);
		group_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		group_2.setLayout(new GridLayout(2, false));

		Label lblSerialize = new Label(group_2, SWT.NONE);
		lblSerialize.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		lblSerialize.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 2, 1));
		lblSerialize.setText("Serialize");

		Button btnSerializeAircraftTo = new Button(group_2, SWT.NONE);
		btnSerializeAircraftTo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

				String runMessage = "Serializing aircraft ...\n",
						endMessage = "...done\n";

				statusLineManager.setMessage(runMessage);
				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(runMessage);

				JPADStaticWriteUtils.serializeObject(
						GlobalData.getTheCurrentAircraft(),
						MyConfiguration.databaseDirectory, GlobalData.getTheCurrentAircraft().get_name());

				statusLineManager.setMessage(endMessage);
				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(endMessage);
			}
		});
		btnSerializeAircraftTo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		btnSerializeAircraftTo.setText("Selected aircraft");

		Button btnSerializeAllAircrafts = new Button(group_2, SWT.NONE);
		btnSerializeAllAircrafts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnSerializeAllAircrafts.setText("All aircrafts");

		Button btnSerializeOperatingConditions = new Button(group_2, SWT.NONE);
		btnSerializeOperatingConditions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnSerializeOperatingConditions.setText("All operating conditions");

		Button btnSerializeApplicationStatus = new Button(group_2, SWT.NONE);
		btnSerializeApplicationStatus.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnSerializeApplicationStatus.setText("Application status");
		createContents();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Save options");
		setSize(603, 537);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}


	public void initialize(Aircraft aircraft, OperatingConditions conditions, StatusLineManager slm) {
		_theAircraft = aircraft;
		_theOperatingConditions = conditions;
		statusLineManager = slm;
	}
}
