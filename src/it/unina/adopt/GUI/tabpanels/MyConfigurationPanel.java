package it.unina.adopt.GUI.tabpanels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

public class MyConfigurationPanel extends MyPanel {
	private Text text;
	private Text txtAlphaEndLinearity;
	private Text txtAlphaStall;
	private Text txtClalpha;
	private Text txtCdmin;
	private Text txtClAtCd;
	private Text text_1;
	private Text text_2;
	private Text text_5;
	private Text text_6;
	private Text text_7;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyConfigurationPanel(Composite parent, CTabItem tab, int style) {
		super(parent, tab, style);

	}

	public MyConfigurationPanel(Composite parent, int style) {
		
		super(parent, style);
		setLayout(new GridLayout(3, false));
		
		Label lblAlphaZeroLift = new Label(this, SWT.NONE);
		lblAlphaZeroLift.setFont(SWTResourceManager.getFont("Times New Roman", 11, SWT.NORMAL));
		GridData gd_lblAlphaZeroLift = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblAlphaZeroLift.widthHint = 100;
		lblAlphaZeroLift.setLayoutData(gd_lblAlphaZeroLift);
		lblAlphaZeroLift.setText("Position along semispan");
		new Label(this, SWT.NONE);
		
		text = new Text(this, SWT.BORDER);
		text.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
			}
		});
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_text.widthHint = 100;
		text.setLayoutData(gd_text);
		
		txtAlphaEndLinearity = new Text(this, SWT.BORDER);
		txtAlphaEndLinearity.setFont(SWTResourceManager.getFont("Times New Roman", 11, SWT.NORMAL));
		txtAlphaEndLinearity.setText("Twist relative to root chord");
		GridData gd_txtAlphaEndLinearity = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAlphaEndLinearity.widthHint = 100;
		txtAlphaEndLinearity.setLayoutData(gd_txtAlphaEndLinearity);
		new Label(this, SWT.NONE);
		
		text_1 = new Text(this, SWT.BORDER);
		
		txtAlphaStall = new Text(this, SWT.BORDER);
		txtAlphaStall.setFont(SWTResourceManager.getFont("Times New Roman", 11, SWT.NORMAL));
		txtAlphaStall.setText("Maximum t/c");
		GridData gd_txtAlphaStall = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtAlphaStall.widthHint = 100;
		txtAlphaStall.setLayoutData(gd_txtAlphaStall);
		new Label(this, SWT.NONE);
		
		text_2 = new Text(this, SWT.BORDER);
		text_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		txtClalpha = new Text(this, SWT.BORDER);
		txtClalpha.setFont(SWTResourceManager.getFont("Times New Roman", 11, SWT.NORMAL));
		txtClalpha.setText("x");
		GridData gd_txtClalpha = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtClalpha.widthHint = 100;
		txtClalpha.setLayoutData(gd_txtClalpha);
		new Label(this, SWT.NONE);
		
		text_5 = new Text(this, SWT.BORDER);
		text_5.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		txtCdmin = new Text(this, SWT.BORDER);
		txtCdmin.setFont(SWTResourceManager.getFont("Times New Roman", 11, SWT.NORMAL));
		txtCdmin.setText("y");
		GridData gd_txtCdmin = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtCdmin.widthHint = 100;
		txtCdmin.setLayoutData(gd_txtCdmin);
		new Label(this, SWT.NONE);
		
		text_6 = new Text(this, SWT.BORDER);
		text_6.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		txtClAtCd = new Text(this, SWT.BORDER);
		txtClAtCd.setFont(SWTResourceManager.getFont("Times New Roman", 11, SWT.NORMAL));
		txtClAtCd.setText("z");
		GridData gd_txtClAtCd = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtClAtCd.widthHint = 100;
		txtClAtCd.setLayoutData(gd_txtClAtCd);
		new Label(this, SWT.NONE);
		
		text_7 = new Text(this, SWT.BORDER);
		text_7.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
