package it.unina.adopt.GUI.tabpanels;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import it.unina.adopt.GUI.dialogs.MyDialogHDFInterpolationCheck;
import ncsa.hdf.hdf5lib.exceptions.HDF5LibraryException;
import standaloneutils.database.hdf.MyHDFReader;

public class MyHDFInterpolationCheckPane extends Composite {

	private Text _textGroupPath;
	private Combo _comboRankValue;
	private Text _textVar0, _textVar1, _textVar2;
	private Text _textInterpolatedValue;
	private Double _var0, _var1, _var2;
	private Double _f1D_v0, _f2D_v0_v1, _f3D_v0_v1_v2;
	
	private boolean _showPlots;

	private Button _btnInterpolate, _btnShowPlots;
	
	private String _hdfFileName ="test/Aerodynamic_Database_Ultimate.h5";
	private MyHDFReader _hdfReader;
	
	private String _group1DFullName = 
			"(AR_v_eff)_k_h_v_vs_S_h_over_S_v";
	
	private String _group2DFullName = 
			"(C_l_beta_w_b)_C_l_beta_over_C_Lift1_(AR)_vs_AR_(lambda)";
	
	private String _group3DFullName = 
			"(C_l_beta_w_b)_C_l_beta_over_C_Lift1_(L_c2)_vs_L_c2_(AR)_(lambda)";		
	
	public MyHDFInterpolationCheckPane(Composite parent, int style, String fileName) {
		
		super(parent, style);
		
		if ( (fileName != null) || (fileName.isEmpty()) ) {
			_hdfFileName = fileName; 
		}

		// set up HDF file reader object
		try {
			_hdfReader = new MyHDFReader(_hdfFileName);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
//		this.setLayout(new FillLayout(SWT.VERTICAL));

		GridLayout gridLayout = new GridLayout(1,false);
		this.setLayout(gridLayout);
		
		GridData gridDataHDFPath = new GridData(GridData.FILL_HORIZONTAL);
		gridDataHDFPath.grabExcessHorizontalSpace = true;
		gridDataHDFPath.widthHint = 350;
		
		// group full path in HDF file, containind data, var_0, etc
		Label labelHDFGroupPath = new Label(this, SWT.NONE);
		labelHDFGroupPath.setText("HDF group path");
		_textGroupPath = new Text(this, SWT.BORDER);
		_textGroupPath.setLayoutData(gridDataHDFPath);
		_textGroupPath.setText(
				// _group1DFullName
				_group2DFullName
				);

		// interpolation type, 1D, 2D, or 3D
		Label lblRank = new Label(this, SWT.NONE);
		lblRank.setText("Rank");
		
		_comboRankValue = new Combo(this, SWT.NONE);
		_comboRankValue.setItems(new String[] {"1D", "2D", "3D"});
		_comboRankValue.select(
				// 0
				1
				);

		GridData gridDataVars = new GridData(GridData.BEGINNING);
		gridDataVars.grabExcessHorizontalSpace = false;
		gridDataVars.widthHint = 250;

		// var_0 value
		Label labelVar0 = new Label(this, SWT.NONE);
		labelVar0.setText("var_0");
		_textVar0 = new Text(this, SWT.BORDER);
		_textVar0.setLayoutData(gridDataVars);
		_textVar0.setText(
				"0.5"
				);
		
		// var_1 value
		Label labelVar1 = new Label(this, SWT.NONE);
		labelVar1.setText("var_1");
		_textVar1 = new Text(this, SWT.BORDER);
		_textVar1.setLayoutData(gridDataVars);
		_textVar1.setText(
				// "0.5"
				"4.0"
				);
		if (
			_comboRankValue.getText().equals("2D")
			|| _comboRankValue.getText().equals("3D")
				) {
			_textVar1.setEnabled(true);
		} else {
			_textVar1.setEnabled(false);
		}

		// var_2 value
		Label labelVar2 = new Label(this, SWT.NONE);
		labelVar2.setText("var_2:");
		_textVar2 = new Text(this, SWT.BORDER);
		_textVar2.setLayoutData(gridDataVars);
		_textVar2.setText("0.0");
		if (
				_comboRankValue.getText().equals("1D")
				|| _comboRankValue.getText().equals("2D")
				) {
			_textVar2.setEnabled(false);
		} else {
			_textVar2.setEnabled(true);
		}

		// interpolated value
		Label labelInterpolatedValue = new Label(this, SWT.NONE);
		labelInterpolatedValue.setText("Interpolated value:");
		_textInterpolatedValue = new Text(this, SWT.BORDER);
		_textInterpolatedValue.setLayoutData(gridDataVars);
		_textInterpolatedValue.setText("");
		_textInterpolatedValue.setEnabled(false);
		
		// interpolate button
		_btnInterpolate = new Button(this, SWT.NONE);
		_btnInterpolate.setText("Interpolate");

		// checkbox button
		_btnShowPlots = new Button(this, SWT.CHECK);
		_btnShowPlots.setText("Show plots");
		_btnInterpolate.setSelection(false);
		_showPlots = false;
		
		applyListeners();
		
	}
	
	private void applyListeners() {
		
		// push action listener
		_btnInterpolate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				String groupFullName = getTextGroupPath().getText();
				System.out.println("Group path: " + groupFullName);
				
				int rank = -1;
				if (getComboRankValue().getText().equals("1D"))
					rank = 1;
				if (getComboRankValue().getText().equals("2D"))
					rank = 2;
				if (getComboRankValue().getText().equals("3D"))
					rank = 3;
				
				int rankDataset = -1;
				try {
					rankDataset = _hdfReader.getDataRankByGroupName(groupFullName);
				} catch (HDF5LibraryException e1) {
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}
				
				System.out.println(
						"Rank: " 
						+ getComboRankValue().getText()
						+ " ... " + rankDataset
				);

				if ( rank == rankDataset ) { // only if specified group has a correct rank
					
					if( _comboRankValue.getText().equals("1D") ) {
						_group1DFullName = groupFullName;
						_var0 = Double.parseDouble(get_textVar0().getText());

						System.out.println(
								"v0 = " + _var0
						);
						
						_f1D_v0 = 
								_hdfReader
								.interpolate1DFromDataset(_group1DFullName, _var0);

						if (_f1D_v0 != null) {
							_textInterpolatedValue.setText(_f1D_v0.toString());
							System.out.println("f() = " + _f1D_v0);
						} else {
							_textInterpolatedValue.setText("");
						}

					} else if( _comboRankValue.getText().equals("2D") ) {
						_group2DFullName = groupFullName;
						_var0 = Double.parseDouble(get_textVar0().getText());
						_var1 = Double.parseDouble(get_textVar1().getText());

						System.out.println(
								"v0 = " + _var0 + "\n" 
								+ "v1 = " + _var1
						);
						
						_f2D_v0_v1 = 
								_hdfReader
								.interpolate2DFromDataset(_group2DFullName, _var0, _var1);

						if (_f2D_v0_v1 != null) {
							_textInterpolatedValue.setText(_f2D_v0_v1.toString());
							System.out.println("f() = " + _f2D_v0_v1);
						} else {
							_textInterpolatedValue.setText("");
						}

					} else if( _comboRankValue.getText().equals("3D") ) {
						_group3DFullName = groupFullName;
						_var0 = Double.parseDouble(get_textVar0().getText());
						_var1 = Double.parseDouble(get_textVar1().getText());
						_var2 = Double.parseDouble(get_textVar2().getText());
						
						System.out.println(
								"v0 = " + _var0 + "\n" 
								+ "v1 = " + _var1 + "\n"
								+ "v2 = " + _var2
						);

						_f3D_v0_v1_v2 = 
								_hdfReader
								.interpolate3DFromDataset(_group3DFullName, _var0, _var1, _var2);

						if (_f3D_v0_v1_v2 != null) {
							_textInterpolatedValue.setText(_f3D_v0_v1_v2.toString());
							System.out.println("f() = " + _f3D_v0_v1_v2);
						} else {
							_textInterpolatedValue.setText("");
						}
						
					} else { // != 1D, 2D, 3D
						// default to 1D case ? do nothing
					}
				
				
					// launch the dialog with plots if necessary
					if (_showPlots) {
						
						System.out.println("Showing plots ...");
						
						MyDialogHDFInterpolationCheck _dialogShellSectionShape = 
								new MyDialogHDFInterpolationCheck(
										Display.getCurrent(), 
										_hdfReader,
										groupFullName,
										rank,
										// TODO: create this list properly
										Arrays.asList(new Double[] { _var0, _var1, _var2 }), 
										Arrays.asList(new Double[] { _f1D_v0, _f2D_v0_v1, _f3D_v0_v1_v2 })
										);
					}
				
				
				} else {
					System.out.println(
							"Group dataset has rank " + rankDataset 
							+ ". Should have rank " + rank
					);
				}
			}
		});
		
		// combo selection listener
		_comboRankValue.addSelectionListener(new SelectionAdapter() {
			@Override
	        public void widgetSelected(SelectionEvent e) {
	            if( _comboRankValue.getText().equals("1D") ) {
	                _textVar0.setEnabled(true);
	                _textVar1.setEnabled(false);
	                _textVar2.setEnabled(false);
	            } else if( _comboRankValue.getText().equals("2D") ) {
	                _textVar0.setEnabled(true);
	                _textVar1.setEnabled(true);
	                _textVar2.setEnabled(false);
	            } else if( _comboRankValue.getText().equals("3D") ) {
	                _textVar0.setEnabled(true);
	                _textVar1.setEnabled(true);
	                _textVar2.setEnabled(true);
	            } else { // default to 1D case
	                _textVar0.setEnabled(true);
	                _textVar1.setEnabled(false);
	                _textVar2.setEnabled(false);
	            }
	        } 
	    });
		
		_btnShowPlots.addSelectionListener(new SelectionAdapter()
		{
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
		        if (_btnShowPlots.getSelection())
		            _showPlots = true;
		        else
		        	_showPlots = false;
		    }
		});
		
		_textVar0.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				doVerifyTextIsFloat(e);
	        }
		});
		
		_textVar1.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				doVerifyTextIsFloat(e);
	        }
		});

		_textVar2.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				doVerifyTextIsFloat(e);
	        }
		});
		
	} // end-of applyListeners
	
	private void doVerifyTextIsFloat(VerifyEvent e) {
		Text text = (Text)e.getSource();

        // get old text and create new text by using the VerifyEvent.text
        final String oldS = text.getText();
        String newS = oldS.substring(0, e.start) + e.text + oldS.substring(e.end);

        boolean isFloat = true;
        try
        {
            Float.parseFloat(newS);
        }
        catch(NumberFormatException ex)
        {
            isFloat = false;
        }
        //System.out.println(newS);

        if(!isFloat)
            e.doit = false;
    }
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	public Combo getComboRankValue() {
		return _comboRankValue;
	}
	public Text getTextGroupPath() {
		return _textGroupPath;
	}
	public Button getInterpolateButton() {
		return _btnInterpolate;
	}
	
	public String getHDFFileName() {
		return _hdfReader.getDatabaseAbsolutePath();
	}

	public Text get_textVar0() {
		return _textVar0;
	}
	public Text get_textVar1() {
		return _textVar1;
	}
	public Text get_textVar2() {
		return _textVar2;
	}
	public Text get_textInterpolatedValue() {
		return _textInterpolatedValue;
	}
	

	
}
