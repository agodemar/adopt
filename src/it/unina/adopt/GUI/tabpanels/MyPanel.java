/**
 * 
 */
package it.unina.adopt.GUI.tabpanels;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.nebula.visualization.xygraph.dataprovider.CircularBufferDataProvider;
import org.eclipse.nebula.visualization.xygraph.figures.ToolbarArmedXYGraph;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.XYGraph;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * @author agodemar
 */
public class MyPanel extends Composite {

	private CTabItem _tabItem = null;
	protected Composite _container = null;
	protected ScrolledComposite _scrolledComposite = null;
	protected Composite _ctrComposite = null;
	protected CTabFolder _tabFolderPlots = null;
	protected CTabItem _tabItemXZView = null;
	protected CTabItem _tabItemXYView = null;
	protected CTabItem _tabItemYZView = null;
	protected FigureCanvas _fcXZ = null;
	protected FigureCanvas _fcXY = null;
	protected FigureCanvas _fcYZ = null;
	protected LightweightSystem _lwsXZ = null;
	protected LightweightSystem _lwsXY = null;
	protected LightweightSystem _lwsYZ = null;
	protected XYGraph _xyGraphXZ = null;
	protected XYGraph _xyGraphXY = null;
	protected XYGraph _xyGraphYZ = null;
	protected ToolbarArmedXYGraph _toolbarArmedXYGraph = null;
	protected ToolbarArmedXYGraph _toolbarArmedYZGraph = null;
	protected ToolbarArmedXYGraph _toolbarArmedXZGraph = null;	
	protected List<CircularBufferDataProvider> _tdpXZ = new ArrayList<CircularBufferDataProvider>();
	protected List<CircularBufferDataProvider> _tdpXY = new ArrayList<CircularBufferDataProvider>();
	protected List<CircularBufferDataProvider> _tdpYZ =  new ArrayList<CircularBufferDataProvider>();
	protected List<Trace> _traces_XZ = new ArrayList<Trace>();
	protected List<Trace> _traces_XY = new ArrayList<Trace>();
	protected List<Trace> _traces_YZ = new ArrayList<Trace>();

	public MyPanel(Composite parent, int style) {

		super(parent, style);

	}

	/**
	 * @param _parent
	 * @param style
	 */
	public MyPanel(Composite parent, CTabItem tabItem, int style) {

		super(parent, style);

		buildPanelLayout(parent, tabItem, style);

	}// end-of-constructor


	public void buildPanelLayout(Composite parent, CTabItem tabItem, int style) {

		_tabItem = tabItem;

		// the top level composite/container
		_container = new Composite( parent, SWT.NONE );
		_container.setLayout(new FillLayout());

		// attach the pane to a CTabItem
		_tabItem.setControl(_container);

		// split the pane in two parts with a sash
		SashForm sashFormLeftRight = new SashForm(_container, SWT.HORIZONTAL|SWT.BORDER);

		// left composite with controls
		_scrolledComposite = 
				new ScrolledComposite(
						sashFormLeftRight, 
						SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
						);

		_ctrComposite = new Composite( _scrolledComposite, SWT.NONE );
		_scrolledComposite.setContent(_ctrComposite);

		// Put plots on the right	    
		_tabFolderPlots = new CTabFolder(sashFormLeftRight, SWT.NONE);

	}

	public CTabItem get_tabItem() {
		return _tabItem;
	}

	public void set_tabItem(CTabItem _tabItem) {
		this._tabItem = _tabItem;
	}

	public Composite get_container() {
		return _container;
	}

	public void set_container(Composite _container) {
		this._container = _container;
	}

	//	public void getInitiatorPane(Object object) {
	//		
	//	}

}
