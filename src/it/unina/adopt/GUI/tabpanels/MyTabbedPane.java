package it.unina.adopt.GUI.tabpanels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
//import it.unina.adopt.GUI.test.javafx.MyTestJavaFXPane2;

import aircraft.calculators.ACAerodynamicsManager;
import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import it.unina.adopt.GUI.tabpanels.analysis.MyAnalysisPanel;
import it.unina.adopt.GUI.tabpanels.fuselage.MyFuselagePanel;
import it.unina.adopt.GUI.tabpanels.liftingSurface.MyLiftingSurfaceNewPanel;
import it.unina.adopt.GUI.tabpanels.liftingSurface.MyLiftingSurfacePanel;
import it.unina.adopt.GUI.test.gef.MyTestGEFPane;
import it.unina.adopt.GUI.test.javafx.TestFXLineChartPane;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.MyCompositeProcessing;
import it.unina.adopt.test.la.MyTestComposite;
import it.unina.adopt.viewer3d.MyPane3D;


public class MyTabbedPane extends Composite {

	private ACAerodynamicsManager _theCalculator;

	public static final String TAB_NAME_3DVIEW = "3D View";
//	public static final String TAB_NAME_FUSELAGE_INITIATOR = "Fuselage";
//	public static final String TAB_NAME_WING_INITIATOR = "Wing";
//	public static final String TAB_NAME_HTAIL_INITIATOR = "HTail";
//	public static final String TAB_NAME_VTAIL_INITIATOR = "VTail";
//	public static final String TAB_NAME_LANDING_GEAR_INITIATOR = "Landing Gear";
//	public static final String TAB_NAME_NACELLE_INITIATOR = "Nacelle";
//	public static final String TAB_NAME_ENGINE_INITIATOR = "Engine";
//	public static final String TAB_NAME_ANALYSIS_INITIATOR = "Analysis";

	public static final String TAB_NAME_HDFCHECKPANE = "HDF Check";

	private MyHDFInterpolationCheckPane _myHDFInterpolationCheckPane;
	
	// TODO: make these private (??)
	public MyFuselagePanel fuselageInitiatorPane = null;
	public MyLiftingSurfacePanel wingInitiatorPane = null;
	public MyLiftingSurfacePanel hTailInitiatorPane = null;
	public MyLiftingSurfacePanel vTailInitiatorPane = null;
	public MyAnalysisPanel analysisInitiatorPane = null;

	private CTabFolder _tabFolder = null;

	private CTabItem _tabItemProcessing3DView = null;

	// TODO: make these private (??)
	
	public CTabItem tabItemFuselageInitiator = null;	
	public CTabItem tabItemWingInitiator = null;
	public CTabItem tabItemHTailInitiator = null;
	public CTabItem tabItemVTailInitiator = null;
	public CTabItem tabItemAnalysisInitiator = null;

	private ScrolledComposite _containerPTest;
	private ScrolledComposite _containerP3DTest;
	private MyCompositeProcessing _compositeProcessing;

	private ScrolledComposite _containerHDFTest;

	private ACAnalysisManager _theAnalysis;

	private ScrolledComposite containerFX3DView;

	private CTabItem tabItemFX3DView;
	private CTabItem tabItemFXLineChartTest;

	private ScrolledComposite containerFXLineChartTest;

	public MyTabbedPane(
			Composite parent, 
			int style, 
			ACAnalysisManager analysis, 
			Aircraft aircraft) {

		super(parent, style);

		_theAnalysis = analysis;

		// the main container must have a FillLayout
		this.setLayout(new FillLayout());

		_tabFolder = new CTabFolder(this, SWT.NONE);
		
//		GridLayout layout = new GridLayout();
//		layout.numColumns = 1;
//		layout.verticalSpacing = 0;
//		layout.marginWidth = 0;
//		layout.marginHeight = 0;
//		_tabFolder.setLayout(layout);
		_tabFolder.setLayout(new FillLayout(SWT.VERTICAL));
		
		//----------------------------------------------------------------------
		// initialize the object by building the appropriate tab items 
		this.init(aircraft);
		// Show only a 3D View initially when no aircraft is assigned 
		//----------------------------------------------------------------------
		
		// force first tab selection to get canvas painted
		_tabFolder.setSelection(0);
		_tabFolder.forceFocus();

		//-----------------------------------------
		//		_tabFolder.pack();
		//		pack();
		this.layout(true);

	} // end of constructor


	/**
	 * Initialize the widget
	 * 
	 * @param aircraft
	 */
	public void init(Aircraft aircraft) {

		// Open at least the 3D View
//		openMyTestProcessingPane();
//		open3DView();
		
		// TODO: put here other initialization tasks

		openMyTestHDFCheckPane(); // OK
		openMyTabTest();
		openFX3DPane();
		
		openFXLineChartPane();

//		openMyTestGEFPane();
		
		// TODO: Apply events listeners ??
		
		applyMouseScrollListenerTo3DView();
		applySelectionListenerTo3DView();
		
		applySelectionListenerToHDFCheckPanel();
		
		applyDisposeEventListeners();
	}


	public void open3DView()
	{
		// TODO: check theAircraft

		//--------------------------------------------------------------------------------
		// Processing.org test view

		System.out.println("open3DView :: _tabItemProcessing3DView null");
		System.out.println("open3DView :: creating CTabItem for 3D View");

		_tabItemProcessing3DView = new CTabItem(_tabFolder, SWT.NONE);
		_tabItemProcessing3DView.setText( TAB_NAME_3DVIEW ); // "P-Test"

		// the main (outer) container
		//Composite _containerPTest = new Composite(_tabFolder,SWT.NONE);
		_containerPTest = new ScrolledComposite(
				_tabFolder,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL
				);
		_tabItemProcessing3DView.setControl(_containerPTest);

		// the outer layout
		_containerPTest.setLayout(new FillLayout());
		_containerPTest.setLayoutData(new GridData(GridData.FILL_BOTH));

		// the child --> contains the PApplet
		_compositeProcessing =
				new MyCompositeProcessing(_containerPTest);

		_containerPTest.setContent(_compositeProcessing);

		_compositeProcessing.pack();
		_containerPTest.pack();
		// http://stackoverflow.com/questions/14445580/scrollable-composite-auto-resize-swt
		// _containerPTest.setMinSize(compositeProcessing.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		this.forceFocus();

	}

	public void openMyTestProcessingPane()
	{
		//--------------------------------------------------------------------------------
		// Test tab

		CTabItem tabItemTest = new CTabItem(_tabFolder, SWT.CLOSE);
		tabItemTest.setText("My Test");

		// the main (outer) container
		_containerP3DTest = new ScrolledComposite(
				_tabFolder,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL
				);
		tabItemTest.setControl(_containerP3DTest);

		// the outer layout
		_containerP3DTest.setLayout(new FillLayout());
		_containerP3DTest.setLayoutData(new GridData(GridData.FILL_BOTH));

		// the child --> contains the PApplet
		MyTestComposite myTestComposite =
				new MyTestComposite(_containerP3DTest);

		_containerP3DTest.setContent(myTestComposite);

		myTestComposite.pack();
		_containerP3DTest.pack();

	}
	
	public void openMyTabTest() {
		
		CTabItem tabItemTest = new CTabItem(_tabFolder, SWT.CLOSE);
		tabItemTest.setText("Tab test");
		
		// the main (outer) container
		ScrolledComposite testTabContainer = new ScrolledComposite(
				_tabFolder,
				SWT.CLOSE | SWT.H_SCROLL | SWT.V_SCROLL
				);
		
		// necessary settings
		testTabContainer.setMinHeight(700);
		testTabContainer.setMinWidth(400);
		testTabContainer.setExpandHorizontal(true);
		testTabContainer.setExpandVertical(true);

		Composite content = new Composite(
				testTabContainer,
				SWT.NONE);
		content.setLayout(new FillLayout(SWT.VERTICAL));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		// Panel contents
//		MyAirfoilsDialog testTab = 
//				new MyAirfoilsDialog(content, SWT.NONE);
		
		MyLiftingSurfaceNewPanel testTab = 
				new MyLiftingSurfaceNewPanel(content, SWT.NONE); 
		
	    // associate content to scrolled composite
	    testTabContainer.setContent(content);
	    
	    // force layout application to the container 
	    testTabContainer.layout(true);

	    // associate container to tab item
	    tabItemTest.setControl(testTabContainer);
		
		
	}

	public void openMyTestHDFCheckPane()
	{
		
		CTabItem tabItemTest = new CTabItem(_tabFolder, SWT.CLOSE);
		tabItemTest.setText("HDF Check");
		
		// the main (outer) container
		_containerHDFTest = new ScrolledComposite(
				_tabFolder,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL
				);
		// necessary settings
		_containerHDFTest.setMinHeight(700);
		_containerHDFTest.setMinWidth(400);
		_containerHDFTest.setExpandHorizontal(true);
		_containerHDFTest.setExpandVertical(true);

		Composite content = new Composite(
				_containerHDFTest,
				SWT.NONE);
		content.setLayout(new FillLayout(SWT.VERTICAL));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		// Panel contents

		_myHDFInterpolationCheckPane = 
				new MyHDFInterpolationCheckPane(
						content, SWT.NONE,
						"test/Aerodynamic_Database_Ultimate.h5"
				);  		
		
	    // associate content to scrolled composite
	    _containerHDFTest.setContent(content);
	    
	    // force layout application to the container 
	    _containerHDFTest.layout(true);

	    // associate container to tab item
	    tabItemTest.setControl(_containerHDFTest);

	}

	public void openMyTestGEFPane()
	{
		
		CTabItem tabItemTest = new CTabItem(_tabFolder, SWT.CLOSE);
		tabItemTest.setText("GEF test");
		
		// the main (outer) container
		ScrolledComposite containerGEFTest = new ScrolledComposite(
				_tabFolder,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL
				);
		// necessary settings
		containerGEFTest.setMinHeight(700);
		containerGEFTest.setMinWidth(400);
		containerGEFTest.setExpandHorizontal(true);
		containerGEFTest.setExpandVertical(true);

		Composite content = new Composite(
				containerGEFTest,
				SWT.NONE);
		content.setLayout(new FillLayout(SWT.VERTICAL));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		// Panel contents

		MyTestGEFPane myTestGEFPane = 
				new MyTestGEFPane(
						content, SWT.NONE
				);
		
	    // associate content to scrolled composite
		containerGEFTest.setContent(content);
	    
	    // force layout application to the container 
		containerGEFTest.layout(true);

	    // associate container to tab item
	    tabItemTest.setControl(containerGEFTest);

	}

	public void openFX3DPane() {
		
		tabItemFX3DView = new CTabItem(_tabFolder, SWT.NONE);
		tabItemFX3DView.setText("3D view");
		
		// the main (outer) container
		containerFX3DView = new ScrolledComposite(
				_tabFolder,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL
				);
		// necessary settings
		containerFX3DView.setMinHeight(700);
		containerFX3DView.setMinWidth(400);
		containerFX3DView.setExpandHorizontal(true);
		containerFX3DView.setExpandVertical(true);

		updateFX3DPane(tabItemFX3DView, containerFX3DView);
	}

	// TODO: check if this is necessary, My3DViewAction
	public void updateFX3DPane(){
		updateFX3DPane(tabItemFX3DView, containerFX3DView);
	}
	
	public void updateFX3DPane(CTabItem tabItem, ScrolledComposite container) {
		
		Composite content = new Composite(container, SWT.NONE);
		content.setLayout(new FillLayout(SWT.VERTICAL));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		// Panel contents
		MyPane3D myFXPane = new MyPane3D(content, SWT.NONE);

	    // associate content to scrolled composite
		container.setContent(content);
	    
	    // force layout application to the container 
		container.layout(true);

	    // associate container to tab item
	    tabItem.setControl(container);
	}

	// Test JavaFX LineChart
	public void openFXLineChartPane() {
		
		tabItemFXLineChartTest = new CTabItem(_tabFolder, SWT.CLOSE);
		tabItemFXLineChartTest.setText("FX LineChart Test");
		
		// the main (outer) container
		containerFXLineChartTest = new ScrolledComposite(
				_tabFolder,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL
				);
		// necessary settings
		containerFXLineChartTest.setMinHeight(700);
		containerFXLineChartTest.setMinWidth(400);
		containerFXLineChartTest.setExpandHorizontal(true);
		containerFXLineChartTest.setExpandVertical(true);

		updateFXLineChartTestPane(tabItemFXLineChartTest, containerFXLineChartTest);

	}

	
	private void updateFXLineChartTestPane(CTabItem tabItem, ScrolledComposite container) {

		// setup minimum size
		Monitor monitor = Display.getDefault().getMonitors()[0];
		container.setMinSize(
	    		(int)( 0.55*monitor.getBounds().width  ),
	    		(int)( 0.55*monitor.getBounds().height )
	    		);
		
		Composite content = new Composite(container, SWT.NONE);
		content.setLayout(new FillLayout(SWT.VERTICAL));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		// Panel contents
		TestFXLineChartPane myFXLineChartPane = new TestFXLineChartPane(content, SWT.NONE);

	    // associate content to scrolled composite
		container.setContent(content);
	    
	    // force layout application to the container 
		container.layout(true);

	    // associate container to tab item
	    tabItem.setControl(container);
	    
	}


	// TODO: only for development purposes, to be removed asap
	private void applyEventListeners0()
	{
		//=============================================================
		// 3D VIEW
		//=============================================================
		if (_containerPTest != null)
		{

			//			_containerPTest.addListener(SWT.Resize,  new Listener () {
			//				@Override
			//				public void handleEvent(Event arg0) {
			//					Rectangle rect = getClientArea ();
			//					System.out.println("_containerPTest RESIZE :: " + rect);
			//					if (!rect.isEmpty())
			//					{
			//						_compositeProcessing.getSketch().frame.setSize(
			//								(int) (1.0*rect.width), rect.height
			//								);
			//					}
			//				}
			//			});

			// A filter that prevents mouse wheel scrolling of 3D view
			Display.getCurrent().addFilter(SWT.MouseWheel, new Listener()
			{
				@Override
				public void handleEvent(Event e)
				{
					// Check if it's the correct widget
					if(e.widget.equals(_containerPTest))
						e.doit = false;
					//		            else
					//		                System.out.println(e.widget);
				}
			});

		}// _containerPTest != null

		if (_tabFolder != null)
		{
			// detect when the tab with 3D view is selected
			_tabFolder.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
					if ( _tabFolder.getSelection().getText() == TAB_NAME_3DVIEW ) // "P-Test"
					{
						//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// EXPERIMENTAL

						System.out.println(_tabFolder.getSelection().getText());

						ADOPT_GUI.getApp().theSketch.redraw();

						//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}				
				}
			});		
		}

//		if (tabItemFuselageInitiator != null)
//		{
//			tabItemFuselageInitiator.addDisposeListener(new DisposeListener() {
//				@Override
//				public void widgetDisposed(DisposeEvent e) {
//
//					// ((CTabItem) e.getSource())
//
//					// make sure the fuselageInitiatorPane is null
//					fuselageInitiatorPane = null;
//					tabItemFuselageInitiator = null;
//					System.out.println("tabItemFuselageInitiator :: null");
//				}
//			});
//		}
		
		
		System.out.println("applyEventListeners :: item count " + _tabFolder.getItemCount());
		for (int i=0; i < _tabFolder.getItems().length; i++)
		{
			int idx = i;
			System.out.println("Item :: " + idx +  " to be disposed ...");
			_tabFolder.getItem(i).addDisposeListener(new DisposeListener() {
						@Override
						public void widgetDisposed(DisposeEvent e) {

							// ((CTabItem) e.getSource())

							System.out.println("Item :: " + ((CTabItem) e.getSource()).getText() + " disposing ...");
							
//							((CTabItem) e.getSource()).
							
							((CTabItem) e.getSource()).dispose();

							System.out.println("Item disposed :: " + ((CTabItem) e.getSource()).isDisposed() );
							
						}
					});
		}
		

		//=============================================================
		// 3D VIEW -- P3D test
		//=============================================================
		if (_containerP3DTest != null)
		{

			// A filter that prevents mouse wheel scrolling of 3D view
			Display.getCurrent().addFilter(SWT.MouseWheel, new Listener()
			{
				@Override
				public void handleEvent(Event e)
				{
					// Check if it's the correct widget
					if(e.widget.equals(_containerP3DTest))
						e.doit = false;
					//		            else
					//		                System.out.println(e.widget);
				}
			});

		}// _containerPTest != null

	} // end-of-function applyEventListeners

	public void applyMouseScrollListenerTo3DView() {
		if (_containerPTest != null) {
			
			// A filter that prevents mouse wheel scrolling of 3D view
			Display.getCurrent().addFilter(SWT.MouseWheel, new Listener()
			{
				@Override
				public void handleEvent(Event e)
				{
					// Check if it's the correct widget
					if(e.widget.equals(_containerPTest)) {
						e.doit = false;
					}
				}
			});
		}
	}
	
	public void applySelectionListenerTo3DView() {
		if (_tabFolder != null)
		{
			// detect when the tab with 3D view is selected
			_tabFolder.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
					if ( _tabFolder.getSelection().getText() == TAB_NAME_3DVIEW ) // "P-Test"
					{
						//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// EXPERIMENTAL
						ADOPT_GUI.getApp()
							.getStatusLineManager()
							.setMessage(
								"3D View. Double click on single scene to maximize."
							);

						System.out.println(_tabFolder.getSelection().getText());

						ADOPT_GUI.getApp().theSketch.redraw();

						//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}				
				}
			});		
		}

	}

	public void applySelectionListenerToHDFCheckPanel() {
		if (_tabFolder != null)
		{
			// detect when the tab with HDF check is selected
			_tabFolder.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
					if ( _tabFolder.getSelection().getText() == TAB_NAME_HDFCHECKPANE ) // "P-Test"
					{
						//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// EXPERIMENTAL

						System.out.println(_tabFolder.getSelection().getText());
						System.out.println(
								"Check file " 
								+ get_myHDFInterpolationCheckPane().getHDFFileName()
								);
						
						ADOPT_GUI.getApp()
							.getStatusLineManager()
							.setMessage(
								"File: " + get_myHDFInterpolationCheckPane().getHDFFileName()
							);
						
						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
								"Check file " 
								+ get_myHDFInterpolationCheckPane().getHDFFileName() + "\n"
								);


						//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}				
				}
			});
		}

	}
	
	public void applyDisposeEventListeners()
	{

		System.out.println("applyEventListeners :: item count " + _tabFolder.getItemCount());
		for (int i=0; i < _tabFolder.getItems().length; i++)
		{
			int idx = i;
			System.out.println("Item :: " + idx +  " ...");
			if (_tabFolder != null) {
				if ( ! _tabFolder.getItem(i).isListening(SWT.Dispose) ) {
					_tabFolder.getItem(i).addDisposeListener(new DisposeListener() {
						@Override
						public void widgetDisposed(DisposeEvent e) {
							System.out.println("Item :: " + ((CTabItem) e.getSource()).getText() + " applying dispose event");
							((CTabItem) e.getSource()).dispose();
							System.out.println("Item disposed :: " + ((CTabItem) e.getSource()).isDisposed() );
						}
					});
					
				}
			}
		}

	} // end-of-function applyEventListeners
	
	
	/** 
	 * look for a given tab (given name)
	 * 
	 *  @return tab index if found, -1 if not found
	 */
	public int findTabItem(String tabName)
	{
		int nTabs = _tabFolder.getItemCount();
		int res = -1;
		boolean bFound = false;
		for (int t = 0; t < nTabs; t++)
		{
			bFound = (
					_tabFolder.getItem(t).getText()
					.equals(tabName)
					);
			if ( bFound )
			{
				res = t;
				break;
			}
		}
		return res;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public MyFuselagePanel getInitiatorPaneFuselage() {
		return fuselageInitiatorPane;
	}

	public void setInitiatorPaneFuselage(MyFuselagePanel fuselageInitiatorPane) {
		this.fuselageInitiatorPane = fuselageInitiatorPane;
	}

	public MyLiftingSurfacePanel getInitiatorPaneWing() {
		return wingInitiatorPane;
	}

	public void setInitiatorPaneWing(MyLiftingSurfacePanel WingInitiatorPane) {
		this.wingInitiatorPane = WingInitiatorPane;
	}

	public MyLiftingSurfacePanel getInitiatorPaneHTail() {
		return hTailInitiatorPane;
	}

	public void setInitiatorPaneHTail(MyLiftingSurfacePanel hTailInitiatorPane) {
		this.hTailInitiatorPane = hTailInitiatorPane;
	}

	public void setInitiatorPaneVTail(MyLiftingSurfacePanel vTailInitiatorPane) {
		this.vTailInitiatorPane = vTailInitiatorPane;
	}

	public CTabFolder get_tabFolder() {
		return _tabFolder;
	}


	public void set_tabFolder(CTabFolder _tabFolder) {
		this._tabFolder = _tabFolder;
	}


	public CTabItem get_tabItemProcessing3DView() {
		return _tabItemProcessing3DView;
	}


	public MyHDFInterpolationCheckPane get_myHDFInterpolationCheckPane() {
		return _myHDFInterpolationCheckPane;
	}


	public CTabItem getTabItemFX() {
		return tabItemFX3DView;
	}


} // end-of-class