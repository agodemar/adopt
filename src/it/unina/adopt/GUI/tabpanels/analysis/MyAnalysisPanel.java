package it.unina.adopt.GUI.tabpanels.analysis;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import javax.measure.unit.NonSI;
import javax.measure.unit.SI;

import org.eclipse.nebula.visualization.xygraph.dataprovider.CircularBufferDataProvider;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.PointStyle;
import org.eclipse.nebula.visualization.xygraph.util.XYGraphMediaFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;

import com.google.common.primitives.Doubles;

import aircraft.calculators.ACAerodynamicsManager;
import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import aircraft.components.fuselage.Fuselage;
import aircraft.components.liftingSurface.LiftingSurface;
import configuration.MyConfiguration;
import it.unina.adopt.GUI.tabpanels.MyPanel;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.MyNomenclatureImageDialog;


public class MyAnalysisPanel extends MyPanel {

	// FILE WING TEST
	String ANALYSIS_FILE_NAME = "analysis.xml";
	String analysisFilePath = "test" + File.separator + ANALYSIS_FILE_NAME;
	File analysisFile = new File(analysisFilePath);
	//	
	private final String EXPORT_FILE_NAME = "wing_initiator.xml";
	private String _exportFilePath =
			MyConfiguration.currentDirectory + File.separator + EXPORT_FILE_NAME;

	private LiftingSurface _theLiftingSurface = null;
	private Fuselage _theFuselage=null;
	private ACAerodynamicsManager _theAerodynamics;
	private Aircraft _theAircraft;

	Combo _comboDropDown = null;
	Combo _comboDropDownSectionData = null;

	// Standard API map
	// Map <MyFuselageAdjustCriteria,String> _mapAdjustCriteria = new HashMap<MyFuselageAdjustCriteria,String>();

	// controls

	private Text 
	_text_01, _text_02, _text_03, _text_04, _text_05,
	_text_06, _text_07,_text_07a,_text_08,_text_08b,_text_09, _text_10,_text_10b,_text_11,_text_11b,_text_12,_text_13,
	_text_14,_text_14b,_text_15,_text_15b,_text_16,_text_16b,_text_17,_text_18,_text_19,_text_20,_text_21,_text_22,_text_23,
	_text_24,_text_25,_text_26,_text_27,_text_28,_text_29,_text_30;
	private final int TEXT_WIDTH_HINT = 50;

	private Slider 
	_slider_01,	_slider_02,	_slider_03,	_slider_04,	_slider_05,
	_slider_06,	_slider_07,	_slider_08,	_slider_09,	_slider_10,
	_slider_11,	_slider_12;

	private File _importFile;
	private Text _text_ImportFile;
	private Button _button_ChooseImportFile, _button_ImportFile;

	private File _exportFile;
	private Text _text_ExportFile;
	private Button _button_ExportFile;
	private Button _button_Nomenclature;
	private Button _button_Eq_Wing;

	private Double sweepQuarterChordEquivalentWingDeg = null;
	// images etc
	protected static Map<String, Image> fCachedImages = new TreeMap<String, Image>();

	private MyNomenclatureImageDialog _nomenclatureWing = null;
	/**
	 * @param _parent
	 * @param tabItem
	 * @param style
	 */

	public MyAnalysisPanel(
			Composite parent, 
			CTabItem tabItem, 
			int style, 
			Aircraft aircraft, 
			ACAnalysisManager analysis) {

		super(parent, tabItem, style);

		_theAircraft = aircraft;
		_theAerodynamics = aircraft.get_theAerodynamics();

		ADOPT_GUI.getApp().setInitiatorPaneAnalysis(this); // make the App know about the object

		_theFuselage = _theAircraft.get_fuselage();

		//		// instantiate the wingobject
		//		_theLiftingSurface = new MyLiftingSurface(
		//				"Wing",             // name
		//				"The Wing object",  // description
		//				0.0, 0.0, 0.0 ,          // ref. point
		//				Math.toRadians(3.2),
		//				type,
		//				_theFuselage
		//				);
		//		
		ADOPT_GUI.getApp().theWing = this._theLiftingSurface;


		// the outer grid layout
		GridLayout layoutCtr = new GridLayout(8,false);
		GridData gridDataCtr = new GridData(SWT.FILL, SWT.FILL, true, false);
		_ctrComposite.setLayoutData(gridDataCtr);
		_ctrComposite.setLayout(layoutCtr);


		//------------------------------------------------------------------------------
		// Controls for settings:
		//    *  file choose/import

		//------------------------------------------------------------------------------

		GridData gd_GroupSettings = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_GroupSettings.horizontalSpan = 2;

		Group groupSettings = new Group (_ctrComposite, SWT.NONE);

		groupSettings.setLayout (new FillLayout (SWT.VERTICAL));
		groupSettings.setLayoutData (gd_GroupSettings);
		groupSettings.setText ("Settings");

		//		// Import file controls
		Group groupImportFile = new Group (groupSettings, SWT.NONE);
		groupImportFile.setLayout (new FillLayout (SWT.HORIZONTAL));
		groupImportFile.setText ("Import wing file");
		_text_ImportFile = new Text(groupImportFile, SWT.BORDER);
		_text_ImportFile.setText( "" );
		_text_ImportFile.setEditable(false);

		_button_ChooseImportFile = new Button(groupImportFile, SWT.NONE);
		_button_ChooseImportFile.setText("Choose ...");

		_button_ImportFile = new Button(groupSettings, SWT.NONE);
		_button_ImportFile.setText("Import");
		//

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		// Nomenclature

		_button_Nomenclature = new Button(groupSettings, SWT.NONE);
		_button_Nomenclature.setText("Wing Nomenclature");

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++


		//---------------Input DATA----------------
		Group groupInput = new Group (_ctrComposite, SWT.NONE);

		groupInput.setLayout( new GridLayout( 2, true ) );
		groupInput.setLayoutData( new GridData( SWT.FILL, SWT.FILL, true, true, 2, 2 ) );
		groupInput.setText ("Analysis Results");



		// I/O controls
		GridData gd_GroupIO = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_GroupIO.horizontalSpan = 2;

		Group groupIO = new Group (_ctrComposite, SWT.NONE);

		groupIO.setLayout (new FillLayout (SWT.VERTICAL));
		groupIO.setLayoutData (gd_GroupIO);
		groupIO.setText("I/O");

		// Import file controls
		Group groupSerializeFile = new Group (groupIO, SWT.NONE);
		groupSerializeFile.setLayout (new FillLayout (SWT.HORIZONTAL));
		groupSerializeFile.setText ("Save status file");
		_text_ExportFile = new Text(groupIO, SWT.BORDER);
		_text_ExportFile.setText( _exportFilePath );
		_text_ExportFile.setEditable(false);

		_button_ExportFile = new Button(groupIO, SWT.NONE);
		_button_ExportFile.setText("Save");


		//-------------------------------------------------------------------------
		// EVENTS
		//-------------------------------------------------------------------------

		_button_ChooseImportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.Selection:
					FileDialog dialog = new FileDialog(ADOPT_GUI.getApp().getShell());
					dialog.setFilterPath( MyConfiguration.currentDirectory.toString() );
					String sFile = dialog.open();
					if ( sFile != null ) {
						_importFile = new File(sFile);
						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
								"The ADOpT|Choose wing data file: " +
										_importFile.toString() + "\n"
								);				    	
						_text_ImportFile.setText(_importFile.toString());
					}				    
					break;
				}
			}
		});

		_button_ImportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.Selection:
					if ( _importFile != null ) {

						if ( _importFile.exists() && _importFile.isFile()  ) {
							//							_theLiftingSurface.importFromXMLFile(_importFile);
							adjustControls(); // update controls in GUI
							//							_theLiftingSurface.calculateGeometry();
							//							adjustOutputWingData();
							redrawDragPolar();
						}
					}				    
					break;
				}
			}
		});

		//		_button_Eq_Wing.addListener(SWT.Selection, new Listener() {
		//			public void handleEvent(Event event) {
		//				chordDistributionEq();
		//			}
		//		});

		_button_Nomenclature.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {


						final Shell dialogNomenclature = new Shell(Display.getCurrent(),
								SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.RESIZE
								);	

						dialogNomenclature.setText("Wing Nomenclature");	
						//								Monitor monitor = Display.getDefault().getMonitors()[0];
						//								dialogNomenclature.setSize(
						//										(int)( 0.75*monitor.getBounds().width  ),
						//										(int)( 0.75*monitor.getBounds().height )
						//										);

						dialogNomenclature.setLayout(new FillLayout());
						_nomenclatureWing= new MyNomenclatureImageDialog( dialogNomenclature,"images/Wing_Topview_2.png");

					}
				}
				); // end of button_Nomenclature listener

		_button_ExportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				//				FileOutputStream fos = null;
				//				ObjectOutputStream out = null;
				String filePath = 
						MyConfiguration.currentDirectory 
						+ File.separator + EXPORT_FILE_NAME;

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The ADOpT | Serialize winginitiator file: " +	filePath + "\n"
						);				    	

				switch (event.type) {
				case SWT.Selection:
					// Write XML
					_exportFile = new File(_exportFilePath);
					//					_theLiftingSurface.exportToXMLFile(_exportFile);
					break;
				}// end-of-switch
			}// end-of-handle-event
		});	

		_scrolledComposite.setContent(_ctrComposite);

		// must call these methods from child class of MyInitiatorPane
		_ctrComposite.pack();
		_scrolledComposite.pack();

		redrawDragPolar();

	}  //end of constructor

	private void adjustControls() {


		BigDecimal bd_01 = BigDecimal.valueOf( _theLiftingSurface.get_surface().getEstimatedValue());
		bd_01 = bd_01.setScale(3, RoundingMode.DOWN);
		_text_01.setText( "  " + bd_01 + " " +_theLiftingSurface.get_surface().getUnit().toString());
		_text_01.setEditable(true);	
		BigDecimal bd_02 = BigDecimal.valueOf( _theLiftingSurface.get_aspectRatio());
		bd_02 = bd_02.setScale(1, RoundingMode.DOWN);
		_text_02.setText( "  " + bd_02 );
		_text_02.setEditable(true);	
		BigDecimal bd_03 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioEquivalent());
		bd_03 = bd_03.setScale(2, RoundingMode.DOWN);
		_text_03.setText( "  " + bd_03);
		_text_03.setEditable(true);
		BigDecimal bd_04 = BigDecimal.valueOf(Math.toDegrees( _theLiftingSurface.get_sweepQuarterChordEq().getEstimatedValue()));
		bd_04 = bd_04.setScale(1, RoundingMode.HALF_UP);
		_text_04.setText( "  " + bd_04 + NonSI.DEGREE_ANGLE);
		_text_04.setEditable(true);
		BigDecimal bd_05 = BigDecimal.valueOf( _theLiftingSurface.get_spanStationKink());
		bd_05 = bd_05.setScale(2, RoundingMode.DOWN);
		_text_05.setText( "  " + bd_05);
		_text_05.setEditable(true);
		BigDecimal bd_06 = BigDecimal.valueOf( _theLiftingSurface.get_extensionLERootChordLinPanel());
		bd_06 = bd_06.setScale(2, RoundingMode.DOWN);
		_text_06.setText( "  " + bd_06);
		_text_06.setEditable(true);
		BigDecimal bd_07 = BigDecimal.valueOf( _theLiftingSurface.get_extensionTERootChordLinPanel());
		bd_07 = bd_07.setScale(2, RoundingMode.DOWN);
		_text_07.setText( "  " + bd_07);
		_text_07.setEditable(true);
		BigDecimal bd_07a = BigDecimal.valueOf( _theLiftingSurface.get_deltaXWingFus().getEstimatedValue());
		bd_07a = bd_07a.setScale(2, RoundingMode.DOWN);
		_text_07a.setText( "  " + bd_07a + " " + _theLiftingSurface.get_deltaXWingFus().getUnit());
		_text_07a.setEditable(true);
	}

	private void adjustOutputWingData(){


		BigDecimal bd_08 = BigDecimal.valueOf( _theLiftingSurface.get_surfaceCranked().getEstimatedValue());
		bd_08 = bd_08.setScale(3, RoundingMode.DOWN);
		_text_08.setText( "  " + bd_08 + " " +_theLiftingSurface.get_surfaceCranked().getUnit().toString());
		_text_08.setEditable(false);

		BigDecimal bd_08b = BigDecimal.valueOf( _theLiftingSurface.get_span().getEstimatedValue());
		bd_08b = bd_08b.setScale(2, RoundingMode.HALF_UP);
		_text_08b.setText( "  " + bd_08b + " " +_theLiftingSurface.get_span().getUnit().toString());
		_text_08b.setEditable(false);

		BigDecimal bd_09 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioActual());
		bd_09 = bd_09.setScale(3, RoundingMode.HALF_UP);
		_text_09.setText( "  " + bd_09 );
		_text_09.setEditable(false);

		BigDecimal bd_10= BigDecimal.valueOf( _theLiftingSurface.get_semiSurfaceInnerPanel().getEstimatedValue());
		bd_10= bd_10.setScale(3, RoundingMode.HALF_UP);
		_text_10.setText( "  " + bd_10+" "+_theLiftingSurface.get_semiSurfaceInnerPanel().getUnit().toString());
		_text_10.setEditable(false);

		BigDecimal bd_10b= BigDecimal.valueOf( _theLiftingSurface.get_aspectRatioInnerPanel());
		bd_10b= bd_10b.setScale(2, RoundingMode.HALF_UP);
		_text_10b.setText( "  " + bd_10b);
		_text_10b.setEditable(false);

		BigDecimal bd_11 = BigDecimal.valueOf(_theLiftingSurface.get_semiSurfaceOuterPanel().getEstimatedValue());
		bd_11 = bd_11.setScale(3, RoundingMode.HALF_UP);
		_text_11.setText( "  " + bd_11 +" "+_theLiftingSurface.get_semiSurfaceOuterPanel().getUnit().toString());
		_text_11.setEditable(false);

		BigDecimal bd_11b= BigDecimal.valueOf( _theLiftingSurface.get_aspectRatioOuterPanel());
		bd_11b= bd_11b.setScale(2, RoundingMode.HALF_UP);
		_text_11b.setText( "  " + bd_11b);
		_text_11b.setEditable(false);

		BigDecimal bd_12 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioInnerPanel());
		bd_12 = bd_12.setScale(3, RoundingMode.HALF_UP);
		_text_12.setText( "  " + bd_12 );
		_text_12.setEditable(false);

		BigDecimal bd_13 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioOuterPanel());
		bd_13 = bd_13.setScale(3, RoundingMode.HALF_UP);
		_text_13.setText( "  " + bd_13 );
		_text_13.setEditable(false);

		BigDecimal bd_14 = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepLEInnerPanel().getEstimatedValue()));
		bd_14 = bd_14.setScale(1, RoundingMode.HALF_UP);
		_text_14.setText( "  " + bd_14+NonSI.DEGREE_ANGLE);
		_text_14.setEditable(false);
		_text_14.setEditable(false);

		BigDecimal bd_14b = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepQuarterChordInnerPanel().getEstimatedValue()));
		bd_14b = bd_14b.setScale(1, RoundingMode.HALF_UP);
		_text_14b.setText( "  " + bd_14b+NonSI.DEGREE_ANGLE);
		_text_14b.setEditable(false);
		_text_14b.setEditable(false);

		BigDecimal bd_15 = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepLEOuterPanel().getEstimatedValue()));
		bd_15 = bd_15.setScale(1, RoundingMode.HALF_UP);
		_text_15.setText( "  " + bd_15+NonSI.DEGREE_ANGLE);
		_text_15.setEditable(false);

		BigDecimal bd_15b = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepQuarterChordOuterPanel().getEstimatedValue()));
		bd_15b = bd_15b.setScale(1, RoundingMode.HALF_UP);
		_text_15b.setText( "  " + bd_15b+NonSI.DEGREE_ANGLE);
		_text_15b.setEditable(false);
		_text_15b.setEditable(false);

		BigDecimal bd_16b = BigDecimal.valueOf( _theLiftingSurface.get_chordRootEquivalentWing().getEstimatedValue());
		bd_16b = bd_16b.setScale(3, RoundingMode.HALF_UP);
		_text_16b.setText( "  " + bd_16b+" "+_theLiftingSurface.get_chordRootEquivalentWing().getUnit().toString());
		_text_16b.setEditable(false);

		BigDecimal bd_16 = BigDecimal.valueOf( _theLiftingSurface.get_chordRoot().getEstimatedValue());
		bd_16 = bd_16.setScale(3, RoundingMode.HALF_UP);
		_text_16.setText( "  " + bd_16+" "+_theLiftingSurface.get_chordRoot().getUnit().toString());
		_text_16.setEditable(false);

		BigDecimal bd_17 = BigDecimal.valueOf( _theLiftingSurface.get_xLERoot().getEstimatedValue());
		bd_17 = bd_17.setScale(3, RoundingMode.HALF_UP);
		_text_17.setText( "  " + bd_17+" "+_theLiftingSurface.get_xLERoot().getUnit().toString());
		_text_17.setEditable(false);

		BigDecimal bd_18 = BigDecimal.valueOf( _theLiftingSurface.get_chordKink().getEstimatedValue());
		bd_18 = bd_18.setScale(3, RoundingMode.HALF_UP);
		_text_18.setText( "  " + bd_18+" "+_theLiftingSurface.get_chordKink().getUnit().toString());
		_text_18.setEditable(false);

		BigDecimal bd_19 = BigDecimal.valueOf( _theLiftingSurface.get_xLEKink().getEstimatedValue());
		bd_19 = bd_19.setScale(3, RoundingMode.HALF_UP);
		_text_19.setText( "  " + bd_19+" "+_theLiftingSurface.get_xLEKink().getUnit().toString());
		_text_19.setEditable(false);

		BigDecimal bd_20 = BigDecimal.valueOf( _theLiftingSurface.get_chordTip().getEstimatedValue());
		bd_20 = bd_20.setScale(3, RoundingMode.HALF_UP);
		_text_20.setText( "  " + bd_20+" "+_theLiftingSurface.get_chordTip().getUnit().toString());
		_text_20.setEditable(false);

		BigDecimal bd_21 = BigDecimal.valueOf( _theLiftingSurface.get_xLETip().getEstimatedValue());
		bd_21 = bd_21.setScale(3, RoundingMode.HALF_UP);
		_text_21.setText( "  " + bd_21+" "+_theLiftingSurface.get_xLETip().getUnit().toString());
		_text_21.setEditable(false);

		BigDecimal bd_22 = BigDecimal.valueOf( _theLiftingSurface.get_geomChordEq().getEstimatedValue());
		bd_22 = bd_22.setScale(2, RoundingMode.HALF_UP);
		_text_22.setText( "  " + bd_22+" "+_theLiftingSurface.get_geomChordEq().getUnit().toString());
		_text_22.setEditable(false);

		BigDecimal bd_23 = BigDecimal.valueOf( _theLiftingSurface.get_meanAerodChordEq().getEstimatedValue());
		bd_23 = bd_23.setScale(2, RoundingMode.HALF_UP);
		_text_23.setText( "  " + bd_23+" "+_theLiftingSurface.get_meanAerodChordEq().getUnit().toString());
		_text_23.setEditable(false);

		BigDecimal bd_24 = BigDecimal.valueOf( _theLiftingSurface.get_x_LE_Mac_Eq().getEstimatedValue());
		bd_24 = bd_24.setScale(2, RoundingMode.HALF_UP);
		_text_24.setText( "  " + bd_24+" "+_theLiftingSurface.get_x_LE_Mac_Eq().getUnit().toString());
		_text_24.setEditable(false);

		BigDecimal bd_25 = BigDecimal.valueOf( _theLiftingSurface.get_y_LE_Mac_Eq().getEstimatedValue());
		bd_25 = bd_25.setScale(2, RoundingMode.HALF_UP);
		_text_25.setText( "  " + bd_25+" "+_theLiftingSurface.get_y_LE_Mac_Eq().getUnit().toString());
		_text_25.setEditable(false);

		BigDecimal bd_26 = BigDecimal.valueOf( _theLiftingSurface.get_y_LE_Mac_Eq().getEstimatedValue()/(0.5*_theLiftingSurface.get_span().getEstimatedValue()));
		bd_26 = bd_26.setScale(3, RoundingMode.HALF_UP);
		_text_26.setText( "  " + bd_26);
		_text_26.setEditable(false);

		BigDecimal bd_27 = BigDecimal.valueOf( _theLiftingSurface.get_meanAerodChordActual().getEstimatedValue());
		bd_27 = bd_27.setScale(2, RoundingMode.HALF_UP);
		_text_27.setText( "  " + bd_27+" "+_theLiftingSurface.get_meanAerodChordActual().getUnit().toString());
		_text_27.setEditable(false);

		BigDecimal bd_28 = BigDecimal.valueOf( _theLiftingSurface.get_xLEMacActualLRF().getEstimatedValue());
		bd_28 = bd_28.setScale(2, RoundingMode.HALF_UP);
		_text_28.setText( "  " + bd_28+" "+_theLiftingSurface.get_xLEMacActualLRF().getUnit().toString());
		_text_28.setEditable(false);

		BigDecimal bd_29 = BigDecimal.valueOf( _theLiftingSurface.get_yLEMacActualLRF().getEstimatedValue());
		bd_29 = bd_29.setScale(2, RoundingMode.HALF_UP);
		_text_29.setText( "  " + bd_29+" "+_theLiftingSurface.get_yLEMacActualLRF().getUnit().toString());
		_text_29.setEditable(false);

		BigDecimal bd_30 = BigDecimal.valueOf( _theLiftingSurface.get_yLEMacActualLRF().getEstimatedValue()/(0.5*_theLiftingSurface.get_span().getEstimatedValue()));
		bd_30 = bd_30.setScale(3, RoundingMode.HALF_UP);
		_text_30.setText( "  " + bd_30);
		_text_30.setEditable(false);

	}// end of constructor

	public void redrawDragPolar(){

		Double[] CD = _theAerodynamics.get_cD();
		Double[] CL = _theAerodynamics.get_cL();

		//		// Cranked Wing Parameters
		//		Amount<Length> span =_theLiftingSurface.get_span();
		//		Double eta_K = _theLiftingSurface.get_spanStationKink();
		//		Amount<Length> x_LE_Root= _theLiftingSurface.get_xLERoot();
		//		Amount<Length> c_root=_theLiftingSurface.get_chordRoot();	
		//		Amount<Length> x_TE_Root = Amount.valueOf(x_LE_Root.getEstimatedValue() +c_root.getEstimatedValue(), SI.METER);
		//		Amount<Length> x_LE_Kink=_theLiftingSurface.get_xLEKink();
		//		Amount<Length> c_kink=_theLiftingSurface.get_chordKink();	
		//		Amount<Length> x_TE_Kink = Amount.valueOf(x_LE_Kink .getEstimatedValue() +c_kink.getEstimatedValue(), SI.METER);
		//		Amount<Length> x_LE_Tip=_theLiftingSurface.get_xLETip();
		//		Amount<Length> c_Tip=_theLiftingSurface.get_chordTip();	
		//		Amount<Length> x_TE_Tip = Amount.valueOf(x_LE_Tip .getEstimatedValue() +c_Tip.getEstimatedValue(), SI.METER);
		//		Amount<Length> x_LE_MAC_Ck = Amount.valueOf(_theLiftingSurface.get_x_LE_Mac_Ck().doubleValue(SI.METER),SI.METER);
		//		Amount<Length> y_LE_MAC_Ck = Amount.valueOf(_theLiftingSurface.get_y_LE_Mac_Ck().doubleValue(SI.METER),SI.METER);
		//		Amount<Length> mac_ck = Amount.valueOf(_theLiftingSurface.get_meanAerodChordCk().doubleValue(SI.METER),SI.METER);
		//
		//		// Wing Cranked Coordinates
		//
		//		Double[] y_Cranked= {x_LE_Root.doubleValue(SI.METER), eta_K*(0.5*span.doubleValue(SI.METER)), 0.5*span.doubleValue(SI.METER), 
		//				0.5*span.doubleValue(SI.METER),eta_K*(0.5*span.doubleValue(SI.METER)),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER)};
		//
		//		ArrayList<Double> y_Cranked_Wing = new ArrayList<Double>(Arrays.asList(y_Cranked));
		//		Double[] x_Cranked= {x_LE_Root.doubleValue(SI.METER),  x_LE_Kink.doubleValue(SI.METER), x_LE_Tip.doubleValue(SI.METER),
		//				x_TE_Tip.doubleValue(SI.METER),x_TE_Kink.doubleValue(SI.METER) ,x_TE_Root.doubleValue(SI.METER),x_TE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER) };
		//
		//		ArrayList<Double> x_Cranked_Wing = new ArrayList<Double>(Arrays.asList(x_Cranked));
		//
		//		Double[] y_Cranked_Mac = {y_LE_MAC_Ck.doubleValue(SI.METER),y_LE_MAC_Ck.doubleValue(SI.METER)};
		//		ArrayList<Double> y_Cranked_Wing_Mac = new ArrayList<Double>(Arrays.asList(y_Cranked_Mac));
		//
		//		Double[] x_Cranked_Mac = {x_LE_MAC_Ck.doubleValue(SI.METER),x_LE_MAC_Ck.doubleValue(SI.METER)+mac_ck.doubleValue(SI.METER)};
		//		ArrayList<Double> x_Cranked_Wing_Mac = new ArrayList<Double>(Arrays.asList(x_Cranked_Mac));

		// initial cleanup
		if ( !_traces_XY.isEmpty()  ) {
			for(int i=0; i<_traces_XY.size(); i++){
				this._xyGraphXY.removeTrace( _traces_XY.get(i) ); // 0: Cranked, 1:Mac_ck , 2: Equivalent
			}
		}
		this._tdpXY.clear();
		this._traces_XY.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		_tdpXY.get(0).setCurrentXDataArray(Doubles.toArray(Arrays.asList(CD)));
		_tdpXY.get(0).setCurrentYDataArray(Doubles.toArray(Arrays.asList(CL)));
		_traces_XY.add(
				new Trace("Drag Polar",					
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(0)
						)
				);			

		//set trace property
		_traces_XY.get(0).setPointStyle(PointStyle.POINT);
		_traces_XY.get(0).setPointSize(5);
		_traces_XY.get(0).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_XY.get(0).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(0) );

		// Set Property of axis
		_xyGraphXY.setTitle("Drag Polar");
		_xyGraphXY.primaryYAxis.setTitle("CL");
		_xyGraphXY.primaryXAxis.setTitle("CD");
		_xyGraphXY.primaryYAxis.setRange(0, 1.5);
		_xyGraphXY.primaryXAxis.setRange(0, 0.10);

	}	// end of redrawDragPolar


	public void chordDistributionEq(){

		ArrayList<Double> Chords_eq= new ArrayList<Double>();
		ArrayList<Double> Chords_ck= new ArrayList<Double>();

		for (int i=0;i<_theLiftingSurface.get_eta().size();i++){

			Chords_ck.add(_theLiftingSurface.get_chordsActualVsYList().get(i).doubleValue(SI.METER)); 
			Chords_eq.add(_theLiftingSurface.get_chordsEqList().get(i).doubleValue(SI.METER)); 
		}



		// initial cleanup
		if ( !_traces_YZ.isEmpty()  ) {
			for(int i=0; i<_traces_YZ.size(); i++){
				this._xyGraphYZ.removeTrace( _traces_YZ.get(i) ); // 0: Cranked, 1: Equivalent
			}
		}
		this._tdpYZ.clear();
		this._traces_YZ.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpYZ.add( new CircularBufferDataProvider(false) );
		_tdpYZ.get(0).setCurrentXDataArray(Doubles.toArray(_theLiftingSurface.get_eta()));
		_tdpYZ.get(0).setCurrentYDataArray(Doubles.toArray(Chords_ck));
		_traces_YZ.add(
				new Trace("Cranked Wing",					
						_xyGraphYZ.primaryXAxis, _xyGraphYZ.primaryYAxis, 
						_tdpYZ.get(0)
						)
				);			

		//set trace property
		_traces_YZ.get(0).setPointStyle(PointStyle.POINT);
		_traces_YZ.get(0).setPointSize(5);
		_traces_YZ.get(0).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_YZ.get(0).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphYZ.addTrace( _traces_YZ.get(0) );

		// add a new trace data provider for Upper outline
		_tdpYZ.add( new CircularBufferDataProvider(false) );
		_tdpYZ.get(1).setCurrentXDataArray(Doubles.toArray(_theLiftingSurface.get_eta()));
		_tdpYZ.get(1).setCurrentYDataArray(Doubles.toArray(Chords_eq));
		_traces_YZ.add(
				new Trace("EquivalentWing",					
						_xyGraphYZ.primaryXAxis, _xyGraphYZ.primaryYAxis, 
						_tdpYZ.get(1)
						)
				);			

		//set trace property
		_traces_YZ.get(1).setPointStyle(PointStyle.POINT);
		_traces_YZ.get(1).setPointSize(5);
		_traces_YZ.get(1).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_RED)
				);
		_traces_YZ.get(1).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphYZ.addTrace( _traces_YZ.get(1) );

		//				Set Property of axis
		_xyGraphYZ.setTitle("Chord Distribution");
		_xyGraphYZ.primaryYAxis.setTitle("Chord (m)");
		_xyGraphYZ.primaryXAxis.setTitle("eta");
		_xyGraphYZ.primaryYAxis.setRange(0,Chords_ck.get(0)+1);
		_xyGraphYZ.primaryXAxis.setRange(0,1);

	} // end of chordDistributionEq

} //end of class





