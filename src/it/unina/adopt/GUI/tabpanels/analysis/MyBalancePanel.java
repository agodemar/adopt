package it.unina.adopt.GUI.tabpanels.analysis;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;

import aircraft.calculators.ACAerodynamicsManager;
import aircraft.components.Aircraft;
import aircraft.components.fuselage.Fuselage;
import aircraft.components.liftingSurface.LiftingSurface;
import configuration.MyConfiguration;
import it.unina.adopt.GUI.tabpanels.MyPanel;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.MyNomenclatureImageDialog;
import it.unina.adopt.utilities.gui.MyGuiUtils;
import it.unina.adopt.utilities.gui.MyGuiUtils.PlotFactory;
import standaloneutils.MyArrayUtils;


public class MyBalancePanel extends MyPanel {

	// FILE WING TEST
	String ANALYSIS_FILE_NAME = "analysis.xml";
	String analysisFilePath = "test" + File.separator + ANALYSIS_FILE_NAME;
	File analysisFile = new File(analysisFilePath);
	//	
	private final String EXPORT_FILE_NAME = "wing_initiator.xml";
	private String _exportFilePath =
			MyConfiguration.currentDirectory + File.separator + EXPORT_FILE_NAME;

	private LiftingSurface _theLiftingSurface = null;
	private Fuselage _theFuselage = null;
	private ACAerodynamicsManager _theCalculator;
	private Aircraft _theAircraft;

	Combo _comboDropDown = null;
	Combo _comboDropDownSectionData = null;

	private Text 
	_text_01;
	private final int TEXT_WIDTH_HINT = 50;

	private Slider 
	_slider_01,	_slider_02,	_slider_03,	_slider_04,	_slider_05,
	_slider_06,	_slider_07,	_slider_08,	_slider_09,	_slider_10,
	_slider_11,	_slider_12;

	private File _importFile;
	private Text _text_ImportFile;
	private Button _button_ChooseImportFile, _button_ImportFile;

	private File _exportFile;
	private Text _text_ExportFile;
	private Button _button_ExportFile;
	private Button _button_Nomenclature;
	private Button _button_Eq_Wing;

	private Double sweepQuarterChordEquivalentWingDeg = null;
	// images etc
	protected static Map<String, Image> fCachedImages = new TreeMap<String, Image>();

	private MyNomenclatureImageDialog _nomenclatureWing = null;
	private PlotFactory _plotFactory;


	/**
	 * 
	 * @param parent
	 * @param tabItem
	 * @param style
	 * @param aircraft
	 */
	public MyBalancePanel(
			Composite parent, 
			CTabItem tabItem, 
			int style, 
			Aircraft aircraft) {

		super(parent, tabItem, style);

		populateThisPanel();

	}  

	private void adjustControls() {

	}

	public void populateThisPanel() {

		_theAircraft = GlobalData.getTheCurrentAircraft();

		ADOPT_GUI.getApp().set_theBalancePanel(this); // make the App know about the object
		_plotFactory = new MyGuiUtils.PlotFactory();

		
		// the outer grid layout
		GridLayout layoutCtr = new GridLayout(8,false);
		GridData gridDataCtr = new GridData(SWT.FILL, SWT.FILL, true, false);
		_ctrComposite.setLayoutData(gridDataCtr);
		_ctrComposite.setLayout(layoutCtr);

		_scrolledComposite.setContent(_ctrComposite);

		// must call these methods from child class of MyInitiatorPane
		_ctrComposite.pack();
		_scrolledComposite.pack();

		redrawLoading();

	}

	public void redrawLoading(){

		List<Double[]> xList = new ArrayList<Double[]>();
		List<Double[]> yList = new ArrayList<Double[]>();

		xList.add(MyArrayUtils.convertListOfAmountToDoubleArray(
						_theAircraft.get_configuration().get_seatsCoGFrontToRear()
						));

		xList.add(
				MyArrayUtils.convertListOfAmountToDoubleArray(
						_theAircraft.get_configuration().get_seatsCoGRearToFront()
						));

		yList.add(
				MyArrayUtils.convertListOfAmountToDoubleArray(
						_theAircraft.get_configuration().get_currentMassList()
						));

		yList.add(
				MyArrayUtils.convertListOfAmountToDoubleArray(
						_theAircraft.get_configuration().get_currentMassList()
						));

		_plotFactory.newTabPlot(xList, yList,
				"Loading", "Loading cycle",
				"Xcg", "Weight",
				"m", "kg", 
				_tabFolderPlots);
		
		// force first tab selection to get canvas painted
		_tabFolderPlots.setSelection(0);
		_tabFolderPlots.forceFocus();

		_ctrComposite.pack();
		_scrolledComposite.pack();
	}


	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}



	public void set_theAircraft(Aircraft _theAircraft) {
		this._theAircraft = _theAircraft;
	}


} //end of class