package it.unina.adopt.GUI.tabpanels.fuselage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.measure.DecimalMeasure;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.nebula.visualization.xygraph.dataprovider.CircularBufferDataProvider;
import org.eclipse.nebula.visualization.xygraph.dataprovider.ISample;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.PointStyle;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.TraceType;
import org.eclipse.nebula.visualization.xygraph.figures.XYGraph;
import org.eclipse.nebula.visualization.xygraph.linearscale.Range;
import org.eclipse.nebula.visualization.xygraph.util.XYGraphMediaFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.jscience.physics.amount.Amount;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.primitives.Doubles;
import com.rits.cloning.Cloner;

import aircraft.components.fuselage.Fuselage;
import aircraft.components.fuselage.MyFuselageAdjustCriteria;
import aircraft.components.fuselage.MyFuselageCurvesSection;
import aircraft.components.fuselage.MyFuselageCurvesSideView;
import aircraft.components.fuselage.MyFuselageCurvesUpperView;
import aircraft.components.fuselage.MyFuselageSectionData;
import configuration.MyConfiguration;
import it.unina.adopt.GUI.dialogs.MyDialogFuselageSection;
import it.unina.adopt.GUI.tabpanels.MyPanel;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.MyNomenclatureImageDialog;
import it.unina.adopt.utilities.gui.GridLayoutUtil;
import it.unina.adopt.utilities.gui.MyGuiUtils;
import it.unina.adopt.utilities.write.MyDataWriter;

/**
 * @author Agodemar
 *
 */
public class MyFuselagePanel extends MyPanel {

	private final String EXPORT_FILE_NAME = "fuselage_initiator.xml";
	private String _exportFilePath =
			MyConfiguration.currentDirectory+ File.separator + EXPORT_FILE_NAME;

	private MyDataWriter myUtilities;
	private Fuselage _theFuselage = null;

	private MyFuselageCurvesSideView  _fuselageCurvesSideView  = null;
	private MyFuselageCurvesUpperView _fuselageCurvesUpperView = null;
	private MyFuselageCurvesSection   _fuselageCurvesSection   = null;

	private int _np_N = 10, _np_C = 4, _np_T = 10; 
	private int _np_SecUp = 10, _np_SecLow = 10;

	private MyFuselageAdjustCriteria _selectedAdjustCriterion = MyFuselageAdjustCriteria.NONE;
	private MyFuselageSectionData  _selectedSectionData = MyFuselageSectionData.CYLINDRICAL_BODY_1;
	private MyDialogFuselageSection _dialogShellSectionShape = null;
	private MyNomenclatureImageDialog _nomenclatureFuselageSection = null;

	Combo _comboDropDown = null;
	Combo _comboDropDownSectionData = null;

	// Standard API map
	// Map <MyFuselageAdjustCriteria,String> _mapAdjustCriteria = new HashMap<MyFuselageAdjustCriteria,String>();

	// rely on Guava
	private BiMap <MyFuselageAdjustCriteria,String> _mapAdjustCriteria = null;
	private BiMap <MyFuselageSectionData,String> _mapSectionData= null;

	// controls

	private Text 
	_text01, _text02, _text_03, _text04, _text_05,
	_text_06, _text_07,	_text_08, _text_09,	_text_10,
	_text_11, _text_12,_text_13,_text_14;
	private final int TEXT_WIDTH_HINT = 50;

	private Slider 
	_slider01,	_slider02,	_slider_03,	_slider04,	_slider_05,
	_slider_06,	_slider_07,	_slider_08,	_slider_09,	_slider_10,
	_slider_11,	_slider_12;

	private File _importFile;
	private Text _text_ImportFile;
	private Button _button_ChooseImportFile, _button_ImportFile;

	private File _exportFile;
	private Text _text_ExportFile;
	private Button _button_ExportFile;
	private Button _button_Nomenclature;

	// Section Shape parameters
	private Button _button_SectionShape;

	// images etc
	protected static Map<String, Image> fCachedImages = new TreeMap<String, Image>();


	public MyFuselagePanel(Composite parent, int style) {
		super(parent, style);
		initialize();
	}

	/**
	 * @param _parent
	 * @param tabItem
	 * @param style
	 */
	public MyFuselagePanel(
			Composite parent, 
			CTabItem tabItem, 
			int style, 
			Fuselage fuselage) {

		// constructor of MyPanel
		super(parent, tabItem, style);

		// make the App know about the object
		ADOPT_GUI.getApp().setFuselagePanel(this);

		// Instantiate the Fuselage object
		if (fuselage != null) {
			_theFuselage = fuselage;			

		} else {
			//			_theFuselage = new MyFuselage();
		}		
		
		initialize();
		
	}// end-of-constructor
	
	
	private void initialize() {

		_selectedAdjustCriterion =_theFuselage.getAdjustCriterion();

		// Create plots
		_tabItemXZView = new CTabItem(_tabFolderPlots, SWT.NULL);
		_fcXZ = new FigureCanvas(_tabFolderPlots);
		_lwsXZ = new LightweightSystem(_fcXZ);
		_xyGraphXZ = new XYGraph();

		_tabItemXYView = new CTabItem(_tabFolderPlots, SWT.NULL);
		_fcXY = new FigureCanvas(_tabFolderPlots);
		_lwsXY = new LightweightSystem(_fcXY);
		_xyGraphXY = new XYGraph();

		_tabItemYZView = new CTabItem(_tabFolderPlots, SWT.NULL);
		_fcYZ = new FigureCanvas(_tabFolderPlots);
		_lwsYZ = new LightweightSystem(_fcYZ);
		_xyGraphYZ = new XYGraph();

		MyGuiUtils.createInnerTabWithGraph(
				_tabItemXZView, _fcXZ, _lwsXZ, 
				_xyGraphXZ, "XZ View");

		MyGuiUtils.createInnerTabWithGraph(_tabItemXYView, _fcXY, _lwsXY, 
				_xyGraphXY, "XY View");

		MyGuiUtils.createInnerTabWithGraph(_tabItemYZView, _fcYZ, _lwsYZ, 
				_xyGraphYZ, "YZ View");

		// force first tab selection to get canvas painted
		_tabFolderPlots.setSelection(0);
		_tabFolderPlots.forceFocus();

		_ctrComposite.pack();
		_scrolledComposite.pack();

		//////////////////////////////////////////////////////

		// the outer grid layout
		GridLayout layoutCtr = new GridLayout(3, false);
		GridData gridDataCtr = new GridData(SWT.FILL, SWT.FILL, true, false);
		_ctrComposite.setLayoutData(gridDataCtr);
		_ctrComposite.setLayout(layoutCtr);

		arrangeControls();
		addListeners();
		

		//  End Parameter of variation : l_F,l_C,l_N,l_T,diam_C
		//  End Implementation of method adjustLength
		//  End Implementation of adjust controls
		//  End Redraw


		_scrolledComposite.setContent(_ctrComposite);

		// must call these methods from child class of MyPanel
		_ctrComposite.pack();
		_scrolledComposite.pack();

		// the XZ view
		// force range settings
		_xyGraphXZ.primaryXAxis.setRange(
				new Range(
						-0.2*_theFuselage.get_len_F().getEstimatedValue(),
						1.2*_theFuselage.get_len_F().getEstimatedValue())
				);
		_xyGraphXZ.primaryYAxis.setRange(
				new Range(
						-0.6*_theFuselage.get_len_F().getEstimatedValue(),
						0.6*_theFuselage.get_len_F().getEstimatedValue())
				);
		redrawFuselageXZ();

		// the XY view
		// force range settings
		_xyGraphXY.primaryXAxis.setRange(
				new Range(
						-0.2*_theFuselage.get_len_F().getEstimatedValue(),
						1.2*_theFuselage.get_len_F().getEstimatedValue())
				);
		_xyGraphXY.primaryYAxis.setRange(
				new Range(
						-0.6*_theFuselage.get_len_F().getEstimatedValue(),
						0.6*_theFuselage.get_len_F().getEstimatedValue())
				);
		redrawFuselageXY();

		// the YZ view
		// force range settings
		_xyGraphYZ.primaryXAxis.setRange(
				new Range(
						-0.85*_theFuselage.get_sectionCylinderWidth().getEstimatedValue(),
						0.85*_theFuselage.get_sectionCylinderWidth().getEstimatedValue())
				);
		_xyGraphYZ.primaryYAxis.setRange(
				new Range(
						-0.85*_theFuselage.get_sectionCylinderWidth().getEstimatedValue(),
						0.85*_theFuselage.get_sectionCylinderWidth().getEstimatedValue())
				);

		redrawFuselageSectionYZ();

		//----------------------------------------------------
		// EVENTS-XY-GRAPH
		//----------------------------------------------------
		_xyGraphXY.setFocusTraversable(true);
		_xyGraphXY.setRequestFocusEnabled(true);
		_xyGraphXY.getPlotArea().addMouseListener(new MouseListener.Stub(){
			@Override
			public void mousePressed(final MouseEvent me) {
				_xyGraphXY.requestFocus();

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The ADOpT | MOUSE EVENT \n"
						);
			}
		}); 

		// http://www.ingegno.it/programming/eclipse/eclipse-draw2d/

		//		RectangleFigure f = new RectangleFigure();
		//		f.setBackgroundColor(ColorConstants.lightGreen);
		//		f.setLayoutManager( new ToolbarLayout() );
		//		f.setPreferredSize( 100, 100 );
		//
		//		Graphics graphics = _fc_XZ;
		//		
		//		_xyGraph_XZ. .getPlotArea().add(
		//				f, 
		//				new Rectangle( new Point(150, 150), f.getPreferredSize() ) 
		//				);



		//		Polyline polyline = new Polyline();
		//		IFigure panel = new Figure();
		//		
		//		polyline.setStart(new Point( 5, 5));
		//		polyline.addPoint(new Point( 5, 45));
		//		polyline.addPoint(new Point( 145, 145));
		//		polyline.addPoint(new Point( 45, 5));
		//		panel.add(polyline);
		//		IFigure xxx = _xyGraph_XZ.getPlotArea();
		//		xxx.add(panel);


		// Java Code Examples for org.eclipse.draw2d.geometry.PointList

		//		MouseListener mouseListenerTrace0 = new MouseListener.Stub() {
		//			@Override
		//			public void mousePressed(final MouseEvent me) {
		//				_xyGraph_XZ.requestFocus();
		//				
		//				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
		//						"The ADOpT | MOUSE ON TRACE EVENT \n"
		//						);
		//			}
		//		};
		//		
		//		Clickable c = new Clickable( _traces_XZ.get(0) );
		//		c.addActionListener((ActionListener) mouseListenerTrace0);

		//		_traces_XZ.get(0).setFocusTraversable(true);
		//		_traces_XZ.get(0).setRequestFocusEnabled(true);
		//		_traces_XZ.get(0).addMouseListener(new MouseListener.Stub(){
		//			@Override
		//			public void mousePressed(final MouseEvent me) {
		//				_xyGraph_XZ.requestFocus();
		//				
		//				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
		//						"The ADOpT | TRACE 0 | MOUSE EVENT \n"
		//						);				    	
		//			}
		//		}); 



		// resize listener
		_tabFolderPlots.addListener(SWT.Resize, new Listener () {
			public void handleEvent (Event e) {

				org.eclipse.swt.graphics.Rectangle rect = _tabFolderPlots.getClientArea();

				//System.out.println(rect);

				double w = rect.width;
				double h = rect.height;

				scaleAxesXZEqual(w, h);
				scaleAxesXYEqual(w, h);
				scaleAxesYZEqual(w, h);
			} // end-of-event
		});
	}



	private void recalculateCurves()
	{
		// get variables

		Double l_F = _theFuselage.get_len_F().getEstimatedValue();
		Double l_N = _theFuselage.get_len_N().getEstimatedValue();
		Double l_C = _theFuselage.get_len_C().getEstimatedValue();
		Double l_T = _theFuselage.get_len_T().getEstimatedValue();
		Double d_C = _theFuselage.get_sectionCylinderHeight().getEstimatedValue();
		Double h_N = _theFuselage.get_height_N().getEstimatedValue(); // Fuselage origin O_T at nose (>0, when below the cylindrical midline)
		Double h_T = _theFuselage.get_height_T().getEstimatedValue();
		Double w_B = _theFuselage.get_sectionCylinderWidth().getEstimatedValue();
		Double a   = _theFuselage.get_sectionLowerToTotalHeightRatio();
		Double rhoUpper = _theFuselage.get_sectionCylinderRhoUpper();
		Double rhoLower = _theFuselage.get_sectionCylinderRhoLower();

		// clean all points before recalculating
		_theFuselage.clearOutlines();

		//------------------------------------------------
		// XZ VIEW -- Side View
		//------------------------------------------------

		_fuselageCurvesSideView = null;
		_fuselageCurvesSideView = new MyFuselageCurvesSideView(
				l_N, h_N, l_C, l_F, h_T, d_C/2,a, // lengths 
				_np_N, _np_C, _np_T        // no. points (nose, cylinder, tail)
				);

		// UPPER CURVES ----------------------------------

		// UPPER NOSE
		for (int i=0; i<=_fuselageCurvesSideView.getNoseUpperPoints().size()-1; i++){
			_theFuselage.getOutlineXZUpperCurveX().add((double) _fuselageCurvesSideView.getNoseUpperPoints().get(i).x);
			_theFuselage.getOutlineXZUpperCurveZ().add((double) _fuselageCurvesSideView.getNoseUpperPoints().get(i).y);
		}

		// UPPER CYLINDER
		for (int i=0; i<=_fuselageCurvesSideView.getCylinderUpperPoints().size()-1; i++){
			_theFuselage.getOutlineXZUpperCurveX().add((double) _fuselageCurvesSideView.getCylinderUpperPoints().get(i).x);
			_theFuselage.getOutlineXZUpperCurveZ().add((double) _fuselageCurvesSideView.getCylinderUpperPoints().get(i).y);
		}

		// UPPER TAIL
		for (int i=0; i<=_fuselageCurvesSideView.getTailUpperPoints().size()-1; i++){
			_theFuselage.getOutlineXZUpperCurveX().add((double) _fuselageCurvesSideView.getTailUpperPoints().get(i).x);
			_theFuselage.getOutlineXZUpperCurveZ().add((double) _fuselageCurvesSideView.getTailUpperPoints().get(i).y);
		}

		// LOWER CURVES ----------------------------------

		// LOWER NOSE
		for (int i=0; i<=_fuselageCurvesSideView.getNoseLowerPoints().size()-1; i++){
			_theFuselage.getOutlineXZLowerCurveX().add((double) _fuselageCurvesSideView.getNoseLowerPoints().get(i).x);
			_theFuselage.getOutlineXZLowerCurveZ().add((double) _fuselageCurvesSideView.getNoseLowerPoints().get(i).y);
		}

		// LOWER CYLINDER
		for (int i=0; i<=_fuselageCurvesSideView.getCylinderLowerPoints().size()-1; i++){
			_theFuselage.getOutlineXZLowerCurveX().add((double) _fuselageCurvesSideView.getCylinderLowerPoints().get(i).x);
			_theFuselage.getOutlineXZLowerCurveZ().add((double) _fuselageCurvesSideView.getCylinderLowerPoints().get(i).y);
		}

		// LOWER TAIL
		for (int i=0; i<=_fuselageCurvesSideView.getTailLowerPoints().size()-1; i++){
			//	
			_theFuselage.getOutlineXZLowerCurveX().add((double) _fuselageCurvesSideView.getTailLowerPoints().get(i).x);
			_theFuselage.getOutlineXZLowerCurveZ().add((double) _fuselageCurvesSideView.getTailLowerPoints().get(i).y);
		}

		//------------------------------------------------
		// XY VIEW -- Upper View
		//------------------------------------------------
		_fuselageCurvesUpperView = null;
		_fuselageCurvesUpperView = new MyFuselageCurvesUpperView(
				l_N, l_C, l_F, w_B/2, // lengths 
				_np_N, _np_C, _np_T        // no. points (nose, cylinder, tail)
				);

		// RIGHT CURVE -----------------------------------

		// RIGHT NOSE
		for (int i=0; i<=_fuselageCurvesUpperView.getNoseUpperPoints().size()-1; i++){
			_theFuselage.getOutlineXYSideRCurveX().add((double) _fuselageCurvesUpperView.getNoseUpperPoints().get(i).x);
			_theFuselage.getOutlineXYSideRCurveY().add((double) _fuselageCurvesUpperView.getNoseUpperPoints().get(i).y);
		}

		// RIGHT CYLINDER
		for (int i=0; i<=_fuselageCurvesUpperView.getCylinderUpperPoints().size()-1; i++){
			_theFuselage.getOutlineXYSideRCurveX().add((double) _fuselageCurvesUpperView.getCylinderUpperPoints().get(i).x);
			_theFuselage.getOutlineXYSideRCurveY().add((double) _fuselageCurvesUpperView.getCylinderUpperPoints().get(i).y);
		}

		// RIGHT TAIL
		for (int i=0; i<=_fuselageCurvesUpperView.getTailUpperPoints().size()-1; i++){
			_theFuselage.getOutlineXYSideRCurveX().add((double) _fuselageCurvesUpperView.getTailUpperPoints().get(i).x);
			_theFuselage.getOutlineXYSideRCurveY().add((double) _fuselageCurvesUpperView.getTailUpperPoints().get(i).y);
		}

		// LEFT CURVE (mirror)----------------------------------
		for (int i = 0; i < _theFuselage.getOutlineXYSideRCurveX().size(); i++){
			//	
			_theFuselage.getOutlineXYSideLCurveX().add(  _theFuselage.getOutlineXYSideRCurveX().get(i) );
			_theFuselage.getOutlineXYSideLCurveY().add( -_theFuselage.getOutlineXYSideRCurveY().get(i) );
		}

		//------------------------------------------------
		// YZ VIEW -- Section/Front view
		//------------------------------------------------

		_fuselageCurvesSection = null;
		_fuselageCurvesSection = new MyFuselageCurvesSection(
				w_B, d_C, a, rhoUpper, rhoLower, // lengths 
				_np_SecUp, _np_SecLow      // no. points (nose, cylinder, tail)
				);

		// UPPER CURVE -----------------------------------
		// counter-clockwise
		for (int i = 0; i <= _fuselageCurvesSection.getSectionUpperRightPoints().size() - 1; i++){
			_theFuselage.getSectionUpperCurveY().add(
					(double) _fuselageCurvesSection.getSectionUpperRightPoints().get(i).x
					);
			_theFuselage.getSectionUpperCurveZ().add(
					(double) _fuselageCurvesSection.getSectionUpperRightPoints().get(i).y
					);
		}
		// TO DO: CAREFUL WITH REPEATED POINTS
		for (int i = 0; i <= _fuselageCurvesSection.getSectionUpperLeftPoints().size() - 1; i++){
			_theFuselage.getSectionUpperCurveY().add(
					(double) _fuselageCurvesSection.getSectionUpperLeftPoints().get(i).x
					);
			_theFuselage.getSectionUpperCurveZ().add(
					(double) _fuselageCurvesSection.getSectionUpperLeftPoints().get(i).y
					);
		}

		// LOWER CURVE -----------------------------------
		// counter-clockwise
		for (int i = 0; i <= _fuselageCurvesSection.getSectionLowerLeftPoints().size() - 1; i++){
			_theFuselage.getSectionLowerCurveY().add(
					(double) _fuselageCurvesSection.getSectionLowerLeftPoints().get(i).x
					);
			_theFuselage.getSectionLowerCurveZ().add(
					(double) _fuselageCurvesSection.getSectionLowerLeftPoints().get(i).y
					);
		}
		// TO DO: CAREFUL WITH REPEATED POINTS
		for (int i = 0; i <= _fuselageCurvesSection.getSectionLowerRightPoints().size() - 1; i++){
			_theFuselage.getSectionLowerCurveY().add(
					(double) _fuselageCurvesSection.getSectionLowerRightPoints().get(i).x
					);
			_theFuselage.getSectionLowerCurveZ().add(
					(double) _fuselageCurvesSection.getSectionLowerRightPoints().get(i).y
					);
		}

	}// end-of recalculateCurves


	public void redrawFuselageXZ () {

		// initial cleanup
		if ( !_traces_XZ.isEmpty()  ) {
			for(int i=0; i<_traces_XZ.size(); i++){
				this._xyGraphXZ.removeTrace( _traces_XZ.get(i) ); // 0: Upper, 1: Lower, 2: Camber
			}
		}
		this._tdpXZ.clear();
		this._traces_XZ.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpXZ.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(0).clearTrace();
		_tdpXZ.get(0).setBufferSize(100);		
		_tdpXZ.get(0).setCurrentXDataArray(
				Doubles.toArray(_theFuselage.getOutlineXZUpperCurveX()) // from Guava lib
				);
		_tdpXZ.get(0).setCurrentYDataArray(
				Doubles.toArray(_theFuselage.getOutlineXZUpperCurveZ()) // from Guava lib
				);

		// make the upper curve trace
		_traces_XZ.add(
				new Trace(
						"Upper outline", 
						_xyGraphXZ.primaryXAxis, _xyGraphXZ.primaryYAxis, 
						_tdpXZ.get(0)
						)
				);			
		//set trace property
		_traces_XZ.get(0).setPointStyle(PointStyle.POINT);
		_traces_XZ.get(0).setPointSize(5);
		_traces_XZ.get(0).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_XZ.get(0).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXZ.addTrace( _traces_XZ.get(0) );
		//_xyGraph_XZ.performAutoScale();

		// add a new trace data provider for Lower outline
		_tdpXZ.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(1).clearTrace();
		_tdpXZ.get(1).setConcatenate_data(false);
		_tdpXZ.get(1).setBufferSize(100);		
		_tdpXZ.get(1).setCurrentXDataArray(
				Doubles.toArray(_theFuselage.getOutlineXZLowerCurveX()) // from Guava lib
				);
		_tdpXZ.get(1).setCurrentYDataArray(
				Doubles.toArray(_theFuselage.getOutlineXZLowerCurveZ()) // from Guava lib
				);

		// make the upper curve trace
		_traces_XZ.add(
				new Trace(
						"Lower outline", 
						_xyGraphXZ.primaryXAxis, _xyGraphXZ.primaryYAxis, 
						_tdpXZ.get(1)
						)
				);			
		//set trace property
		_traces_XZ.get(1).setPointStyle(PointStyle.POINT);
		_traces_XZ.get(1).setPointSize(5);
		_traces_XZ.get(1).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_RED)
				);
		_traces_XZ.get(1).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXZ.addTrace( _traces_XZ.get(1) );


		// add a new trace data provider for Lower outline
		_tdpXZ.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(1).clearTrace();
		_tdpXZ.get(2).setConcatenate_data(false);
		_tdpXZ.get(2).setBufferSize(100);		
		_tdpXZ.get(2).setCurrentXDataArray(
				Doubles.toArray(_theFuselage.getOutlineXZCamberLineX()) // from Guava lib
				);
		_tdpXZ.get(2).setCurrentYDataArray(
				Doubles.toArray(_theFuselage.getOutlineXZCamberLineZ()) // from Guava lib
				);

		// make the upper curve trace
		_traces_XZ.add(
				new Trace(
						"Camber outline", 
						_xyGraphXZ.primaryXAxis, _xyGraphXZ.primaryYAxis, 
						_tdpXZ.get(2)
						)
				);			
		//set trace property

		_traces_XZ.get(2).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLACK)
				);
		_traces_XZ.get(2).setLineWidth(1); 
		_traces_XZ.get(2).setTraceType(TraceType.DASH_LINE);
		//add the trace to xyGraph
		_xyGraphXZ.addTrace( _traces_XZ.get(2) );

		// set a proper scale
		_xyGraphXZ.setTitle("Side View");
		_xyGraphXZ.primaryXAxis.setTitle("X-Axis (m)");
		_xyGraphXZ.primaryYAxis.setTitle("Z-Axis (m)");
		// _xyGraph_XZ.performAutoScale();
		org.eclipse.swt.graphics.Rectangle rect = _tabFolderPlots.getClientArea();
		scaleAxesXZEqual(rect.width, rect.height);


	}// end of redraw

	public void redrawFuselageXY () {

		// initial cleanup
		if ( !_traces_XY.isEmpty()  ) {
			for(int i=0; i<_traces_XY.size(); i++){
				this._xyGraphXY.removeTrace( _traces_XY.get(i) ); // 0: Upper, 1: Lower
			}
		}
		this._tdpXY.clear();
		this._traces_XY.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(0).clearTrace();
		_tdpXY.get(0).setBufferSize(100);		
		_tdpXY.get(0).setCurrentXDataArray(
				Doubles.toArray(_theFuselage.getOutlineXYSideRCurveX()) // from Guava lib
				);
		_tdpXY.get(0).setCurrentYDataArray(
				Doubles.toArray(_theFuselage.getOutlineXYSideRCurveY()) // from Guava lib
				);

		// make the upper curve trace
		_traces_XY.add(
				new Trace(
						"Upper outline", 
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(0)
						)
				);			
		//set trace property
		_traces_XY.get(0).setPointStyle(PointStyle.POINT);
		_traces_XY.get(0).setPointSize(5);
		_traces_XY.get(0).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_XY.get(0).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(0) );
		//_xyGraph_XZ.performAutoScale();

		// add a new trace data provider for Lower outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(1).clearTrace();
		_tdpXY.get(1).setConcatenate_data(false);
		_tdpXY.get(1).setBufferSize(100);		
		_tdpXY.get(1).setCurrentXDataArray(
				Doubles.toArray(_theFuselage.getOutlineXYSideLCurveX()) // from Guava lib
				);
		_tdpXY.get(1).setCurrentYDataArray(
				Doubles.toArray(_theFuselage.getOutlineXYSideLCurveY()) // from Guava lib
				);

		// make the upper curve trace
		_traces_XY.add(
				new Trace(
						"Lower outline", 
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(1)
						)
				);			
		//set trace property
		_traces_XY.get(1).setPointStyle(PointStyle.POINT);
		_traces_XY.get(1).setPointSize(5);
		_traces_XY.get(1).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_RED)
				);
		_traces_XY.get(1).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(1) );

		// set a proper scale
		_xyGraphXY.setTitle("Upper View");
		_xyGraphXY.primaryXAxis.setTitle("X-Axis (m)");
		_xyGraphXY.primaryYAxis.setTitle("Y-Axis (m)");
		// _xyGraph_XZ.performAutoScale();
		org.eclipse.swt.graphics.Rectangle rect = _tabFolderPlots.getClientArea();
		scaleAxesXYEqual(rect.width, rect.height);

	}// end of redraw

	public void redrawFuselageSectionYZ() {

		int idxGUIselected = get_selectedSectionIdx();
		Color colorCurve = XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_GRAY);

		// initial cleanup
		if ( !_traces_YZ.isEmpty()  ) {
			for(int i=0; i<_traces_YZ.size(); i++){
				this._xyGraphYZ.removeTrace( _traces_YZ.get(i) ); // 0: Upper, 1: Lower
			}
		}
		this._tdpYZ.clear();
		this._traces_YZ.clear();

		int curveCount = 0;
		for (int idx = 1; idx < _theFuselage.NUM_SECTIONS_YZ-1; idx++)
		{
			// skip duplicate cylinder section in YZ view
			if ( idx == _theFuselage.IDX_SECTION_YZ_CYLINDER_2 ) continue;

			// add a new trace data provider for Upper outline
			_tdpYZ.add( new CircularBufferDataProvider(false) );
			_tdpYZ.get(curveCount).setBufferSize(100);	
			_tdpYZ.get(curveCount).setCurrentXDataArray(
					Doubles.toArray(_theFuselage.getSectionUpperCurvesY(idx)) // from Guava lib
					);
			_tdpYZ.get(curveCount).setCurrentYDataArray(
					Doubles.toArray(_theFuselage.getSectionUpperCurvesZ(idx)) // from Guava lib
					);
			// make the upper curve trace
			_traces_YZ.add(
					new Trace(
							"Upper outline "+ idx, 
							_xyGraphYZ.primaryXAxis, _xyGraphYZ.primaryYAxis, 
							_tdpYZ.get(curveCount)
							)
					);
			//set trace property

			if ( idxGUIselected == idx )
				colorCurve = XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE);
			else
				colorCurve = XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_GRAY);

			_traces_YZ.get(curveCount).setPointStyle(PointStyle.POINT);
			_traces_YZ.get(curveCount).setPointSize(5);
			_traces_YZ.get(curveCount).setTraceColor(
					//					XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
					colorCurve
					);
			_traces_YZ.get(curveCount).setLineWidth(2);

			//add the trace to xyGraph
			_xyGraphYZ.addTrace( _traces_YZ.get(curveCount) );			

			curveCount++;

			// add a new trace data provider for Upper outline
			_tdpYZ.add( new CircularBufferDataProvider(false) );
			_tdpYZ.get(curveCount).setBufferSize(100);	
			_tdpYZ.get(curveCount).setCurrentXDataArray(
					Doubles.toArray(_theFuselage.getSectionLowerCurvesY(idx)) // from Guava lib
					);
			_tdpYZ.get(curveCount).setCurrentYDataArray(
					Doubles.toArray(_theFuselage.getSectionLowerCurvesZ(idx)) // from Guava lib
					);
			// make the upper curve trace
			_traces_YZ.add(
					new Trace(
							"Lower outline "+ idx, 
							_xyGraphYZ.primaryXAxis, _xyGraphYZ.primaryYAxis, 
							_tdpYZ.get(curveCount)
							)
					);

			//set trace property
			if ( idxGUIselected == idx )
				colorCurve = XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_RED);
			else
				colorCurve = XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_GRAY);

			_traces_YZ.get(curveCount).setPointStyle(PointStyle.POINT);
			_traces_YZ.get(curveCount).setPointSize(5);
			_traces_YZ.get(curveCount).setTraceColor(
					//					XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_RED)
					colorCurve
					);
			_traces_YZ.get(curveCount).setLineWidth(2);

			//add the trace to xyGraph
			_xyGraphYZ.addTrace( _traces_YZ.get(curveCount) );			

			// set a proper scale
			_xyGraphYZ.setTitle("Section View");
			_xyGraphYZ.primaryXAxis.setTitle("Y-Axis (m)");
			_xyGraphYZ.primaryYAxis.setTitle("Z-Axis (m)");

			curveCount++;

			//			System.out.println("traces: "+ _traces_YZ.size());
			//			System.out.println(_xyGraph_YZ.getPlotArea().getTraceList());
			//			System.out.println("trace data providers: "+ _tdpYZ.size());


		} // end-of-for


		org.eclipse.swt.graphics.Rectangle rect = _tabFolderPlots.getClientArea();
		scaleAxesYZEqual(rect.width, rect.height);

	} // end of redraw section


	private void showControls() {

		// show/hide controls
		_button_ChooseImportFile.setEnabled(false);
		_button_ImportFile.setEnabled(false);

		// Show all texts/sliders first
		_text01.setEnabled(true);
		_text01.setEditable(true);
		_slider01.setEnabled(true);
		_text02.setEnabled(true);
		_text02.setEditable(true);
		_slider02.setEnabled(true);
		_text_03.setEnabled(true);
		_text_03.setEditable(true);
		_slider_03.setEnabled(true);
		_text04.setEnabled(true);
		_text04.setEditable(true);
		_slider04.setEnabled(true);
		_text_05.setEnabled(true);
		_text_05.setEditable(true);
		_slider_05.setEnabled(true);
		_text_06.setEnabled(true);
		_text_06.setEditable(true);
		_slider_06.setEnabled(true);
		_text_07.setEnabled(true);
		_text_07.setEditable(true);
		_slider_07.setEnabled(true);
		_text_08.setEnabled(true);
		_text_08.setEditable(true);
		_slider_08.setEnabled(true);
		_text_09.setEnabled(true);
		_text_09.setEditable(true);
		_slider_09.setEnabled(true);
		_text_10.setEnabled(true);
		_text_10.setEditable(true);
		_slider_10.setEnabled(true);
		_text_11.setEnabled(true);
		_text_11.setEditable(true);
		_slider_11.setEnabled(true);
		_text_12.setEnabled(true);
		_text_12.setEditable(true);
		_slider_12.setEnabled(true);
		_text_13.setEditable(true);

		// show/hide controls
		switch (_selectedAdjustCriterion) {

		case NONE:
			_button_ChooseImportFile.setEnabled(true);
			_button_ChooseImportFile.setGrayed(false);
			_button_ImportFile.setEnabled(true);
			_button_ImportFile.setGrayed(false);

			// Fuselage length, l_F
			_slider01.setEnabled(false);
			_text01.setEditable(false);
			// Ratio l_N / l_F
			_slider_06.setEnabled(false);
			_text_06.setEditable(false);
			// Ratio l_C / l_F
			_slider_07.setEnabled(false);
			_text_07.setEditable(false);
			// Ratio l_T / l_F
			_slider_08.setEnabled(false);
			_text_08.setEditable(false);
			// Fineness ratio, l_N / d_C
			_slider_09.setEnabled(false);
			_text_09.setEditable(false);
			// Fineness ratio, l_C / d_C
			_slider_10.setEnabled(false);
			_text_10.setEditable(false);
			// Fineness ratio, l_T / d_C
			_slider_11.setEnabled(false);
			_text_11.setEditable(false);
			// Fineness ratio, l_F / d_C
			_slider_12.setEnabled(false);
			_text_12.setEditable(false);
			break;

		case ADJ_TOT_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS:
			// Fuselage length, l_F
			// --- _text_01 
			// --- _slider_01
			// Nose Length, l_N
			_text02.setEnabled(false);	
			_slider02.setEnabled(false);
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			_text04.setEnabled(false);
			_slider04.setEnabled(false);
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;

		case ADJ_TOT_LENGTH_CONST_FINENESS_RATIOS:
			// Fuselage length, l_F
			// --- _text_01 
			// --- _slider_01
			// Nose Length, l_N
			_text02.setEnabled(false);	
			_slider02.setEnabled(false);
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			_text04.setEnabled(false);
			_slider04.setEnabled(false);
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;

		case ADJ_CYL_LENGTH:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose Length, l_N
			_text02.setEnabled(false);	
			_slider02.setEnabled(false);
			// Cylinder length, l_C
			// --- _text_03
			// --- _slider_03
			// Tail length, l_T
			_text04.setEnabled(false);
			_slider04.setEnabled(false);
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;

		case ADJ_NOSE_LENGTH_CONST_TOT_LENGTH_DIAMETERS:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose length, l_N
			// --- _text_02
			// --- _slider_02
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			_text04.setEnabled(false);
			_slider04.setEnabled(false);
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;
		case ADJ_NOSE_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose length, l_N
			// --- _text_02
			// --- _slider_02
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			_text04.setEnabled(false);
			_slider04.setEnabled(false);
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;
		case ADJ_NOSE_LENGTH_CONST_FINENESS_RATIOS_VAR_LENGTHS:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose length, l_N
			// --- _text_02
			// --- _slider_02
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			_text04.setEnabled(false);
			_slider04.setEnabled(false);
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;
		case ADJ_TAILCONE_LENGTH_CONST_TOT_LENGTH_DIAMETERS:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose Length, l_N
			_text02.setEnabled(false);	
			_slider02.setEnabled(false);
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			// --- _text_04
			// --- _slider_04		
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;			
		case ADJ_TAILCONE_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose Length, l_N
			_text02.setEnabled(false);	
			_slider02.setEnabled(false);
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			// --- _text_04
			// --- _slider_04		
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;			
		case ADJ_TAILCONE_LENGTH_CONST_FINENESS_RATIOS_VAR_LENGTHS:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose Length, l_N
			_text02.setEnabled(false);	
			_slider02.setEnabled(false);
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail  length, l_T
			// --- _text_04
			// --- _slider_04		
			// Diameter, d_C
			_text_05.setEnabled(false);
			_slider_05.setEnabled(false);
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;	
		case ADJ_FUS_LENGTH_CONST_FINENESS_RATIOS_VAR_DIAMETERS:
			// Fuselage length, l_F
			_text01.setEnabled(false);	
			_slider01.setEnabled(false);  // see alternatives
			// Nose Length, l_N
			_text02.setEnabled(false);	
			_slider02.setEnabled(false);
			// Cylinder length, l_C
			_text_03.setEnabled(false);
			_slider_03.setEnabled(false);
			// Tail length, l_T
			_text04.setEnabled(false);
			_slider04.setEnabled(false);
			// Diameter, diam_C
			// --- _text_05
			// --- _slider_05		
			// Ratio l_N / l_F
			_text_06.setEnabled(false);
			_slider_06.setEnabled(false);
			// Ratio l_C / l_F
			_text_07.setEnabled(false);
			_slider_07.setEnabled(false);
			// Ratio l_T / l_F
			_text_08.setEnabled(false);
			_slider_08.setEnabled(false);
			// Fineness ratio, l_N / d_C
			_text_09.setEnabled(false);
			_slider_09.setEnabled(false);
			// Fineness ratio, l_C / d_C
			_text_10.setEnabled(false);
			_slider_10.setEnabled(false);
			// Fineness ratio, l_T / d_C
			_text_11.setEnabled(false);
			_slider_11.setEnabled(false);
			// Fineness ratio, l_F / d_C
			_text_12.setEnabled(false);
			_slider_12.setEnabled(false);
			break;			
		default:
			break;
		}// end of case


	}// end of show controls



	public String getSerializationFilePath(){
		return _exportFilePath;
	}

	public void setSerializationFilePath(){
		_exportFilePath = 
				MyConfiguration.currentDirectory 
				+ File.separator + EXPORT_FILE_NAME;
	}

	public Text getTextSerializeFile() {
		return _text_ExportFile;
	}

	private void adjustControls() {

		// NOTE: this function must be called AFTER adjustLength

		// adjust controls
		BigDecimal bd_01 = BigDecimal.valueOf( _theFuselage.get_len_F().getEstimatedValue());
		bd_01 = bd_01.setScale(3, RoundingMode.HALF_UP);
		_text01.setText( "  " + bd_01 + " "+ _theFuselage.get_len_F().getUnit().toString());
		_slider01.setSelection((int) _theFuselage.get_len_F().getEstimatedValue());

		BigDecimal bd_02 = BigDecimal.valueOf( _theFuselage.get_len_N().getEstimatedValue());
		bd_02 = bd_02.setScale(3, RoundingMode.HALF_UP);
		_text02.setText( "  " + bd_02 + " "+ _theFuselage.get_len_N().getUnit().toString());
		_slider02.setSelection( (int) _theFuselage.get_len_N().getEstimatedValue());

		BigDecimal bd_03 = BigDecimal.valueOf( _theFuselage.get_len_C().getEstimatedValue());
		bd_03 = bd_03.setScale(3, RoundingMode.HALF_UP);
		_text_03.setText( "  " + bd_03 + " "+ _theFuselage.get_len_C().getUnit().toString());
		_slider_03.setSelection( (int) _theFuselage.get_len_C().getEstimatedValue());

		BigDecimal bd_04 = BigDecimal.valueOf( _theFuselage.get_len_T().getEstimatedValue());
		bd_04 = bd_04.setScale(3, RoundingMode.HALF_UP);
		_text04.setText( "  " + bd_04 + " "+ _theFuselage.get_len_T().getUnit().toString());
		_slider04.setSelection( (int) _theFuselage.get_len_T().getEstimatedValue());

		BigDecimal bd_05 = BigDecimal.valueOf(_theFuselage.get_sectionCylinderHeight().getEstimatedValue());
		bd_05 = bd_05.setScale(3, RoundingMode.HALF_UP);
		_text_05.setText( "  " + bd_05 + " "+ _theFuselage.get_sectionCylinderHeight().getUnit().toString());
		_slider_05.setSelection( (int) _theFuselage.get_sectionCylinderHeight().getEstimatedValue());

		BigDecimal bd_06 = BigDecimal.valueOf(_theFuselage.get_lenRatio_NF());
		bd_06 = bd_06.setScale(3, RoundingMode.HALF_UP);
		_text_06.setText( " "+bd_06);
		_slider_06.setSelection( _theFuselage.get_lenRatio_NF().intValue() );

		BigDecimal bd_07 = BigDecimal.valueOf(_theFuselage.get_lenRatio_CF());
		bd_07 = bd_07.setScale(3, RoundingMode.HALF_UP);
		_text_07.setText( " "+bd_07);
		_slider_07.setSelection( _theFuselage.get_lenRatio_CF().intValue() );

		BigDecimal bd_08 = BigDecimal.valueOf(_theFuselage.get_lenRatio_TF());
		bd_08 = bd_08.setScale(3, RoundingMode.HALF_UP);
		_text_08.setText( " "+bd_08);
		_slider_08.setSelection( _theFuselage.get_lenRatio_TF().intValue() );

		BigDecimal bd_09= BigDecimal.valueOf(_theFuselage.get_lambda_N());
		bd_09 = bd_09.setScale(2, RoundingMode.HALF_UP);
		_text_09.setText(" "+ bd_09);
		_slider_09.setSelection( _theFuselage.get_lambda_N().intValue() );

		BigDecimal bd_10= BigDecimal.valueOf(_theFuselage.get_lambda_C());
		bd_10 = bd_10.setScale(2, RoundingMode.HALF_UP);
		_text_10.setText(" "+ bd_10);
		_slider_10.setSelection( _theFuselage.get_lambda_C().intValue() );

		BigDecimal bd_11= BigDecimal.valueOf(_theFuselage.get_lambda_T());
		bd_11 = bd_11.setScale(2, RoundingMode.HALF_UP);
		_text_11.setText(" "+ bd_11);
		_slider_11.setSelection( _theFuselage.get_lambda_T().intValue() );



		BigDecimal bd_12= BigDecimal.valueOf(_theFuselage.get_lambda_F());
		bd_12 = bd_12.setScale(2, RoundingMode.HALF_UP);
		_text_12.setText(" "+ bd_12);
		_slider_12.setSelection( _theFuselage.get_lambda_F().intValue() );

		BigDecimal bd_13= BigDecimal.valueOf(_theFuselage.get_height_N().getEstimatedValue() );
		bd_13 = bd_13.setScale(3, RoundingMode.DOWN);
		_text_13.setText(  " "+ bd_13 +" "+_theFuselage.get_height_N().getUnit().toString());

		BigDecimal bd_14= BigDecimal.valueOf(_theFuselage.get_height_T().getEstimatedValue() );
		bd_14 = bd_14.setScale(3, RoundingMode.DOWN);
		_text_14.setText(  " "+ bd_14 +" "+_theFuselage.get_height_T().getUnit().toString());

	}


	private void scaleAxesXZEqual(double w, double h) 
	{

		//--------------------------------------------------------------
		// XZ View
		//--------------------------------------------------------------

		if (_traces_XZ.size() < 2) return; // we want upper & lower curves

		List <ISample> lis0 = _traces_XZ.get(0).getHotSampleList();
		List <ISample> lis1 = _traces_XZ.get(1).getHotSampleList();

		// System.out.println( _traces_XZ.get(0).getHotSampleList().size() );

		if ( 
				(_traces_XZ.get(0).getHotSampleList().size() > 0) 
				||  
				(_traces_XZ.get(1).getHotSampleList().size() > 0) 
				) 
		{

			// find min and max
			double xMax0 = -1e12;
			double xMin0 = 1e12;
			double yMax0 = -1e12;
			double yMin0 = 1e12;
			double xMax1 = -1e12;
			double xMin1 = 1e12;
			double yMax1 = -1e12;
			double yMin1 = 1e12;
			if (_traces_XZ.get(0).getHotSampleList().size() > 0) 
			{
				xMax0 = lis0.get(0).getXValue();
				xMin0 = lis0.get(0).getXValue();
				yMax0 = lis0.get(0).getYValue();
				yMin0 = lis0.get(0).getYValue();
				for (int k=1; k<lis0.size(); k++) {
					double x = lis0.get(k).getXValue();
					double y = lis0.get(k).getYValue();
					if (x > xMax0) xMax0 = x;
					if (x < xMin0) xMin0 = x;
					if (y > yMax0) yMax0 = y;
					if (y < yMin0) yMin0 = y;
				}
			}
			if (_traces_XZ.get(1).getHotSampleList().size() > 0) 
			{
				xMax1 = lis1.get(0).getXValue();
				xMin1 = lis1.get(0).getXValue();
				yMax1 = lis1.get(0).getYValue();
				yMin1 = lis1.get(0).getYValue();
				for (int k=1; k<lis1.size(); k++) {
					double x = lis1.get(k).getXValue();
					double y = lis1.get(k).getYValue();
					if (x > xMax1) xMax1 = x;
					if (x < xMin1) xMin1 = x;
					if (y > yMax1) yMax1 = y;
					if (y < yMin1) yMin1 = y;
				}
			}
			double xMin = xMin0;
			double xMax = xMax0;
			double yMin = yMin0;
			double yMax = yMax0;
			if (xMin1 < xMin) xMin = xMin1;
			if (xMax1 > xMax) xMax = xMax1;
			if (yMin1 < yMin) yMin = yMin1;
			if (yMax1 > yMax) yMax = yMax1;

			if ( 
					(w > 0) && (h > 0) 
					&& 
					(Math.abs( xMax - xMin ) > 0) && (Math.abs( yMax - yMin ) > 0)
					) 
			{
				double ar = w/h;
				double arx = Math.abs( xMax - xMin )/Math.abs( yMax - yMin );
				if (ar < arx)
				{
					double xmin = xMin;
					double xmax = xMax;
					double ymin = yMin;
					double ymax = ymin + (xmax - xmin)/ar;

					// TO DO: force a shift after capturing the midpoint coordinates

					// shift
					//					 xmin = xmin - 0.1*(xmax - xmin);
					//					 xmax = xmax - 0.1*(xmax - xmin);
					//					 ymin = ymin - 0.1*(ymax - ymin);
					//					 ymax = ymax - 0.1*(ymax - ymin);

					//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
					//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);

					_xyGraphXZ.primaryXAxis.setRange(new Range( xmin, xmax));
					_xyGraphXZ.primaryYAxis.setRange(new Range( ymin, ymax));
				}
				else // arx < ar
				{
					double ymin = yMin;
					double ymax = yMax;
					double xmin = xMin;
					double xmax = xmin + (ymax - ymin)*ar;

					// shift
					//					 xmin = xmin - 0.1*(xmax - xmin);
					//					 xmax = xmax - 0.1*(xmax - xmin);
					//					 ymin = ymin - 0.1*(ymax - ymin);
					//					 ymax = ymax - 0.1*(ymax - ymin);

					//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
					//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);

					_xyGraphXZ.primaryXAxis.setRange(new Range( xmin, xmax));
					_xyGraphXZ.primaryYAxis.setRange(new Range( ymin, ymax));

				}




			} // end-of-if ... size 0 check 

		} // end-of-if getHotSampleList.size >0


	} // end-of scaleAxesXZEqual


	private void scaleAxesXYEqual(double w, double h) 
	{

		//--------------------------------------------------------------
		// XZ View
		//--------------------------------------------------------------

		if (_traces_XY.size() < 2) return; // we want upper & lower curves

		List <ISample> lis0 = _traces_XY.get(0).getHotSampleList();
		List <ISample> lis1 = _traces_XY.get(1).getHotSampleList();

		// System.out.println( _traces_XZ.get(0).getHotSampleList().size() );

		if ( 
				(_traces_XY.get(0).getHotSampleList().size() > 0) 
				||  
				(_traces_XY.get(1).getHotSampleList().size() > 0) 
				) 
		{

			// find min and max
			double xMax0 = -1e12;
			double xMin0 = 1e12;
			double yMax0 = -1e12;
			double yMin0 = 1e12;
			double xMax1 = -1e12;
			double xMin1 = 1e12;
			double yMax1 = -1e12;
			double yMin1 = 1e12;
			if (_traces_XY.get(0).getHotSampleList().size() > 0) 
			{
				xMax0 = lis0.get(0).getXValue();
				xMin0 = lis0.get(0).getXValue();
				yMax0 = lis0.get(0).getYValue();
				yMin0 = lis0.get(0).getYValue();
				for (int k=1; k<lis0.size(); k++) {
					double x = lis0.get(k).getXValue();
					double y = lis0.get(k).getYValue();
					if (x > xMax0) xMax0 = x;
					if (x < xMin0) xMin0 = x;
					if (y > yMax0) yMax0 = y;
					if (y < yMin0) yMin0 = y;
				}
			}
			if (_traces_XY.get(1).getHotSampleList().size() > 0) 
			{
				xMax1 = lis1.get(0).getXValue();
				xMin1 = lis1.get(0).getXValue();
				yMax1 = lis1.get(0).getYValue();
				yMin1 = lis1.get(0).getYValue();
				for (int k=1; k<lis1.size(); k++) {
					double x = lis1.get(k).getXValue();
					double y = lis1.get(k).getYValue();
					if (x > xMax1) xMax1 = x;
					if (x < xMin1) xMin1 = x;
					if (y > yMax1) yMax1 = y;
					if (y < yMin1) yMin1 = y;
				}
			}
			double xMin = xMin0;
			double xMax = xMax0;
			double yMin = yMin0;
			double yMax = yMax0;
			if (xMin1 < xMin) xMin = xMin1;
			if (xMax1 > xMax) xMax = xMax1;
			if (yMin1 < yMin) yMin = yMin1;
			if (yMax1 > yMax) yMax = yMax1;

			if ( 
					(w > 0) && (h > 0) 
					&& 
					(Math.abs( xMax - xMin ) > 0) && (Math.abs( yMax - yMin ) > 0)
					) 
			{
				double ar = w/h;
				double arx = Math.abs( xMax - xMin )/Math.abs( yMax - yMin );
				if (ar < arx)
				{
					double xmin = xMin;
					double xmax = xMax;
					double ymin = yMin;
					double ymax = ymin + (xmax - xmin)/ar;

					// TO DO: force a shift after capturing the midpoint coordinates

					// shift
					//					 xmin = xmin - 0.1*(xmax - xmin);
					//					 xmax = xmax - 0.1*(xmax - xmin);
					//					 ymin = ymin - 0.1*(ymax - ymin);
					//					 ymax = ymax - 0.1*(ymax - ymin);

					//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
					//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);

					_xyGraphXY.primaryXAxis.setRange(new Range( xmin, xmax));
					_xyGraphXY.primaryYAxis.setRange(new Range( ymin, ymax));
				}
				else // arx < ar
				{
					double ymin = yMin;
					double ymax = yMax;
					double xmin = xMin;
					double xmax = xmin + (ymax - ymin)*ar;

					// shift
					//					 xmin = xmin - 0.1*(xmax - xmin);
					//					 xmax = xmax - 0.1*(xmax - xmin);
					//					 ymin = ymin - 0.1*(ymax - ymin);
					//					 ymax = ymax - 0.1*(ymax - ymin);

					//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
					//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);

					_xyGraphXY.primaryXAxis.setRange(new Range( xmin, xmax));
					_xyGraphXY.primaryYAxis.setRange(new Range( ymin, ymax));

				}




			} // end-of-if ... size 0 check 

		} // end-of-if getHotSampleList.size >0


	} // end-of scaleAxesXZEqual


	private void scaleAxesYZEqual(double w, double h) 
	{

		//--------------------------------------------------------------
		// XZ View
		//--------------------------------------------------------------

		if (_traces_YZ.size() < 2) return; // we want upper & lower curves

		//		List <ISample> lis0 = _traces_YZ.get(0).getHotSampleList();
		//		List <ISample> lis1 = _traces_YZ.get(1).getHotSampleList();
		//		
		//		// System.out.println( _traces_XZ.get(0).getHotSampleList().size() );
		//		
		//		if ( 
		//				(_traces_YZ.get(0).getHotSampleList().size() > 0) 
		//				||  
		//				(_traces_YZ.get(1).getHotSampleList().size() > 0) 
		//				) 
		//		{
		//			
		//			// find min and max
		//			double xMax0 = -1e12;
		//			double xMin0 = 1e12;
		//			double yMax0 = -1e12;
		//			double yMin0 = 1e12;
		//			double xMax1 = -1e12;
		//			double xMin1 = 1e12;
		//			double yMax1 = -1e12;
		//			double yMin1 = 1e12;
		//			if (_traces_YZ.get(0).getHotSampleList().size() > 0) 
		//			{
		//				xMax0 = lis0.get(0).getXValue();
		//				xMin0 = lis0.get(0).getXValue();
		//				yMax0 = lis0.get(0).getYValue();
		//				yMin0 = lis0.get(0).getYValue();
		//				for (int k=1; k<lis0.size(); k++) {
		//					double x = lis0.get(k).getXValue();
		//					double y = lis0.get(k).getYValue();
		//					if (x > xMax0) xMax0 = x;
		//					if (x < xMin0) xMin0 = x;
		//					if (y > yMax0) yMax0 = y;
		//					if (y < yMin0) yMin0 = y;
		//				}
		//			}
		//			if (_traces_YZ.get(1).getHotSampleList().size() > 0) 
		//			{
		//				xMax1 = lis1.get(0).getXValue();
		//				xMin1 = lis1.get(0).getXValue();
		//				yMax1 = lis1.get(0).getYValue();
		//				yMin1 = lis1.get(0).getYValue();
		//				for (int k=1; k<lis1.size(); k++) {
		//					double x = lis1.get(k).getXValue();
		//					double y = lis1.get(k).getYValue();
		//					if (x > xMax1) xMax1 = x;
		//					if (x < xMin1) xMin1 = x;
		//					if (y > yMax1) yMax1 = y;
		//					if (y < yMin1) yMin1 = y;
		//				}
		//			}
		//			double xMin = xMin0;
		//			double xMax = xMax0;
		//			double yMin = yMin0;
		//			double yMax = yMax0;
		//			if (xMin1 < xMin) xMin = xMin1;
		//			if (xMax1 > xMax) xMax = xMax1;
		//			if (yMin1 < yMin) yMin = yMin1;
		//			if (yMax1 > yMax) yMax = yMax1;
		//
		//			if ( 
		//					(w > 0) && (h > 0) 
		//					&& 
		//					(Math.abs( xMax - xMin ) > 0) && (Math.abs( yMax - yMin ) > 0)
		//					) 
		//			{
		//				double ar = w/h;
		//				double arx = Math.abs( xMax - xMin )/Math.abs( yMax - yMin );
		//				if (ar < arx)
		//				{
		//					double xmin = xMin;
		//					double xmax = xMax;
		//					double ymin = yMin;
		//					double ymax = ymin + (xmax - xmin)/ar;
		//					
		//					// TO DO: force a shift after capturing the midpoint coordinates
		//					
		//					// shift
		////					 xmin = xmin - 0.1*(xmax - xmin);
		////					 xmax = xmax - 0.1*(xmax - xmin);
		////					 ymin = ymin - 0.1*(ymax - ymin);
		////					 ymax = ymax - 0.1*(ymax - ymin);
		//					
		//					//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
		//					//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);
		//					
		//					_xyGraph_YZ.primaryXAxis.setRange(new Range( xmin, xmax));
		//					_xyGraph_YZ.primaryYAxis.setRange(new Range( ymin, ymax));
		//				}
		//				else // arx < ar
		//				{
		//					double ymin = yMin;
		//					double ymax = yMax;
		//					double xmin = xMin;
		//					double xmax = xmin + (ymax - ymin)*ar;
		//					
		//					// shift
		////					 xmin = xmin - 0.1*(xmax - xmin);
		////					 xmax = xmax - 0.1*(xmax - xmin);
		////					 ymin = ymin - 0.1*(ymax - ymin);
		////					 ymax = ymax - 0.1*(ymax - ymin);
		//					
		//					//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
		//					//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);
		//					
		//					_xyGraph_YZ.primaryXAxis.setRange(new Range( xmin, xmax));
		//					_xyGraph_YZ.primaryYAxis.setRange(new Range( ymin, ymax));
		//					
		//				}
		//				
		//			} // end-of-if ... size 0 check 
		//			
		//		} // end-of-if getHotSampleList.size >0



		double yMin = -0.5*_theFuselage.get_sectionCylinderHeight().getEstimatedValue();
		double yMax = 0.5*_theFuselage.get_sectionCylinderHeight().getEstimatedValue();
		double xMin = -0.5*_theFuselage.get_sectionCylinderWidth().getEstimatedValue();
		double xMax = 0.5*_theFuselage.get_sectionCylinderWidth().getEstimatedValue();

		if ( 
				(w > 0) && (h > 0) 
				&& 
				(Math.abs( xMax - xMin ) > 0) && (Math.abs( yMax - yMin ) > 0)
				) 
		{
			double ar = w/h;
			double arx = Math.abs( xMax - xMin )/Math.abs( yMax - yMin );
			if (ar < arx)
			{
				double xmin = xMin;
				double xmax = xMax;
				double ymin = yMin;
				double ymax = ymin + (xmax - xmin)/ar;

				// TO DO: force a shift after capturing the midpoint coordinates

				// shift
				//		 xmin = xmin - 0.1*(xmax - xmin);
				//		 xmax = xmax - 0.1*(xmax - xmin);
				//		 ymin = ymin - 0.1*(ymax - ymin);
				//		 ymax = ymax - 0.1*(ymax - ymin);

				//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
				//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);

				_xyGraphYZ.primaryXAxis.setRange(new Range( xmin, xmax));
				_xyGraphYZ.primaryYAxis.setRange(new Range( ymin, ymax));
			}
			else // arx < ar
			{
				double ymin = yMin;
				double ymax = yMax;
				double xmin = xMin;
				double xmax = xmin + (ymax - ymin)*ar;

				// shift
				//		 xmin = xmin - 0.1*(xmax - xmin);
				//		 xmax = xmax - 0.1*(xmax - xmin);
				//		 ymin = ymin - 0.1*(ymax - ymin);
				//		 ymax = ymax - 0.1*(ymax - ymin);

				//System.out.println("w = "+ w +" ** h = "+ h +" ** ar = "+ ar);
				//System.out.println("dx = "+ (xmax-xmin) +" ** dy = "+ (ymax-ymin) +" ** arx = "+ arx);

				_xyGraphYZ.primaryXAxis.setRange(new Range( xmin, xmax));
				_xyGraphYZ.primaryYAxis.setRange(new Range( ymin, ymax));

			}
		}

	} // end-of scaleAxesXZEqual

	public void arrangeControls() {

		//------------------------------------------------------------------------------
		// Controls for settings:
		//    *  file choose/import
		//    *  adjustment criteria
		//------------------------------------------------------------------------------

		GridData gd_GroupSettings = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_GroupSettings.horizontalSpan = 3;

		Group groupSettings = new Group (_ctrComposite, SWT.NONE);

		groupSettings.setLayout (new FillLayout (SWT.VERTICAL));
		groupSettings.setLayoutData (gd_GroupSettings);
		groupSettings.setFont(SWTResourceManager.getFont("Times New Roman", 14, SWT.NORMAL));
		groupSettings.setText ("Settings");

		// Import file controls
		Group groupImportFile = new Group (groupSettings, SWT.NONE);
		groupImportFile.setLayout (new FillLayout (SWT.HORIZONTAL));
		groupImportFile.setText ("Import fuselage file");
		_text_ImportFile = new Text(groupImportFile, SWT.BORDER);
		_text_ImportFile.setText( "" );
		_text_ImportFile.setEditable(false);

		_button_ChooseImportFile = new Button(groupImportFile, SWT.NONE);
		_button_ChooseImportFile.setText("Choose ...");

		_button_ImportFile = new Button(groupSettings, SWT.NONE);
		_button_ImportFile.setText("Import");

		// Adjust criteria
		_comboDropDown = new Combo(groupSettings, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);

		_mapAdjustCriteria = ImmutableBiMap.<MyFuselageAdjustCriteria,String>builder()
				.put(
						MyFuselageAdjustCriteria.NONE, 
						"0 - None")
						.put(
								MyFuselageAdjustCriteria.ADJ_TOT_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS, 
								"1 - Adjust tot. length. Keep const. length ratios and diameter.")
								.put(
										MyFuselageAdjustCriteria.ADJ_TOT_LENGTH_CONST_FINENESS_RATIOS, 
										"2 - Adjust tot. length. Keep const. fineness ratios.")
										.put(
												MyFuselageAdjustCriteria.ADJ_CYL_LENGTH, 
												"3 - Stretch: adjust cylinder length only.")
												.put(
														MyFuselageAdjustCriteria.ADJ_NOSE_LENGTH_CONST_TOT_LENGTH_DIAMETERS, 
														"4 - Adjust nose. Keep const. tot. length and diameter.")
														.put(
																MyFuselageAdjustCriteria.ADJ_NOSE_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS, 
																"5 - Adjust nose. Keep const. length ratios and diameter.")
																.put(
																		MyFuselageAdjustCriteria.ADJ_NOSE_LENGTH_CONST_FINENESS_RATIOS_VAR_LENGTHS, 
																		"6 - Adjust nose. Keep const. fineness ratios and change lengths.")

																		.put(
																				MyFuselageAdjustCriteria.ADJ_TAILCONE_LENGTH_CONST_TOT_LENGTH_DIAMETERS, 
																				"7 - Adjust tail cone. Keep const. tot. length and diameter.")
																				.put(
																						MyFuselageAdjustCriteria.ADJ_TAILCONE_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS, 
																						"8 - Adjust tail cone. Keep const. length ratios and diameter.")
																						.put(
																								MyFuselageAdjustCriteria.ADJ_TAILCONE_LENGTH_CONST_FINENESS_RATIOS_VAR_LENGTHS, 
																								"9 - Adjust tail cone. Keep const. fineness ratios and change lengths.")
																								.put(
																										MyFuselageAdjustCriteria.ADJ_FUS_LENGTH_CONST_FINENESS_RATIOS_VAR_DIAMETERS, 
																										"10 - Adjust fuselage. Keep const. fineness ratios and change diameter.")
																										.build();


		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.NONE) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_TOT_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_TOT_LENGTH_CONST_FINENESS_RATIOS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_CYL_LENGTH) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_NOSE_LENGTH_CONST_TOT_LENGTH_DIAMETERS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_NOSE_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_NOSE_LENGTH_CONST_FINENESS_RATIOS_VAR_LENGTHS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_TAILCONE_LENGTH_CONST_TOT_LENGTH_DIAMETERS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_TAILCONE_LENGTH_CONST_LENGTH_RATIOS_DIAMETERS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_TAILCONE_LENGTH_CONST_FINENESS_RATIOS_VAR_LENGTHS) );
		_comboDropDown.add("" + _mapAdjustCriteria.get(MyFuselageAdjustCriteria.ADJ_FUS_LENGTH_CONST_FINENESS_RATIOS_VAR_DIAMETERS) );

		// init flag
		_comboDropDown.select(0); // 0 --> NONE; 1 --> adjust ...
		_selectedAdjustCriterion = _mapAdjustCriteria.inverse().get(_comboDropDown.getText());
		// Update fuselage object
		_theFuselage.setAdjustCriterion(_selectedAdjustCriterion);


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// Nomenclature

		GridData gdButtonNomenclature = new GridData(SWT.FILL, SWT.FILL, true, false);
		gdButtonNomenclature.horizontalSpan = 3;
		_button_Nomenclature = new Button(_ctrComposite, SWT.NONE);
		_button_Nomenclature.setText("Fuselage Nomenclature");
		_button_Nomenclature.setLayoutData(gdButtonNomenclature);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//------------------------------------------------------------------------------
		// Label 01: length of fuselage
		//------------------------------------------------------------------------------
		_text01 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Fuselage length", "\\ell_\\mathrm{F}", 
				_theFuselage.get_len_F(), 
				3, 0, TEXT_WIDTH_HINT);

		_slider01 = GridLayoutUtil.createSlider(_ctrComposite, 
				_theFuselage.get_len_F().getEstimatedValue(), 
				_theFuselage.get_len_F_MIN().getMinimumValue(), 
				_theFuselage.get_len_F_MAX().getMaximumValue());

		//------------------------------------------------------------------------------
		// Label 02: length of NOSE of fuselage, l_N
		//------------------------------------------------------------------------------

		_text02 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Fuselage nose length", "\\ell_\\mathrm{N}", 
				_theFuselage.get_len_N(), 
				3, 0, TEXT_WIDTH_HINT);

		_slider02 = GridLayoutUtil.createSlider(_ctrComposite, 
				_theFuselage.get_len_N().getEstimatedValue(), 
				_theFuselage.get_len_N_MIN().getMinimumValue(), 
				_theFuselage.get_len_N_MAX().getMaximumValue());


		//------------------------------------------------------------------------------
		// Label 03: length of central fuselage
		//------------------------------------------------------------------------------

		_text_03 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Central (Cylindrical) Fuselage Length", "\\ell_\\mathrm{C}", 
				_theFuselage.get_len_C(), 
				3, 0, TEXT_WIDTH_HINT);

		_slider_03 = GridLayoutUtil.createSlider(_ctrComposite, 
				_theFuselage.get_len_C().getEstimatedValue(), 
				_theFuselage.get_len_C_MIN().getMinimumValue(), 
				_theFuselage.get_len_C_MAX().getMaximumValue());

		//------------------------------------------------------------------------------
		// Label 04: length of tail cone fuselage
		//------------------------------------------------------------------------------

		_text04 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Fuselage Tail Cone Length", "\\ell_\\mathrm{T}", 
				_theFuselage.get_len_T(), 
				3, 0, TEXT_WIDTH_HINT);

		_slider04 = GridLayoutUtil.createSlider(_ctrComposite, 
				_theFuselage.get_len_T().getEstimatedValue(), 
				_theFuselage.get_len_T_MIN().getMinimumValue(), 
				_theFuselage.get_len_T_MAX().getMaximumValue());

		//------------------------------------------------------------------------------
		// Label 05: Diameter fuselage
		//------------------------------------------------------------------------------

		_text_05 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Fuselage Height (of Cylindrical part)", "d_\\mathrm{C}", 
				_theFuselage.get_sectionCylinderHeight(), 
				3, 0, TEXT_WIDTH_HINT);

		_slider_05 = GridLayoutUtil.createSlider(_ctrComposite, 
				_theFuselage.get_sectionCylinderHeight().getEstimatedValue(), 
				_theFuselage.get_diam_C_MIN().getMinimumValue(), 
				_theFuselage.get_diam_C_MAX().getMaximumValue());

		//				Label label = new Label(_slider_05, SWT.SEPARATOR | SWT.HORIZONTAL);
		//				label.setBounds(groupSectionData.getBounds().x, groupSectionData.getBounds().y, 300, 2);

		//+++++++++++++++++++++++++++++++ Group Section Data++++++++++++++++++++++++++++++++++++++

		GridData gd_GroupSectionData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_GroupSectionData.horizontalSpan = 3;

		Group groupSectionData = new Group (_ctrComposite, SWT.NONE);
		groupSectionData.setLayout (new FillLayout (SWT.VERTICAL));
		groupSectionData.setLayoutData (gd_GroupSectionData);
		groupSectionData.setFont(SWTResourceManager.getFont("Times New Roman", 14, SWT.NORMAL));
		groupSectionData.setText("Section Data");

		_comboDropDownSectionData = new Combo(groupSectionData, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);

		_mapSectionData = ImmutableBiMap.<MyFuselageSectionData,String>builder()
				.put(
						MyFuselageSectionData.CYLINDRICAL_BODY_1,
						"0 - Cylindrical Body")
						.put(
								MyFuselageSectionData.NOSE_CAP, 
								"1 - Nose Cap")
								.put(
										MyFuselageSectionData.MID_NOSE,
										"2 - Middle Nose")
										.put(
												MyFuselageSectionData.MID_TAIL, 
												"3 - Middle Tail")
												.put(
														MyFuselageSectionData.TAIL_CAP, 
														"4 - Tail Cap")
														.build();

		_comboDropDownSectionData.add("" + _mapSectionData.get(MyFuselageSectionData.CYLINDRICAL_BODY_1) );
		_comboDropDownSectionData.add("" + _mapSectionData.get(MyFuselageSectionData.NOSE_CAP) );
		_comboDropDownSectionData.add("" + _mapSectionData.get(MyFuselageSectionData.MID_NOSE) );
		_comboDropDownSectionData.add("" + _mapSectionData.get(MyFuselageSectionData.MID_TAIL) );
		_comboDropDownSectionData.add("" + _mapSectionData.get(MyFuselageSectionData.TAIL_CAP) );

		// init flag
		_comboDropDownSectionData.select(0); 
		_selectedSectionData = _mapSectionData.inverse().get(_comboDropDownSectionData.getText());		
		_button_SectionShape = new Button(groupSectionData, SWT.NONE);
		_button_SectionShape.setText("Section Shape");


		// end++++++++++++++++++++++++++++++Group Section Data+++++++++++++++++++++++++++++++++++++++++++

		_text_13 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Fuselage Nose tip", "h_\\mathrm{N}", 
				_theFuselage.get_height_N(), 
				3, 0, TEXT_WIDTH_HINT);

		_text_14 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Fuselage Tail tip", "h_\\mathrm{T}", 
				_theFuselage.get_height_T(), 
				3, 0, TEXT_WIDTH_HINT);


		//------------------------------------------------------------------------------
		// Label 06: Nose length ratio
		//------------------------------------------------------------------------------

		_text_06 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Nose Length Ratio", "\\rho_\\mathrm{N}=\\ell_\\mathrm{N}/\\ell_\\mathrm{F}", 
				_theFuselage.get_lenRatio_NF(), 
				3, 0, TEXT_WIDTH_HINT);

		_slider_06 = GridLayoutUtil.createSlider(_ctrComposite, 
				_theFuselage.get_lenRatio_NF().intValue() , 
				_theFuselage.get_lenRatio_NF_MIN().intValue(), 
				_theFuselage.get_lenRatio_NF_MAX().intValue());

		//------------------------------------------------------------------------------
		// Label 07: Central length ratio
		//------------------------------------------------------------------------------

		_text_07 = GridLayoutUtil.putLabel(
				_ctrComposite, 
				"Central Length Ratio", "\\rho_\\mathrm{C}=\\ell_\\mathrm{C}/\\ell_\\mathrm{F}", 
				_theFuselage.get_lenRatio_CF(), 
				3, 0, TEXT_WIDTH_HINT);

		_slider_07 = GridLayoutUtil.createSlider(_ctrComposite, 
				_theFuselage.get_lenRatio_CF().intValue() , 
				_theFuselage.get_lenRatio_CF_MIN().intValue(), 
				_theFuselage.get_lenRatio_CF_MAX().intValue());

		//------------------------------------------------------------------------------
		// Label 08: Tail Cone length ratio
		//------------------------------------------------------------------------------

		GridData gd_Label_08 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_08.horizontalSpan = 3;

		Label label_08 = new Label(_ctrComposite, SWT.NONE);
		label_08.setText("Tail Cone Length Ratio");
		label_08.setLayoutData(gd_Label_08);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_08 = "\\rho_\\mathrm{T}=\\ell_\\mathrm{T}/\\ell_\\mathrm{F}";
		Image formulaImage_08 = MyGuiUtils.renderLatexFormula(this,latex_text_08, 22);
		Label label_08a = new Label(_ctrComposite, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_08, formulaImage_08);
		label_08a.setImage (formulaImage_08);

		GridData gd_Label_08a = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_08a.horizontalSpan = 1;
		gd_Label_08a.widthHint = 25;
		label_08a.setLayoutData(gd_Label_08a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_08 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_08.horizontalSpan = 1;
		gd_text_08.grabExcessHorizontalSpace = true;
		gd_text_08.horizontalAlignment = GridData.FILL;
		gd_text_08.widthHint = TEXT_WIDTH_HINT;

		_text_08 = new Text(_ctrComposite, SWT.BORDER);
		BigDecimal bd_08 = BigDecimal.valueOf( _theFuselage.get_lenRatio_TF());
		bd_08 = bd_08.setScale(3, RoundingMode.HALF_UP);
		_text_08.setText( "  " + bd_08);
		_text_08.setLayoutData(gd_text_08);

		_slider_08 = new Slider(_ctrComposite, SWT.HORIZONTAL);
		_slider_08.setBounds(0, 0, 40, 200);
		_slider_08.setMaximum( _theFuselage.get_lenRatio_TF_MAX().intValue() );  // m
		_slider_08.setMinimum( _theFuselage.get_lenRatio_TF_MIN().intValue() );  // m
		_slider_08.setSelection( _theFuselage.get_lenRatio_TF().intValue() );    // m
		_slider_08.setIncrement(1); // m
		_slider_08.setPageIncrement(5);
		_slider_08.setThumb(1);


		//------------------------------------------------------------------------------
		// Label 09:  Cone Fineness ratio
		//------------------------------------------------------------------------------

		GridData gd_Label_09 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_09.horizontalSpan = 3;

		Label label_09 = new Label(_ctrComposite, SWT.NONE);
		label_09.setText("Nose Fineness ratio");
		label_09.setLayoutData(gd_Label_09);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_09 = "\\lambda_\\mathrm{N}=\\ell_\\mathrm{N}/d_\\mathrm{C}";
		Image formulaImage_09 = MyGuiUtils.renderLatexFormula(this,latex_text_09, 22);
		Label label_09a = new Label(_ctrComposite, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_09, formulaImage_09);
		label_09a.setImage (formulaImage_09);

		GridData gd_Label_09a = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_09a.horizontalSpan = 1;
		gd_Label_09a.widthHint = 25;
		label_09a.setLayoutData(gd_Label_09a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_09 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_09.horizontalSpan = 1;
		gd_text_09.grabExcessHorizontalSpace = true;
		gd_text_09.horizontalAlignment = GridData.FILL;
		gd_text_09.widthHint = TEXT_WIDTH_HINT;

		_text_09 = new Text(_ctrComposite, SWT.BORDER);
		BigDecimal bd_09 = BigDecimal.valueOf( + _theFuselage.get_lambda_N() );
		bd_09 = bd_09.setScale(2, RoundingMode.HALF_UP);
		_text_09.setText( "  " + bd_09);
		_text_09.setLayoutData(gd_text_09);

		_slider_09 = new Slider(_ctrComposite, SWT.HORIZONTAL);
		_slider_09.setBounds(0, 0, 40, 200);
		_slider_09.setMaximum( _theFuselage.get_lambda_N_MAX().intValue() );  // m
		_slider_09.setMinimum( _theFuselage.get_lambda_N_MIN().intValue() );  // m
		_slider_09.setSelection( _theFuselage.get_lambda_N().intValue() );    // m
		_slider_09.setIncrement(1); // m
		_slider_09.setPageIncrement(5);
		_slider_09.setThumb(1);

		//------------------------------------------------------------------------------
		// Label 10:  Central Fineness ratio
		//------------------------------------------------------------------------------

		GridData gd_Label_10 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_10.horizontalSpan = 3;

		Label label_10 = new Label(_ctrComposite, SWT.NONE);
		label_10.setText("Central (Cylindrical) Fuselage Fineness ratio");
		label_10.setLayoutData(gd_Label_10);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_10 = "\\lambda_\\mathrm{C}=\\ell_\\mathrm{C}/d_\\mathrm{C}";
		Image formulaImage_10 = MyGuiUtils.renderLatexFormula(this,latex_text_10, 22);
		Label label_10a = new Label(_ctrComposite, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_10, formulaImage_10);
		label_10a.setImage (formulaImage_10);

		GridData gd_Label_10a = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_10a.horizontalSpan = 1;
		gd_Label_10a.widthHint = 25;
		label_10a.setLayoutData(gd_Label_10a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_10 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_10.horizontalSpan = 1;
		gd_text_10.grabExcessHorizontalSpace = true;
		gd_text_10.horizontalAlignment = GridData.FILL;
		gd_text_10.widthHint = TEXT_WIDTH_HINT;

		_text_10 = new Text(_ctrComposite, SWT.BORDER);
		BigDecimal bd_10 = BigDecimal.valueOf( + _theFuselage.get_lambda_C() );
		bd_10 = bd_10.setScale(2, RoundingMode.HALF_UP);
		_text_10.setText( "  " + bd_10);
		_text_10.setLayoutData(gd_text_10);

		_slider_10 = new Slider(_ctrComposite, SWT.HORIZONTAL);
		_slider_10.setBounds(0, 0, 40, 200);
		_slider_10.setMaximum( _theFuselage.get_lambda_C_MAX().intValue() );  // m
		_slider_10.setMinimum( _theFuselage.get_lambda_C_MIN().intValue() );  // m
		_slider_10.setSelection( _theFuselage.get_lambda_C().intValue() );    // m
		_slider_10.setIncrement(1); // m
		_slider_10.setPageIncrement(5);
		_slider_10.setThumb(1);


		//------------------------------------------------------------------------------
		// Label 11: Tail Cone Fineness ratio
		//------------------------------------------------------------------------------

		GridData gd_Label_11 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_11.horizontalSpan = 3;

		Label label_11 = new Label(_ctrComposite, SWT.NONE);
		label_11.setText("Tail Cone Fineness ratio");
		label_11.setLayoutData(gd_Label_11);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_11 = "\\lambda_\\mathrm{T}=\\ell_\\mathrm{T}/d_\\mathrm{C}";
		Image formulaImage_11 = MyGuiUtils.renderLatexFormula(this,latex_text_11, 22);
		Label label_11a = new Label(_ctrComposite, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_11, formulaImage_11);
		label_11a.setImage (formulaImage_11);

		GridData gd_Label_11a = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_11a.horizontalSpan = 1;
		gd_Label_11a.widthHint = 25;
		label_11a.setLayoutData(gd_Label_11a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_11 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_11.horizontalSpan = 1;
		gd_text_11.grabExcessHorizontalSpace = true;
		gd_text_11.horizontalAlignment = GridData.FILL;
		gd_text_11.widthHint = TEXT_WIDTH_HINT;

		_text_11 = new Text(_ctrComposite, SWT.BORDER);
		BigDecimal bd_11 = BigDecimal.valueOf( + _theFuselage.get_lambda_T() );
		bd_11 = bd_11.setScale(2, RoundingMode.HALF_UP);
		_text_11.setText( "  " + bd_11);
		_text_11.setLayoutData(gd_text_11);

		_slider_11 = new Slider(_ctrComposite, SWT.HORIZONTAL);
		_slider_11.setBounds(0, 0, 40, 200);
		_slider_11.setMaximum( _theFuselage.get_lambda_T_MAX().intValue() );  // m
		_slider_11.setMinimum( _theFuselage.get_lambda_T_MIN().intValue() );  // m
		_slider_11.setSelection( _theFuselage.get_lambda_T().intValue() );    // m
		_slider_11.setIncrement(1); // m
		_slider_11.setPageIncrement(5);
		_slider_11.setThumb(1);


		//------------------------------------------------------------------------------
		// Label 12: Fuselage Fineness ratio
		//------------------------------------------------------------------------------

		GridData gd_Label_12 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_12.horizontalSpan = 3;

		Label label_12 = new Label(_ctrComposite, SWT.NONE);
		label_12.setText("Fuselage Fineness ratio");
		label_12.setLayoutData(gd_Label_12);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_12 = "\\lambda_\\mathrm{F}=\\ell_\\mathrm{F}/d_\\mathrm{C}";
		Image formulaImage_12 = MyGuiUtils.renderLatexFormula(this,latex_text_12, 22);
		Label label_12a = new Label(_ctrComposite, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_12, formulaImage_12);
		label_12a.setImage (formulaImage_12);

		GridData gd_Label_12a = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_Label_12a.horizontalSpan = 1;
		gd_Label_12a.widthHint = 25;
		label_12a.setLayoutData(gd_Label_12a);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_12 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_12.horizontalSpan = 1;
		gd_text_12.grabExcessHorizontalSpace = true;
		gd_text_12.horizontalAlignment = GridData.FILL;
		gd_text_12.widthHint = TEXT_WIDTH_HINT;

		_text_12 = new Text(_ctrComposite, SWT.BORDER);
		BigDecimal bd_12 = BigDecimal.valueOf( + _theFuselage.get_lambda_F() );
		bd_12 = bd_12.setScale(2, RoundingMode.HALF_UP);
		_text_12.setText( "  " + bd_12);
		_text_12.setLayoutData(gd_text_12);		

		_slider_12 = new Slider(_ctrComposite, SWT.HORIZONTAL);
		_slider_12.setBounds(0, 0, 40, 200);
		_slider_12.setMaximum( _theFuselage.get_lambda_F_MAX().intValue() );  // m
		_slider_12.setMinimum( _theFuselage.get_lambda_F_MIN().intValue() );  // m
		_slider_12.setSelection( _theFuselage.get_lambda_F().intValue() );    // m
		_slider_12.setIncrement(1); // m
		_slider_12.setPageIncrement(5);
		_slider_12.setThumb(1);

		// enable/disable buttons/sliders according to criteria
		showControls();

		// I/O controls

		GridData gd_GroupIO = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_GroupIO.horizontalSpan = 3;

		Group groupIO = new Group (_ctrComposite, SWT.NONE);

		groupIO.setLayout (new FillLayout (SWT.VERTICAL));
		groupIO.setLayoutData (gd_GroupIO);
		groupIO.setText("I/O");

		// Import file controls
		Group groupSerializeFile = new Group (groupIO, SWT.NONE);
		groupSerializeFile.setLayout (new FillLayout (SWT.HORIZONTAL));
		groupSerializeFile.setText ("Save status file");
		_text_ExportFile = new Text(groupIO, SWT.BORDER);
		_text_ExportFile.setText( _exportFilePath );
		_text_ExportFile.setEditable(false);

		_button_ExportFile = new Button(groupIO, SWT.NONE);
		_button_ExportFile.setText("Save");
	}
	
	
	private void addListeners() {


		_button_ChooseImportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.Selection:
					FileDialog dialog = new FileDialog(ADOPT_GUI.getApp().getShell());
					dialog.setFilterPath( MyConfiguration.currentDirectory.toString() );
					String sFile = dialog.open();
					if ( sFile != null ) {
						_importFile = new File(sFile);
						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
								"The ADOpT|Choose fuselage data file: " +
										_importFile.toString() + "\n"
								);				    	
						_text_ImportFile.setText(_importFile.toString());
					}				    
					break;
				}
			}
		});

		_button_ImportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.Selection:
					if ( _importFile != null ) {

						if ( _importFile.exists() && _importFile.isFile()  ) {
							_theFuselage.importFromXMLFile(_importFile);
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing	
							redrawFuselageXY();
							redrawFuselageSectionYZ();

						}
					}				    
					break;
				}
			}
		});


		_button_Nomenclature.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {


						final Shell dialogNomenclature = new Shell(Display.getCurrent(),
								SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.RESIZE
								);	

						dialogNomenclature.setText("Fuselage Nomenclature");	
						//						Monitor monitor = Display.getDefault().getMonitors()[0];
						//						dialogNomenclature.setSize(
						//								(int)( 0.75*monitor.getBounds().width  ),
						//								(int)( 0.75*monitor.getBounds().height )
						//								);

						dialogNomenclature.setLayout(new FillLayout());
						_nomenclatureFuselageSection = new MyNomenclatureImageDialog( 
								dialogNomenclature,
								System.getProperty("user.dir")+"/src/images/Fuselage_Nomenclature_Sideview_Topview2.png");

					}
				}
				); // end of button_Nomenclature listener


		_button_SectionShape.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				_dialogShellSectionShape = new MyDialogFuselageSection(
						Display.getCurrent(), ADOPT_GUI.getApp().getInitiatorPaneFuselage(), _theFuselage);





			} // end of selection event
		});  // end of button Section Shape add Selection Listener


		_comboDropDown.addSelectionListener(
				new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent event) {
						// when an item is selected
						// get the string
						String s = _comboDropDown.getText();
						// find the key into the bidi-map
						_selectedAdjustCriterion = _mapAdjustCriteria.inverse().get(s);
						// Update fuselage object
						_theFuselage.setAdjustCriterion(_selectedAdjustCriterion);
						// print to console
						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
								"Criterion: " + s + " " + _selectedAdjustCriterion.toString() + "\n"
								);
						showControls();

					}
				}
				);

		_comboDropDownSectionData.addSelectionListener(
				new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent event) {
						// when an item is selected
						// get the string
						String s = _comboDropDownSectionData.getText();
						// find the key into the bidi-map
						_selectedSectionData = _mapSectionData.inverse().get(s);
						//System.out.println(s);
						redrawFuselageSectionYZ();


					}
				}
				);




		// Parameter of variation : l_F,l_C,l_N,l_T,diam_C,h_N,h_T
		// Implementation of method adjustLength
		// Implementation of adjust controls
		// Redraw

		// l_F
		_text01.addTraverseListener(new TraverseListener() {

			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_01.getText();

				s_value = s_value.trim();

				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {

					// check for illegal values
					try{  

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						len =len.to(SI.METER);


						//						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						//								"The ADOpT | DEBUG: " + len.toString() + "\n"
						//								);

						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER)); 
						if( value < _slider01.getMinimum() || value > _slider01.getMaximum() ){  
							e.doit = false;

							// check bounds
							if( value < _slider01.getMinimum() ) {

								_theFuselage.adjustLength(_theFuselage.get_len_F_MIN(), _selectedAdjustCriterion);
								ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
										"The ADOpT | WARNING: Value in Text box is smallest of minimum value.Insert new value!\n");
							}
							if( value > _slider01.getMaximum() ) {
								ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
										"The ADOpT | WARNING: Value in Text box is greatest of maximum value.Insert new value!\n");
								_theFuselage.adjustLength(_theFuselage.get_len_F_MAX(), _selectedAdjustCriterion);
							}
						}
						else {


							Double l_F = new Double(value);
							_theFuselage.adjustLength(
									Amount.valueOf(l_F.doubleValue(), SI.METRE), 
									_selectedAdjustCriterion);
						}

						adjustControls(); // update controls in GUI
						//recalculateCurves();
						_theFuselage.calculateOutlines();
						redrawFuselageXZ(); // update drawing
						redrawFuselageXY();

					}  
					catch( Exception ex ){  
						if(!s_value.equals("")) {					
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
							e.doit = false;

							// get a convenience value from the slider
							Double l_F = new Double(_slider01.getSelection());
							_theFuselage.adjustLength(
									Amount.valueOf(l_F.doubleValue(), SI.METRE), 
									_selectedAdjustCriterion);
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing	
							redrawFuselageXY();

						}
					}
				}
			}
		});                // end listener text_01


		// l_F
		_slider01.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				// calculate the slider value
				Double perspectiveValue = 
						(double) _slider01.getSelection();

				//_theFuselage.adjustLength(perspectiveValue, _selectedAdjustCriterion);
				Amount<Length> l_F = Amount.valueOf(perspectiveValue, SI.METRE);

				// set the text box value
				_text01.setText(
						l_F.toString() // convert to string
						);

				// update the fuselage object
				_theFuselage.adjustLength(l_F, _selectedAdjustCriterion);
				adjustControls();
				//recalculateCurves();
				_theFuselage.calculateOutlines();
				// update drawing
				redrawFuselageXZ();	
				redrawFuselageXY();


			}   
		});      //  end listener slider_01


		// l_C
		_text_03.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_01.getText();

				s_value= s_value.trim();
				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {


					// check for illegal values
					try{  

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						len =len.to(SI.METER);




						//						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						//								"The ADOpT | DEBUG: " + len.toString() + "\n"
						//								);


						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER)); 
						// check bounds
						if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {

							if( value < _slider_03.getMinimum() || value > _slider_03.getMaximum() ){  
								e.doit = false;

								if( value < _slider_03.getMinimum() ) {

									_theFuselage.adjustLength(_theFuselage.get_len_C_MIN(), _selectedAdjustCriterion);
									ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
											"The ADOpT | WARNING: Value in Text box is smallest of minimum value.Insert new value!\n");
								}
								if( value > _slider_03.getMaximum() ) {
									_theFuselage.adjustLength(_theFuselage.get_len_C_MAX(), _selectedAdjustCriterion);
									ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
											"The ADOpT | WARNING: Value in Text box is greatest of minimum value.Insert new value!\n");
								}
							}
							else {

								Double l_C = new Double(value);
								_theFuselage.adjustLength(
										Amount.valueOf(l_C.doubleValue(), SI.METRE), 
										_selectedAdjustCriterion);
							}
						}
						else {
							Amount<Length> len_C =Amount.valueOf(value, SI.METRE);
							_theFuselage.set_len_C(len_C);
						}
						adjustControls(); // update controls in GUI
						//recalculateCurves();
						_theFuselage.calculateOutlines();
						redrawFuselageXZ(); // update drawing	
						redrawFuselageXY();

					}  
					catch( Exception ex ){  
						if(!s_value.equals("")) {
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
							e.doit = false;

							// get a convenience value from the slider
							Double l_C = new Double(_slider_03.getSelection());
							_theFuselage.adjustLength(
									Amount.valueOf(l_C.doubleValue(), SI.METRE), 
									_selectedAdjustCriterion);
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing	
							redrawFuselageXY();

						}
					}
				}
			}
		});                // end listener text_03

		// l_C
		_slider_03.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				// calculate the slider value
				Double perspectiveValue = 
						(double) _slider_03.getSelection();

				//_theFuselage.adjustLength(perspectiveValue, _selectedAdjustCriterion);
				Amount<Length> len_C = Amount.valueOf(perspectiveValue, SI.METRE);

				// set the text box value
				_text_03.setText(
						len_C.toString() // convert to string
						);

				if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {
					// update the fuselage object
					_theFuselage.adjustLength(len_C, _selectedAdjustCriterion);
				} 
				else {
					_theFuselage.set_len_C(len_C);
				}
				// update GUI controls
				adjustControls(); 
				//recalculateCurves();
				_theFuselage.calculateOutlines();
				// update drawing
				redrawFuselageXZ();	
				redrawFuselageXY();

			}   
		});      //  end listener slider_03

		// l_N
		_text02.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_02.getText();

				s_value = s_value.trim();

				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {

					// check for illegal values
					try{  

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						len =len.to(SI.METER);

						//						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						//								"The ADOpT | DEBUG: " + len.toString() + "\n"
						//								);
						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER));

						if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {

							if( value < _slider02.getMinimum() || value > _slider02.getMaximum() ){  
								e.doit = false;

								if( value < _slider02.getMinimum() ) {

									_theFuselage.adjustLength(_theFuselage.get_len_N_MIN(), _selectedAdjustCriterion);
								}
								if( value > _slider02.getMaximum() ) {

									_theFuselage.adjustLength(_theFuselage.get_len_N_MAX(), _selectedAdjustCriterion);
								}
							}

							else {

								Double l_N = new Double(value);
								_theFuselage.adjustLength(
										Amount.valueOf(l_N.doubleValue(), SI.METRE), 
										_selectedAdjustCriterion);
							}
						}
						else {
							Amount<Length> len_N = Amount.valueOf(value, SI.METRE);
							_theFuselage.set_len_N(len_N);
						}
						adjustControls(); // update controls in GUI
						//recalculateCurves();
						_theFuselage.calculateOutlines();
						redrawFuselageXZ(); // update drawing	
						redrawFuselageXY();

					}  
					catch( Exception ex ){  
						if(!s_value.equals("")) {
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
							e.doit = false;

							// get a convenience value from the slider
							Double l_N = new Double(_slider02.getSelection());
							_theFuselage.adjustLength(
									Amount.valueOf(l_N.doubleValue(), SI.METRE), 
									_selectedAdjustCriterion);
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing	
							redrawFuselageXY();
						}
					}
				}
			}
		});                // end listener text_02

		// l_N
		_slider02.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				// calculate the slider value
				Double perspectiveValue = 
						(double) _slider02.getSelection();

				Amount<Length> len_N =  Amount.valueOf(perspectiveValue, SI.METRE);

				// set the text box value
				_text02.setText(
						len_N.toString() // convert to string
						);

				if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {
					// update the fuselage object
					_theFuselage.adjustLength(len_N, _selectedAdjustCriterion);
				} 
				else {
					_theFuselage.set_len_N(len_N);
				}
				adjustControls();
				//recalculateCurves();
				_theFuselage.calculateOutlines();
				// update drawing
				redrawFuselageXZ();	
				redrawFuselageXY();

			}   
		});      //  end listener slider_02


		// l_T
		_text04.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_04.getText();

				s_value = s_value.trim();

				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {

					// check for illegal values
					try{  

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						len =len.to(SI.METER);

						//						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						//								"The ADOpT | DEBUG: " + len.toString() + "\n"
						//								);

						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER)); 

						// check bounds
						if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {
							if( value < _slider04.getMinimum() || value > _slider04.getMaximum() ){  
								e.doit = false;

								if( value < _slider04.getMinimum() ) {

									_theFuselage.adjustLength(_theFuselage.get_len_T_MIN(), _selectedAdjustCriterion);
								}
								if( value > _slider04.getMaximum() ) {

									_theFuselage.adjustLength(_theFuselage.get_len_T_MAX(), _selectedAdjustCriterion);
								}
							}
							else {

								Double l_T = new Double(value);
								_theFuselage.adjustLength(
										Amount.valueOf(l_T.doubleValue(), SI.METRE), 
										_selectedAdjustCriterion);
							}
						}
						else {
							Amount<Length> len_T = Amount.valueOf(value, SI.METRE);
							_theFuselage.set_len_T(len_T);
						}
						adjustControls(); // update controls in GUI
						//recalculateCurves();
						_theFuselage.calculateOutlines();
						redrawFuselageXZ(); // update drawing	
						redrawFuselageXY();

					}  
					catch( Exception ex ){  
						if(!s_value.equals("")) {
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
							e.doit = false;

							// get a convenience value from the slider
							Double l_T = new Double(_slider04.getSelection());
							_theFuselage.adjustLength(
									Amount.valueOf(l_T.doubleValue(), SI.METRE), 
									_selectedAdjustCriterion);
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing
							redrawFuselageXY();
						}
					}
				}
			}
		});                // end listener text_04

		// l_T
		_slider04.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				// calculate the slider value
				Double perspectiveValue = 
						(double) _slider04.getSelection();

				//_theFuselage.adjustLength(perspectiveValue, _selectedAdjustCriterion);
				Amount<Length> len_T = Amount.valueOf(perspectiveValue, SI.METRE);

				// set the text box value
				_text04.setText(
						len_T.toString() // convert to string
						);

				// update the fuselage object
				if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {
					// update the fuselage object
					_theFuselage.adjustLength(len_T, _selectedAdjustCriterion);
				} 
				else {
					_theFuselage.set_len_T(len_T);
				}
				adjustControls(); 
				//recalculateCurves();
				_theFuselage.calculateOutlines();
				// update drawing
				redrawFuselageXZ();	
				redrawFuselageXY();


			}   
		});      //  end listener slider_04

		// diam_C
		_text_05.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_05.getText();

				s_value = s_value.trim();

				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {

					// check for illegal values
					try{  

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						len =len.to(SI.METER);

						//						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						//								"The ADOpT | DEBUG: " + len.toString() + "\n"
						//								);

						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER)); 
						// check bounds
						if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {

							if( value < _slider_05.getMinimum() || value > _slider_05.getMaximum() ){  
								e.doit = false;


								if( value < _slider_05.getMinimum() ) {

									_theFuselage.adjustLength(_theFuselage.get_diam_C_MIN(), _selectedAdjustCriterion);
								}
								if( value > _slider_05.getMaximum() ) {
									//_text_01.setText( _theFuselage.get_len_F_MAX().toString() );
									// update the fuselage object
									_theFuselage.adjustLength(_theFuselage.get_diam_C_MAX(), _selectedAdjustCriterion);
								}

							}

							else {

								Double d_C = new Double(value);
								_theFuselage.adjustLength(
										Amount.valueOf(d_C.doubleValue(), SI.METRE), 
										_selectedAdjustCriterion);

							}
						}
						// update the fuselage object

						else {
							Amount<Length> diam_C = Amount.valueOf(value, SI.METRE);
							_theFuselage.set_sectionCylinderHeight(diam_C);


						}

						adjustControls(); // update controls in GUI
						//recalculateCurves();
						_theFuselage.calculateOutlines();
						redrawFuselageXZ(); // update drawing
						redrawFuselageSectionYZ();

					}  
					catch( Exception ex ){  
						if(!s_value.equals("")) {
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
							e.doit = false;

							// get a convenience value from the slider
							Double diam_C = new Double(_slider_05.getSelection());					
							_theFuselage.adjustLength(
									Amount.valueOf(diam_C.doubleValue(), SI.METRE), 
									_selectedAdjustCriterion);
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing
							redrawFuselageSectionYZ();
						}
					}
				}
			}
		});                // end listener text_05

		// diam_C
		_slider_05.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				// calculate the slider value
				Double perspectiveValue = 
						(double) _slider_05.getSelection();

				//_theFuselage.adjustLength(perspectiveValue, _selectedAdjustCriterion);
				Amount<Length> diam_C = Amount.valueOf(perspectiveValue, SI.METRE);
				//				LengthAmount h_B = new LengthAmount(perspectiveValue, SI.METRE);
				// set the text box value
				_text_05.setText(
						diam_C.toString() // convert to string
						);

				// update the fuselage object
				// update the fuselage object
				if (_selectedAdjustCriterion != MyFuselageAdjustCriteria.NONE) {
					// update the fuselage object
					_theFuselage.adjustLength(diam_C, _selectedAdjustCriterion);
				} 
				else {
					_theFuselage.set_sectionCylinderHeight(diam_C);

				}
				adjustControls(); 
				//recalculateCurves();
				_theFuselage.calculateOutlines();
				// update drawing
				redrawFuselageXZ();	
				redrawFuselageSectionYZ();

				// TODO:
				// update global observable object
				// Note: To get notified you have to assign different references. 
				System.out.println("----------------->");
				// using cloning library
				// https://code.google.com/p/cloning/wiki/Usage
				Cloner cloner=new Cloner();
				GlobalData
				.theCurrentFuselageProperty
				.setValue(
						// _theFuselage this won't get a notification
						// new MyFuselage(_theFuselage) // OK
						// (MyFuselage)deepClone(_theFuselage) // require MyFuselage and several other classes to implement Serializable
						cloner.deepClone(_theFuselage) // deep clone
						);
				// ADOPT_GUI.getApp().setTheCurrentFuselage(_theFuselage);
				// TODO: implement this mechanism for all other event handlers

			}   
		});      //  end listener slider_05


		//h_N
		_text_13.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; // text_05.getText();

				s_value = s_value.trim();

				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {

					// check for illegal values
					try{ 

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						len =len.to(SI.METER);

						//						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						//								"The ADOpT | DEBUG: " + len.toString() + "\n"
						//								);

						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER)); 
						// check bounds

						if( value < _theFuselage.get_height_N_MIN().doubleValue(SI.METRE) || value > _theFuselage.get_height_N_MAX().doubleValue(SI.METRE)){  
							e.doit = false;

							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: bad h_N .Insert new value!\n");
						}

						else {
							Amount<Length> h_N = Amount.valueOf(value, SI.METRE);
							_theFuselage.set_height_N(h_N);
						}

						adjustControls(); // update controls in GUI
						//recalculateCurves();
						_theFuselage.calculateOutlines();
						redrawFuselageXZ(); // update drawing
						redrawFuselageSectionYZ();

					}  
					catch( Exception ex ){  
						if(!s_value.equals("")) {
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
							e.doit = false;
							// get a convenience value from the slider				
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing
							redrawFuselageSectionYZ();
						}
					}
				}
			}
		});                // end listener text_13

		//h_T
		_text_14.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				String currentText = ((Text)e.widget).getText();
				String s_value = currentText; 

				s_value = s_value.trim();

				// catch the TRAVERSE_RETURN event and captures value
				if (e.detail == SWT.TRAVERSE_RETURN) {

					// check for illegal values
					try{ 

						// parse text string and get the Amount<Length> object
						//http://jscience.org/api/javax/measure/DecimalMeasure.html
						DecimalMeasure<Length> len =  DecimalMeasure.valueOf(s_value);	
						len =len.to(SI.METER);

						//						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						//								"The ADOpT | DEBUG: " + len.toString() + "\n"
						//								);

						Double value = Double.valueOf(len.doubleValue(javax.measure.unit.SI.METER)); 
						// check bounds

						if( value < _theFuselage.get_height_T_MIN().doubleValue(SI.METRE) || value > _theFuselage.get_height_T_MAX().doubleValue(SI.METRE)){  
							e.doit = false;

							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: bad h_T .Insert new value!\n");
						}

						else {
							Amount<Length> h_T = Amount.valueOf(value, SI.METRE);
							_theFuselage.set_height_T(h_T);
						}

						adjustControls(); // update controls in GUI
						//recalculateCurves();
						_theFuselage.calculateOutlines();
						redrawFuselageXZ(); // update drawing
						redrawFuselageSectionYZ();

					}  
					catch( Exception ex ){  
						if(!s_value.equals("")) {
							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
									"The ADOpT | WARNING: unacceptable value in Text box. Insert unit measure.\n"
									);
							e.doit = false;
							// get a convenience value from the slider				
							adjustControls(); // update controls in GUI
							//recalculateCurves();
							_theFuselage.calculateOutlines();
							redrawFuselageXZ(); // update drawing
							redrawFuselageSectionYZ();
						}
					}
				}
			}
		});                // end listener text_14



		_button_ExportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				//				FileOutputStream fos = null;
				//				ObjectOutputStream out = null;
				String filePath = 
						MyConfiguration.currentDirectory 
						+ File.separator + EXPORT_FILE_NAME;

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The ADOpT | Serialize fuselage initiator file: " +	filePath + "\n"
						);				    	

				switch (event.type) {
				case SWT.Selection:
					// Write XML
					_exportFile = new File(_exportFilePath);
					//					myUtilities.exportToXMLFile(EXPORT_FILE_NAME);
					break;
				}// end-of-switch
			}// end-of-handle-event
		});
	}


	public MyFuselageSectionData get_selectedSectionData() {
		return _selectedSectionData;
	}	

	public int get_selectedSectionIdx() {
		int idx = 0;
		MyFuselageSectionData sd = get_selectedSectionData();
		switch (sd)
		{
		case NOSE_TIP:
			idx = _theFuselage.IDX_SECTION_YZ_NOSE_TIP;
			break;
		case NOSE_CAP:
			idx = _theFuselage.IDX_SECTION_YZ_NOSE_CAP;
			break;
		case MID_NOSE:
			idx = _theFuselage.IDX_SECTION_YZ_MID_NOSE;
			break;
		case CYLINDRICAL_BODY_1:
			idx = _theFuselage.IDX_SECTION_YZ_CYLINDER_1;
			break;
		case CYLINDRICAL_BODY_2:
			idx = _theFuselage.IDX_SECTION_YZ_CYLINDER_2;
			break;
		case MID_TAIL:
			idx = _theFuselage.IDX_SECTION_YZ_MID_TAIL;
			break;
		case TAIL_CAP:
			idx = _theFuselage.IDX_SECTION_YZ_TAIL_CAP;
			break;
		case TAIL_TIP:
			idx = _theFuselage.IDX_SECTION_YZ_TAIL_TIP;
			break;
		default:
			idx = 0;
			break;
		}
		return idx;
	}	


	/**
	 * This method makes a "deep clone" of any object it is given.
	 */
	public static Object deepClone(Object object) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return ois.readObject();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}// end of class


//GridData gd_Label_01 = new GridData(SWT.FILL, SWT.FILL, true, false);
//gd_Label_01.horizontalSpan = 3;
////gd_Label_01.widthHint = 100;
//Label label_01 = new Label(_ctrComposite, SWT.NONE);
//label_01.setText("Fuselage length");
//label_01.setLayoutData(gd_Label_01);
//
////++++++++++++++++++++++++++++++++++++++++++++++++++++++
//String latex_text_01 = "\\ell_\\mathrm{F}";
//Image formulaImage_01 = MyGuiUtils.renderLatexFormula(this,latex_text_01, 22);
//Label label_01a = new Label(_ctrComposite, SWT.NONE);
//ADOPT_GUI.getfCachedImages().put(latex_text_01, formulaImage_01);
//label_01a.setImage (formulaImage_01);
//
//GridData gd_Label_01a = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
//gd_Label_01a.horizontalSpan = 1;
//gd_Label_01a.widthHint = 25;
//label_01a.setLayoutData(gd_Label_01a);
////++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//GridData gd_text_01 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
//gd_text_01.horizontalSpan = 1;
//gd_text_01.grabExcessHorizontalSpace = true;
//gd_text_01.horizontalAlignment = GridData.FILL;
//gd_text_01.widthHint = TEXT_WIDTH_HINT;
//_text_01 = new Text(_ctrComposite, SWT.BORDER);
//BigDecimal bd_01 = BigDecimal.valueOf( _theFuselage.get_len_F().getEstimatedValue());
//bd_01 = bd_01.setScale(3, RoundingMode.HALF_UP);
//_text_01.setText( "  " +bd_01+ " " + _theFuselage.get_len_F().getUnit().toString());
//_text_01.setLayoutData(gd_text_01);