package it.unina.adopt.GUI.tabpanels.liftingSurface;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import it.unina.adopt.utilities.gui.MyGuiUtils;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.layout.TreeColumnLayout;

public class MyActualLiftingSurface extends Composite {
	private DataBindingContext m_bindingContext;
	private Text text_11;
	private Text text_12;
	private Text text_13;
	private Text text_14;
	private Text text_15;
	private Text text_20;
	private Text text_16;
	private Text text_17;
	private Text text_18;
	private Text text_19;
	private Text text_21;
	private Text text_27;
	private Text text_28;
	private Text text_29;
	private Text text_32;
	private Text text_33;
	private Text text_36;
	private Text text_37;
	private Text text_38;
	private Text text_39;
	private Group grpActualWing;
	private Text text;
	private Button btnUseActualWing;
	private MyEquivalentLiftingSurface equivalentLSComposite = null;
	private boolean isActualSelected = true;
	private boolean isEquivalentSelected = false;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyActualLiftingSurface(Composite parent, int style) {

		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		grpActualWing = new Group(this, SWT.FILL);
		grpActualWing.setText("Actual Wing");
		grpActualWing.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		grpActualWing.setLayout(new GridLayout(6, false));

		btnUseActualWing = new Button(grpActualWing, SWT.RADIO);
		btnUseActualWing.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (equivalentLSComposite!=null) {
					equivalentLSComposite.getBtnUseEquivalentWing().setSelection(false);
					MyGuiUtils.recursiveSetEditable(grpActualWing, true);
					MyGuiUtils.recursiveSetEditable(equivalentLSComposite, false);
				}
			}
		});
		btnUseActualWing.setText("Use actual wing");
		btnUseActualWing.setSelection(false);
		
		Label lblNewLabel_3 = new Label(grpActualWing, SWT.NONE);
		lblNewLabel_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 2, 1));
		lblNewLabel_3.setText("Taper ratio");

		text_11 = new Text(grpActualWing, SWT.BORDER);
		text_11.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 2, 1));
		new Label(grpActualWing, SWT.NONE);
		
		Group grpChords_1 = new Group(grpActualWing, SWT.NONE);
		GridData gd_grpChords_1 = new GridData(SWT.FILL, SWT.FILL, true, true, 5, 3);
		gd_grpChords_1.widthHint = 448;
		grpChords_1.setLayoutData(gd_grpChords_1);
		grpChords_1.setText("Chords");
		grpChords_1.setLayout(new GridLayout(4, false));

		Label label_15 = new Label(grpChords_1, SWT.NONE);
		label_15.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_15.setText("Root chord");

		text_29 = new Text(grpChords_1, SWT.BORDER);
		text_29.setText("");
		text_29.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		new Label(grpChords_1, SWT.NONE);
		new Label(grpChords_1, SWT.NONE);

		Label label_21 = new Label(grpChords_1, SWT.NONE);
		label_21.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_21.setText("Kink chord");

		text_32 = new Text(grpChords_1, SWT.BORDER);
		text_32.setText("");
		text_32.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_7 = new Label(grpChords_1, SWT.NONE);
		label_7.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_7.setText("Mean Geometric");

		text_28 = new Text(grpChords_1, SWT.BORDER | SWT.READ_ONLY);
		text_28.setText("");
		text_28.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_22 = new Label(grpChords_1, SWT.NONE);
		label_22.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_22.setText("Tip chord");

		text_33 = new Text(grpChords_1, SWT.BORDER);
		text_33.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_14 = new Label(grpChords_1, SWT.NONE);
		label_14.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_14.setText("Mean Aerodynamic");

		text_27 = new Text(grpChords_1, SWT.BORDER | SWT.READ_ONLY);
		text_27.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblXLeKink = new Label(grpChords_1, SWT.NONE);
		lblXLeKink.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		lblXLeKink.setText("X LE kink chord");

		text_38 = new Text(grpChords_1, SWT.BORDER);
		text_38.setText("");
		text_38.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblXLeMac = new Label(grpChords_1, SWT.NONE);
		lblXLeMac.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		lblXLeMac.setText("X LE MAC");

		text_37 = new Text(grpChords_1, SWT.BORDER | SWT.READ_ONLY);
		text_37.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblYLeKink = new Label(grpChords_1, SWT.NONE);
		lblYLeKink.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		lblYLeKink.setText("Y LE kink chord");

		text_39 = new Text(grpChords_1, SWT.BORDER);
		text_39.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblYLeMac = new Label(grpChords_1, SWT.NONE);
		lblYLeMac.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		lblYLeMac.setText("Y LE MAC");

		text_36 = new Text(grpChords_1, SWT.BORDER | SWT.READ_ONLY);
		text_36.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblXLeTip = new Label(grpChords_1, SWT.NONE);
		lblXLeTip.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		lblXLeTip.setText("X LE tip chord");

		text = new Text(grpChords_1, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		new Label(grpChords_1, SWT.NONE);
		new Label(grpChords_1, SWT.NONE);

		Group group_1 = new Group(grpActualWing, SWT.NONE);
		group_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		group_1.setText("Inner panel");
		group_1.setLayout(new GridLayout(2, false));

		Label lblSurface_1 = new Label(group_1, SWT.NONE);
		lblSurface_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblSurface_1.setText("Surface");

		text_12 = new Text(group_1, SWT.BORDER);
		text_12.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblAspectRatio_1 = new Label(group_1, SWT.NONE);
		lblAspectRatio_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		lblAspectRatio_1.setText("Aspect Ratio");

		text_13 = new Text(group_1, SWT.BORDER);
		text_13.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_11 = new Label(group_1, SWT.NONE);
		label_11.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_11.setText("Taper ratio");

		text_14 = new Text(group_1, SWT.BORDER);
		text_14.setText("");
		text_14.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_12 = new Label(group_1, SWT.NONE);
		label_12.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_12.setText("Sweep LE");

		text_15 = new Text(group_1, SWT.BORDER);
		text_15.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_13 = new Label(group_1, SWT.NONE);
		label_13.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_13.setText("Sweep c/4");

		text_20 = new Text(group_1, SWT.BORDER);
		text_20.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Group group = new Group(grpActualWing, SWT.NONE);
		group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		group.setText("Outer panel");
		group.setLayout(new GridLayout(2, false));

		Label lblSurface_2 = new Label(group, SWT.NONE);
		lblSurface_2.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblSurface_2.setText("Surface");

		text_16 = new Text(group, SWT.BORDER);
		text_16.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblAspectRatio_2 = new Label(group, SWT.NONE);
		lblAspectRatio_2.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		lblAspectRatio_2.setText("Aspect Ratio");

		text_17 = new Text(group, SWT.BORDER);
		text_17.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_5 = new Label(group, SWT.NONE);
		label_5.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_5.setText("Taper ratio");

		text_18 = new Text(group, SWT.BORDER);
		text_18.setText("");
		text_18.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_6 = new Label(group, SWT.NONE);
		label_6.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_6.setText("Sweep LE");

		text_19 = new Text(group, SWT.BORDER);
		text_19.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_9 = new Label(group, SWT.NONE);
		label_9.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_9.setText("Sweep c/4");

		text_21 = new Text(group, SWT.BORDER);
		text_21.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		m_bindingContext = initDataBindings();

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Group getGrpActualWing() {
		return grpActualWing;
	}
	public Button getBtnUseActualWing() {
		return btnUseActualWing;
	}

	public void setEquivalentLSComposite(
			MyEquivalentLiftingSurface equivalentLSComposite) {
		this.equivalentLSComposite = equivalentLSComposite;
	}

	public boolean isActualSelected() {
		return isActualSelected;
	}

	public void setActualSelected(boolean isActualSelected) {
		this.isActualSelected = isActualSelected;
	}

	public boolean isEquivalentSelected() {
		return isEquivalentSelected;
	}

	public void setEquivalentSelected(boolean isEquivalentSelected) {
		this.isEquivalentSelected = isEquivalentSelected;
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		return bindingContext;
	}
}
