package it.unina.adopt.GUI.tabpanels.liftingSurface;

import javax.measure.unit.NonSI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.jscience.physics.amount.Amount;

import aircraft.auxiliary.airfoil.MyAirfoil;
import it.unina.adopt.GUI.tabpanels.MyPanel;

public class MyAirfoilAerodynamicsPanel extends MyPanel {
	private Text text;
	private Label txtAlphaEndLinearity;
	private Label txtAlphaStall;
	private Label txtClalpha;
	private Label txtCdmin;
	private Label txtClAtCd;
	private Label txtClEndLinearity;
	private Label txtClMax;
	private Label txtKFactorDrag;
	private Label txtCmAlpha;
	private Label txtXac;
	private Label txtCmAc;
	private Label txtCmacAtStall;
	private Text text_1;
	private Text text_2;
	private Text text_5;
	private Text text_6;
	private Text text_7;
	private Text text_8;
	private Text text_9;
	private Text text_10;
	private Text text_11;
	private Text text_12;
	private Text text_13;
	private Text text_14;
	private MyAirfoil airfoil;
	private Label label_11;
	private Label label_12;
	private Group groupAerodynamics;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyAirfoilAerodynamicsPanel(Composite parent, CTabItem tab, int style) {
		super(parent, tab, style);
	}

	public MyAirfoilAerodynamicsPanel(Composite parent, int style) {
		
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		groupAerodynamics = new Group(this, SWT.FILL);
		groupAerodynamics.setText("Section aerodynamics");
		groupAerodynamics.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		groupAerodynamics.setLayout(new GridLayout(2, false));
		
		Label lblAlphaZeroLift = new Label(groupAerodynamics, SWT.NONE);
		lblAlphaZeroLift.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		lblAlphaZeroLift.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		lblAlphaZeroLift.setText("Alpha zero lift");
		
		text = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text.widthHint = 72;
		text.setLayoutData(gd_text);
		text.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		text.setText("-3");
		text.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				String value = ((Text)e.widget).getText();
				if (airfoil != null)
				airfoil.getAerodynamics().set_alphaZeroLift(Amount.valueOf(Double.parseDouble(value), NonSI.DEGREE_ANGLE));
			}
		});
		
		txtAlphaEndLinearity = new Label(groupAerodynamics, SWT.NONE);
		txtAlphaEndLinearity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtAlphaEndLinearity.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtAlphaEndLinearity.setText("Alpha end linearity");
		
		text_1 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_1 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_1.widthHint = 74;
		text_1.setLayoutData(gd_text_1);
		text_1.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				String value = ((Text)arg0.widget).getText();
				if (airfoil != null)
				airfoil.getAerodynamics().set_alphaStar((Amount.valueOf(Double.parseDouble(value), NonSI.DEGREE_ANGLE)));
			}
		});
		
		text_1.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		text_1.setText("15");
		
		txtAlphaStall = new Label(groupAerodynamics, SWT.NONE);
		txtAlphaStall.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtAlphaStall.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtAlphaStall.setText("Alpha stall");
		
		text_2 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_2 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_2.widthHint = 124;
		text_2.setLayoutData(gd_text_2);
		text_2.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				
			}
		});
		text_2.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		label_12 = new Label(groupAerodynamics, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_12.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		
		txtClalpha = new Label(groupAerodynamics, SWT.NONE);
		txtClalpha.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtClalpha.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtClalpha.setText("Cl_alpha");
		
		text_5 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_5 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_5.widthHint = 124;
		text_5.setLayoutData(gd_text_5);
		text_5.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
			}
		});
		text_5.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtClEndLinearity = new Label(groupAerodynamics, SWT.NONE);
		txtClEndLinearity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtClEndLinearity.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtClEndLinearity.setText("Cl end linearity");
		
		text_8 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_8 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_8.widthHint = 124;
		text_8.setLayoutData(gd_text_8);
		text_8.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtClMax = new Label(groupAerodynamics, SWT.NONE);
		txtClMax.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtClMax.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtClMax.setText("Cl max");
		
		text_9 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_9 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_9.widthHint = 124;
		text_9.setLayoutData(gd_text_9);
		text_9.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtCdmin = new Label(groupAerodynamics, SWT.NONE);
		txtCdmin.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtCdmin.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtCdmin.setText("Cd min");
		
		text_6 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_6 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_6.widthHint = 124;
		text_6.setLayoutData(gd_text_6);
		text_6.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtClAtCd = new Label(groupAerodynamics, SWT.NONE);
		txtClAtCd.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtClAtCd.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtClAtCd.setText("Cl at Cd min");
		
		text_7 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_7 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_7.widthHint = 124;
		text_7.setLayoutData(gd_text_7);
		text_7.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtKFactorDrag = new Label(groupAerodynamics, SWT.NONE);
		txtKFactorDrag.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtKFactorDrag.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtKFactorDrag.setText("K factor drag");
		
		text_10 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_10 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_10.widthHint = 124;
		text_10.setLayoutData(gd_text_10);
		text_10.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		label_11 = new Label(groupAerodynamics, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_11.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		
		txtCmAlpha = new Label(groupAerodynamics, SWT.NONE);
		txtCmAlpha.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtCmAlpha.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtCmAlpha.setText("Cm alpha");
		
		text_11 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_11 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_11.widthHint = 124;
		text_11.setLayoutData(gd_text_11);
		text_11.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtXac = new Label(groupAerodynamics, SWT.NONE);
		txtXac.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtXac.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtXac.setText("Xac");
		
		text_12 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_12 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_12.widthHint = 124;
		text_12.setLayoutData(gd_text_12);
		text_12.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtCmAc = new Label(groupAerodynamics, SWT.NONE);
		txtCmAc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtCmAc.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtCmAc.setText("CmAC");
		
		text_13 = new Text(groupAerodynamics, SWT.BORDER);
		GridData gd_text_13 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text_13.widthHint = 124;
		text_13.setLayoutData(gd_text_13);
		text_13.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		txtCmacAtStall = new Label(groupAerodynamics, SWT.NONE);
		txtCmacAtStall.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtCmacAtStall.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtCmacAtStall.setText("CmAC at stall");
		
		text_14 = new Text(groupAerodynamics, SWT.BORDER);
		text_14.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		text_14.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void setAirfoil(MyAirfoil airfoil) {
		this.airfoil = airfoil;
	}

}
