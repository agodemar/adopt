package it.unina.adopt.GUI.tabpanels.liftingSurface;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import it.unina.adopt.GUI.tabpanels.MyPanel;

public class MyAirfoilGeometryPanel extends MyPanel {
	private Text text;
	private Label lblPositionAlongSemispan;
	private Label lblTwistRelativeTo;
	private Label lblMaximumTc;
	private Label lblX;
	private Label lblY;
	private Label lblZ;
	private Text text_1;
	private Text text_2;
	private Text text_3;
	private Text text_4;
	private Text text_5;
	private Group groupGeometry;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyAirfoilGeometryPanel(Composite parent, CTabItem tab, int style) {
		super(parent, tab, style);

	}

	public MyAirfoilGeometryPanel(Composite parent, int style) {
		
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		groupGeometry = new Group(this, SWT.FILL);
		groupGeometry.setText("Section geometry");
		groupGeometry.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		groupGeometry.setLayout(new GridLayout(2, false));
		
		lblPositionAlongSemispan = new Label(groupGeometry, SWT.NONE);
		lblPositionAlongSemispan.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		lblPositionAlongSemispan.setText("Position along semispan");
		
		text = new Text(groupGeometry, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		text.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
			}
		});
		
		lblTwistRelativeTo = new Label(groupGeometry, SWT.NONE);
		lblTwistRelativeTo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		lblTwistRelativeTo.setText("Twist relative to root chord");
		
		text_1 = new Text(groupGeometry, SWT.BORDER);
		text_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		lblMaximumTc = new Label(groupGeometry, SWT.NONE);
		lblMaximumTc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		lblMaximumTc.setText("Maximum t/c");
		
		text_2 = new Text(groupGeometry, SWT.BORDER);
		text_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		lblX = new Label(groupGeometry, SWT.NONE);
		lblX.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		lblX.setText("x");
		
		text_3 = new Text(groupGeometry, SWT.BORDER);
		text_3.setText("");
		text_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		lblY = new Label(groupGeometry, SWT.NONE);
		lblY.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		lblY.setText("y");
		
		text_4 = new Text(groupGeometry, SWT.BORDER);
		text_4.setText("");
		text_4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		lblZ = new Label(groupGeometry, SWT.NONE);
		lblZ.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		lblZ.setText("z");
		
		text_5 = new Text(groupGeometry, SWT.BORDER);
		text_5.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
