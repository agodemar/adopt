package it.unina.adopt.GUI.tabpanels.liftingSurface;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

public class MyCommonData extends Composite {
	private Text text_3;
	private Text text_4;
	private Text text_5;
	private Text text_40;
	private Text text;
	private Text text_1;
	private Text text_2;
	private Group grpPosition;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyCommonData(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
//		Composite leftComposite = new Composite(parent, SWT.FILL);
//		leftComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		grpPosition = new Group(this, style);
		grpPosition.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		grpPosition.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_FOREGROUND));
		grpPosition.setLayout(new GridLayout(3, false));
		grpPosition.setText("Common data");
		
		Composite composite_2 = new Composite(grpPosition, SWT.NONE);
		GridData gd_composite_2 = new GridData(SWT.CENTER, SWT.FILL, true, true, 1, 4);
		gd_composite_2.widthHint = 149;
		composite_2.setLayoutData(gd_composite_2);
		composite_2.setLayout(new GridLayout(2, false));
		
		Label label_3 = new Label(composite_2, SWT.NONE);
		label_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_3.setText("X (BRF)");
		
		text = new Text(composite_2, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		Label label_1 = new Label(composite_2, SWT.NONE);
		label_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_1.setText("Y (BRF)");
		
		text_1 = new Text(composite_2, SWT.BORDER);
		text_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		Label label = new Label(composite_2, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label.setText("Z (BRF)");
		
		text_2 = new Text(composite_2, SWT.BORDER);
		text_2.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		Label lblAspectRatio = new Label(grpPosition, SWT.NONE);
		lblAspectRatio.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		lblAspectRatio.setText("Aspect Ratio");
		
		text_3 = new Text(grpPosition, SWT.BORDER);
		text_3.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		Label lblSurface = new Label(grpPosition, SWT.NONE);
		lblSurface.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		lblSurface.setText("Surface");
		
		text_4 = new Text(grpPosition, SWT.BORDER);
		text_4.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		Label label_27 = new Label(grpPosition, SWT.NONE);
		label_27.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_27.setText("Span");
		
		text_40 = new Text(grpPosition, SWT.BORDER);
		text_40.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		Label label_2 = new Label(grpPosition, SWT.NONE);
		label_2.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
		label_2.setText("Kink span station");
		
		text_5 = new Text(grpPosition, SWT.BORDER);
		text_5.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Group getGrpPosition() {
		return grpPosition;
	}
}
