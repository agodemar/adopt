package it.unina.adopt.GUI.tabpanels.liftingSurface;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import it.unina.adopt.utilities.gui.MyGuiUtils;

public class MyEquivalentLiftingSurface extends Composite {
	private Binding radio_binding;
	private DataBindingContext m_bindingContext;
	private Text text_6;
	private Text text_7;
	private Text text_8;
	private Text text_9;
	private Text text_10;
	private Group cmpEquivalentWing;
	private Text text_11;
	private Text text;
	private Text text_1;
	private Text text_2;
	private Text text_3;
	private Text text_4;
	private Text text_5;
	private Text text_12;
	private Text text_13;
	private Text text_14;
	private Button btnUseEquivalentWing;
	private MyActualLiftingSurface actualLSComposite;
	private boolean isActualSelected = false;
	private boolean isEquivalentSelected = true;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyEquivalentLiftingSurface(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));

		cmpEquivalentWing = new Group(this, SWT.FILL);
		cmpEquivalentWing.setText("Equivalent wing");
		cmpEquivalentWing.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		cmpEquivalentWing.setLayout(new GridLayout(3, false));

		btnUseEquivalentWing = new Button(cmpEquivalentWing, SWT.RADIO);
		btnUseEquivalentWing.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (actualLSComposite!=null) {
					actualLSComposite.getBtnUseActualWing().setSelection(false);
					MyGuiUtils.recursiveSetEditable(cmpEquivalentWing, true);
					MyGuiUtils.recursiveSetEditable(actualLSComposite, false);
				}
			}
		});
		btnUseEquivalentWing.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 2, 1));
		btnUseEquivalentWing.setText("Use equivalent wing");
		btnUseEquivalentWing.setSelection(false);

		Group group = new Group(cmpEquivalentWing, SWT.NONE);
		group.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 6));
		group.setText("Chords");
		group.setLayout(new GridLayout(4, false));

		Label label = new Label(group, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label.setText("Root chord");

		text = new Text(group, SWT.BORDER);
		text.setText("");
		text.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_1 = new Label(group, SWT.NONE);
		label_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_1.setText("Mean Geometric");

		text_1 = new Text(group, SWT.BORDER | SWT.READ_ONLY);
		text_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_2 = new Label(group, SWT.NONE);
		label_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_2.setText("Kink chord");

		text_2 = new Text(group, SWT.BORDER);
		text_2.setText("");
		text_2.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_3 = new Label(group, SWT.NONE);
		label_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_3.setText("Mean Aerodynamic");

		text_3 = new Text(group, SWT.BORDER | SWT.READ_ONLY);
		text_3.setText("");
		text_3.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_4 = new Label(group, SWT.NONE);
		label_4.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_4.setText("Tip chord");

		text_4 = new Text(group, SWT.BORDER);
		text_4.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_5 = new Label(group, SWT.NONE);
		label_5.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_5.setText("MAC x LE");

		text_5 = new Text(group, SWT.BORDER | SWT.READ_ONLY);
		text_5.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_6 = new Label(group, SWT.NONE);
		label_6.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_6.setText("Kink chord x");

		text_12 = new Text(group, SWT.BORDER);
		text_12.setText("");
		text_12.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_7 = new Label(group, SWT.NONE);
		label_7.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_7.setText("MAC y LE");

		text_13 = new Text(group, SWT.BORDER | SWT.READ_ONLY);
		text_13.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label label_8 = new Label(group, SWT.NONE);
		label_8.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		label_8.setText("Kink chord y");

		text_14 = new Text(group, SWT.BORDER);
		text_14.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		new Label(group, SWT.NONE);
		new Label(group, SWT.NONE);

		Label lblTaperRatio = new Label(cmpEquivalentWing, SWT.NONE);
		lblTaperRatio.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblTaperRatio.setText("Taper ratio");

		text_6 = new Text(cmpEquivalentWing, SWT.BORDER);
		text_6.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblSweepLe = new Label(cmpEquivalentWing, SWT.NONE);
		lblSweepLe.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblSweepLe.setText("Sweep LE");

		text_10 = new Text(cmpEquivalentWing, SWT.BORDER);
		text_10.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblSweepC = new Label(cmpEquivalentWing, SWT.NONE);
		lblSweepC.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblSweepC.setText("Sweep c/4");

		text_7 = new Text(cmpEquivalentWing, SWT.BORDER);
		text_7.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblLeExtension = new Label(cmpEquivalentWing, SWT.NONE);
		lblLeExtension.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblLeExtension.setText("LE extension");

		text_8 = new Text(cmpEquivalentWing, SWT.BORDER);
		text_8.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		Label lblTeExtension = new Label(cmpEquivalentWing, SWT.NONE);
		lblTeExtension.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblTeExtension.setText("TE extension");

		text_9 = new Text(cmpEquivalentWing, SWT.BORDER);
		text_9.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		m_bindingContext = initDataBindings();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}



	public Group getCmpEquivalentWing() {
		return cmpEquivalentWing;
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		return bindingContext;
	}
	protected Button getBtnUseEquivalentWing() {
		return btnUseEquivalentWing;
	}

	public void setActualLSComposite(MyActualLiftingSurface actualLSComposite) {
		this.actualLSComposite = actualLSComposite;
	}

	public boolean isActualSelected() {
		return isActualSelected;
	}

	public void setActualSelected(boolean isActualSelected) {
		this.isActualSelected = isActualSelected;
	}

	public boolean isEquivalentSelected() {
		return isEquivalentSelected;
	}

	public void setEquivalentSelected(boolean isEquivalentSelected) {
		this.isEquivalentSelected = isEquivalentSelected;
	}

	public void setEquivalentSelected() {
		btnUseEquivalentWing.setSelection(!actualLSComposite.getBtnUseActualWing().getSelection());
	}
}
