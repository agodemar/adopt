package it.unina.adopt.GUI.tabpanels.liftingSurface;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.wb.swt.SWTResourceManager;

public class MyLiftingSurfaceNewPanel extends Composite {
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MyLiftingSurfaceNewPanel(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		SashForm sashForm = new SashForm(this, SWT.FILL);
		
		Composite leftComposite = new Composite(sashForm, SWT.NONE);
		leftComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		ScrolledForm scrldfrmWingData = formToolkit.createScrolledForm(leftComposite);
		scrldfrmWingData.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_FOREGROUND));
		scrldfrmWingData.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.BOLD));
		formToolkit.paintBordersFor(scrldfrmWingData);
		scrldfrmWingData.setText("Wing geometry");
		scrldfrmWingData.getBody().setLayout(new GridLayout(1, false));

		MyCommonData cmpCommonData = new MyCommonData(scrldfrmWingData.getBody(), SWT.NONE);
		cmpCommonData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		cmpCommonData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		
//		ExpandBar expandBar = new ExpandBar(scrldfrmWingData.getBody(), SWT.NONE);
//		expandBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		formToolkit.adapt(expandBar);
//		formToolkit.paintBordersFor(expandBar);
//		
//		ExpandItem xpndtmEquivalentWing = new ExpandItem(expandBar, SWT.NONE);
//		xpndtmEquivalentWing.setText("Equivalent Wing");
//		xpndtmEquivalentWing.setControl(cmpCommonData);
		
		MyEquivalentLiftingSurface cmpEquivalentWing = new MyEquivalentLiftingSurface(scrldfrmWingData.getBody(), SWT.NONE);
		cmpEquivalentWing.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
//		cmpEquivalentWing.setEnabled(false);
		
		MyActualLiftingSurface cmpActualWing = new MyActualLiftingSurface(scrldfrmWingData.getBody(), SWT.FILL);
		cmpActualWing.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		// Make each composite aware of the other
		cmpActualWing.setEquivalentLSComposite(cmpEquivalentWing);
		cmpEquivalentWing.setActualLSComposite(cmpActualWing);
		
		Composite composite_3 = new Composite(scrldfrmWingData.getBody(), SWT.NONE);
		composite_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite_3.setLayout(new FillLayout(SWT.HORIZONTAL));
		formToolkit.adapt(composite_3);
		formToolkit.paintBordersFor(composite_3);
		
		Button button_1 = new Button(composite_3, SWT.NONE);
		button_1.setText("Define wing by coordinates");
		formToolkit.adapt(button_1, true, true);
		
		Button button_2 = new Button(composite_3, SWT.NONE);
		button_2.setText("Run analysis");
		formToolkit.adapt(button_2, true, true);
		
		Composite composite = new Composite(scrldfrmWingData.getBody(), SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		formToolkit.adapt(composite);
		formToolkit.paintBordersFor(composite);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Button button = new Button(composite, SWT.NONE);
		button.setText("Import");
		formToolkit.adapt(button, true, true);
		
		Button btnSave = new Button(composite, SWT.NONE);
		formToolkit.adapt(btnSave, true, true);
		btnSave.setText("Save");

		Group grpPlot = new Group(sashForm, SWT.NONE);
		grpPlot.setText("Plots");
		sashForm.setWeights(new int[] {3,2});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
