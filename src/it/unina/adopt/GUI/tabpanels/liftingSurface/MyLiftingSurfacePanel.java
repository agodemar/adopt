package it.unina.adopt.GUI.tabpanels.liftingSurface;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.nebula.visualization.xygraph.dataprovider.CircularBufferDataProvider;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.PointStyle;
import org.eclipse.nebula.visualization.xygraph.figures.XYGraph;
import org.eclipse.nebula.visualization.xygraph.util.XYGraphMediaFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;
import org.jscience.physics.amount.Amount;

import com.google.common.primitives.Doubles;

import aircraft.components.Aircraft;
import aircraft.components.fuselage.Fuselage;
import aircraft.components.liftingSurface.LiftingSurface;
import configuration.MyConfiguration;
import it.unina.adopt.GUI.tabpanels.MyPanel;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.MyNomenclatureImageDialog;
import it.unina.adopt.utilities.gui.GridLayoutUtil;
import it.unina.adopt.utilities.gui.MyGuiUtils;


public class MyLiftingSurfacePanel extends MyPanel {


	// FILE WING TEST
	String WING_FILE_NAME = "wing_test.xml";
	String wingFilePath = "test" + File.separator + WING_FILE_NAME;
	File wingFile = new File(wingFilePath);
	//	
	private final String EXPORT_FILE_NAME = "wing_initiator.xml";
	private String _exportFilePath =
			MyConfiguration.currentDirectory+ File.separator + EXPORT_FILE_NAME;

	private LiftingSurface _theLiftingSurface = null;
	private Fuselage _theFuselage=null;

	Combo _comboDropDown = null;
	Combo _comboDropDownSectionData = null;

	// Standard API map
	// Map <MyFuselageAdjustCriteria,String> _mapAdjustCriteria = new HashMap<MyFuselageAdjustCriteria,String>();

	
	// controls

	private Text 
	_text_01, _text_02, _text_03, _text_04, _text_05,
	_text_06, _text_07,_text_07a,_text_08,_text_08b,_text_09, _text_10,_text_10b,_text_11,_text_11b,_text_12,_text_13,
	_text_14,_text_14b,_text_15,_text_15b,_text_16,_text_16b,_text_17,_text_18,_text_19,_text_20,_text_21,_text_22,_text_23,
	_text_24,_text_25,_text_26,_text_27,_text_28,_text_29,_text_30;
	private final int TEXT_WIDTH_HINT = 50;

	private Slider 
	_slider_01,	_slider_02,	_slider_03,	_slider_04,	_slider_05,
	_slider_06,	_slider_07,	_slider_08,	_slider_09,	_slider_10,
	_slider_11,	_slider_12;

	private File _importFile;
	private Text _text_ImportFile;
	private Button _button_ChooseImportFile, _button_ImportFile;

	private File _exportFile;
	private Text _text_ExportFile;
	private Button _button_ExportFile;
	private Button _button_Nomenclature;
	private Button _button_Eq_Wing;

	private Double sweepQuarterChordEquivalentWingDeg = null;
	// images etc
	protected static Map<String, Image> fCachedImages = new TreeMap<String, Image>();

	private MyNomenclatureImageDialog _nomenclatureWing = null;
	private Aircraft _theAircraft;
	/**
	 * @param _parent
	 * @param tabItem
	 * @param style
	 */

	public MyLiftingSurfacePanel(
			Composite parent, CTabItem tabItem, int style,
			Aircraft aircraft,
			LiftingSurface ls
			) {

		super(parent, tabItem, style);

		ADOPT_GUI.getApp().setInitiatorPaneWing(this); // make the App know about the object

//		if ( (aircraft != null) && (aircraft.get_fuselage() != null))
//		{
//			_theFuselage = aircraft.get_fuselage();
//		}

		// instantiate the lifting surface object
//		if ( (aircraft != null) && (aircraft.get_wing() != null))
//		{
//			if (type == MyComponent.ComponentEnum.MAIN_LIFTING_SURFACE) {
//				_theLiftingSurface = aircraft.get_wing();
//			} else if(type == MyComponent.ComponentEnum.HORIZONTAL_TAIL) {
//				_theLiftingSurface = aircraft.get_HTail();
//			} else if (type == MyComponent.ComponentEnum.VERTICAL_TAIL) {
//				_theLiftingSurface = aircraft.get_VTail();
//			} else {
//				_theLiftingSurface = aircraft.get_Canard();
//			}
//		}
//		else
//		{
//			_theLiftingSurface = new MyLiftingSurface(type);
//		}
		
		/////
		if (aircraft != null) _theFuselage = aircraft.get_fuselage();
		if (ls != null) _theLiftingSurface = ls ;
		
		System.out.println("_theLiftingSurface :: " + (_theLiftingSurface != null));
		System.out.println("_theLiftingSurface :: " + _theLiftingSurface.getDescription());
		System.out.println("_theLiftingSurface :: " + _theLiftingSurface.get_surface());

		// Create plots
		_tabItemXZView = new CTabItem(_tabFolderPlots, SWT.NULL);
		_fcXZ = new FigureCanvas(_tabFolderPlots);
		_lwsXZ = new LightweightSystem(_fcXZ);
		_xyGraphXZ = new XYGraph();

		_tabItemXYView = new CTabItem(_tabFolderPlots, SWT.NULL);
		_fcXY = new FigureCanvas(_tabFolderPlots);
		_lwsXY = new LightweightSystem(_fcXY);
		_xyGraphXY = new XYGraph();

		_tabItemYZView = new CTabItem(_tabFolderPlots, SWT.NULL);
		_fcYZ = new FigureCanvas(_tabFolderPlots);
		_lwsYZ = new LightweightSystem(_fcYZ);
		_xyGraphYZ = new XYGraph();

		MyGuiUtils.createInnerTabWithGraph(
				_tabItemXZView, _fcXZ, _lwsXZ, 
				_xyGraphXZ, "XZ View");

		MyGuiUtils.createInnerTabWithGraph(_tabItemXYView, _fcXY, _lwsXY, 
				_xyGraphXY, "XY View");

		MyGuiUtils.createInnerTabWithGraph(_tabItemYZView, _fcYZ, _lwsYZ, 
				_xyGraphYZ, "YZ View");

		// force first tab selection to get canvas painted
		_tabFolderPlots.setSelection(0);
		_tabFolderPlots.forceFocus();

		_ctrComposite.pack();
		_scrolledComposite.pack();
		//////////////////////////////////////////////////////////

		// the outer grid layout
		GridLayout layoutCtr = new GridLayout(8,false);
		GridData gridDataCtr = new GridData(SWT.FILL, SWT.FILL, true, false);
		_ctrComposite.setLayoutData(gridDataCtr);
		_ctrComposite.setLayout(layoutCtr);


		//------------------------------------------------------------------------------
		// Controls for settings:
		//    *  file choose/import

		//------------------------------------------------------------------------------

		GridData gd_GroupSettings = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_GroupSettings.horizontalSpan = 2;

		Group groupSettings = new Group (_ctrComposite, SWT.NONE);

		groupSettings.setLayout (new FillLayout (SWT.VERTICAL));
		groupSettings.setLayoutData (gd_GroupSettings);
		groupSettings.setText ("Settings");

		//		// Import file controls
		Group groupImportFile = new Group (groupSettings, SWT.NONE);
		groupImportFile.setLayout (new FillLayout (SWT.HORIZONTAL));
		groupImportFile.setText ("Import wing file");
		_text_ImportFile = new Text(groupImportFile, SWT.BORDER);
		_text_ImportFile.setText( "" );
		_text_ImportFile.setEditable(false);

		_button_ChooseImportFile = new Button(groupImportFile, SWT.NONE);
		_button_ChooseImportFile.setText("Choose ...");

		_button_ImportFile = new Button(groupSettings, SWT.NONE);
		_button_ImportFile.setText("Import");
		//

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		// Nomenclature

		_button_Nomenclature = new Button(groupSettings, SWT.NONE);
		_button_Nomenclature.setText("Wing Nomenclature");

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//---------------Input DATA----------------
		Group groupInput = new Group (_ctrComposite, SWT.NONE);

		groupInput.setLayout( new GridLayout( 2, true ) );
		groupInput.setLayoutData( new GridData( SWT.FILL, SWT.FILL, true, true, 2, 2 ) );
		groupInput.setText ("Equivalent Wing, Input Geometry Data");

		// Label 01:Surface Wing
		_text_01 = putLabel(groupInput, "Surface", "S_\\mathrm{w}", _theLiftingSurface.get_surface(), 3);

		// Label 02: AR Wing
		_text_02 = putLabel(groupInput, "Aspect Ratio", "AR",_theLiftingSurface.get_aspectRatio(), 1);

		// Label 03: Eq. Wing Taper ratio
		_text_03 = putLabel(groupInput, "Taper Ratio", "\\lambda", _theLiftingSurface.get_taperRatioEquivalent(), 2);

		// Label 04: Sweep c/4 equivalent wing
		_text_04 = putLabel(groupInput, "Sweep c/4", "\\Lambda_\\mathrm{c/4}", _theLiftingSurface.get_sweepQuarterChordEq(), 2);

		// Label 05: span station kink
		_text_05 = putLabel(groupInput, "Span station kink", "\\eta_\\mathrm{K}", _theLiftingSurface.get_spanStationKink(), 2);

		// Label 06: extensionLERootChordCrankedWing
		_text_06 = putLabel(groupInput, "Extension LE", "\\Delta X_\\mathrm{le}", _theLiftingSurface.get_extensionLERootChordLinPanel(), 2);

		// Label 07: extensionLERootChordCrankedWing
		_text_07 = putLabel(groupInput, "Extension TE", "\\Delta X_\\mathrm{te}", _theLiftingSurface.get_extensionTERootChordLinPanel(), 2);

		// Label 07a: deltaXWingFuselage
		_text_07a = putLabel(groupInput, "Delta X Wing-Fuselage Position", "\\Delta X_\\mathrm{wf}", _theLiftingSurface.get_deltaXWingFus(), 2);


		/////////////////////////////////////////////////////////////////////////////////
		GridData gd_button_Eq_Wing  = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_button_Eq_Wing .horizontalSpan = 2;
		_button_Eq_Wing = new Button(groupInput, SWT.NONE);
		_button_Eq_Wing.setText("Show Equivalent Wing");
		_button_Eq_Wing.setLayoutData(gd_button_Eq_Wing);
		/////////////////////////////////////////////////////////////////////////////////

		//---------------Output DATA----------------
		//-------------------------------------------------------------
		Group groupOutput = new Group (_ctrComposite, SWT.NONE);

		groupOutput.setLayout( new GridLayout( 2, true ) );
		groupOutput.setLayoutData( new GridData( SWT.FILL, SWT.FILL, true, true, 2, 2 ) );
		groupOutput.setText ("Actual Wing, Output Geometry Data");

		//------------------------------------------------------------------------------
		// Label 08: CrankedWing Surface
		//------------------------------------------------------------------------------			

		Label label_08 = new Label(groupOutput, SWT.NONE);
		label_08.setText("Surface");
		label_08.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_08 = "S_\\mathrm{w}";
		Image formulaImage_08 = MyGuiUtils.renderLatexFormula(this,latex_text_08, 22);
		Label label_08a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_08, formulaImage_08);
		label_08a.setImage (formulaImage_08);


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_08 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_08.horizontalSpan = 1;
		gd_text_08.grabExcessHorizontalSpace = true;
		gd_text_08.horizontalAlignment = GridData.FILL;
		gd_text_08.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_08 = BigDecimal.valueOf( _theLiftingSurface.get_surface().getEstimatedValue());
		bd_08 = bd_08.setScale(3, RoundingMode.HALF_UP);
		_text_08 = new Text(groupOutput, SWT.BORDER);
		_text_08.setText( "  " + bd_08 + " " +_theLiftingSurface.get_surface().getUnit().toString());
		_text_08.setLayoutData(gd_text_08);
		_text_08.setEditable(false);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//------------------------------------------------------------------------------
		// Label 08: CrankedWing Surface
		//------------------------------------------------------------------------------			

		Label label_08b = new Label(groupOutput, SWT.NONE);
		label_08b.setText("Span ");
		label_08b.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_08b = "b";
		Image formulaImage_08b = MyGuiUtils.renderLatexFormula(this,latex_text_08b, 22);
		Label label_08ba = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_08b, formulaImage_08b);
		label_08ba.setImage (formulaImage_08b);


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_08b = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_08b.horizontalSpan = 1;
		gd_text_08b.grabExcessHorizontalSpace = true;
		gd_text_08b.horizontalAlignment = GridData.FILL;
		gd_text_08b.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_08b = BigDecimal.valueOf( _theLiftingSurface.get_span().getEstimatedValue());
		bd_08b = bd_08b.setScale(2, RoundingMode.HALF_UP);
		_text_08b = new Text(groupOutput, SWT.BORDER);
		_text_08b.setText( "  " + bd_08b + " " +_theLiftingSurface.get_span().getUnit().toString());
		_text_08b.setLayoutData(gd_text_08b);
		_text_08b.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 09: CrankedWing Taper Ratio
		//------------------------------------------------------------------------------			

		Label label_09 = new Label(groupOutput, SWT.NONE);
		label_09.setText("Taper Ratio Cranked Wing");
		label_09.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_09 = "\\lambda";
		Image formulaImage_09 = MyGuiUtils.renderLatexFormula(this,latex_text_09, 22);
		Label label_09a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_09, formulaImage_09);
		label_09a.setImage (formulaImage_09);


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_09 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_09.horizontalSpan = 1;
		gd_text_09.grabExcessHorizontalSpace = true;
		gd_text_09.horizontalAlignment = GridData.FILL;
		gd_text_09.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_09 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioActual());
		bd_09 = bd_09.setScale(3, RoundingMode.HALF_UP);
		_text_09 = new Text(groupOutput, SWT.BORDER);
		_text_09.setText( "  " + bd_09 );
		_text_09.setLayoutData(gd_text_09);
		_text_09.setEditable(false);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//------------------------------------------------------------------------------
		// Label 10: CrankedWing Inner Panel Surface
		//------------------------------------------------------------------------------

		Label label_10 = new Label(groupOutput, SWT.NONE);
		label_10.setText("Inner Panel Semi-Surface Cranked Wing");
		label_10.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_10 = "S_\\mathrm{ip}";
		Image formulaImage_10 = MyGuiUtils.renderLatexFormula(this,latex_text_10, 22);
		Label label_10a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_10, formulaImage_10);
		label_10a.setImage (formulaImage_10);


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_10 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_10.horizontalSpan = 1;
		gd_text_10.grabExcessHorizontalSpace = true;
		gd_text_10.horizontalAlignment = GridData.FILL;
		gd_text_10.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_10 = BigDecimal.valueOf( _theLiftingSurface.get_semiSurfaceInnerPanel().getEstimatedValue());
		bd_10 = bd_10.setScale(3, RoundingMode.HALF_UP);
		_text_10 = new Text(groupOutput, SWT.BORDER);
		_text_10.setText( "  " + bd_10 + " " + _theLiftingSurface.get_semiSurfaceInnerPanel().getUnit().toString());
		_text_10.setLayoutData(gd_text_10);
		_text_10.setEditable(false);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//------------------------------------------------------------------------------
		// Label 10b: CrankedWing Inner Panel Aspect Ratio
		//------------------------------------------------------------------------------

		Label label_10b = new Label(groupOutput, SWT.NONE);
		label_10b.setText("Inner Panel Aspect Ratio Cranked Wing");
		label_10b.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_10b = "AR_\\mathrm{ip}";
		Image formulaImage_10b = MyGuiUtils.renderLatexFormula(this,latex_text_10b, 22);
		Label label_10ba = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_10b, formulaImage_10b);
		label_10ba.setImage (formulaImage_10b);


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_10b = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_10b.horizontalSpan = 1;
		gd_text_10b.grabExcessHorizontalSpace = true;
		gd_text_10b.horizontalAlignment = GridData.FILL;
		gd_text_10b.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_10b = BigDecimal.valueOf( _theLiftingSurface.get_aspectRatioInnerPanel());
		bd_10b = bd_10b.setScale(2, RoundingMode.HALF_UP);
		_text_10b = new Text(groupOutput, SWT.BORDER);
		_text_10b.setText( "  " + bd_10b);
		_text_10b.setLayoutData(gd_text_10b);
		_text_10b.setEditable(false);


		//------------------------------------------------------------------------------
		// Label 11: CrankedWing Outer Panel Surface
		//------------------------------------------------------------------------------			

		Label label_11 = new Label(groupOutput, SWT.NONE);
		label_11.setText(" Outer Panel Semi-Surface Cranked Wing");
		label_11.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_11 = "S_\\mathrm{op}";
		Image formulaImage_11 = MyGuiUtils.renderLatexFormula(this,latex_text_11, 22);
		Label label_11a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_11, formulaImage_11);
		label_11a.setImage (formulaImage_11);		


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_11 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_11.horizontalSpan = 1;
		gd_text_11.grabExcessHorizontalSpace = true;
		gd_text_11.horizontalAlignment = GridData.FILL;
		gd_text_11.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_11 = BigDecimal.valueOf( _theLiftingSurface.get_semiSurfaceOuterPanel().getEstimatedValue());
		bd_11 = bd_11.setScale(3, RoundingMode.HALF_UP);
		_text_11 = new Text(groupOutput, SWT.BORDER);
		_text_11.setText( "  " + bd_11 + " " + _theLiftingSurface.get_semiSurfaceOuterPanel().getUnit().toString());
		_text_11.setLayoutData(gd_text_11);
		_text_11.setEditable(false);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++


		//------------------------------------------------------------------------------
		// Label 11b: CrankedWing Outer Panel Aspect Ratio
		//------------------------------------------------------------------------------

		Label label_11b = new Label(groupOutput, SWT.NONE);
		label_11b.setText("Outer Panel Aspect Ratio Cranked Wing");
		label_11b.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_11b = "AR_\\mathrm{op}";
		Image formulaImage_11b = MyGuiUtils.renderLatexFormula(this,latex_text_11b, 22);
		Label label_11ba = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_11b, formulaImage_11b);
		label_11ba.setImage (formulaImage_11b);


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gd_text_11b = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_11b.horizontalSpan = 1;
		gd_text_11b.grabExcessHorizontalSpace = true;
		gd_text_11b.horizontalAlignment = GridData.FILL;
		gd_text_11b.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_11b = BigDecimal.valueOf( _theLiftingSurface.get_aspectRatioOuterPanel());
		bd_11b = bd_11b.setScale(2, RoundingMode.HALF_UP);
		_text_11b = new Text(groupOutput, SWT.BORDER);
		_text_11b.setText( "  " + bd_11b);
		_text_11b.setLayoutData(gd_text_11b);
		_text_11b.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 12: CrankedWing Outer Panel Taper Ratio
		//------------------------------------------------------------------------------			

		Label label_12 = new Label(groupOutput, SWT.NONE);
		label_12.setText(" Inner Panel Taper Ratio Cranked Wing");
		label_12.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_12 = "\\lambda_\\mathrm{ip}";
		Image formulaImage_12 = MyGuiUtils.renderLatexFormula(this,latex_text_12, 22);
		Label label_12a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_12, formulaImage_12);
		label_12a.setImage (formulaImage_12);		


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_12 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_12.horizontalSpan = 1;
		gd_text_12.grabExcessHorizontalSpace = true;
		gd_text_12.horizontalAlignment = GridData.FILL;
		gd_text_12.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_12 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioInnerPanel());
		bd_12 = bd_12.setScale(3, RoundingMode.HALF_UP);
		_text_12 = new Text(groupOutput, SWT.BORDER);
		_text_12.setText( "  " + bd_12 );
		_text_12.setLayoutData(gd_text_12);
		_text_12.setEditable(false);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//------------------------------------------------------------------------------
		// Label 13: CrankedWing Outer Panel Surface
		//------------------------------------------------------------------------------			

		Label label_13 = new Label(groupOutput, SWT.NONE);
		label_13.setText(" Outer Panel Taper Ratio Cranked Wing");
		label_13.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_13 = "\\lambda_\\mathrm{op}";
		Image formulaImage_13 = MyGuiUtils.renderLatexFormula(this,latex_text_13, 22);
		Label label_13a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_13, formulaImage_13);
		label_13a.setImage (formulaImage_13);		


		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_13 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_13.horizontalSpan = 1;
		gd_text_13.grabExcessHorizontalSpace = true;
		gd_text_13.horizontalAlignment = GridData.FILL;
		gd_text_13.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_13 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioOuterPanel());
		bd_13 = bd_13.setScale(3, RoundingMode.HALF_UP);
		_text_13 = new Text(groupOutput, SWT.BORDER);
		_text_13.setText( "  " + bd_13 );
		_text_13.setLayoutData(gd_text_13);
		_text_13.setEditable(false);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++


		//------------------------------------------------------------------------------
		// Label 14: CrankedWing Sweep_LE Inner  Panel Surface
		//------------------------------------------------------------------------------			

		Label label_14 = new Label(groupOutput, SWT.NONE);
		label_14.setText("Sweep LE Inner Panel Cranked Wing");
		label_14.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_14 = "\\Lambda_\\mathrm{le_{ip}}";
		Image formulaImage_14 = MyGuiUtils.renderLatexFormula(this,latex_text_14, 22);
		Label label_14a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_14, formulaImage_14);
		label_14a.setImage (formulaImage_14);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_14 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_14.horizontalSpan = 1;
		gd_text_14.grabExcessHorizontalSpace = true;
		gd_text_14.horizontalAlignment = GridData.FILL;
		gd_text_14.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_14 = BigDecimal.valueOf(Math.toDegrees(_theLiftingSurface.get_sweepLEInnerPanel().getEstimatedValue()));
		bd_14 = bd_14.setScale(1, RoundingMode.HALF_UP);
		_text_14 = new Text(groupOutput, SWT.BORDER);
		_text_14.setText( "  " + bd_14 + NonSI.DEGREE_ANGLE);
		_text_14.setLayoutData(gd_text_14);
		_text_14.setEditable(false);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//------------------------------------------------------------------------------
		// Label 14b: CrankedWing Sweep_c4 Inner  Panel Surface
		//------------------------------------------------------------------------------			

		Label label_14b = new Label(groupOutput, SWT.NONE);
		label_14b.setText("Sweep c/4 Inner Panel Cranked Wing");
		label_14b.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_14b = "\\Lambda_\\mathrm{c/4_{ip}}";
		Image formulaImage_14b = MyGuiUtils.renderLatexFormula(this,latex_text_14b, 22);
		Label label_14ba = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_14b, formulaImage_14b);
		label_14ba.setImage (formulaImage_14b);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_14b = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_14b.horizontalSpan = 1;
		gd_text_14b.grabExcessHorizontalSpace = true;
		gd_text_14b.horizontalAlignment = GridData.FILL;
		gd_text_14b.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_14b = BigDecimal.valueOf(Math.toDegrees(_theLiftingSurface.get_sweepQuarterChordInnerPanel().getEstimatedValue()));
		bd_14b = bd_14b.setScale(1, RoundingMode.HALF_UP);
		_text_14b = new Text(groupOutput, SWT.BORDER);
		_text_14b.setText( "  " + bd_14b + NonSI.DEGREE_ANGLE);
		_text_14b.setLayoutData(gd_text_14b);
		_text_14b.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 15: CrankedWing Sweep_LE Outer  Panel Surface
		//------------------------------------------------------------------------------			

		Label label_15 = new Label(groupOutput, SWT.NONE);
		label_15.setText("Sweep LE Outer Panel Cranked Wing");
		label_15.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_15 = "\\Lambda_\\mathrm{le_{op}}";
		Image formulaImage_15 = MyGuiUtils.renderLatexFormula(this,latex_text_15, 22);
		Label label_15a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_15, formulaImage_15);
		label_15a.setImage (formulaImage_15);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_15 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_15.horizontalSpan = 1;
		gd_text_15.grabExcessHorizontalSpace = true;
		gd_text_15.horizontalAlignment = GridData.FILL;
		gd_text_15.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_15 = BigDecimal.valueOf(Math.toDegrees(_theLiftingSurface.get_sweepLEOuterPanel().getEstimatedValue()));
		bd_15 = bd_15.setScale(1, RoundingMode.HALF_UP);
		_text_15 = new Text(groupOutput, SWT.BORDER);
		_text_15.setText( "  " + bd_15 + NonSI.DEGREE_ANGLE);
		_text_15.setLayoutData(gd_text_15);
		_text_15.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 15b: CrankedWing Sweep_c4 Outer  Panel Surface
		//------------------------------------------------------------------------------			

		Label label_15b = new Label(groupOutput, SWT.NONE);
		label_15b.setText("Sweep c/4 Outer Panel Cranked Wing");
		label_15b.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_15b = "\\Lambda_\\mathrm{c/4_{op}}";
		Image formulaImage_15b = MyGuiUtils.renderLatexFormula(this,latex_text_15b, 22);
		Label label_15ba = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_15b, formulaImage_15b);
		label_15ba.setImage (formulaImage_15b);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_15b = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_15b.horizontalSpan = 1;
		gd_text_15b.grabExcessHorizontalSpace = true;
		gd_text_15b.horizontalAlignment = GridData.FILL;
		gd_text_15b.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_15b = BigDecimal.valueOf(Math.toDegrees(_theLiftingSurface.get_sweepQuarterChordOuterPanel().getEstimatedValue()));
		bd_15b = bd_15b.setScale(1, RoundingMode.HALF_UP);
		_text_15b = new Text(groupOutput, SWT.BORDER);
		_text_15b.setText( "  " + bd_15b + NonSI.DEGREE_ANGLE);
		_text_15b.setLayoutData(gd_text_15b);
		_text_15b.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 16b: EqWing Root Chord
		//------------------------------------------------------------------------------			

		Label label_16b = new Label(groupOutput, SWT.NONE);
		label_16b.setText("Root Chord Equivalent Wing");
		label_16b.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_16b = "c_\\mathrm{r_{eq}}";
		Image formulaImage_16b = MyGuiUtils.renderLatexFormula(this,latex_text_16b, 22);
		Label label_16ba = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_16b, formulaImage_16b);
		label_16ba.setImage (formulaImage_16b);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_16b = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_16b.horizontalSpan = 1;
		gd_text_16b.grabExcessHorizontalSpace = true;
		gd_text_16b.horizontalAlignment = GridData.FILL;
		gd_text_16b.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_16b = BigDecimal.valueOf(_theLiftingSurface.get_chordRootEquivalentWing().getEstimatedValue());
		bd_16b = bd_16b.setScale(3, RoundingMode.HALF_UP);
		_text_16b = new Text(groupOutput, SWT.BORDER);
		_text_16b.setText( "  " + bd_16b +" "+_theLiftingSurface.get_chordRootEquivalentWing().getUnit().toString());
		_text_16b.setLayoutData(gd_text_16b);
		_text_16b.setEditable(false);




		//------------------------------------------------------------------------------
		// Label 16: CrankedWing Root Chord
		//------------------------------------------------------------------------------			

		Label label_16 = new Label(groupOutput, SWT.NONE);
		label_16.setText("Root Chord Cranked Wing");
		label_16.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_16 = "c_\\mathrm{r}";
		Image formulaImage_16 = MyGuiUtils.renderLatexFormula(this,latex_text_16, 22);
		Label label_16a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_16, formulaImage_16);
		label_16a.setImage (formulaImage_16);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_16 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_16.horizontalSpan = 1;
		gd_text_16.grabExcessHorizontalSpace = true;
		gd_text_16.horizontalAlignment = GridData.FILL;
		gd_text_16.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_16 = BigDecimal.valueOf(_theLiftingSurface.get_chordRoot().getEstimatedValue());
		bd_16 = bd_16.setScale(3, RoundingMode.HALF_UP);
		_text_16 = new Text(groupOutput, SWT.BORDER);
		_text_16.setText( "  " + bd_16 +" "+_theLiftingSurface.get_chordRoot().getUnit().toString());
		_text_16.setLayoutData(gd_text_16);
		_text_16.setEditable(false);


		//------------------------------------------------------------------------------
		// Label 17: CrankedWing Root Chord Xle
		//------------------------------------------------------------------------------			

		Label label_17 = new Label(groupOutput, SWT.NONE);
		label_17.setText("Root Chord Cranked Wing X l.e");
		label_17.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_17 = "X_\\mathrm{le_r}";
		Image formulaImage_17 = MyGuiUtils.renderLatexFormula(this,latex_text_17, 22);
		Label label_17a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_17, formulaImage_17);
		label_17a.setImage (formulaImage_17);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_17 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_17.horizontalSpan = 1;
		gd_text_17.grabExcessHorizontalSpace = true;
		gd_text_17.horizontalAlignment = GridData.FILL;
		gd_text_17.widthHint = TEXT_WIDTH_HINT;
		_text_17 = new Text(groupOutput, SWT.BORDER);
		_text_17.setText( "  "+_theLiftingSurface.get_xLERoot().getEstimatedValue()+" "+_theLiftingSurface.get_xLERoot().getUnit().toString());
		_text_17.setLayoutData(gd_text_17);
		_text_17.setEditable(false);


		//------------------------------------------------------------------------------
		// Label 18: CrankedWing Kink Chord 
		//------------------------------------------------------------------------------			

		Label label_18 = new Label(groupOutput, SWT.NONE);
		label_18.setText("Kink Chord Cranked Wing");
		label_18.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_18 = "c_\\mathrm{k}";
		Image formulaImage_18 = MyGuiUtils.renderLatexFormula(this,latex_text_18, 22);
		Label label_18a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_18, formulaImage_18);
		label_18a.setImage (formulaImage_18);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_18 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_18.horizontalSpan = 1;
		gd_text_18.grabExcessHorizontalSpace = true;
		gd_text_18.horizontalAlignment = GridData.FILL;
		gd_text_18.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_18 = BigDecimal.valueOf(_theLiftingSurface.get_chordKink().getEstimatedValue());
		bd_18 = bd_18.setScale(3, RoundingMode.HALF_UP);
		_text_18 = new Text(groupOutput, SWT.BORDER);
		_text_18.setText( "  "+bd_18+" "+_theLiftingSurface.get_chordKink().getUnit().toString());
		_text_18.setLayoutData(gd_text_18);
		_text_18.setEditable(false);


		//------------------------------------------------------------------------------
		// Label 19: CrankedWing Kink Chord Xle
		//------------------------------------------------------------------------------			

		Label label_19 = new Label(groupOutput, SWT.NONE);
		label_19.setText("Kink Chord Cranked Wing X l.e");
		label_19.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_19 = "X_\\mathrm{le_k}";
		Image formulaImage_19 = MyGuiUtils.renderLatexFormula(this,latex_text_19, 22);
		Label label_19a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_19, formulaImage_19);
		label_19a.setImage (formulaImage_19);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_19 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_19.horizontalSpan = 1;
		gd_text_19.grabExcessHorizontalSpace = true;
		gd_text_19.horizontalAlignment = GridData.FILL;
		gd_text_19.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_19 = BigDecimal.valueOf(_theLiftingSurface.get_xLEKink().getEstimatedValue());
		bd_19 = bd_19.setScale(3, RoundingMode.HALF_UP);
		_text_19 = new Text(groupOutput, SWT.BORDER);
		_text_19.setText( "  "+bd_19+" "+_theLiftingSurface.get_xLEKink().getUnit().toString());
		_text_19.setLayoutData(gd_text_19);
		_text_19.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 20: CrankedWing Kink Chord 
		//------------------------------------------------------------------------------			

		Label label_20 = new Label(groupOutput, SWT.NONE);
		label_20.setText("Tip Chord Cranked Wing ");
		label_20.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_20 = "c_\\mathrm{t}";
		Image formulaImage_20 = MyGuiUtils.renderLatexFormula(this,latex_text_20, 22);
		Label label_20a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_20, formulaImage_20);
		label_20a.setImage (formulaImage_20);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_20 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_20.horizontalSpan = 1;
		gd_text_20.grabExcessHorizontalSpace = true;
		gd_text_20.horizontalAlignment = GridData.FILL;
		gd_text_20.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_20 = BigDecimal.valueOf(_theLiftingSurface.get_chordTip().getEstimatedValue());
		bd_20 = bd_20.setScale(3, RoundingMode.HALF_UP);
		_text_20 = new Text(groupOutput, SWT.BORDER);
		_text_20.setText( "  "+bd_20+" "+_theLiftingSurface.get_chordTip().getUnit().toString());
		_text_20.setLayoutData(gd_text_20);
		_text_20.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 21: CrankedWing Tip Chord Xle
		//------------------------------------------------------------------------------			

		Label label_21 = new Label(groupOutput, SWT.NONE);
		label_21.setText("Tip Chord Cranked Wing X l.e");
		label_21.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_21 = "X_\\mathrm{le_t}";
		Image formulaImage_21 = MyGuiUtils.renderLatexFormula(this,latex_text_21, 22);
		Label label_21a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_21, formulaImage_21);
		label_21a.setImage (formulaImage_21);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_21 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_21.horizontalSpan = 1;
		gd_text_21.grabExcessHorizontalSpace = true;
		gd_text_21.horizontalAlignment = GridData.FILL;
		gd_text_21.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_21 = BigDecimal.valueOf(_theLiftingSurface.get_xLETip().getEstimatedValue());
		bd_21 = bd_21.setScale(3, RoundingMode.HALF_UP);
		_text_21 = new Text(groupOutput, SWT.BORDER);
		_text_21.setText( "  "+bd_21+" "+_theLiftingSurface.get_xLETip().getUnit().toString());
		_text_21.setLayoutData(gd_text_21);
		_text_21.setEditable(false);


		//------------------------------------------------------------------------------
		// Label 22: Mean Geometric chord
		//------------------------------------------------------------------------------			

		Label label_22 = new Label(groupOutput, SWT.NONE);
		label_22.setText("Mean Geometric Chord");
		label_22.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_22 = "\\mathrm{M.G.C}";
		Image formulaImage_22 = MyGuiUtils.renderLatexFormula(this,latex_text_22, 22);
		Label label_22a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_22, formulaImage_22);
		label_22a.setImage (formulaImage_22);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_22 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_22.horizontalSpan = 1;
		gd_text_22.grabExcessHorizontalSpace = true;
		gd_text_22.horizontalAlignment = GridData.FILL;
		gd_text_22.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_22 = BigDecimal.valueOf(_theLiftingSurface.get_geomChordEq().getEstimatedValue());
		bd_22 = bd_22.setScale(2, RoundingMode.HALF_UP);
		_text_22 = new Text(groupOutput, SWT.BORDER);
		_text_22.setText( "  "+bd_22+" "+_theLiftingSurface.get_geomChordEq().getUnit().toString());
		_text_22.setLayoutData(gd_text_22);
		_text_22.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 23: Mean Aerodynamic chord
		//------------------------------------------------------------------------------			

		Label label_23 = new Label(groupOutput, SWT.NONE);
		label_23.setText("Mean Aerodynamic Chord Equivalent Wing");
		label_23.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_23 = "\\mathrm{M.A.C_{eq}}";
		Image formulaImage_23 = MyGuiUtils.renderLatexFormula(this,latex_text_23, 23);
		Label label_23a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_23, formulaImage_23);
		label_23a.setImage (formulaImage_23);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_23 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_23.horizontalSpan = 1;
		gd_text_23.grabExcessHorizontalSpace = true;
		gd_text_23.horizontalAlignment = GridData.FILL;
		gd_text_23.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_23 = BigDecimal.valueOf(_theLiftingSurface.get_meanAerodChordEq().getEstimatedValue());
		bd_23 = bd_23.setScale(2, RoundingMode.HALF_UP);
		_text_23 = new Text(groupOutput, SWT.BORDER);
		_text_23.setText( "  "+bd_23+" "+_theLiftingSurface.get_meanAerodChordEq().getUnit().toString());
		_text_23.setLayoutData(gd_text_23);
		_text_23.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 24: X_LE_Mean Aerodynamic  chord
		//------------------------------------------------------------------------------			

		Label label_24 = new Label(groupOutput, SWT.NONE);
		label_24.setText(" M.A.C Equivalent Wing X l.e");
		label_24.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_24 = "X_\\mathrm{le_{mac_{eq}}}";
		Image formulaImage_24 = MyGuiUtils.renderLatexFormula(this,latex_text_24, 24);
		Label label_24a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_24, formulaImage_24);
		label_24a.setImage (formulaImage_24);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_24 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_24.horizontalSpan = 1;
		gd_text_24.grabExcessHorizontalSpace = true;
		gd_text_24.horizontalAlignment = GridData.FILL;
		gd_text_24.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_24 = BigDecimal.valueOf(_theLiftingSurface.get_x_LE_Mac_Eq().getEstimatedValue());
		bd_24 = bd_24.setScale(2, RoundingMode.HALF_UP);
		_text_24 = new Text(groupOutput, SWT.BORDER);
		_text_24.setText( "  "+bd_24+" "+_theLiftingSurface.get_x_LE_Mac_Eq().getUnit().toString());
		_text_24.setLayoutData(gd_text_24);
		_text_24.setEditable(false);	

		//------------------------------------------------------------------------------
		// Label 25: Y_LE_Mean Aerodynamic  chord
		//------------------------------------------------------------------------------			

		Label label_25 = new Label(groupOutput, SWT.NONE);
		label_25.setText(" M.A.C Equivalent Wing Y l.e.");
		label_25.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_25 = "Y_\\mathrm{le_{mac_{eq}}}";
		Image formulaImage_25 = MyGuiUtils.renderLatexFormula(this,latex_text_25, 25);
		Label label_25a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_25, formulaImage_25);
		label_25a.setImage (formulaImage_25);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_25 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_25.horizontalSpan = 1;
		gd_text_25.grabExcessHorizontalSpace = true;
		gd_text_25.horizontalAlignment = GridData.FILL;
		gd_text_25.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_25 = BigDecimal.valueOf(_theLiftingSurface.get_y_LE_Mac_Eq().getEstimatedValue());
		bd_25 = bd_25.setScale(2, RoundingMode.HALF_UP);
		_text_25 = new Text(groupOutput, SWT.BORDER);
		_text_25.setText( "  "+bd_25+" "+_theLiftingSurface.get_y_LE_Mac_Eq().getUnit().toString());
		_text_25.setLayoutData(gd_text_25);
		_text_25.setEditable(false);		


		//------------------------------------------------------------------------------
		// Label 26: eta_Mean Aerodynamic  chord
		//------------------------------------------------------------------------------			

		Label label_26 = new Label(groupOutput, SWT.NONE);
		label_26.setText(" M.A.C Equivalent Wing Station");
		label_26.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_26 = "\\eta_\\mathrm{le_{mac_{eq}}}";
		Image formulaImage_26 = MyGuiUtils.renderLatexFormula(this,latex_text_26, 26);
		Label label_26a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_26, formulaImage_26);
		label_26a.setImage (formulaImage_26);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_26 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_26.horizontalSpan = 1;
		gd_text_26.grabExcessHorizontalSpace = true;
		gd_text_26.horizontalAlignment = GridData.FILL;
		gd_text_26.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_26 = BigDecimal.valueOf((_theLiftingSurface.get_y_LE_Mac_Eq().getEstimatedValue()/(0.5*_theLiftingSurface.get_span().getEstimatedValue())));
		bd_26 = bd_26.setScale(3, RoundingMode.HALF_UP);
		_text_26 = new Text(groupOutput, SWT.BORDER);
		_text_26.setText( "  "+bd_26);
		_text_26.setLayoutData(gd_text_26);
		_text_26.setEditable(false);		


		//------------------------------------------------------------------------------
		// Label 27: Mean Aerodynamic chord cranked wing
		//------------------------------------------------------------------------------			

		Label label_27 = new Label(groupOutput, SWT.NONE);
		label_27.setText("Mean Aerodynamic Chord Cranked Wing");
		label_27.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_27 = "\\mathrm{M.A.C_{ck}}";
		Image formulaImage_27 = MyGuiUtils.renderLatexFormula(this,latex_text_27, 27);
		Label label_27a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_27, formulaImage_27);
		label_27a.setImage (formulaImage_27);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_27 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_27.horizontalSpan = 1;
		gd_text_27.grabExcessHorizontalSpace = true;
		gd_text_27.horizontalAlignment = GridData.FILL;
		gd_text_27.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_27 = BigDecimal.valueOf(_theLiftingSurface.get_meanAerodChordActual().getEstimatedValue());
		bd_27 = bd_27.setScale(2, RoundingMode.HALF_UP);
		_text_27 = new Text(groupOutput, SWT.BORDER);
		_text_27.setText( "  "+bd_27+" "+_theLiftingSurface.get_meanAerodChordActual().getUnit().toString());
		_text_27.setLayoutData(gd_text_27);
		_text_27.setEditable(false);

		//------------------------------------------------------------------------------
		// Label 28: X_LE_Mean Aerodynamic chord cranked wing
		//------------------------------------------------------------------------------			

		Label label_28 = new Label(groupOutput, SWT.NONE);
		label_28.setText(" M.A.C Cranked Wing X l.e");
		label_28.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_28 = "X_\\mathrm{le_{mac_{ck}}}";
		Image formulaImage_28 = MyGuiUtils.renderLatexFormula(this,latex_text_28, 28);
		Label label_28a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_28, formulaImage_28);
		label_28a.setImage (formulaImage_28);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_28 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_28.horizontalSpan = 1;
		gd_text_28.grabExcessHorizontalSpace = true;
		gd_text_28.horizontalAlignment = GridData.FILL;
		gd_text_28.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_28 = BigDecimal.valueOf(_theLiftingSurface.get_xLEMacActualLRF().getEstimatedValue());
		bd_28 = bd_28.setScale(2, RoundingMode.HALF_UP);
		_text_28 = new Text(groupOutput, SWT.BORDER);
		_text_28.setText( "  "+bd_28+" "+_theLiftingSurface.get_xLEMacActualLRF().getUnit().toString());
		_text_28.setLayoutData(gd_text_28);
		_text_28.setEditable(false);	

		//------------------------------------------------------------------------------
		// Label 29: Y_LE_Mean Aerodynamic chord cranked wing
		//------------------------------------------------------------------------------			

		Label label_29 = new Label(groupOutput, SWT.NONE);
		label_29.setText(" M.A.C Cranked Wing Y l.e.");
		label_29.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_29 = "Y_\\mathrm{le_{mac_{ck}}}";
		Image formulaImage_29 = MyGuiUtils.renderLatexFormula(this,latex_text_29, 29);
		Label label_29a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_29, formulaImage_29);
		label_29a.setImage (formulaImage_29);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_29 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_29.horizontalSpan = 1;
		gd_text_29.grabExcessHorizontalSpace = true;
		gd_text_29.horizontalAlignment = GridData.FILL;
		gd_text_29.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_29 = BigDecimal.valueOf(_theLiftingSurface.get_yLEMacActualLRF().getEstimatedValue());
		bd_29 = bd_29.setScale(2, RoundingMode.HALF_UP);
		_text_29 = new Text(groupOutput, SWT.BORDER);
		_text_29.setText( "  "+bd_29+" "+_theLiftingSurface.get_yLEMacActualLRF().getUnit().toString());
		_text_29.setLayoutData(gd_text_29);
		_text_29.setEditable(false);		

		//------------------------------------------------------------------------------
		// Label 30: eta_Mean Aerodynamic chord cranked wing
		//------------------------------------------------------------------------------			

		Label label_30 = new Label(groupOutput, SWT.NONE);
		label_30.setText(" M.A.C Cranked Wing Station");
		label_30.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String latex_text_30 = "\\eta_\\mathrm{le_{mac_{ck}}}";
		Image formulaImage_30 = MyGuiUtils.renderLatexFormula(this,latex_text_30, 30);
		Label label_30a = new Label(groupOutput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text_30, formulaImage_30);
		label_30a.setImage (formulaImage_30);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text_30 = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text_30.horizontalSpan = 1;
		gd_text_30.grabExcessHorizontalSpace = true;
		gd_text_30.horizontalAlignment = GridData.FILL;
		gd_text_30.widthHint = TEXT_WIDTH_HINT;
		BigDecimal bd_30 = BigDecimal.valueOf((_theLiftingSurface.get_yLEMacActualLRF().getEstimatedValue()/(0.5*_theLiftingSurface.get_span().getEstimatedValue())));
		bd_30 = bd_30.setScale(3, RoundingMode.HALF_UP);
		_text_30 = new Text(groupOutput, SWT.BORDER);
		_text_30.setText( "  "+bd_30);
		_text_30.setLayoutData(gd_text_30);
		_text_30.setEditable(false);	

		//-------------------end Output Data----------------------------

		//		// I/O controls
		//
		GridData gd_GroupIO = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_GroupIO.horizontalSpan = 2;

		Group groupIO = new Group (_ctrComposite, SWT.NONE);

		groupIO.setLayout (new FillLayout (SWT.VERTICAL));
		groupIO.setLayoutData (gd_GroupIO);
		groupIO.setText("I/O");

		// Import file controls
		Group groupSerializeFile = new Group (groupIO, SWT.NONE);
		groupSerializeFile.setLayout (new FillLayout (SWT.HORIZONTAL));
		groupSerializeFile.setText ("Save status file");
		_text_ExportFile = new Text(groupIO, SWT.BORDER);
		_text_ExportFile.setText( _exportFilePath );
		_text_ExportFile.setEditable(false);

		_button_ExportFile = new Button(groupIO, SWT.NONE);
		_button_ExportFile.setText("Save");


		//-------------------------------------------------------------------------
		// EVENTS
		//-------------------------------------------------------------------------


		_button_ChooseImportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.Selection:
					FileDialog dialog = new FileDialog(ADOPT_GUI.getApp().getShell());
					dialog.setFilterPath( MyConfiguration.currentDirectory.toString() );
					String sFile = dialog.open();
					if ( sFile != null ) {
						_importFile = new File(sFile);
						ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
								"The ADOpT|Choose wing data file: " +
										_importFile.toString() + "\n"
								);				    	
						_text_ImportFile.setText(_importFile.toString());
					}				    
					break;
				}
			}
		});

		_button_ImportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.Selection:
					if ( _importFile != null ) {

						if ( _importFile.exists() && _importFile.isFile()  ) {
							//							_theLiftingSurface.importFromXMLFile(_importFile);
							adjustControls(); // update controls in GUI
							//							_theLiftingSurface.calculateGeometry();
							adjustOutputWingData();
							redrawWingPlanform();
							redrawWingBody();
							chordDistribution();
						}
					}				    
					break;
				}
			}
		});

		_button_Eq_Wing.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				redrawEqWing();
				chordDistributionEq();
			}
		});

		_button_Nomenclature.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {


						final Shell dialogNomenclature = new Shell(Display.getCurrent(),
								SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.RESIZE
								);	

						dialogNomenclature.setText("Wing Nomenclature");	
						//								Monitor monitor = Display.getDefault().getMonitors()[0];
						//								dialogNomenclature.setSize(
						//										(int)( 0.75*monitor.getBounds().width  ),
						//										(int)( 0.75*monitor.getBounds().height )
						//										);

						dialogNomenclature.setLayout(new FillLayout());
						_nomenclatureWing= new MyNomenclatureImageDialog( dialogNomenclature,"images/Wing_Topview_2.png");

					}
				}
				); // end of button_Nomenclature listener

		_button_ExportFile.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				//				FileOutputStream fos = null;
				//				ObjectOutputStream out = null;
				String filePath = 
						MyConfiguration.currentDirectory 
						+ File.separator + EXPORT_FILE_NAME;

				ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"The ADOpT | Serialize winginitiator file: " +	filePath + "\n"
						);				    	

				switch (event.type) {
				case SWT.Selection:
					// Write XML
					_exportFile = new File(_exportFilePath);
					//					_theLiftingSurface.exportToXMLFile(_exportFile);
					break;
				}// end-of-switch
			}// end-of-handle-event
		});	

		_scrolledComposite.setContent(_ctrComposite);

		// must call these methods from child class of MyInitiatorPane
		_ctrComposite.pack();
		_scrolledComposite.pack();

		redrawWingPlanform();
		//WingDrawing();
		redrawWingBody();
		chordDistribution();
		
		this.forceFocus();


	}  //end of constructor

	private Text putLabel(
			Group groupInput, 
			String labelText, 
			String latex_text,
			Object objectToPrint, 
			int rounding){
		
		return GridLayoutUtil.putLabel(groupInput,
				labelText, latex_text,
				objectToPrint, 
				rounding, 0,
				TEXT_WIDTH_HINT);
		
	}

	private void adjustControls() {

		BigDecimal bd_01 = BigDecimal.valueOf( _theLiftingSurface.get_surface().getEstimatedValue());
		bd_01 = bd_01.setScale(3, RoundingMode.DOWN);
		_text_01.setText( "  " + bd_01 + " " +_theLiftingSurface.get_surface().getUnit().toString());
		_text_01.setEditable(true);	
		BigDecimal bd_02 = BigDecimal.valueOf( _theLiftingSurface.get_aspectRatio());
		bd_02 = bd_02.setScale(1, RoundingMode.DOWN);
		_text_02.setText( "  " + bd_02 );
		_text_02.setEditable(true);	
		BigDecimal bd_03 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioEquivalent());
		bd_03 = bd_03.setScale(2, RoundingMode.DOWN);
		_text_03.setText( "  " + bd_03);
		_text_03.setEditable(true);
		BigDecimal bd_04 = BigDecimal.valueOf(Math.toDegrees( _theLiftingSurface.get_sweepQuarterChordEq().getEstimatedValue()));
		bd_04 = bd_04.setScale(1, RoundingMode.HALF_UP);
		_text_04.setText( "  " + bd_04 + NonSI.DEGREE_ANGLE);
		_text_04.setEditable(true);
		BigDecimal bd_05 = BigDecimal.valueOf( _theLiftingSurface.get_spanStationKink());
		bd_05 = bd_05.setScale(2, RoundingMode.DOWN);
		_text_05.setText( "  " + bd_05);
		_text_05.setEditable(true);
		BigDecimal bd_06 = BigDecimal.valueOf( _theLiftingSurface.get_extensionLERootChordLinPanel());
		bd_06 = bd_06.setScale(2, RoundingMode.DOWN);
		_text_06.setText( "  " + bd_06);
		_text_06.setEditable(true);
		BigDecimal bd_07 = BigDecimal.valueOf( _theLiftingSurface.get_extensionTERootChordLinPanel());
		bd_07 = bd_07.setScale(2, RoundingMode.DOWN);
		_text_07.setText( "  " + bd_07);
		_text_07.setEditable(true);
		BigDecimal bd_07a = BigDecimal.valueOf( _theLiftingSurface.get_deltaXWingFus().getEstimatedValue());
		bd_07a = bd_07a.setScale(2, RoundingMode.DOWN);
		_text_07a.setText( "  " + bd_07a + " " + _theLiftingSurface.get_deltaXWingFus().getUnit());
		_text_07a.setEditable(true);
	}

	private void adjustOutputWingData(){


		BigDecimal bd_08 = BigDecimal.valueOf( _theLiftingSurface.get_surfaceCranked().getEstimatedValue());
		bd_08 = bd_08.setScale(3, RoundingMode.DOWN);
		_text_08.setText( "  " + bd_08 + " " +_theLiftingSurface.get_surfaceCranked().getUnit().toString());
		_text_08.setEditable(false);

		BigDecimal bd_08b = BigDecimal.valueOf( _theLiftingSurface.get_span().getEstimatedValue());
		bd_08b = bd_08b.setScale(2, RoundingMode.HALF_UP);
		_text_08b.setText( "  " + bd_08b + " " +_theLiftingSurface.get_span().getUnit().toString());
		_text_08b.setEditable(false);

		BigDecimal bd_09 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioActual());
		bd_09 = bd_09.setScale(3, RoundingMode.HALF_UP);
		_text_09.setText( "  " + bd_09 );
		_text_09.setEditable(false);

		BigDecimal bd_10= BigDecimal.valueOf( _theLiftingSurface.get_semiSurfaceInnerPanel().getEstimatedValue());
		bd_10= bd_10.setScale(3, RoundingMode.HALF_UP);
		_text_10.setText( "  " + bd_10+" "+_theLiftingSurface.get_semiSurfaceInnerPanel().getUnit().toString());
		_text_10.setEditable(false);

		BigDecimal bd_10b= BigDecimal.valueOf( _theLiftingSurface.get_aspectRatioInnerPanel());
		bd_10b= bd_10b.setScale(2, RoundingMode.HALF_UP);
		_text_10b.setText( "  " + bd_10b);
		_text_10b.setEditable(false);

		BigDecimal bd_11 = BigDecimal.valueOf(_theLiftingSurface.get_semiSurfaceOuterPanel().getEstimatedValue());
		bd_11 = bd_11.setScale(3, RoundingMode.HALF_UP);
		_text_11.setText( "  " + bd_11 +" "+_theLiftingSurface.get_semiSurfaceOuterPanel().getUnit().toString());
		_text_11.setEditable(false);

		BigDecimal bd_11b= BigDecimal.valueOf( _theLiftingSurface.get_aspectRatioOuterPanel());
		bd_11b= bd_11b.setScale(2, RoundingMode.HALF_UP);
		_text_11b.setText( "  " + bd_11b);
		_text_11b.setEditable(false);

		BigDecimal bd_12 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioInnerPanel());
		bd_12 = bd_12.setScale(3, RoundingMode.HALF_UP);
		_text_12.setText( "  " + bd_12 );
		_text_12.setEditable(false);

		BigDecimal bd_13 = BigDecimal.valueOf( _theLiftingSurface.get_taperRatioOuterPanel());
		bd_13 = bd_13.setScale(3, RoundingMode.HALF_UP);
		_text_13.setText( "  " + bd_13 );
		_text_13.setEditable(false);

		BigDecimal bd_14 = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepLEInnerPanel().getEstimatedValue()));
		bd_14 = bd_14.setScale(1, RoundingMode.HALF_UP);
		_text_14.setText( "  " + bd_14+NonSI.DEGREE_ANGLE);
		_text_14.setEditable(false);
		_text_14.setEditable(false);

		BigDecimal bd_14b = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepQuarterChordInnerPanel().getEstimatedValue()));
		bd_14b = bd_14b.setScale(1, RoundingMode.HALF_UP);
		_text_14b.setText( "  " + bd_14b+NonSI.DEGREE_ANGLE);
		_text_14b.setEditable(false);
		_text_14b.setEditable(false);

		BigDecimal bd_15 = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepLEOuterPanel().getEstimatedValue()));
		bd_15 = bd_15.setScale(1, RoundingMode.HALF_UP);
		_text_15.setText( "  " + bd_15+NonSI.DEGREE_ANGLE);
		_text_15.setEditable(false);

		BigDecimal bd_15b = BigDecimal.valueOf( Math.toDegrees(_theLiftingSurface.get_sweepQuarterChordOuterPanel().getEstimatedValue()));
		bd_15b = bd_15b.setScale(1, RoundingMode.HALF_UP);
		_text_15b.setText( "  " + bd_15b+NonSI.DEGREE_ANGLE);
		_text_15b.setEditable(false);
		_text_15b.setEditable(false);

		BigDecimal bd_16b = BigDecimal.valueOf( _theLiftingSurface.get_chordRootEquivalentWing().getEstimatedValue());
		bd_16b = bd_16b.setScale(3, RoundingMode.HALF_UP);
		_text_16b.setText( "  " + bd_16b+" "+_theLiftingSurface.get_chordRootEquivalentWing().getUnit().toString());
		_text_16b.setEditable(false);

		BigDecimal bd_16 = BigDecimal.valueOf( _theLiftingSurface.get_chordRoot().getEstimatedValue());
		bd_16 = bd_16.setScale(3, RoundingMode.HALF_UP);
		_text_16.setText( "  " + bd_16+" "+_theLiftingSurface.get_chordRoot().getUnit().toString());
		_text_16.setEditable(false);

		BigDecimal bd_17 = BigDecimal.valueOf( _theLiftingSurface.get_xLERoot().getEstimatedValue());
		bd_17 = bd_17.setScale(3, RoundingMode.HALF_UP);
		_text_17.setText( "  " + bd_17+" "+_theLiftingSurface.get_xLERoot().getUnit().toString());
		_text_17.setEditable(false);

		BigDecimal bd_18 = BigDecimal.valueOf( _theLiftingSurface.get_chordKink().getEstimatedValue());
		bd_18 = bd_18.setScale(3, RoundingMode.HALF_UP);
		_text_18.setText( "  " + bd_18+" "+_theLiftingSurface.get_chordKink().getUnit().toString());
		_text_18.setEditable(false);

		BigDecimal bd_19 = BigDecimal.valueOf( _theLiftingSurface.get_xLEKink().getEstimatedValue());
		bd_19 = bd_19.setScale(3, RoundingMode.HALF_UP);
		_text_19.setText( "  " + bd_19+" "+_theLiftingSurface.get_xLEKink().getUnit().toString());
		_text_19.setEditable(false);

		BigDecimal bd_20 = BigDecimal.valueOf( _theLiftingSurface.get_chordTip().getEstimatedValue());
		bd_20 = bd_20.setScale(3, RoundingMode.HALF_UP);
		_text_20.setText( "  " + bd_20+" "+_theLiftingSurface.get_chordTip().getUnit().toString());
		_text_20.setEditable(false);

		BigDecimal bd_21 = BigDecimal.valueOf( _theLiftingSurface.get_xLETip().getEstimatedValue());
		bd_21 = bd_21.setScale(3, RoundingMode.HALF_UP);
		_text_21.setText( "  " + bd_21+" "+_theLiftingSurface.get_xLETip().getUnit().toString());
		_text_21.setEditable(false);

		BigDecimal bd_22 = BigDecimal.valueOf( _theLiftingSurface.get_geomChordEq().getEstimatedValue());
		bd_22 = bd_22.setScale(2, RoundingMode.HALF_UP);
		_text_22.setText( "  " + bd_22+" "+_theLiftingSurface.get_geomChordEq().getUnit().toString());
		_text_22.setEditable(false);

		BigDecimal bd_23 = BigDecimal.valueOf( _theLiftingSurface.get_meanAerodChordEq().getEstimatedValue());
		bd_23 = bd_23.setScale(2, RoundingMode.HALF_UP);
		_text_23.setText( "  " + bd_23+" "+_theLiftingSurface.get_meanAerodChordEq().getUnit().toString());
		_text_23.setEditable(false);

		BigDecimal bd_24 = BigDecimal.valueOf( _theLiftingSurface.get_x_LE_Mac_Eq().getEstimatedValue());
		bd_24 = bd_24.setScale(2, RoundingMode.HALF_UP);
		_text_24.setText( "  " + bd_24+" "+_theLiftingSurface.get_x_LE_Mac_Eq().getUnit().toString());
		_text_24.setEditable(false);

		BigDecimal bd_25 = BigDecimal.valueOf( _theLiftingSurface.get_y_LE_Mac_Eq().getEstimatedValue());
		bd_25 = bd_25.setScale(2, RoundingMode.HALF_UP);
		_text_25.setText( "  " + bd_25+" "+_theLiftingSurface.get_y_LE_Mac_Eq().getUnit().toString());
		_text_25.setEditable(false);

		BigDecimal bd_26 = BigDecimal.valueOf( _theLiftingSurface.get_y_LE_Mac_Eq().getEstimatedValue()/(0.5*_theLiftingSurface.get_span().getEstimatedValue()));
		bd_26 = bd_26.setScale(3, RoundingMode.HALF_UP);
		_text_26.setText( "  " + bd_26);
		_text_26.setEditable(false);

		BigDecimal bd_27 = BigDecimal.valueOf( _theLiftingSurface.get_meanAerodChordActual().getEstimatedValue());
		bd_27 = bd_27.setScale(2, RoundingMode.HALF_UP);
		_text_27.setText( "  " + bd_27+" "+_theLiftingSurface.get_meanAerodChordActual().getUnit().toString());
		_text_27.setEditable(false);

		BigDecimal bd_28 = BigDecimal.valueOf( _theLiftingSurface.get_xLEMacActualLRF().getEstimatedValue());
		bd_28 = bd_28.setScale(2, RoundingMode.HALF_UP);
		_text_28.setText( "  " + bd_28+" "+_theLiftingSurface.get_xLEMacActualLRF().getUnit().toString());
		_text_28.setEditable(false);

		BigDecimal bd_29 = BigDecimal.valueOf( _theLiftingSurface.get_yLEMacActualLRF().getEstimatedValue());
		bd_29 = bd_29.setScale(2, RoundingMode.HALF_UP);
		_text_29.setText( "  " + bd_29+" "+_theLiftingSurface.get_yLEMacActualLRF().getUnit().toString());
		_text_29.setEditable(false);

		BigDecimal bd_30 = BigDecimal.valueOf( _theLiftingSurface.get_yLEMacActualLRF().getEstimatedValue()/(0.5*_theLiftingSurface.get_span().getEstimatedValue()));
		bd_30 = bd_30.setScale(3, RoundingMode.HALF_UP);
		_text_30.setText( "  " + bd_30);
		_text_30.setEditable(false);

	}// end of constructor

	public void redrawWingPlanform(){
		// Cranked Wing Parameters
		Amount<Length> span =_theLiftingSurface.get_span();
		Double eta_K = _theLiftingSurface.get_spanStationKink();
		Amount<Length> x_LE_Root= _theLiftingSurface.get_xLERoot();
		Amount<Length> c_root=_theLiftingSurface.get_chordRoot();	
		Amount<Length> x_TE_Root = Amount.valueOf(x_LE_Root.getEstimatedValue() +c_root.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_Kink=_theLiftingSurface.get_xLEKink();
		Amount<Length> c_kink=_theLiftingSurface.get_chordKink();	
		Amount<Length> x_TE_Kink = Amount.valueOf(x_LE_Kink .getEstimatedValue() +c_kink.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_Tip=_theLiftingSurface.get_xLETip();
		Amount<Length> c_Tip=_theLiftingSurface.get_chordTip();	
		Amount<Length> x_TE_Tip = Amount.valueOf(x_LE_Tip .getEstimatedValue() +c_Tip.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_MAC_Ck = Amount.valueOf(_theLiftingSurface.get_xLEMacActualLRF().doubleValue(SI.METER),SI.METER);
		Amount<Length> y_LE_MAC_Ck = Amount.valueOf(_theLiftingSurface.get_yLEMacActualLRF().doubleValue(SI.METER),SI.METER);
		Amount<Length> mac_ck = Amount.valueOf(_theLiftingSurface.get_meanAerodChordActual().doubleValue(SI.METER),SI.METER);

		// Wing Cranked 	Coordinates

		Double[] y_Cranked= {x_LE_Root.doubleValue(SI.METER), eta_K*(0.5*span.doubleValue(SI.METER)), 0.5*span.doubleValue(SI.METER), 
				0.5*span.doubleValue(SI.METER),eta_K*(0.5*span.doubleValue(SI.METER)),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER)};

		ArrayList<Double> y_Cranked_Wing = new ArrayList<Double>(Arrays.asList(y_Cranked));
		Double[] x_Cranked= {x_LE_Root.doubleValue(SI.METER),  x_LE_Kink.doubleValue(SI.METER), x_LE_Tip.doubleValue(SI.METER),
				x_TE_Tip.doubleValue(SI.METER),x_TE_Kink.doubleValue(SI.METER) ,x_TE_Root.doubleValue(SI.METER),x_TE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER) };

		ArrayList<Double> x_Cranked_Wing = new ArrayList<Double>(Arrays.asList(x_Cranked));

		Double[] y_Cranked_Mac = {y_LE_MAC_Ck.doubleValue(SI.METER),y_LE_MAC_Ck.doubleValue(SI.METER)};
		ArrayList<Double> y_Cranked_Wing_Mac = new ArrayList<Double>(Arrays.asList(y_Cranked_Mac));

		Double[] x_Cranked_Mac = {x_LE_MAC_Ck.doubleValue(SI.METER),x_LE_MAC_Ck.doubleValue(SI.METER)+mac_ck.doubleValue(SI.METER)};
		ArrayList<Double> x_Cranked_Wing_Mac = new ArrayList<Double>(Arrays.asList(x_Cranked_Mac));

		// initial cleanup
		if ( !_traces_XY.isEmpty()  ) {
			for(int i=0; i<_traces_XY.size(); i++){
				this._xyGraphXY.removeTrace( _traces_XY.get(i) ); // 0: Cranked, 1:Mac_ck , 2: Equivalent
			}
		}
		this._tdpXY.clear();
		this._traces_XY.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		_tdpXY.get(0).setCurrentXDataArray(Doubles.toArray(y_Cranked_Wing));
		_tdpXY.get(0).setCurrentYDataArray(Doubles.toArray(x_Cranked_Wing));
		_traces_XY.add(
				new Trace("Cranked Wing",					
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(0)
						)
				);			

		//set trace property
		_traces_XY.get(0).setPointStyle(PointStyle.POINT);
		_traces_XY.get(0).setPointSize(5);
		_traces_XY.get(0).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_XY.get(0).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(0) );
		// add a new trace data provider for Lower outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(1).clearTrace();
		_tdpXY.get(1).setConcatenate_data(false);
		_tdpXY.get(1).setBufferSize(100);		
		_tdpXY.get(1).setCurrentXDataArray(
				Doubles.toArray(y_Cranked_Wing_Mac) // from Guava lib
				);
		_tdpXY.get(1).setCurrentYDataArray(
				Doubles.toArray(x_Cranked_Wing_Mac) // from Guava lib
				);

		// make the upper curve trace
		_traces_XY.add(
				new Trace(
						" Mean Aerodynamic chord", 
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(1)
						)
				);			
		//set trace property
		//_traces_XY.get(1).setPointStyle(PointStyle.POINT);
		_traces_XY.get(1).setPointSize(5);
		_traces_XY.get(1).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLACK)
				);
		_traces_XY.get(1).setLineWidth(2);
		//_traces_XY.get(1).setTraceType(TraceType.DASH_LINE);
		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(1) );

		//			Set Property of axis
		_xyGraphXY.setTitle("Wing Planform");
		_xyGraphXY.primaryYAxis.setTitle("X-Axis (m)");
		_xyGraphXY.primaryXAxis.setTitle("Y-Axis (m)");
		_xyGraphXY.primaryYAxis.setRange(15 ,0);
		_xyGraphXY.primaryXAxis.setRange(0,( 0.5*span.doubleValue(SI.METER))+2);

	}	// end of redrawWingPlanform

	public void redrawEqWing(){

		// Cranked Wing Parameters
		Amount<Length> span =_theLiftingSurface.get_span();
		Double eta_K = _theLiftingSurface.get_spanStationKink();
		Amount<Length> x_LE_Root= _theLiftingSurface.get_xLERoot();
		Amount<Length> c_root=_theLiftingSurface.get_chordRoot();	
		Amount<Length> x_TE_Root = Amount.valueOf(x_LE_Root.getEstimatedValue() +c_root.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_Kink=_theLiftingSurface.get_xLEKink();
		Amount<Length> c_kink=_theLiftingSurface.get_chordKink();	
		Amount<Length> x_TE_Kink = Amount.valueOf(x_LE_Kink .getEstimatedValue() +c_kink.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_Tip=_theLiftingSurface.get_xLETip();
		Amount<Length> c_Tip=_theLiftingSurface.get_chordTip();	
		Amount<Length> x_TE_Tip = Amount.valueOf(x_LE_Tip .getEstimatedValue() +c_Tip.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_MAC_Eq = Amount.valueOf(_theLiftingSurface.get_x_LE_Mac_Eq().doubleValue(SI.METER),SI.METER);
		Amount<Length> y_LE_MAC_Eq = Amount.valueOf(_theLiftingSurface.get_y_LE_Mac_Eq().doubleValue(SI.METER),SI.METER);
		Amount<Length> x_LE_MAC_Ck = Amount.valueOf(_theLiftingSurface.get_xLEMacActualLRF().doubleValue(SI.METER),SI.METER);
		Amount<Length> y_LE_MAC_Ck = Amount.valueOf(_theLiftingSurface.get_yLEMacActualLRF().doubleValue(SI.METER),SI.METER);
		Amount<Length> mac_eq = Amount.valueOf(_theLiftingSurface.get_meanAerodChordEq().doubleValue(SI.METER),SI.METER);
		Amount<Length> mac_ck = Amount.valueOf(_theLiftingSurface.get_meanAerodChordActual().doubleValue(SI.METER),SI.METER);

		// Equivalent Wing Parameters

		Amount<Length> c_root_eq=_theLiftingSurface.get_chordRootEquivalentWing();	
		Amount<Length> x_TE_Root_eq = Amount.valueOf(_theLiftingSurface.get_x_LE_Root_eq().getEstimatedValue() +c_root_eq.getEstimatedValue(), SI.METER);

		// Wing Cranked 	Coordinates

		Double[] y_Cranked= {x_LE_Root.doubleValue(SI.METER), eta_K*(0.5*span.doubleValue(SI.METER)), 0.5*span.doubleValue(SI.METER), 
				0.5*span.doubleValue(SI.METER),eta_K*(0.5*span.doubleValue(SI.METER)),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER)};

		ArrayList<Double> y_Cranked_Wing = new ArrayList<Double>(Arrays.asList(y_Cranked));
		Double[] x_Cranked= {x_LE_Root.doubleValue(SI.METER),  x_LE_Kink.doubleValue(SI.METER), x_LE_Tip.doubleValue(SI.METER),
				x_TE_Tip.doubleValue(SI.METER),x_TE_Kink.doubleValue(SI.METER) ,x_TE_Root.doubleValue(SI.METER),x_TE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER) };

		ArrayList<Double> x_Cranked_Wing = new ArrayList<Double>(Arrays.asList(x_Cranked));

		Double[] y_Cranked_Mac = {y_LE_MAC_Ck.doubleValue(SI.METER),y_LE_MAC_Ck.doubleValue(SI.METER)};
		ArrayList<Double> y_Cranked_Wing_Mac = new ArrayList<Double>(Arrays.asList(y_Cranked_Mac));

		Double[] x_Cranked_Mac = {x_LE_MAC_Ck.doubleValue(SI.METER),x_LE_MAC_Ck.doubleValue(SI.METER)+mac_ck.doubleValue(SI.METER)};
		ArrayList<Double> x_Cranked_Wing_Mac = new ArrayList<Double>(Arrays.asList(x_Cranked_Mac));

		// Wing Equivalent 	Coordinates

		Double[] y_Eq= {x_LE_Root.doubleValue(SI.METER), 0.5*span.doubleValue(SI.METER), 
				0.5*span.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER),y_LE_MAC_Eq.doubleValue(SI.METER),y_LE_MAC_Eq.doubleValue(SI.METER)};

		ArrayList<Double> y_Eq_Wing = new ArrayList<Double>(Arrays.asList(y_Eq));

		Double[] x_Eq= {_theLiftingSurface.get_x_LE_Root_eq().doubleValue(SI.METER), x_LE_Tip.doubleValue(SI.METER),
				x_TE_Tip.doubleValue(SI.METER),x_TE_Root_eq.doubleValue(SI.METER),x_TE_Root_eq.doubleValue(SI.METER),_theLiftingSurface.get_x_LE_Root_eq().doubleValue(SI.METER),x_LE_MAC_Eq.doubleValue(SI.METER),x_LE_MAC_Eq.doubleValue(SI.METER)+mac_eq.doubleValue(SI.METER) };

		ArrayList<Double> x_Eq_Wing = new ArrayList<Double>(Arrays.asList(x_Eq));


		// initial cleanup
		if ( !_traces_XY.isEmpty()  ) {
			for(int i=0; i<_traces_XY.size(); i++){
				this._xyGraphXY.removeTrace( _traces_XY.get(i) ); // 0: Cranked, 1:Mac_ck , 2: Equivalent
			}
		}
		this._tdpXY.clear();
		this._traces_XY.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		_tdpXY.get(0).setCurrentXDataArray(Doubles.toArray(y_Cranked_Wing));
		_tdpXY.get(0).setCurrentYDataArray(Doubles.toArray(x_Cranked_Wing));
		_traces_XY.add(
				new Trace("Cranked Wing",					
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(0)
						)
				);			

		//set trace property
		_traces_XY.get(0).setPointStyle(PointStyle.POINT);
		_traces_XY.get(0).setPointSize(5);
		_traces_XY.get(0).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_XY.get(0).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(0) );
		// add a new trace data provider for Lower outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(1).clearTrace();
		_tdpXY.get(1).setConcatenate_data(false);
		_tdpXY.get(1).setBufferSize(100);		
		_tdpXY.get(1).setCurrentXDataArray(
				Doubles.toArray(y_Cranked_Wing_Mac) // from Guava lib
				);
		_tdpXY.get(1).setCurrentYDataArray(
				Doubles.toArray(x_Cranked_Wing_Mac) // from Guava lib
				);

		// make the upper curve trace
		_traces_XY.add(
				new Trace(
						" Mean Aerodynamic chord", 
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(1)
						)
				);			
		//set trace property
		//_traces_XY.get(1).setPointStyle(PointStyle.POINT);
		_traces_XY.get(1).setPointSize(5);
		_traces_XY.get(1).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLACK)
				);
		_traces_XY.get(1).setLineWidth(2);
		//_traces_XY.get(1).setTraceType(TraceType.DASH_LINE);
		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(1) );
		// add a new trace data provider for Lower outline
		_tdpXY.add( new CircularBufferDataProvider(false) );
		// this._tdpXZ.get(1).clearTrace();
		_tdpXY.get(2).setConcatenate_data(false);
		_tdpXY.get(2).setBufferSize(200);		
		_tdpXY.get(2).setCurrentXDataArray(
				Doubles.toArray(y_Eq_Wing) // from Guava lib
				);
		_tdpXY.get(2).setCurrentYDataArray(
				Doubles.toArray(x_Eq_Wing) // from Guava lib
				);

		// make the upper curve trace
		_traces_XY.add(
				new Trace(
						"Equivalent Wing", 
						_xyGraphXY.primaryXAxis, _xyGraphXY.primaryYAxis, 
						_tdpXY.get(2)
						)
				);			
		//set trace property
		//_traces_XY.get(2).setPointStyle(PointStyle.POINT);
		_traces_XY.get(2).setPointSize(5);
		_traces_XY.get(2).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_RED)
				);
		_traces_XY.get(2).setLineWidth(2);
		//_traces_XY.get(2).setTraceType(TraceType.DASH_LINE);
		//add the trace to xyGraph
		_xyGraphXY.addTrace( _traces_XY.get(2) );
		//					

		//				Set Property of axis
		_xyGraphXY.setTitle("Wing Planform");
		_xyGraphXY.primaryYAxis.setTitle("X-Axis (m)");
		_xyGraphXY.primaryXAxis.setTitle("Y-Axis (m)");
		_xyGraphXY.primaryYAxis.setRange(15 ,0);
		_xyGraphXY.primaryXAxis.setRange(0,( 0.5*span.doubleValue(SI.METER))+2);

	}  //end of redrawEqWing

	public void redrawWingBody(){

		// initial cleanup
		if ( !_traces_XZ.isEmpty()  ) {
			for(int i=0; i<_traces_XZ.size(); i++){
				this._xyGraphXZ.removeTrace( _traces_XZ.get(i) ); // 0: Upper, 1: Lower
			}
		}
		this._tdpXZ.clear();
		this._traces_XZ.clear();

		// MAKE TRACES

		int idxTrace = -1;
		if (_theFuselage != null)
		{
			// add a new trace data provider for Upper outline
			_tdpXZ.add( new CircularBufferDataProvider(false) );
			idxTrace = _tdpXZ.size() - 1; 
			// this._tdpXZ.get(0).clearTrace();
			_tdpXZ.get(idxTrace).setBufferSize(100);		
			_tdpXZ.get(idxTrace).setCurrentXDataArray(
					Doubles.toArray(_theFuselage.getOutlineXYSideRCurveX()) // from Guava lib
					);
			_tdpXZ.get(idxTrace).setCurrentYDataArray(
					Doubles.toArray(_theFuselage.getOutlineXYSideRCurveY()) // from Guava lib
					);

			// make the upper curve trace
			_traces_XZ.add(
					new Trace(
							"Body", 
							_xyGraphXZ.primaryXAxis, _xyGraphXZ.primaryYAxis, 
							_tdpXZ.get(idxTrace)
							)
					);			
			//set trace property
			_traces_XZ.get(idxTrace).setPointStyle(PointStyle.POINT);
			_traces_XZ.get(idxTrace).setPointSize(5);
			_traces_XZ.get(idxTrace).setTraceColor(
					XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLACK)
					);
			_traces_XZ.get(idxTrace).setLineWidth(2);

			//add the trace to xyGraph
			_xyGraphXZ.addTrace( _traces_XZ.get(idxTrace) );
		}
		// add a new trace data provider for Lower outline

		// Cranked Wing Parameters
		Amount<Length> span =_theLiftingSurface.get_span();
		Double eta_K = _theLiftingSurface.get_spanStationKink();
		Amount<Length> x_LE_Root= _theLiftingSurface.get_xLERoot();
		Amount<Length> c_root=_theLiftingSurface.get_chordRoot();	
		Amount<Length> x_TE_Root = Amount.valueOf(x_LE_Root.getEstimatedValue() +c_root.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_Kink=_theLiftingSurface.get_xLEKink();
		Amount<Length> c_kink=_theLiftingSurface.get_chordKink();	
		Amount<Length> x_TE_Kink = Amount.valueOf(x_LE_Kink .getEstimatedValue() +c_kink.getEstimatedValue(), SI.METER);
		Amount<Length> x_LE_Tip=_theLiftingSurface.get_xLETip();
		Amount<Length> c_Tip=_theLiftingSurface.get_chordTip();	
		Amount<Length> x_TE_Tip = Amount.valueOf(x_LE_Tip .getEstimatedValue() +c_Tip.getEstimatedValue(), SI.METER);
		Amount<Length> deltaX_WingFus = _theLiftingSurface.get_deltaXWingFus();

		// Wing Cranked 	Coordinates

		Double[] y_Cranked= {x_LE_Root.doubleValue(SI.METER), eta_K*(0.5*span.doubleValue(SI.METER)), 0.5*span.doubleValue(SI.METER), 
				0.5*span.doubleValue(SI.METER),eta_K*(0.5*span.doubleValue(SI.METER)),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER),x_LE_Root.doubleValue(SI.METER)};

		ArrayList<Double> y_Cranked_Wing = new ArrayList<Double>(Arrays.asList(y_Cranked));
		Double[] x_Cranked= {deltaX_WingFus.doubleValue(SI.METER), deltaX_WingFus.doubleValue(SI.METER)+ x_LE_Kink.doubleValue(SI.METER),deltaX_WingFus.doubleValue(SI.METER)+ x_LE_Tip.doubleValue(SI.METER),
				deltaX_WingFus.doubleValue(SI.METER)+ x_TE_Tip.doubleValue(SI.METER),deltaX_WingFus.doubleValue(SI.METER)+x_TE_Kink.doubleValue(SI.METER) ,deltaX_WingFus.doubleValue(SI.METER)+x_TE_Root.doubleValue(SI.METER),deltaX_WingFus.doubleValue(SI.METER)+x_TE_Root.doubleValue(SI.METER),deltaX_WingFus.doubleValue(SI.METER)+x_LE_Root.doubleValue(SI.METER) };

		ArrayList<Double> x_Cranked_Wing = new ArrayList<Double>(Arrays.asList(x_Cranked));
		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpXZ.add( new CircularBufferDataProvider(false) );
		idxTrace = _tdpXZ.size() - 1; 
		_tdpXZ.get(idxTrace).setCurrentXDataArray(Doubles.toArray(x_Cranked_Wing));
		_tdpXZ.get(idxTrace).setCurrentYDataArray(Doubles.toArray(y_Cranked_Wing));
		_traces_XZ.add(
				new Trace("Wing",					
						_xyGraphXZ.primaryXAxis, _xyGraphXZ.primaryYAxis, 
						_tdpXZ.get(idxTrace)
						)
				);			

		//set trace property
		_traces_XZ.get(idxTrace).setPointStyle(PointStyle.POINT);
		_traces_XZ.get(idxTrace).setPointSize(5);
		_traces_XZ.get(idxTrace).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_XZ.get(idxTrace).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphXZ.addTrace( _traces_XZ.get(idxTrace) );

		//				Set Property of axis
		_xyGraphXZ.setTitle("Wing-Body");
		_xyGraphXZ.primaryYAxis.setTitle("Y-Axis (m)");
		_xyGraphXZ.primaryXAxis.setTitle("X-Axis (m)");
		_xyGraphXZ.primaryYAxis.setRange(0 ,0.5*span.doubleValue(SI.METER)+2);
		_xyGraphXZ.primaryXAxis.setRange(-2,_theFuselage.get_len_F().doubleValue(SI.METER)+5);

	}	// end of redrawWingBody

	public void chordDistribution(){

		ArrayList<Double> Chords_ck= new ArrayList<Double>();

		for (int i=0;i<_theLiftingSurface.get_eta().size();i++){

			Chords_ck.add(_theLiftingSurface.get_chordsActualVsYList().get(i).doubleValue(SI.METER)); 

		}

		// initial cleanup
		if ( !_traces_YZ.isEmpty()  ) {
			for(int i=0; i<_traces_YZ.size(); i++){
				this._xyGraphYZ.removeTrace( _traces_YZ.get(i) ); // 0: Cranked, 1: Equivalent
			}
		}
		this._tdpYZ.clear();
		this._traces_YZ.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpYZ.add( new CircularBufferDataProvider(false) );
		int idxTrace = _tdpYZ.size() - 1;
		_tdpYZ.get(idxTrace).setCurrentXDataArray(Doubles.toArray(_theLiftingSurface.get_eta()));
		_tdpYZ.get(idxTrace).setCurrentYDataArray(Doubles.toArray(Chords_ck));
		_traces_YZ.add(
				new Trace("Cranked Wing",					
						_xyGraphYZ.primaryXAxis, _xyGraphYZ.primaryYAxis, 
						_tdpYZ.get(idxTrace)
						)
				);			

		//set trace property
		_traces_YZ.get(idxTrace).setPointStyle(PointStyle.POINT);
		_traces_YZ.get(idxTrace).setPointSize(5);
		_traces_YZ.get(idxTrace).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_YZ.get(idxTrace).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphYZ.addTrace( _traces_YZ.get(idxTrace) );


		//				Set Property of axis
		_xyGraphYZ.setTitle("Chord Distribution");
		_xyGraphYZ.primaryYAxis.setTitle("Chord (m)");
		_xyGraphYZ.primaryXAxis.setTitle("eta");
		_xyGraphYZ.primaryYAxis.setRange(0,Chords_ck.get(0)+1);
		_xyGraphYZ.primaryXAxis.setRange(0,1);


	}	// end of chord distribution cranked wing

	public void chordDistributionEq(){

		ArrayList<Double> Chords_eq= new ArrayList<Double>();
		ArrayList<Double> Chords_ck= new ArrayList<Double>();

		for (int i=0;i<_theLiftingSurface.get_eta().size();i++){

			Chords_ck.add(_theLiftingSurface.get_chordsActualVsYList().get(i).doubleValue(SI.METER)); 
			Chords_eq.add(_theLiftingSurface.get_chordsEqList().get(i).doubleValue(SI.METER)); 
		}

		// initial cleanup
		if ( !_traces_YZ.isEmpty()  ) {
			for(int i=0; i<_traces_YZ.size(); i++){
				this._xyGraphYZ.removeTrace( _traces_YZ.get(i) ); // 0: Cranked, 1: Equivalent
			}
		}
		this._tdpYZ.clear();
		this._traces_YZ.clear();

		// MAKE TRACES

		// add a new trace data provider for Upper outline
		_tdpYZ.add( new CircularBufferDataProvider(false) );
		int idxTrace = _tdpYZ.size() - 1;
		_tdpYZ.get(idxTrace).setCurrentXDataArray(Doubles.toArray(_theLiftingSurface.get_eta()));
		_tdpYZ.get(idxTrace).setCurrentYDataArray(Doubles.toArray(Chords_ck));
		_traces_YZ.add(
				new Trace("Cranked Wing",					
						_xyGraphYZ.primaryXAxis, _xyGraphYZ.primaryYAxis, 
						_tdpYZ.get(idxTrace)
						)
				);			

		//set trace property
		_traces_YZ.get(idxTrace).setPointStyle(PointStyle.POINT);
		_traces_YZ.get(idxTrace).setPointSize(5);
		_traces_YZ.get(idxTrace).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		_traces_YZ.get(idxTrace).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphYZ.addTrace( _traces_YZ.get(idxTrace) );

		// add a new trace data provider for Upper outline
		_tdpYZ.add( new CircularBufferDataProvider(false) );
		idxTrace = _tdpYZ.size() - 1;
		_tdpYZ.get(idxTrace).setCurrentXDataArray(Doubles.toArray(_theLiftingSurface.get_eta()));
		_tdpYZ.get(idxTrace).setCurrentYDataArray(Doubles.toArray(Chords_eq));
		_traces_YZ.add(
				new Trace("EquivalentWing",					
						_xyGraphYZ.primaryXAxis, _xyGraphYZ.primaryYAxis, 
						_tdpYZ.get(idxTrace)
						)
				);			

		//set trace property
		_traces_YZ.get(idxTrace).setPointStyle(PointStyle.POINT);
		_traces_YZ.get(idxTrace).setPointSize(5);
		_traces_YZ.get(idxTrace).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_RED)
				);
		_traces_YZ.get(idxTrace).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraphYZ.addTrace( _traces_YZ.get(idxTrace) );

		//				Set Property of axis
		_xyGraphYZ.setTitle("Chord Distribution");
		_xyGraphYZ.primaryYAxis.setTitle("Chord (m)");
		_xyGraphYZ.primaryXAxis.setTitle("eta");
		_xyGraphYZ.primaryYAxis.setRange(0,Chords_ck.get(0)+1);
		_xyGraphYZ.primaryXAxis.setRange(0,1);

	} // end of chordDistributionEq

} //end of class





