package it.unina.adopt.GUI.test;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class TestApp {
	private DataBindingContext m_bindingContext;

	protected Shell shell;
	private Text text;
	private Button btnRadioButton;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = Display.getDefault();
		Realm.runWithDefault(SWTObservables.getRealm(display), new Runnable() {
			public void run() {
				try {
					TestApp window = new TestApp();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		btnRadioButton = new Button(shell, SWT.RADIO);
		btnRadioButton.setBounds(61, 68, 90, 16);
		btnRadioButton.setText("Radio Button");
		
		text = new Text(shell, SWT.BORDER);
		text.setBounds(221, 129, 76, 21);
		m_bindingContext = initDataBindings();

	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeSelectionBtnRadioButtonObserveWidget = WidgetProperties.selection().observe(btnRadioButton);
		IObservableValue editableTextObserveValue = PojoProperties.value("editable").observe(text);
		bindingContext.bindValue(observeSelectionBtnRadioButtonObserveWidget, editableTextObserveValue, null, null);
		//
		return bindingContext;
	}
}
