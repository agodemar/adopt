package it.unina.adopt.GUI.test.javafx;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class MyMainController {

	@FXML
	public VBox myTopBox;
	
	@FXML
	private Button _agodemarButton;

	@FXML
	private Button _buttonSettings;
	
	public MyMainController() {

	}

	@FXML
	public void agodemarButtonHandler() {
		System.out.println("Hello!!!!");
	}

	@FXML
	public void settingsButtonHandler() {
		System.out.println("Settings :: Hello!!!!");
	}
	
}
