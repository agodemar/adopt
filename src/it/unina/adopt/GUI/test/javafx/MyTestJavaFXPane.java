package it.unina.adopt.GUI.test.javafx;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import it.unina.adopt.main.ADOPT_GUI;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.embed.swt.FXCanvas;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;

public class MyTestJavaFXPane extends Composite {

	double anchorX, anchorY;
	private static final float EDGE_LENGTH = 380;
	private double anchorAngleX = 0;
	private double anchorAngleY = 0;
	private final DoubleProperty angleX = new SimpleDoubleProperty(25);
	private final DoubleProperty angleY = new SimpleDoubleProperty(40);
		
	private static MyFX3DContentModel _contentModel;
	private SessionManager _sessionManager;
	public static final String FILE_URL_PROPERTY = "fileUrl";
	
    public static MyFX3DContentModel getContentModel() {
        return _contentModel;
    }
	
    private FXCanvas _fxCanvas;
    
	
	public MyTestJavaFXPane(Composite parent, int style) {

		super(parent, style);
		
		populatePane0(parent);

	}
	
	private void populatePane1(Composite parent) {
		
//		_sessionManager = SessionManager.createSessionManager("MyTestJavaFXPane_3DViewer");
//		_sessionManager.loadSession();
		
//		this.setLayout(new FillLayout());
        
//		GridLayout gridLayout = new GridLayout(1,false);
//		this.setLayout(gridLayout);
//
//		/* Create an FXCanvas inside the current composite */
//        _fxCanvas = new FXCanvas(this, SWT.NONE) {
//        	@Override
//        	public Point computeSize(int wHint, int hHint, boolean changed) {
////        		getScene().getWindow().sizeToScene();
////        		int width = (int) getScene().getWidth();
////        		int height = (int) getScene().getHeight();
//
//        		int width = (int) getParent().getSize().x;
//        		int height = (int) getParent().getSize().y;
//        		
//        		Point p = getParent().getSize();
//        		
//        		System.out.println("00x --> " + p.x + ", " + p.y);
//        		System.out.println("000 --> " + getScene().getWindow().getWidth() + ", " + getScene().getWindow().getHeight());
//        		
//        		System.out.println("w, h --> " + width + ", " + height);
//        		
//        		return p; // new Point(width, height);
//        	}
//        };
//        
//		GridData gridDataFXCanvas = new GridData(GridData.FILL_BOTH);
//		gridDataFXCanvas.grabExcessHorizontalSpace = true;
//		gridDataFXCanvas.grabExcessVerticalSpace = true;
//		// gridDataFXCanvas.widthHint = 850;
//		_fxCanvas.setLayoutData(gridDataFXCanvas);
		
		this.setLayout(new FillLayout(SWT.VERTICAL));
		this.setLayoutData(new GridData(GridData.FILL_BOTH));
		_fxCanvas = new FXCanvas(this, SWT.NONE) {
			@Override
			public Point computeSize(int wHint, int hHint, boolean changed) {
				getScene().getWindow().sizeToScene();
				int width = (int) getScene().getWidth();
				int height = (int) getScene().getHeight();

				System.out.println("w, h --> " + width + ", " + height);

				return new Point(width, height);
			}
		};

		final GridData data = new GridData();
		data.widthHint = 1000;
		data.heightHint = 1000;
		_fxCanvas.setLayoutData(data);
		_fxCanvas.layout(true);
		
//		this.setLayout(new FillLayout());		
//		_fxCanvas = new FXCanvas(this, SWT.NONE) {
//			@Override
//			public Point computeSize(int wHint, int hHint, boolean changed) {
//				getScene().getWindow().sizeToScene();
//				int width = (int) getScene().getWidth();
//				int height = (int) getScene().getHeight();
//
//				System.out.println("w, h --> " + width + ", " + height);
//
//				return new Point(width, height);
//			}
//		};

		populateFXCanvas(_fxCanvas);

//		_fxCanvas.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
		
		this.layout(true);
		
	}
	
	private void populateFXCanvas(FXCanvas fxCanvas) {
		
		/* Create a JavaFX Group node */
		Group rootGroup = new Group();
		
//		/* Create a JavaFX button */
//		final Button jfxButton = new Button("JFX Button");
//
//		/* Assign the CSS ID ipad-dark-grey */
//		jfxButton.setId(
//				// "ipad-dark-grey"
//				// "shiny-orange"
//				"big-yellow"
//				);
//		
//		/* Add the button as a child of the Group node */
//		rootGroup.getChildren().add(jfxButton);
		
		
		// the content model
//		_contentModel = new MyFX3DContentModel();
		
		// rootGroup.getChildren().add(_contentModel.getContent());

		
		FXMLLoader fxmlLoader = 
				new FXMLLoader(
//					ADOPT_GUI.class
//						.getResource("../GUI/test/javafx/my_main.fxml")
					MyTestJavaFXPane.class
						.getResource("my_main2.fxml")
				);
		try {
			// VBox vbox =  
			Pane pane = fxmlLoader.load();
			rootGroup = new Group(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* Create the Scene instance and set the group node as root */
		Scene scene = new Scene(
				rootGroup, 
				Color.rgb(
						this.getBackground().getRed(),
						this.getBackground().getGreen(),
						this.getBackground().getBlue()
						)
				);
		
		// assign scene to canvas
		fxCanvas.setScene(scene);
	}
	
	private void populatePane0(Composite parent) {
		
		this.setLayout(new FillLayout());

		/* Create the SWT button */
		final org.eclipse.swt.widgets.Button swtButton =
				new org.eclipse.swt.widgets.Button(this, SWT.PUSH);
		swtButton.setText("SWT Button");

		/* Create an FXCanvas */
		final FXCanvas fxCanvas = new FXCanvas(this, SWT.NONE) {
			public Point computeSize(int wHint, int hHint, boolean changed) {
				getScene().getWindow().sizeToScene();
				int width = (int) getScene().getWidth();
				int height = (int) getScene().getHeight();
				
				Point p = getParent().getSize();

				System.out.println("00x --> " + p.x + ", " + p.y);
				System.out.println("000 --> " + getScene().getWindow().getWidth() + ", " + getScene().getWindow().getHeight());
				System.out.println("w,h --> " + width + ", " + height);

				return new Point(width, height);
			}
		};
		
		/*
		 *  you set JavaFX content into an FXCanvas with the setScene() method 
		 *  in the FXCanvas class. To force SWT to lay out the canvas based on 
		 *  the new JavaFX content, resize the JavaFX content first. 
		 *  To do this, get the JavaFX Window that contains the JavaFX content 
		 *  and call sizeToScene(). When JavaFX is embedded in SWT, a new preferred 
		 *  size is set for FXCanvas, enabling SWT to resize the embedded JFX 
		 *  content in the same manner as other SWT controls.
		 *  
		 */
		
		

		/* Create a JavaFX Group node */
		Group group = new Group();
		/* Create a JavaFX button */
		final Button jfxButton = new Button("JFX Button");

		/* Assign the CSS ID ipad-dark-grey */
		jfxButton.setId(
				// "ipad-dark-grey"
				// "shiny-orange"
				"big-yellow"
				);
		
		/* Add the button as a child of the Group node */
		group.getChildren().add(jfxButton);
		
		/* Create the Scene instance and set the group node as root */
		Scene scene = new Scene(group, Color.rgb(
				this.getBackground().getRed(),
				this.getBackground().getGreen(),
				this.getBackground().getBlue()));

		/* Attach an external stylesheet */
		scene.getStylesheets().add(
				ADOPT_GUI.class.getResource("../css/Buttons.css").toExternalForm()
				);


		/* investigate 3D */
		
		boolean supported = Platform.isSupported(ConditionalFeature.SCENE3D);
		System.out.println("3D supported: " + supported);
		
		Sphere sphere = new Sphere(100);
		group.getChildren().add(sphere);
		
		Box box = new Box(80, 70, 20);
		group.getChildren().add(box);
		
		DoubleProperty translateX = new SimpleDoubleProperty();
		sphere.translateXProperty().bind(translateX);
		box.translateXProperty().bind(translateX);

		DoubleProperty translateY = new SimpleDoubleProperty();
		sphere.translateYProperty().bind(translateY);
		box.translateYProperty().bind(translateY.add(150));

		DoubleProperty rotateY = new SimpleDoubleProperty();
		box.rotateProperty().bind(rotateY);
		
		// materials
		PhongMaterial material1 = new PhongMaterial(Color.BLUE);
		material1.setSpecularColor(Color.LIGHTBLUE);
		material1.setSpecularPower(10.0d);
		sphere.setMaterial(material1);

		PhongMaterial material2 = new PhongMaterial(Color.GREEN);
		material2.setSpecularColor(Color.LIGHTGREEN);
		material2.setSpecularPower(10.0d);
		box.setMaterial(material2);
		
		translateX.set(200);
		translateY.set(300);
		rotateY.set(20);
		
		Rotate xRotate;
		Rotate yRotate;
		group.getTransforms().setAll(
				xRotate = new Rotate(0, Rotate.X_AXIS),
				yRotate = new Rotate(0, Rotate.Y_AXIS)
				);
		xRotate.angleProperty().bind(angleX);
		yRotate.angleProperty().bind(angleY);
		
		scene.setOnMousePressed((MouseEvent event) -> {
			anchorX = event.getSceneX();
			anchorY = event.getSceneY();
			anchorAngleX = angleX.get();
			anchorAngleY = angleY.get();
			});
		scene.setOnMouseDragged((MouseEvent event) -> {
			angleX.set(anchorAngleX - (anchorY - event.getSceneY()));
			angleY.set(anchorAngleY + anchorX - event.getSceneX());
		});
//		scene.setOnMouseDragged(event -> {
//			angleX.set(anchorAngleX - (anchorY - event.getSceneY()));
//			angleY.set(anchorAngleY + anchorX - event.getSceneX());
//		});
		
//		Timeline timeline = new Timeline(
//				new KeyFrame(Duration.seconds(0), new KeyValue(translateX, 0)),
//				new KeyFrame(Duration.seconds(2), new KeyValue(translateX, 320))
//				);
//		timeline.setCycleCount(Animation.INDEFINITE);
//		timeline.play();

		
		// assign scene to canvas
		fxCanvas.setScene(scene);
		
		
		/* Add Listeners */
		swtButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				jfxButton.setText("JFX Button: Hello from SWT");
				parent.layout();
			}
		});
		jfxButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				swtButton.setText("SWT Button: Hello from JFX");
				parent.layout();
			}
		});
		
	}
	
	
}
