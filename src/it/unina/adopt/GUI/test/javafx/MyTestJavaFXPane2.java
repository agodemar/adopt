package it.unina.adopt.GUI.test.javafx;

import static java.lang.Math.toRadians;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.measure.quantity.Angle;
import javax.measure.unit.SI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.fxyz.shapes.primitives.BezierMesh;
import org.fxyz.shapes.primitives.helper.InterpolateBezier;
import org.jscience.physics.amount.Amount;

import com.interactivemesh.jfx.importer.ImportException;
import com.interactivemesh.jfx.importer.stl.StlMeshImporter;

import it.unina.adopt.GUI.javafxaddons.MyCurveExtrusion;
import it.unina.adopt.GUI.javafxaddons.MyCylinder;
import it.unina.adopt.GUI.javafxaddons.MyPoint3D;
import it.unina.adopt.GUI.javafxaddons.MyPolyLine3D;
import it.unina.adopt.GUI.javafxaddons.MySimpleQuad;
import it.unina.adopt.GUI.javafxaddons.MySimpleTriangle;
import it.unina.adopt.GUI.javafxaddons.MyTorusMesh;
import it.unina.adopt.GUI.javafxaddons.Xform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.embed.swt.FXCanvas;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.scene.AmbientLight;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Sphere;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import jmatrix.Matrix;
import standaloneutils.customdata.MyArray;

public class MyTestJavaFXPane2 extends Composite {

	private FXCanvas _fxCanvas;
	private Parent _parent;

	final Group root = new Group();
	final Xform axisGroup = new Xform();
	final Xform moleculeGroup = new Xform();
	final Xform testGroup = new Xform();
	final Xform world = new Xform();
	final PerspectiveCamera camera = new PerspectiveCamera(true);
	final Xform cameraXform = new Xform();
	final Xform cameraXform2 = new Xform();
	final Xform cameraXform3 = new Xform();
	private static final double CAMERA_INITIAL_DISTANCE = -450;
	private static final double CAMERA_INITIAL_X_ANGLE = 70.0;
	private static final double CAMERA_INITIAL_Y_ANGLE = 320.0;
	private static final double CAMERA_NEAR_CLIP = 0.1;
	private static final double CAMERA_FAR_CLIP = 10000.0;
	private static final double AXIS_LENGTH = 250.0;
	private static final double HYDROGEN_ANGLE = 104.5;
	private static final double CONTROL_MULTIPLIER = 0.1;
	private static final double SHIFT_MULTIPLIER = 10.0;
	private static final double MOUSE_SPEED = 0.1;
	private static final double ROTATION_SPEED = 2.0;
	private static final double TRACK_SPEED = 0.3;

	final Xform lightXform = new Xform();
	final Xform light1Xform = new Xform();
	final Xform light2Xform = new Xform();
	final Xform light3Xform = new Xform();
	final AmbientLight ambientLight = new AmbientLight(Color.DARKGREY);
	final PointLight light1 = new PointLight(Color.WHITE);
	final PointLight light2 = new PointLight(Color.ANTIQUEWHITE);
	final PointLight light3 = new PointLight(Color.ALICEBLUE);
	
	private SimpleBooleanProperty ambientLightEnabled = new SimpleBooleanProperty(false){
		@Override protected void invalidated() {
			if (get()) {
				world.getChildren().add(ambientLight);
			} else {
				world.getChildren().remove(ambientLight);
			}
		}
	};
	private SimpleBooleanProperty light1Enabled = new SimpleBooleanProperty(false){
		@Override protected void invalidated() {
			if (get()) {
				world.getChildren().add(light1);
			} else {
				world.getChildren().remove(light1);
			}
		}
	};
	private SimpleBooleanProperty light2Enabled = new SimpleBooleanProperty(false){
		@Override protected void invalidated() {
			if (get()) {
				world.getChildren().add(light2);
			} else {
				world.getChildren().remove(light2);
			}
		}
	};
	private SimpleBooleanProperty light3Enabled = new SimpleBooleanProperty(false){
		@Override protected void invalidated() {
			if (get()) {
				world.getChildren().add(light3);
			} else {
				world.getChildren().remove(light3);
			}
		}
	};
	
	double mousePosX;
	double mousePosY;
	double mouseOldX;
	double mouseOldY;
	double mouseDeltaX;
	double mouseDeltaY;	

	public MyTestJavaFXPane2(Composite parent, int style) {

		super(parent, style);

		this.setLayout(new FillLayout());
		_fxCanvas = new FXCanvas(this, SWT.NONE);

		_parent = 
				// createScene()
				createSceneParent()
				;

		Scene scene = new Scene(_parent, 1024, 768, true);
		scene.setFill(Color.CORNFLOWERBLUE);

		// listeners here
		handleMouse(scene,world);
		handleKeyboard(scene, world);

		// associate camera to scene
		scene.setCamera(camera);

		// finally, attach the scene to the JavaFX-to-SWT bridging object
		_fxCanvas.setScene(scene);

	}

	// create a scene programmatically
	private Parent createSceneParent() {

		// the container
		VBox vBox = new VBox();
		vBox.setAlignment(Pos.TOP_LEFT);

		// the 3D scene

		System.out.println("JavaFX Test :: createSceneParent()");

		root.getChildren().add(world);
		root.setDepthTest(DepthTest.ENABLE);
		buildCamera();
		buildLight();
		buildAxes();
		//buildMolecule();
		// TODO: replace this buildMolecule() with the production drawing
		//       function, e.g. drawAircraft()

		buildTestObjects();
		
		vBox.getChildren().add(root);

		return vBox;
	}

	private void buildLight() {
		System.out.println("buildLight()");
		root.getChildren().add(lightXform);
		root.getChildren().add(light1Xform);
		root.getChildren().add(light2Xform);
		root.getChildren().add(light3Xform);
		
		lightXform.getChildren().add(ambientLight);
		light1Xform.getChildren().add(light1);
		light2Xform.getChildren().add(light2);
		light3Xform.getChildren().add(light3);
		
		light1Xform.setTranslate( 350d, -300d, -600d);
		light2Xform.setTranslate(-350d,  300d,  7600d);
		light3Xform.setTranslate(   0d,    0d, 1700d);
	}
	
	private void buildCamera() {
		System.out.println("buildCamera()");
		root.getChildren().add(cameraXform);
		cameraXform.getChildren().add(cameraXform2);
		cameraXform2.getChildren().add(cameraXform3);
		cameraXform3.getChildren().add(camera);
		cameraXform3.setRotateZ(180.0);

		camera.setNearClip(CAMERA_NEAR_CLIP);
		camera.setFarClip(CAMERA_FAR_CLIP);
		camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
		cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
		cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
	}

	private void buildAxes() {
		System.out.println("buildAxes()");
		final PhongMaterial redMaterial = new PhongMaterial();
		redMaterial.setDiffuseColor(Color.DARKRED);
		redMaterial.setSpecularColor(Color.RED);

		final PhongMaterial greenMaterial = new PhongMaterial();
		greenMaterial.setDiffuseColor(Color.DARKGREEN);
		greenMaterial.setSpecularColor(Color.GREEN);

		final PhongMaterial blueMaterial = new PhongMaterial();
		blueMaterial.setDiffuseColor(Color.DARKBLUE);
		blueMaterial.setSpecularColor(Color.BLUE);

		final Box xAxis = new Box(AXIS_LENGTH, 1, 1);
		final Box yAxis = new Box(1, AXIS_LENGTH, 1);
		final Box zAxis = new Box(1, 1, AXIS_LENGTH);

		xAxis.setMaterial(redMaterial);
		yAxis.setMaterial(greenMaterial);
		zAxis.setMaterial(blueMaterial);

		axisGroup.getChildren().addAll(xAxis, yAxis, zAxis);
//		axisGroup.setVisible(false);
		axisGroup.setVisible(true);
		world.getChildren().addAll(axisGroup);
	}

	private void handleMouse(Scene scene, final Node root) {
		scene.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent me) {
				mousePosX = me.getSceneX();
				mousePosY = me.getSceneY();
				mouseOldX = me.getSceneX();
				mouseOldY = me.getSceneY();
			}
		});
		scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent me) {
				mouseOldX = mousePosX;
				mouseOldY = mousePosY;
				mousePosX = me.getSceneX();
				mousePosY = me.getSceneY();
				mouseDeltaX = (mousePosX - mouseOldX); 
				mouseDeltaY = (mousePosY - mouseOldY); 

				double modifier = 1.0;

				if (me.isControlDown()) {
					modifier = CONTROL_MULTIPLIER;
				} 
				if (me.isShiftDown()) {
					modifier = SHIFT_MULTIPLIER;
				}     
				if (me.isPrimaryButtonDown()) {
					cameraXform.ry.setAngle(cameraXform.ry.getAngle() - mouseDeltaX*MOUSE_SPEED*modifier*ROTATION_SPEED);  
					cameraXform.rx.setAngle(cameraXform.rx.getAngle() + mouseDeltaY*MOUSE_SPEED*modifier*ROTATION_SPEED);  
				}
				else if (me.isSecondaryButtonDown()) {
					double z = camera.getTranslateZ();
//					double newZ = z + mouseDeltaX*MOUSE_SPEED*modifier;
					double newZ = z + mouseDeltaY*MOUSE_SPEED*modifier;
					camera.setTranslateZ(newZ);
				}
				else if (me.isMiddleButtonDown()) {
					cameraXform2.t.setX(cameraXform2.t.getX() + mouseDeltaX*MOUSE_SPEED*modifier*TRACK_SPEED);  
					cameraXform2.t.setY(cameraXform2.t.getY() + mouseDeltaY*MOUSE_SPEED*modifier*TRACK_SPEED);  
				}
			}
		});
	}

	private void handleKeyboard(Scene scene, final Node root) {
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				switch (event.getCode()) {
				case Z:
					cameraXform2.t.setX(0.0);
					cameraXform2.t.setY(0.0);
					camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
					cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
					cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
					break;
				case X:
					axisGroup.setVisible(!axisGroup.isVisible());
					break;
				case V:
					moleculeGroup.setVisible(!moleculeGroup.isVisible());
					break;
				case T:
					testGroup.setVisible(!testGroup.isVisible());
					break;
				}
			}
		});
	}

	private void buildMolecule() {
		//======================================================================
		// THIS IS THE IMPORTANT MATERIAL FOR THE TUTORIAL
		//======================================================================

		final PhongMaterial redMaterial = new PhongMaterial();
		redMaterial.setDiffuseColor(Color.DARKRED);
		redMaterial.setSpecularColor(Color.RED);

		final PhongMaterial whiteMaterial = new PhongMaterial();
		whiteMaterial.setDiffuseColor(Color.WHITE);
		whiteMaterial.setSpecularColor(Color.LIGHTBLUE);

		final PhongMaterial greyMaterial = new PhongMaterial();
		greyMaterial.setDiffuseColor(Color.DARKGREY);
		greyMaterial.setSpecularColor(Color.GREY);

		// Molecule Hierarchy
		// [*] moleculeXform
		//     [*] oxygenXform
		//         [*] oxygenSphere
		//     [*] hydrogen1SideXform
		//         [*] hydrogen1Xform
		//             [*] hydrogen1Sphere
		//         [*] bond1Cylinder
		//     [*] hydrogen2SideXform
		//         [*] hydrogen2Xform
		//             [*] hydrogen2Sphere
		//         [*] bond2Cylinder
		Xform moleculeXform = new Xform();
		Xform oxygenXform = new Xform();
		Xform hydrogen1SideXform = new Xform();
		Xform hydrogen1Xform = new Xform();
		Xform hydrogen2SideXform = new Xform();
		Xform hydrogen2Xform = new Xform();

		Sphere oxygenSphere = new Sphere(40.0);
		oxygenSphere.setMaterial(redMaterial);

		Sphere hydrogen1Sphere = new Sphere(30.0);
		hydrogen1Sphere.setMaterial(whiteMaterial);
		hydrogen1Sphere.setTranslateX(0.0);

		Sphere hydrogen2Sphere = new Sphere(30.0);
		hydrogen2Sphere.setMaterial(whiteMaterial);
		hydrogen2Sphere.setTranslateZ(0.0);

		Cylinder bond1Cylinder = new Cylinder(5, 100);
		bond1Cylinder.setMaterial(greyMaterial);
		bond1Cylinder.setTranslateX(50.0);
		bond1Cylinder.setRotationAxis(Rotate.Z_AXIS);
		bond1Cylinder.setRotate(90.0);

		Cylinder bond2Cylinder = new Cylinder(5, 100);
		bond2Cylinder.setMaterial(greyMaterial);
		bond2Cylinder.setTranslateX(50.0);
		bond2Cylinder.setRotationAxis(Rotate.Z_AXIS);
		bond2Cylinder.setRotate(90.0);

		moleculeXform.getChildren().add(oxygenXform);
		moleculeXform.getChildren().add(hydrogen1SideXform);
		moleculeXform.getChildren().add(hydrogen2SideXform);
		oxygenXform.getChildren().add(oxygenSphere);
		hydrogen1SideXform.getChildren().add(hydrogen1Xform);
		hydrogen2SideXform.getChildren().add(hydrogen2Xform);
		hydrogen1Xform.getChildren().add(hydrogen1Sphere);
		hydrogen2Xform.getChildren().add(hydrogen2Sphere);
		hydrogen1SideXform.getChildren().add(bond1Cylinder);
		hydrogen2SideXform.getChildren().add(bond2Cylinder);

		hydrogen1Xform.setTx(100.0);
		hydrogen2Xform.setTx(100.0);
		hydrogen2SideXform.setRotateY(HYDROGEN_ANGLE);

		moleculeGroup.getChildren().add(moleculeXform);
		
		world.getChildren().addAll(moleculeGroup);
		
		// TODO: experimental code here, e.g. lines etc
		//       to be added to moleculeGroup 
		
		
		// FXyz add-on
		
		// https://github.com/Birdasaur/FXyz/blob/master/src/org/fxyz/tests/PolyLine3DTest.java
		
//		List<org.fxyz.geometry.Point3D> points = new ArrayList<org.fxyz.geometry.Point3D>();
//		points.add(
//			new org.fxyz.geometry.Point3D(
//				10.0f, 10.0f, 10.0f
//			));
//		points.add(
//				new org.fxyz.geometry.Point3D(
//					150.0f, 50.0f, 0.0f
//				));
//		points.add(
//				new org.fxyz.geometry.Point3D(
//					150.0f, 250.0f, 0.0f
//				));
//		points.add(
//				new org.fxyz.geometry.Point3D(
//					350.0f, 350.0f, 200.0f
//				));
		
		List<MyPoint3D> points = new ArrayList<MyPoint3D>();
		for(int i=-50;i<50;i++) {
			points.add(new MyPoint3D(
			(float) i,
			(float) Math.sin(i)*50+i,
			(float) Math.cos(i)*50+i));
			}
		
		MyPolyLine3D polyline = new MyPolyLine3D(points, 2, Color.DARKBLUE);
		// Xform polylineXform = new Xform();
		// polylineXform.getChildren().add(polyline);
		// moleculeXform.getChildren().add(polylineXform);

		world.getChildren().add(polyline);

//		MyTorusMesh polyline1 = new MyTorusMesh(points, 10, 5); 
//		world.getChildren().add(polyline1);
		
		MyCylinder c1 = new MyCylinder(
				new MyPoint3D(0,0,0), 
				new MyPoint3D(170,220,0), 
				50);
		world.getChildren().addAll(c1);
		
		
		//////////////////////////////////////////////////

		// Test reading .stl
		StlMeshImporter stlImporter = new StlMeshImporter();
		try {
			
			System.out.println("Loading STL file");
		    
			URL modelUrl = this.getClass().getResource("test03d_fuselage.stl");
			stlImporter.read(modelUrl);

//			String filePath = MyWriteUtils._testDirectory +
//					"cad" + File.separator + "test03d_fuselage.stl"; 
//			System.out.println("Reading: " + filePath);
//			URL modelUrl = new URL(filePath);
//		    stlImporter.read(modelUrl);
			
			TriangleMesh stlMesh = stlImporter.getImport();

			System.out.println(
					"STL TriangleMesh: \n"
					+ "points: " + stlMesh.getPointElementSize() + "\n"
					+ "faces: " + stlMesh.getFaceElementSize()
			);
			
			MeshView stlMeshView = new MeshView(stlMesh);
			
			stlMeshView.scaleXProperty().set(10.0d);
			stlMeshView.scaleYProperty().set(10.0d);
			stlMeshView.scaleZProperty().set(10.0d);
			
			world.getChildren().add(stlMeshView);
			
		}
		catch (ImportException e) { //  | MalformedURLException e
		    // handle exception
		}
		
	}

	private void buildTestObjects() {
		
		final PhongMaterial redMaterial = new PhongMaterial();
		redMaterial.setDiffuseColor(Color.RED);
		redMaterial.setSpecularColor(Color.YELLOW);

		final PhongMaterial greenMaterial = new PhongMaterial();
		greenMaterial.setDiffuseColor(Color.DARKGREEN);
		greenMaterial.setSpecularColor(Color.YELLOWGREEN);
		
		final PhongMaterial yellowMaterial = new PhongMaterial();
		yellowMaterial.setDiffuseColor(Color.YELLOW);
		yellowMaterial.setSpecularColor(Color.WHITE);

		final PhongMaterial cyanMaterial = new PhongMaterial();
		cyanMaterial.setDiffuseColor(Color.CYAN);
		cyanMaterial.setSpecularColor(Color.ANTIQUEWHITE);

		Xform testXform1 = new Xform();
		MyTorusMesh torus = new MyTorusMesh(72, 36, 100d, 10d);
		torus.setMaterial(greenMaterial);
		testXform1.getChildren().add(torus);
		testGroup.getChildren().add(testXform1);

		Xform testXform2 = new Xform();
		MySimpleTriangle tr1 = new MySimpleTriangle(
				new Point3D(  0,  0,  0), 
				new Point3D( 50,  0,  0), 
				new Point3D(  0, 50,  0),
				true // double-face
				);
		tr1.setMaterial(yellowMaterial);
		testXform2.getChildren().add(tr1);

		MySimpleTriangle tr2 = new MySimpleTriangle(
				new Point3D(  0, 50,  0), 
				new Point3D( 50, 50,  0), 
				new Point3D( 50,  0,  0),
				true // double-face
				);
		tr2.setMaterial(greenMaterial);
		testXform2.getChildren().add(tr2);
		testGroup.getChildren().add(testXform2);
		
		Xform testXform3 = new Xform();
		MySimpleQuad qd1 = new MySimpleQuad(
				new Point3D(   0,   0,   0), // p0
				new Point3D(   0,   0, 100), // p1
				new Point3D(   0,  50, 100), // p2
				new Point3D( -10,  50,   0), // p3
				true // double-face				
				);
		//qd1.setDrawMode(DrawMode.LINE);
		qd1.setMaterial(cyanMaterial);
		testXform3.getChildren().add(qd1);
		testGroup.getChildren().add(testXform3);
		
		Xform testXform4 = new Xform();
		MyArray angleArray = new MyArray(SI.RADIAN);
		Amount<Angle> angleStart = Amount.valueOf(toRadians(-180.), SI.RADIAN);
		Amount<Angle> angleEnd   = Amount.valueOf(toRadians( 180.), SI.RADIAN);
		int numberOfAngles = 70;
		angleArray.setDouble(
			Matrix.linspace(
				angleStart.getEstimatedValue(), 
				angleEnd.getEstimatedValue(), 
				numberOfAngles)
				.data
		);
		List<Point3D> points = new ArrayList<Point3D>();
		for(int i = 0; i < numberOfAngles; i++) {
			float x = (float) angleArray.get(i) * 10.0f;
			float y = (float) Math.sin( angleArray.get(i) ) * 50.0f;
			float z = -10.0f;
			points.add(new Point3D(x,y,z));
		}
		MyCurveExtrusion ce1 = new MyCurveExtrusion(
				points, 
				new Point3D(0,0,0), new Point3D(0,0,40), // pi & p2 >> dp = p2 - p1 
				true // doubleFace
				);
		ce1.getMeshView().setMaterial(redMaterial);
		testXform4.getChildren().add(ce1);
		testGroup.getChildren().add(testXform4);

		// see https://github.com/Birdasaur/FXyz
		Xform testXform5 = new Xform();
		
		Rotate rotateY = new Rotate(0, 0, 0, 0, Rotate.Y_AXIS);
		
		// the control points
		List<org.fxyz.geometry.Point3D> knots = Arrays.asList(
				new org.fxyz.geometry.Point3D(30f,0f,0f),
				new org.fxyz.geometry.Point3D(120,50,10),
				new org.fxyz.geometry.Point3D(200,50,-50),
				new org.fxyz.geometry.Point3D(250,70,-20),
				new org.fxyz.geometry.Point3D(300,50,30)
				);
		
		boolean showControlPoints=true;
		boolean showKnots=true;
		
		// the interpolating bezier
		InterpolateBezier interpolate = new InterpolateBezier(knots);

		ArrayList<BezierMesh> beziers=new ArrayList<>();
		AtomicInteger sp=new AtomicInteger();
		if(showKnots || showControlPoints){
			interpolate.getSplines().forEach(spline->{
				org.fxyz.geometry.Point3D k0=spline.getPoints().get(0);
				org.fxyz.geometry.Point3D k1=spline.getPoints().get(1);
				org.fxyz.geometry.Point3D k2=spline.getPoints().get(2);
				org.fxyz.geometry.Point3D k3=spline.getPoints().get(3);
				if(showKnots){
					Sphere s=new Sphere(0.4d);
					s.getTransforms().add(new Translate(k0.x, k0.y, k0.z));
					s.setMaterial(new PhongMaterial(Color.GREENYELLOW));
					testXform5.getChildren().add(s);
					s=new Sphere(0.4d);
					s.getTransforms().add(new Translate(k3.x, k3.y, k3.z));
					s.setMaterial(new PhongMaterial(Color.GREENYELLOW));
					testXform5.getChildren().add(s);
				}
				if(showControlPoints){
					org.fxyz.geometry.Point3D dir=k1.substract(k0).crossProduct(new org.fxyz.geometry.Point3D(0,-1,0));
					double angle=Math.acos(k1.substract(k0).normalize().dotProduct(new org.fxyz.geometry.Point3D(0,-1,0)));
					double h1=k1.substract(k0).magnitude();
					Cylinder c=new Cylinder(0.03d,h1);
					c.getTransforms().addAll(new Translate(k0.x, k0.y-h1/2d, k0.z),
							new Rotate(-Math.toDegrees(angle), 0d,h1/2d,0d,
									new javafx.geometry.Point3D(dir.x,-dir.y,dir.z)));
					c.setMaterial(new PhongMaterial(Color.GREEN));
					testXform5.getChildren().add(c);

					dir=k2.substract(k1).crossProduct(new org.fxyz.geometry.Point3D(0,-1,0));
					angle=Math.acos(k2.substract(k1).normalize().dotProduct(new org.fxyz.geometry.Point3D(0,-1,0)));
					h1=k2.substract(k1).magnitude();
					c=new Cylinder(0.03d,h1);
					c.getTransforms().addAll(new Translate(k1.x, k1.y-h1/2d, k1.z),
							new Rotate(-Math.toDegrees(angle), 0d,h1/2d,0d,
									new javafx.geometry.Point3D(dir.x,-dir.y,dir.z)));
					c.setMaterial(new PhongMaterial(Color.GREEN));
					testXform5.getChildren().add(c);

					dir=k3.substract(k2).crossProduct(new org.fxyz.geometry.Point3D(0,-1,0));
					angle=Math.acos(k3.substract(k2).normalize().dotProduct(new org.fxyz.geometry.Point3D(0,-1,0)));
					h1=k3.substract(k2).magnitude();
					c=new Cylinder(0.03d,h1);
					c.getTransforms().addAll(new Translate(k2.x, k2.y-h1/2d, k2.z),
							new Rotate(-Math.toDegrees(angle), 0d,h1/2d,0d,
									new javafx.geometry.Point3D(dir.x,-dir.y,dir.z)));
					c.setMaterial(new PhongMaterial(Color.GREEN));
					testXform5.getChildren().add(c);

					Sphere s=new Sphere(0.1d);
					s.getTransforms().add(new Translate(k1.x, k1.y, k1.z));
					s.setMaterial(new PhongMaterial(Color.RED));
					testXform5.getChildren().add(s);
					s=new Sphere(0.1d);
					s.getTransforms().add(new Translate(k2.x, k2.y, k2.z));
					s.setMaterial(new PhongMaterial(Color.RED));
					testXform5.getChildren().add(s);
				}
			});
		}
		long time=System.currentTimeMillis();
		interpolate.getSplines().stream().forEach(spline->{
			BezierMesh bezier = new BezierMesh(spline,0.1d,
					300,20,0,0);
			//            bezier.setDrawMode(DrawMode.LINE);
			bezier.setCullFace(CullFace.NONE);
			//          bezier.setSectionType(SectionType.TRIANGLE);

			// NONE
			//          bezier.setTextureModeNone(Color.hsb(360d*sp.getAndIncrement()/interpolate.getSplines().size(), 1, 1));
			// IMAGE
			//          bezier.setTextureModeImage(getClass().getResource("res/LaminateSteel.jpg").toExternalForm());
			// PATTERN
			//         bezier.setTextureModePattern(3d);
			// FUNCTION
			//          bezier.setTextureModeVertices1D(256*256,t->spline.getKappa(t));
			// DENSITY
			//          bezier.setTextureModeVertices3D(256*256,dens);
			// FACES
			bezier.setTextureModeFaces(256*256);

			bezier.getTransforms().addAll(new Rotate(0,Rotate.X_AXIS),rotateY);
			beziers.add(bezier);
		});
		System.out.println("time: "+(System.currentTimeMillis()-time)); //43.815->25.606->15
		testXform5.getChildren().addAll(beziers);
		testGroup.getChildren().add(testXform5);
		
		world.getChildren().addAll(testGroup);
		
	}
	
	
	// create a scene programmatically
	// a first successful test on JavaFX layout 
	private Parent createScene() {

		VBox vBox = new VBox(10); // spacing
		vBox.setPadding(new Insets(10, 10, 10, 10));
		vBox.setAlignment(Pos.TOP_LEFT);

		/* Create a JavaFX button */
		Button jfxButton = new Button("JFX Button");

		/* Assign the CSS ID ipad-dark-grey */
		jfxButton.setId(
				"ipad-dark-grey"
				// "shiny-orange"
				// "big-yellow"
				);

		ToolBar toolBar = new ToolBar(
				new Button("New"),
				new Button("Open"),
				new Button("Save"),
				new Separator(),
				new Button("Clean"),
				new Button("Compile"),
				new Button("Run"),
				new Separator(),
				new Button("Debug"),
				new Button("Profile"),
				jfxButton
				);


		FlowPane flowPane = new FlowPane();
		flowPane.setPadding(new Insets(1));
		flowPane.setHgap(10);
		flowPane.setVgap(10);
		for (int i = 0; i < 10; i++) {
			Rectangle r = new Rectangle();
			r.setY(i * 20);
			r.setWidth(200);
			r.setHeight(40);
			r.setFill(Color.RED);
			flowPane.getChildren().add(r);
		}

		Text text = new Text();
		text.setText("The quick brown fox jumps over the lazy dog� (in italiano La rapida volpe bruna salta oltre il pigro cane) � un pangramma in lingua inglese, ovvero una frase di senso compiuto nella quale vengono usate tutte le lettere dell'alfabeto.");
		text.setTextAlignment(TextAlignment.LEFT);
		text.setWrappingWidth(300);
		text.setFill(Color.GREEN);

		flowPane.getChildren().add(text);

		vBox.getChildren().addAll(
				//jfxButton,
				toolBar,
				flowPane
				);

		return vBox;
	}


}
