package it.unina.adopt.GUI.test.javafx;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.jfree.fx.FXGraphics2D;
import org.scilab.forge.jlatexmath.Box;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.embed.swt.FXCanvas;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.geometry.Side;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import jmatrix.Matrix;

public class TestFXLineChartPane extends Composite {

	private FXCanvas _fxCanvas;
	private LineChart _lineChart; // LineChart<Integer, Double> 
	private ImageView _imageView;
	private Scene _scene;

	// Constructor
	@SuppressWarnings("rawtypes")
	public TestFXLineChartPane(Composite parent, int style) {

		super(parent, style);

		this.setLayout(new FillLayout());
		_fxCanvas = new FXCanvas(this, SWT.NONE);

		// System.out.println("TestFXLineChartPane >>>>>> _fxCanvas null? " + (_fxCanvas == null));
		
		// TODO: implement this
		// http://java-buddy.blogspot.it/2013/03/javafx-interaction-between-table-and.html
		// http://stackoverflow.com/questions/14615590/javafx-linechart-hover-values
		// https://gist.github.com/jewelsea/4681797

		final Rectangle2D screenRect = Screen.getPrimary().getBounds();
		final double screenWidth = screenRect.getWidth();
		final double screenHeight = screenRect.getHeight();

		final double size = Math.min(screenWidth*0.8, screenHeight*0.8);
		System.out.println("TestFXLineChartPane >>>>>> size: " + size);

		final Group rootGroup = new Group();

		// the axes
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();

		// the chart object
		_lineChart = new LineChart(xAxis, yAxis);

		// produce data with a custom function
		ObservableList<XYChart.Series<Double, Double>> chartData = getChartData();
		
		// assign data to the chart object
		_lineChart.setData(chartData);

		// Extracting min, max on data set 
		Double minX = chartData.get(0).getData().stream()
			.map(data -> data.getXValue())
			.min(Comparator.comparing(num -> num)).get();

		Double maxX = chartData.get(0).getData().stream()
				.map(data -> data.getXValue())
				.max(Comparator.comparing(num -> num)).get();
		
		System.out.println("TestFXLineChartPane >>>>>> min X: " + minX);
		System.out.println("TestFXLineChartPane >>>>>> max X: " + maxX);
		
		// axis settings
		xAxis.setAutoRanging(false);
		xAxis.setLowerBound(minX);
		xAxis.setUpperBound(maxX);
		xAxis.setTickUnit(0.5);

		// title
		_lineChart.setTitle("Example of curve (Double -> Double)");
		_lineChart.setLegendSide(Side.RIGHT);

		// TODO: try FXGraphics2D
		javafx.scene.text.Font.loadFont(TeXFormula.class.getResourceAsStream("/org/scilab/forge/jlatexmath/fonts/base/jlm_cmmi10.ttf"), 1);
		javafx.scene.text.Font.loadFont(TeXFormula.class.getResourceAsStream("/org/scilab/forge/jlatexmath/fonts/maths/jlm_cmsy10.ttf"), 1);
		javafx.scene.text.Font.loadFont(TeXFormula.class.getResourceAsStream("/org/scilab/forge/jlatexmath/fonts/latin/jlm_cmr10.ttf"), 1);
		MyCanvas canvas = new MyCanvas(); // contains the LaTeX formula


		//--------------------------------------------------------------------------------
		// Experiment with ImageView in LineChart

		TeXFormula formula = new TeXFormula("\\sqrt {x}");
		TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
		java.awt.Image awtImage = 
				formula
				.createBufferedImage(
						TeXConstants.STYLE_DISPLAY, 20.0f, java.awt.Color.BLACK, java.awt.Color.WHITE
				);
		// ImageData swtImageData = MyGuiUtils.convertToSWT((BufferedImage) awtImage);
		
		Image image = SwingFXUtils.toFXImage((BufferedImage)awtImage, null);
		_imageView = new ImageView(image);
		
		//--------------------------------------------------------------------------------
		
		// a container for the chart object
		VBox vBox = new VBox();
        vBox.getChildren().add(_lineChart);
        VBox.setVgrow(_lineChart, Priority.ALWAYS);

        StackPane stackPane = new StackPane(); 
        stackPane.getChildren().add(vBox);
        stackPane.getChildren().add(canvas);
        //stackPane.getChildren().add(imageGroup);
        
        
        // Bind canvas size to stack pane size. 
        canvas.widthProperty().bind( stackPane.widthProperty()); 
        canvas.heightProperty().bind( stackPane.heightProperty());  
		

        // add the main container to the root node
        //rootGroup.getChildren().add(vBox);
        rootGroup.getChildren().add(stackPane);
        
        rootGroup.getChildren().add(_imageView); // on the root node
        
        // create the scene and add the root node to the scene
		_scene = new Scene(rootGroup, size, size, true);
		
		
		// initial positioning
		applyMiscPositioning();
		applyListeners();
		
		// ********* LISTENERS
		
		// a listener that resizes the chart inside the scene as scene is resized
		final ChangeListener sceneBoundsListener = new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldXY, Object newXY) {
				
				applyMiscPositioning();

				//--------------------------------------
				// TODO: experiments with image positioning
				
				Bounds boundsChartInScene = _lineChart.getBoundsInParent(); // localToScene(_lineChart.getBoundsInLocal());
		        System.out.println("-----------------------------------------------------------------");
		        System.out.println(">>>>>>> TestFXLineChartPane");
		        System.out.println("boundsChartInScene width  = " + boundsChartInScene.getWidth());
		        System.out.println("boundsChartInScene heigth = " + boundsChartInScene.getHeight());
		        System.out.println("boundsChartInScene X min = " + boundsChartInScene.getMinX());
		        System.out.println("boundsChartInScene X max = " + boundsChartInScene.getMaxX());
		        System.out.println("boundsChartInScene Y min = " + boundsChartInScene.getMinY());
		        System.out.println("boundsChartInScene Y max = " + boundsChartInScene.getMaxY());
		        System.out.println("-----------------------------------------------------------------");

				
			}
		};        
		_scene.widthProperty().addListener(sceneBoundsListener);
		_scene.heightProperty().addListener(sceneBoundsListener);
		
		// finally, attach the scene to the JavaFX-to-SWT bridging object
		_fxCanvas.setScene(_scene);
		
//		this.layout(true);
//		this.redraw();
//		this.update();
	}
	
	private void applyMiscPositioning() {
		_lineChart.setPrefHeight(
				_scene.getHeight()*0.9
				// this.getBounds().height*0.9
				);
		_lineChart.setPrefWidth(
				_scene.getWidth()*0.9
				// this.getBounds().width*0.9
				);
		Bounds boundsChartInScene = _lineChart.getBoundsInParent(); // localToScene(_lineChart.getBoundsInLocal());
		_imageView.setX(0.5*boundsChartInScene.getMaxX());
		_imageView.setY(0.99*boundsChartInScene.getMaxY());
	}
	
	// TODO: check resize policies and _scene resize
	private void applyListeners() {
		// resize listener
		this.addListener(SWT.Resize, 
			event -> { //  <================= LAMBDA

				org.eclipse.swt.graphics.Rectangle rect = 
						// this.getBounds();
						this.getClientArea();

				double w = rect.width;
				double h = rect.height;

				System.out.println("##########################");
				System.out.println(String.format("w = %s, h = %s", w, h));
				System.out.println("##########################");

				applyMiscPositioning();
				
			} // end-of-event-handler
		);
		
	}
	
	private ObservableList<XYChart.Series<Double, Double>> getChartData() {
		
		double value0 = 17.56;
		double value1 = 17.06;
		double value2 = 8.25;

		ObservableList<XYChart.Series<Double, Double>> result = FXCollections.observableArrayList();
		Series<Double, Double> series0 = new Series<>();
		series0.setName("Curve 0");
		Series<Double, Double> series1 = new Series<>();
		series1.setName("Curve 1");
		Series<Double, Double> series2 = new Series<>();
		series2.setName("Curve 2");
		
		// generate x-values
		List<Double> xList = Arrays.asList(ArrayUtils.toObject(
				Matrix.linspace(
						0.0, 2.5, 
						25
						).data
				));
		// Generate Data in objects of type XYChart and populate series
		for (Double x : xList) {
			series0.getData().add(new XYChart.Data(x, value0));
			value0 = value0 + 4 * Math.random() - 2;
			series1.getData().add(new XYChart.Data(x, value1));
			value1 = value1 + Math.random() - .5;
			series2.getData().add(new XYChart.Data(x, value2));
			value2 = value2 + 4 * Math.random() - 2;
		}
		// populate result
		result.addAll(series0, series1, series2);
		// and return
		return result;
	}

	
	
	// Code from
	// https://gist.github.com/jewelsea/4681797
	
	/** @return plotted y values for monotonically increasing integer x values, starting from x=1 */
	public ObservableList<XYChart.Data<Integer, Integer>> plot(int... y) {
		final ObservableList<XYChart.Data<Integer, Integer>> dataset = FXCollections.observableArrayList();
		int i = 0;
		while (i < y.length) {
			final XYChart.Data<Integer, Integer> data = new XYChart.Data<>(i + 1, y[i]);
			data.setNode(
					new HoveredThresholdNode(
							(i == 0) ? 0 : y[i-1],
									y[i]
							)
					);

			dataset.add(data);
			i++;
		}

		return dataset;
	}

	/** a node which displays a value on hover, but is otherwise empty */
	class HoveredThresholdNode extends StackPane {
		HoveredThresholdNode(int priorValue, int value) {
			setPrefSize(15, 15);

			final Label label = createDataThresholdLabel(priorValue, value);

			setOnMouseEntered(new EventHandler<MouseEvent>() {
				@Override public void handle(MouseEvent mouseEvent) {
					getChildren().setAll(label);
					setCursor(Cursor.NONE);
					toFront();
				}
			});
			setOnMouseExited(new EventHandler<MouseEvent>() {
				@Override public void handle(MouseEvent mouseEvent) {
					getChildren().clear();
					setCursor(Cursor.CROSSHAIR);
				}
			});
		}

		private Label createDataThresholdLabel(int priorValue, int value) {
			final Label label = new Label(value + "");
			label.getStyleClass().addAll("default-color0", "chart-line-symbol", "chart-series-line");
			label.setStyle("-fx-font-size: 20; -fx-font-weight: bold;");

			if (priorValue == 0) {
				label.setTextFill(Color.DARKGRAY);
			} else if (value > priorValue) {
				label.setTextFill(Color.FORESTGREEN);
			} else {
				label.setTextFill(Color.FIREBRICK);
			}

			label.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
			return label;
		}
	}
	
	// TODO: code from FXGraphics2D
	// http://stackoverflow.com/questions/25027060/running-swing-application-in-javafx/25037747#25037747

	static class MyCanvas extends Canvas { 

		private FXGraphics2D g2;

		private Box box;

		public MyCanvas() {
			this.g2 = new FXGraphics2D(getGraphicsContext2D());
			this.g2.scale(20, 20);

			// create a formula
			TeXFormula formula = new TeXFormula("x=\\frac{-b \\pm \\sqrt {b^2-4ac}}{2a}");
			TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);

			// the 'Box' seems to be the thing we can draw directly to Graphics2D
			this.box = icon.getBox();

			// Redraw canvas when size changes. 
			widthProperty().addListener(evt -> draw()); 
			heightProperty().addListener(evt -> draw()); 
		}  

		private void draw() { 
			double width = getWidth(); 
			double height = getHeight();
			getGraphicsContext2D().clearRect(0, 0, width, height);
			this.box.draw(g2, 1, 5);
		} 

		@Override 
		public boolean isResizable() { 
			return true;
		}  

		@Override 
		public double prefWidth(double height) { return getWidth(); }  

		@Override 
		public double prefHeight(double width) { return getHeight(); } 
	} 


}
