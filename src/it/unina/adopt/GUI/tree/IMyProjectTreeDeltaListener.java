package it.unina.adopt.GUI.tree;


public interface IMyProjectTreeDeltaListener {
	public void add(MyProjectTreeDeltaEvent event);
	public void remove(MyProjectTreeDeltaEvent event);
}
