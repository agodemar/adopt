package it.unina.adopt.GUI.tree;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import aircraft.calculators.ACAnalysisManager;
import aircraft.componentmodel.Component;
import aircraft.components.Aircraft;

/** 
 * Manage the contents that have to be put in the tree
 */
public class MyProjectContentProvider implements ITreeContentProvider, IMyProjectTreeDeltaListener {
	
	private static final Object[] _EMPTY_OBJECT_ARRAY = new Object[0];
	private MyProjectTree _theProjectTree;
	private Aircraft _theAircraft;
	private TreeViewer _theViewer;
	private ACAnalysisManager _theAnalysis;  

	public MyProjectContentProvider(TreeViewer viewer, MyProjectTree projectTree) {
		_theViewer = viewer;
		_theProjectTree = projectTree;
		_theAircraft = _theProjectTree.getAircraft();
		_theAnalysis = _theProjectTree.getAnalysis();
	}

	@Override
	public void dispose() {

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this._theViewer = (TreeViewer)viewer;
		if(oldInput != null) {
			removeListenerFrom((MyProjectTree) oldInput);
		}
		if(newInput != null) {
			addListenerTo((MyProjectTree) newInput);
		}		
	}

	@Override
	public Object[] getChildren(Object parent) {
		
		if (parent instanceof MyProjectTree) {
			MyProjectTree pt = (MyProjectTree)parent;
			return pt.getNodes().toArray();

		} else if (parent instanceof Aircraft) {
			System.out.println("getChildren :: MyAircraft");			
			return ((Aircraft)parent).get_componentsList().toArray();

		} else if (parent instanceof ACAnalysisManager) {
			
//			return MyUtilities.capitalizeFullyArray(
//					MyUtilities.enumToStringArray(MyAnalysisTypeEnum.values()));
			return ((ACAnalysisManager)parent).get_theCalculatorsList().toArray();
		}

		// TODO: manage a tree with more levels
		return _EMPTY_OBJECT_ARRAY;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		// we don't care about inputElement, for now
		// TODO: review logic in case of multiple objects in tree root

		return getChildren(inputElement);
	}

	@Override
	public Object getParent(Object element) {
		
		if (element instanceof Aircraft) {
			return (Object) _theAircraft;
		} else {
			return (Object) _theAnalysis;
		}
	}

	/** 
	 * Check if an element in the tree has children
	 */
	@Override
	public boolean hasChildren(Object element) {
		
		if (element instanceof MyProjectTree) {
			return getChildren(element).length > 0;
		
		} else if (element instanceof Aircraft) {
			return ((Aircraft)element).get_componentsList().size() > 0;
		
		} else if (element instanceof Component) {
			//			System.out.println("hasChildren :: MyComponent!");
			return false;
			// TODO: manage sublayers
		}

		else if (element instanceof ACAnalysisManager) {
//			return MyAnalysisTypeEnum.values().length > 0;
			return ((ACAnalysisManager)element).get_theCalculatorsList().size() > 0;
		
		} else {
			return false;
		}

	}

	// listeners

	/** 
	 * Because the domain model does not have a richer
	 * listener model, recursively add this listener
	 * to each child box of the given box. 
	 */
	protected void addListenerTo(MyProjectTree projectTree) {
		projectTree.addListener(this);
		// TODO: manage this code if you want more sublayers in tree
		//	    for (Iterator iterator = aircraft.get_componentsList().iterator(); iterator.hasNext();) {
		//	    	MyComponent comp = (MyComponent) iterator.next();
		//	       addListenerTo(comp);
		//	    }
	}

	/** 
	 * Because the domain model does not have a richer
	 * listener model, recursively remove this listener
	 * from each child box of the given box. 
	 */
	protected void removeListenerFrom(MyProjectTree projectTree) {
		projectTree.removeListener(this);
		// TODO: manage this code if you want more sublayers in tree
		//		for (Iterator iterator = box.getBoxes().iterator(); iterator.hasNext();) {
		//			MyComponent comp = (MyComponent) iterator.next();
		//			removeListenerFrom(comp);
		//		}
	}


	@Override
	public void add(MyProjectTreeDeltaEvent event) {
		//		Object movingBox = ((Model)event.receiver()).getParent();
		//		viewer.refresh(movingBox, false);		
		_theViewer.refresh(_theProjectTree, false);		
	}

	@Override
	public void remove(MyProjectTreeDeltaEvent event) {
		add(event);
	}

	protected Object[] concat(Object[] object, Object[] more) {
		Object[] both = new Object[object.length + more.length];
		System.arraycopy(object, 0, both, 0, object.length);
		System.arraycopy(more, 0, both, object.length, more.length);
		return both;
	}

	protected Object[] concat(Object[] object, Object[] more, Object[] more2) {
		Object[] both = new Object[object.length + more.length + more2.length];
		System.arraycopy(object, 0, both, 0, object.length);
		System.arraycopy(more, 0, both, object.length, more.length);
		System.arraycopy(more2, 0, both, object.length + more.length, more2.length);		
		return both;
	}

}

