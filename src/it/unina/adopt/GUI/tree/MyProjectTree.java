package it.unina.adopt.GUI.tree;

import java.util.ArrayList;
import java.util.List;

import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import it.unina.adopt.main.MyNullAircraftDeltaListener;

public class MyProjectTree {

	String _name;
	protected List<Object> _nodesProjectTree;
	protected MyProjectTree _parent;
	private Aircraft _theAircraft;
	private ACAnalysisManager _theAnalysis;

	protected IMyProjectTreeDeltaListener listener = MyNullAircraftDeltaListener.getSoleInstance();

	public MyProjectTree() {
		init("", null, null);
	}

	public MyProjectTree(String name) {
		init(name, null, null);
	}

	public MyProjectTree(Aircraft aircraft, ACAnalysisManager analysis) {
		init("", aircraft, analysis);
	}

	public MyProjectTree(String name, Aircraft aircraft, ACAnalysisManager analysis) {
		init(name, aircraft, analysis);
	}

	public void init(String name, Aircraft aircraft, ACAnalysisManager analysis) {
		_nodesProjectTree = new ArrayList<Object>();
		_theAircraft = aircraft;
		_theAnalysis = analysis;
		_name = name;
	}

	public void addListener(IMyProjectTreeDeltaListener listener) {
		this.listener = listener;
	}

	public void removeListener(IMyProjectTreeDeltaListener listener) {
		if (this.listener.equals(listener)) 
		{
			this.listener = MyNullAircraftDeltaListener.getSoleInstance();
		}
	}

	public Aircraft getAircraft() {
		return _theAircraft;
	}

	public void setAircraft(Aircraft ac) {
		this._theAircraft = ac;
	}

	public String getName() {
		return this._name;
	}

	public void setName(String name) {
		this._name = name;
	}

	public ACAnalysisManager getAnalysis() {
		return _theAnalysis;
	}

	public void setAnalysis(ACAnalysisManager _theAnalysis) {
		this._theAnalysis = _theAnalysis;
	}

	public void addObject(Object obj) {
		
		_nodesProjectTree.add(obj);

		if (obj instanceof MyProjectTree) {
			((MyProjectTree)obj)._parent = this;
			fireAdd((MyProjectTree)obj);

		} else if (obj instanceof Aircraft) {
			// setAircraft((MyAircraft)obj);
			//((MyProjectTree)obj)._parent = this;
			fireAdd((Aircraft)obj);

		} else if (obj instanceof ACAnalysisManager) {

			fireAdd((ACAnalysisManager)obj); 
			//			setAnalysis((MyAnalysis)obj);
		}
	}

	protected void fireAdd(Object added) {
		listener.add(new MyProjectTreeDeltaEvent(added));
	}

	public List getNodes() {
		return _nodesProjectTree;
	}

	public MyProjectTree getParent() {
		return _parent;
	}

}
