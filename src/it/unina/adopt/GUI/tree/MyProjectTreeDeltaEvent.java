package it.unina.adopt.GUI.tree;

public class MyProjectTreeDeltaEvent {

	protected Object actedUpon;
	
	public MyProjectTreeDeltaEvent(Object receiver) {
		actedUpon = receiver;
	}
	
	public Object receiver() {
		return actedUpon;
	}
}
