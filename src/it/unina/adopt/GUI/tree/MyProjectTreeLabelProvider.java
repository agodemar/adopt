package it.unina.adopt.GUI.tree;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import aircraft.calculators.ACAnalysisManager;
import aircraft.calculators.ACCalculatorManager;
import aircraft.componentmodel.Component;
import aircraft.components.Aircraft;
import it.unina.adopt.core.GlobalData;

public class MyProjectTreeLabelProvider implements ILabelProvider {

	private Map<ImageDescriptor, Image> _imageCache = new HashMap<ImageDescriptor, Image>();

	public MyProjectTreeLabelProvider() { }

	@Override
	public void addListener(ILabelProviderListener arg0) {

	}

	@Override
	public void dispose() {
		for (Iterator i = _imageCache.values().iterator(); i.hasNext();) {
			((Image) i.next()).dispose();
		}
		_imageCache.clear();	
	}


	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
	}

	/**
	 * @see ILabelProvider#getImage(Object)
	 */
	@Override
	public Image getImage(Object element) {

		ImageDescriptor descriptor = null;

		Image img = new Image(
				Display.getCurrent(), 
				GlobalData.get_imagesMap().get(element.getClass()));

		descriptor = ImageDescriptor.createFromImage(img);

		//obtain the cached image corresponding to the descriptor
		Image image = (Image)_imageCache.get(descriptor);
		if (image == null) {
			image = descriptor.createImage();
			_imageCache.put(descriptor, image);
		}
		return image;
	}

	@Override
	public String getText(Object element) {

		if (element instanceof MyProjectTree) {
			return ((MyProjectTree)element).getName();

		} else if (element instanceof Aircraft) {
			if(((Aircraft)element).getName() == null) {
				return "Aircraft";
			} else {
				return ((Aircraft)element).getName();
			}

		} else if (element instanceof Component) {
			return ((Component)element).getName();

		} else if (element instanceof String) {
			if ((String)element != null)
				return (String)element;
			else return "empty";

		} else if (element instanceof ACAnalysisManager) {

			if(((ACAnalysisManager)element).get_name() == null) {
				return "ANALYSIS";
			} else {
				return ((ACAnalysisManager)element).get_name();
			}
			
		} else if (element instanceof ACCalculatorManager) {

			if(((ACCalculatorManager)element).get_name() == null) {
				return "CALCULATOR";
			} else {
				return ((ACCalculatorManager)element).get_name();
			}

		} else {
			throw unknownElement(element);
		}
	}

	protected RuntimeException unknownElement(Object element) {
		return new RuntimeException("Unknown type of element in tree of type " + element.getClass().getName());
	}

}// end-of class
