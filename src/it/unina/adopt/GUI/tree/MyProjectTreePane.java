package it.unina.adopt.GUI.tree;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import aircraft.calculators.ACAnalysisManager;
import aircraft.calculators.ACCalculatorManager;
import aircraft.componentmodel.Component;
import aircraft.components.Aircraft;
import it.unina.adopt.GUI.tabpanels.MyTabbedPane;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;

public class MyProjectTreePane extends Composite {

	private static final Map<Object, String> _tabNameMap = new HashMap<Object, String>();
	final private TreeViewer _theTreeViewer;
	private MyProjectTreeLabelProvider _theLabelProvider;
	private MyProjectTree _theProjectTree;
	protected Text _text;

	private Aircraft _theAircraft;
	private ACAnalysisManager _theAnalysis;

	public MyProjectTreePane(Composite parent, int style) {

		super(parent, style);

		/* 
		 * Create a grid layout object so the text and treeviewer
		 * are layed out the way I want.
		 */
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.verticalSpacing = 2;
		layout.marginWidth = 0;
		layout.marginHeight = 2;
		this.setLayout(layout);

		/* 
		 * Create a "label" to display information in. I'm
		 * using a text field instead of a label so you can
		 * copy-paste out of it. 
		 */
		_text = new Text(this, SWT.READ_ONLY | SWT.SINGLE | SWT.BORDER);

		// layout the text field above the treeviewer
		GridData layoutData = new GridData();
		layoutData.grabExcessHorizontalSpace = true;
		layoutData.horizontalAlignment = GridData.FILL;
		_text.setLayoutData(layoutData);

		// Create the tree viewer as a child of the composite _parent
		// Create the tree viewer to display the file tree
		_theTreeViewer = new TreeViewer(this); // this? or _parent?

		// layout the tree viewer below the text field
		layoutData = new GridData();
		layoutData.grabExcessHorizontalSpace = true;
		layoutData.grabExcessVerticalSpace = true;
		layoutData.horizontalAlignment = GridData.FILL;
		layoutData.verticalAlignment = GridData.FILL;
		_theTreeViewer.getControl().setLayoutData(layoutData);

		// see if there's a tree to display
		_theProjectTree = new MyProjectTree("PROJECT");
		populateTreeViewer();

		//-----------------------------------------------------

		//		group.pack();
		//		pack();

		// LISTENERS

		_theTreeViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				
				TreeViewer viewer = (TreeViewer) event.getViewer();
				IStructuredSelection thisSelection = (IStructuredSelection) event.getSelection(); 
				
				System.out.println("addDoubleClickListener :: " + thisSelection.toString());
				
				Object selectedNode = thisSelection.getFirstElement(); 
				viewer.setExpandedState(selectedNode,
						!viewer.getExpandedState(selectedNode));

				// Lambda function
				/*IopenInitiator doOpenInitiator = (myTabbedPane, object) -> {
					myTabbedPane.openPane(object);					
				};*/

				// open tab panel as appropriate
				doOpenTab(selectedNode);
			} 
		}); 

		ADOPT_GUI.getApp().setTheProjectPane(this);

	} // end-of-constructor

	/*
	private void doubleClickToTabbedPane(IopenInitiator doOpenInitiator, Object selectedNode){

		// doOpenTab(doOpenInitiator, selectedNode);
		doOpenTabG( selectedNode);
	}
	 */

	interface IopenInitiator {
		void doOpen(MyTabbedPane myTabbedPane, Object obj);
	}


	/** 
	 * Open a different tab for each object
	 * 
	 * @param doOpenInitiator lambda function interface
	 * @param object
	 */
	private void doOpenTab(Object object) {

		MyTabbedPane myTabbedPane = ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane();
		int idx = -1;

		if (object instanceof Component) {

			Component component = (Component) object;

			idx = ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
					.findTabItem(component.getName());

			System.out.println("DBL CLICK :: " + component.getName());
			System.out.println("idx = " + idx);

			// Tab not open yet
//			if ( idx == -1 ) {

				ADOPT_GUI.get_strategyHandlers().get(component.get_type())
				.openInitiator(myTabbedPane.get_tabFolder(), _theAircraft, component);

				// Search for itself
				idx = ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
						.findTabItem(component.getName());
//			}

		} else if (object instanceof ACCalculatorManager) {

			ACCalculatorManager calculator = (ACCalculatorManager) object;

			idx = ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
					.findTabItem(calculator.get_name());

			System.out.println("DBL CLICK :: " + calculator.get_name());
			System.out.println("idx = " + idx);

			// Tab not open yet
//			if ( idx == -1 ) {

				ADOPT_GUI.get_strategyHandlers().get(calculator.get_type())
				.openInitiator(myTabbedPane.get_tabFolder(), _theAircraft, calculator.get_type());

				// Search for itself
				idx = ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
						.findTabItem(calculator.get_name());
//			} 

			/* } else if (object instanceof MyAnalysis) {

			MyAnalysis analysis = (MyAnalysis) object;

			idx = ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
					.findTabItem(analysis.get_name());

			System.out.println("DBL CLICK :: " + analysis.get_name());
			System.out.println("idx = " + idx);

			// Tab not open yet
			if ( idx == -1 ) {

				ADOPT_GUI.get_strategyHandlers().get(analysis)
				.openInitiator(myTabbedPane.get_tabFolder(), null);

				// Search for itself
				idx = ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane()
						.findTabItem(analysis.get_name());

			} else {
				System.out.println("DBL CLICK :: tab item already there!");
			}
			 */
		} else {
			System.out.println("DBL CLICK :: tab item already there!");
		}			

		// select & focus the tab
		ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().get_tabFolder().setSelection(idx);
		ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().get_tabFolder().forceFocus();
		ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().get_tabFolder().redraw();
	}



	//	@Focus
	//	public void setFocus() {
	//		_viewerAircraft.getControl().setFocus();
	//	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void setAircraft(Aircraft aircraft)
	{
		_theAircraft = aircraft;

		if (_theAircraft != null) {
			_theTreeViewer.setInput(_theAircraft);
		}
	}

	public void setAnalysis(ACAnalysisManager analysis)
	{
		_theAnalysis = analysis;

		if (_theAnalysis != null) {
			_theTreeViewer.setInput(_theAnalysis);
		}
	}

	public MyProjectTree getInitialInput() {
		
		MyProjectTree root = new MyProjectTree("root");
		_theProjectTree.addObject(_theAircraft);
		_theProjectTree.addObject(_theAnalysis);
		root.addObject(_theProjectTree);
		return root;
	}

	public void populateTreeViewer() {
		
		// do nothing if aircraft is null
		if (GlobalData.getTheCurrentAircraft() == null) return;
		if (GlobalData.getTheCurrentAnalysis() == null) GlobalData.setTheCurrentAnalysis(new ACAnalysisManager()); 
		
		// do nothing if tree is already in place
		//		if (_theTreeViewer != null) return;
		// TODO: check this! User might want to re-display from a new aircraft
		//       ask user and delete current treeview

		// the aircraft is not null
		_theAircraft = GlobalData.getTheCurrentAircraft();
		_theAnalysis = GlobalData.getTheCurrentAnalysis();

		_text.setText(_theAircraft.getName());

		// the tree object manager
		_theProjectTree.setAircraft(_theAircraft);
		_theProjectTree.setAnalysis(_theAnalysis);

		// associate a content provider
		_theTreeViewer.setContentProvider(
				new MyProjectContentProvider(_theTreeViewer, _theProjectTree)
				);

		_theLabelProvider = new MyProjectTreeLabelProvider();
		_theTreeViewer.setLabelProvider(_theLabelProvider);

//		_theTreeViewer.setUseHashlookup(false);

		// tell the tree view who's the starting point (root element) 
		_theTreeViewer.setInput(getInitialInput()); // (_theProjectTree);
		_theTreeViewer.expandAll();		

	}


	public TreeViewer get_theTreeViewer() {
		return _theTreeViewer;
	}

}