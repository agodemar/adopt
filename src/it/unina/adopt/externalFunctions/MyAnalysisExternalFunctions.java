package it.unina.adopt.externalFunctions;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;

import aircraft.components.Aircraft;
import it.unina.adopt.GUI.tabpanels.analysis.MyAnalysisPanel;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.ADOPT_GUI.IExternalFunctionsHandler;

/**
 *  Concrete class for FuselageExternalFunctions handling
 * 
 * @author LA
 */
public class MyAnalysisExternalFunctions implements IExternalFunctionsHandler {

	private CTabFolder _tabFolder;
	private CTabItem _tabItemInitiator;
	
	public static final String TAB_NAME_INITIATOR = "Analysis";

	public void openInitiator(CTabFolder tabFolder, Aircraft aircraft, Object type) {
		
		_tabFolder = tabFolder;
		_tabItemInitiator = new CTabItem(_tabFolder, SWT.CLOSE);
		_tabItemInitiator.setText(TAB_NAME_INITIATOR);

		System.out.println("openAnalysisInitiatorPane :: ");

		// draw and setup GUI elements if fuselage is in memory
		if (GlobalData.getTheCurrentAircraft() != null) {

				System.out.println("openFuselageInitiatorPane :: fuselage != 0");
				ADOPT_GUI.getApp().setInitiatorPaneAnalysis(
						new MyAnalysisPanel(
								_tabFolder,
								_tabItemInitiator,
								SWT.CLOSE,
								GlobalData.getTheCurrentAircraft(), 
								GlobalData.getTheCurrentAnalysis()));
			}
		}
	}
