package it.unina.adopt.externalFunctions;

import org.apache.commons.lang3.text.WordUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;

import aircraft.components.Aircraft;
import configuration.enumerations.AnalysisTypeEnum;
import it.unina.adopt.GUI.tabpanels.analysis.MyBalancePanel;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.ADOPT_GUI.IExternalFunctionsHandler;

public class MyBalanceExternalFunctions implements IExternalFunctionsHandler {

	private CTabFolder _tabFolder;
	private CTabItem _tabItemInitiator;
	
	public static final String TAB_NAME_INITIATOR = 
			WordUtils.capitalizeFully(AnalysisTypeEnum.BALANCE.name());

	public void openInitiator(CTabFolder tabFolder, Aircraft aircraft, Object type) {
		
		_tabFolder = tabFolder;
		_tabItemInitiator = new CTabItem(_tabFolder, SWT.CLOSE);
		_tabItemInitiator.setText(TAB_NAME_INITIATOR);

		// draw and setup GUI elements if fuselage is in memory
		if (GlobalData.getTheCurrentAircraft() != null) {

				ADOPT_GUI.getApp().set_theBalancePanel(
						new MyBalancePanel(
								_tabFolder,
								_tabItemInitiator,
								SWT.CLOSE,
								GlobalData.getTheCurrentAircraft()));
			}
		}
	}
