package it.unina.adopt.externalFunctions;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;

import aircraft.components.Aircraft;
import aircraft.components.fuselage.Fuselage;
import configuration.enumerations.ComponentEnum;
import it.unina.adopt.GUI.tabpanels.fuselage.MyFuselagePanel;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.ADOPT_GUI.IExternalFunctionsHandler;

/**
 *  Concrete class for FuselageExternalFunctions handling
 * 
 * @author LA
 */
public class MyFuselageExternalFunctions implements IExternalFunctionsHandler {

	// TODO: use _type for each external function to distinguish between them
	private final ComponentEnum _type = ComponentEnum.FUSELAGE;
	private CTabFolder _tabFolder;
	private CTabItem _tabItemFuselageInitiator;
	
	public static final String TAB_NAME_FUSELAGE_INITIATOR = "Fuselage";

	public void openInitiator(CTabFolder tabFolder, Aircraft aircraft, Object comp) {
		
		Fuselage component = (Fuselage) comp;
		
		_tabFolder = tabFolder;
		_tabItemFuselageInitiator = new CTabItem(_tabFolder, SWT.CLOSE);
		_tabItemFuselageInitiator.setText(aircraft.get_name() + " " + TAB_NAME_FUSELAGE_INITIATOR);

		System.out.println("openFuselageInitiatorPane :: ");

		// draw and setup GUI elements if fuselage is in memory
		if (GlobalData.getTheCurrentAircraft() != null) {

			System.out.println("openFuselageInitiatorPane :: _theAircraft != 0");
			if (GlobalData.getTheCurrentAircraft().get_fuselage() != null) {

				System.out.println("openFuselageInitiatorPane :: fuselage != 0");
				ADOPT_GUI.getApp().setFuselagePanel(
						new MyFuselagePanel(
								_tabFolder,
								_tabItemFuselageInitiator,
								SWT.CLOSE,
								component));
			}
		}
	}
}


