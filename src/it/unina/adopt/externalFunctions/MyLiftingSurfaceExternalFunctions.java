package it.unina.adopt.externalFunctions;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;

import aircraft.components.Aircraft;
import aircraft.components.liftingSurface.LiftingSurface;
import configuration.enumerations.ComponentEnum;
import it.unina.adopt.GUI.tabpanels.liftingSurface.MyLiftingSurfacePanel;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.main.ADOPT_GUI;
import it.unina.adopt.main.ADOPT_GUI.IExternalFunctionsHandler;

/**
 *  Concrete class for FuselageExternalFunctions handling
 * 
 * @author LA
 */
public class MyLiftingSurfaceExternalFunctions implements IExternalFunctionsHandler {

	private CTabFolder _tabFolder;
	private CTabItem _tabItemInitiator;

	public static String TAB_NAME_INITIATOR;

	public void openInitiator(CTabFolder tabFolder, Aircraft aircraft, Object t) {

		LiftingSurface type = (LiftingSurface) t;
		
		_tabFolder = tabFolder;
		_tabItemInitiator = new CTabItem(_tabFolder, SWT.CLOSE);

		System.out.println("openWingInitiatorPane :: ");

		// draw and setup GUI elements if fuselage is in memory
		if (GlobalData.getTheCurrentAircraft() != null) {

			System.out.println("openWingInitiatorPane :: _theAircraft != null");

			if (type.get_type() == ComponentEnum.WING) {
				TAB_NAME_INITIATOR = aircraft.get_name() + " Wing";
				
				if (aircraft.get_wing() != null) {

					System.out.println("openWingInitiatorPane :: wing != 0");
					ADOPT_GUI.getApp().setInitiatorPaneWing(
							new MyLiftingSurfacePanel(
									_tabFolder,
									_tabItemInitiator,
									SWT.CLOSE,
									aircraft,
									type));
				}

			} else if (type.get_type() == ComponentEnum.HORIZONTAL_TAIL) {
				
				TAB_NAME_INITIATOR = aircraft.get_name() + "HTail";
				
				if (aircraft.get_HTail() != null) {

					System.out.println("openWingInitiatorPane :: HTail != 0");
					ADOPT_GUI.getApp().setInitiatorPaneHTail(
							new MyLiftingSurfacePanel(
									_tabFolder,
									_tabItemInitiator,
									SWT.CLOSE,
									aircraft,
									type));
				}

			} else if (type.get_type() == ComponentEnum.VERTICAL_TAIL) {

				TAB_NAME_INITIATOR = aircraft.get_name() + "VTail";

				if (aircraft.get_VTail() != null) {

					System.out.println("openWingInitiatorPane :: VTail != 0");
					ADOPT_GUI.getApp().setInitiatorPaneVTail(
							new MyLiftingSurfacePanel(
									_tabFolder,
									_tabItemInitiator,
									SWT.CLOSE,
									aircraft,
									type));
				}

			} else {

				if (aircraft.get_Canard() != null) {

					//					System.out.println("openWingInitiatorPane :: canard != 0");
					//					ADOPT_GUI.getApp().setInitiatorPaneCanard(
					//							new MyInitiatorPaneLiftingSurface(
					//									_tabFolder,
					//									_tabItemInitiator,
					//									SWT.CLOSE,
					//									GlobalData.getTheCurrentAircraft(),
					//									type));
				}
			}
		}
		
		_tabItemInitiator.setText(TAB_NAME_INITIATOR);
		
	}
}
