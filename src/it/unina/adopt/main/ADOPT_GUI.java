package it.unina.adopt.main;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import aircraft.calculators.ACAerodynamicsManager;
import aircraft.components.Aircraft;
import aircraft.components.fuselage.Fuselage;
import aircraft.components.liftingSurface.LiftingSurface;
import configuration.MyConfiguration;
import configuration.enumerations.AnalysisTypeEnum;
import configuration.enumerations.ComponentEnum;
import it.unina.adopt.GUI.MyTopLevelComposite;
import it.unina.adopt.GUI.actions.My3DViewAction;
import it.unina.adopt.GUI.actions.MyExportAircraftAction;
import it.unina.adopt.GUI.actions.MyExportCadAction;
import it.unina.adopt.GUI.actions.MyHelpAction;
import it.unina.adopt.GUI.actions.MyImportAircraftAction;
import it.unina.adopt.GUI.actions.MyImportSerializedAircraftAction;
import it.unina.adopt.GUI.actions.MyNewAircraftAction;
import it.unina.adopt.GUI.actions.MyNewAnalysisAction;
import it.unina.adopt.GUI.actions.MyOptionsAction;
import it.unina.adopt.GUI.actions.MySelectAircraftAction;
import it.unina.adopt.GUI.actions.MySerializeAircraftAction;
import it.unina.adopt.GUI.actions.MyStatusAction;
import it.unina.adopt.GUI.actions.MyToggleFuselageInitiatorViewAction;
import it.unina.adopt.GUI.actions.MyToggleMessageViewAction;
import it.unina.adopt.GUI.actions.MyToggleProjectViewAction;
import it.unina.adopt.GUI.actions.MyWorkingDirectoryAction;
import it.unina.adopt.GUI.tabpanels.analysis.MyAnalysisPanel;
import it.unina.adopt.GUI.tabpanels.analysis.MyBalancePanel;
import it.unina.adopt.GUI.tabpanels.analysis.MyWeightsPanel;
import it.unina.adopt.GUI.tabpanels.fuselage.MyFuselagePanel;
import it.unina.adopt.GUI.tabpanels.liftingSurface.MyLiftingSurfacePanel;
import it.unina.adopt.GUI.tree.MyProjectTreePane;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.externalFunctions.MyBalanceExternalFunctions;
import it.unina.adopt.externalFunctions.MyFuselageExternalFunctions;
import it.unina.adopt.externalFunctions.MyLiftingSurfaceExternalFunctions;
import it.unina.adopt.externalFunctions.MyWeightsExternalFunctions;
import it.unina.adopt.test.la.MySandbox_LA;
import it.unina.adopt.utilities.gui.MyGuiUtils;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import processing.core.PApplet;
import sandbox.adm.MySandbox_ADM;

public class ADOPT_GUI extends ApplicationWindow {

	public boolean isGUIMode = false;

	public static boolean useSandBox_ADM = false;
	public static boolean useSandBox_LA = false;

	public static MyCommandLineOptions theCmdLineOptions = null;
	public static CmdLineParser theCmdLineParser = null;

	private static Map<Object, IExternalFunctionsHandler> _strategyHandlers = 
			new HashMap<Object, IExternalFunctionsHandler>();

	/** The pointer to the app */
	private static final ADOPT_GUI _theApp = new ADOPT_GUI();

	/**
	 * Gets the running application
	 * 
	 * @return ADOPT_GUI
	 */
	public static ADOPT_GUI getApp() {
		return _theApp;
	}

	private static FileHandler _fh;
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// EXPERIMENTAL
	public Fuselage theFuselage = null;
	public LiftingSurface theWing = null;
	public PApplet theSketch = null;

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// images etc
	private static Map<String, Image> fCachedImages = new TreeMap<String, Image>();

	// ------------------------------------------------------------------
	// JFace events/action stuff

	private MyNewAircraftAction _newAircraftAction;
	private ActionContributionItem _aciNewAircraftAction;
	
	private MySelectAircraftAction _selectAircraftAction;
	private ActionContributionItem _aciSelectAircraftAction;

	private MyStatusAction _statusAction;
	private ActionContributionItem _aciStatusAction;

	private MyOptionsAction _optionsAction;
	private ActionContributionItem _aciOptionsAction;

	private MyWorkingDirectoryAction _workingDirectoryAction;
	private ActionContributionItem _aciWorkingDirectoryAction;

	private MyHelpAction _helpAction;
	private ActionContributionItem _aciHelpAction;

	private MyToggleProjectViewAction _toggleProjectViewAction;
	private ActionContributionItem _aciToggleProjectViewAction;

	private MyToggleMessageViewAction _toggleMessageViewAction;
	private ActionContributionItem _aciToggleMessageViewAction;

	private MyToggleFuselageInitiatorViewAction _toggleFuselageInitiatorViewAction;
	private ActionContributionItem _aciToggleFuselageInitiatorViewAction;

	private Action _exitAction;

	// ------------------------------------------------------------------
	private MyTopLevelComposite _topLevelContainer = null;

	// ------------------------------------------------------------------
	private MenuManager _mainMenuManager = null;

	// ------------------------------------------------------------------
	private ToolBarManager _toolBarManager = null;

	// ------------------------------------------------------------------
	private StatusLineManager _statusLineManager = new StatusLineManager();

	// ------------------------------------------------------------------

	private MyFuselagePanel _theFuselagePanel = null;
	private MyLiftingSurfacePanel _theWingPanel = null;
	private MyLiftingSurfacePanel _theHTailPanel = null;
	private MyLiftingSurfacePanel _theVTailPanel = null;
	private MyAnalysisPanel _theAnalysisPanel = null;
	private MyWeightsPanel _theWeightsPanel = null;
	private MyBalancePanel _theBalancePanel = null;

	private MyProjectTreePane _theProjectPanel;
	
	private ACAerodynamicsManager _theCalculator;
	private MyImportAircraftAction _importAircraftAction;
	private ActionContributionItem _aciImportAircraftAction;
	
	private MyImportSerializedAircraftAction _importSerializedAircraftAction;
	private ActionContributionItem _aciImportSerializedAircraftAction;
	
	private MyExportAircraftAction _exportAircraftAction;
	private ActionContributionItem _aciExportAircraftAction;

	private MyExportCadAction _exportCADAction;
	private ActionContributionItem _aciExportCADAction;
	private MyNewAnalysisAction _createAnalysisAction;
	private ActionContributionItem _aciCreateAnalysisAction;

	private My3DViewAction _3DViewAction;
	private ActionContributionItem _aci3DViewAction;

	private final MenuManager fileMenu = new MenuManager("&File");

	private MySerializeAircraftAction _serializeAircraftAction;

	private static Shell _shell;

	public static BooleanProperty aircraftInMemoryProperty = new SimpleBooleanProperty(false);
	public static BooleanProperty fuselageInMemoryProperty = new SimpleBooleanProperty(false);

	/* -----------------------------------------------------------
	 * STRATEGY PATTERN
	 * -----------------------------------------------------------
	 * 
	 * // Interface
	 * public interface IResponseHandler {
	 * public void handleResponse(XmlPullParser xxp);
	 * }
	 * 
	 * // Concrete class for EditorialOffice response
	 * private class EditorialOfficeHandler implements IResponseHandler {
	 * public void handleResponse(XmlPullParser xxp) {
	 * // Do something to handle Editorial Office response
	 * 	}
	 * }
	 * 
	 * // Concrete class for EditorialBoard response
	 * private class EditorialBoardHandler implements IResponseHandler {
	 * public void handleResponse(XmlPullParser xxp) {
	 * // Do something to handle Editorial Board response
	 *	}
	 * }
	 * 
	 * On a place you need to create the handlers:
	 * 
	 * Map<String, IResponseHandler> strategyHandlers = new
	 * HashMap<String,IResponseHandler>();
	 * strategyHandlers.put("EditorialOffice", new EditorialOfficeHandler());
	 * strategyHandlers.put("EditorialBoard", new EditorialBoardHandler());
	 * 
	 * Where you received the response:
	 * 
	 * IResponseHandler responseHandler = strategyHandlers.get(soapResponse);
	 * responseHandler.HandleResponse(xxp);
	 */
	/** 
	 * Implements the strategy pattern for handling
	 * methods which are external to components (typically
	 * GUI related methods) that is preferred to hold outside
	 * the core classes.
	 * 
	 * @author LA
	 */
	public interface IExternalFunctionsHandler {
		public void openInitiator(CTabFolder tabFolder, Aircraft aircraft, Object component);
	}

	/**
	 * Create the application window.
	 */
	public ADOPT_GUI() {

		super(null);

		_strategyHandlers.put(ComponentEnum.FUSELAGE, new MyFuselageExternalFunctions());
		_strategyHandlers.put(ComponentEnum.WING, new MyLiftingSurfaceExternalFunctions());
		_strategyHandlers.put(ComponentEnum.HORIZONTAL_TAIL, new MyLiftingSurfaceExternalFunctions());
		_strategyHandlers.put(ComponentEnum.VERTICAL_TAIL, new MyLiftingSurfaceExternalFunctions());
		_strategyHandlers.put(AnalysisTypeEnum.WEIGHTS, new MyWeightsExternalFunctions());
		_strategyHandlers.put(AnalysisTypeEnum.BALANCE, new MyBalanceExternalFunctions());

		//		_strategyHandlers.put(MyAnalysisTypeEnum.AERODYNAMIC, new MyAnalysisExternalFunctions());

		// TODO: add other components
		//		_strategyHandlers.put(MyComponent.ComponentEnum.CANARD, new MyLiftingSurfaceExternalFunctions());
		//		_strategyHandlers.put(MyComponent.ComponentEnum.NACELLE, new MyFuselageExternalFunctions());
		//		_strategyHandlers.put(MyComponent.ComponentEnum.LANDING_GEAR, new MyFuselageExternalFunctions());

		// ------------------------------------------------------------------
		// GUI stuff
		MyGuiUtils.populateTreeImagesMap(GlobalData.imagesMap);
		createActions();
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		addStatusLine();
		// TODO: Sandbox here ???

	} // end-of- ADOPT_GUI constructor

	
	/**
	 * Create contents of the application window.
	 * 
	 * @param _parent
	 */
	@Override
	protected Control createContents(Composite parent) {

		_topLevelContainer = new MyTopLevelComposite(
				parent,
				SWT.NONE,
				GlobalData.getTheCurrentAircraft(),
				GlobalData.theCurrentAnalysis);

		// place the ActionContributionItem in the GUI
		_aciStatusAction.fill(parent);
		_aciToggleProjectViewAction.fill(parent);
		_aciToggleMessageViewAction.fill(parent);
		_aciOptionsAction.fill(parent);
		_aciWorkingDirectoryAction.fill(parent);
		_aciHelpAction.fill(parent);

		return _topLevelContainer;
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {

		// The status line
		_statusAction = new MyStatusAction(_statusLineManager);
		_aciStatusAction = new ActionContributionItem(_statusAction);

		// Test the options
		// TODO: open a preference JFace Dialog
		_optionsAction = new MyOptionsAction(_statusLineManager);
		_aciOptionsAction = new ActionContributionItem(_optionsAction);

		_workingDirectoryAction = new MyWorkingDirectoryAction(_statusLineManager);
		_aciWorkingDirectoryAction = new ActionContributionItem(_workingDirectoryAction);

		// Left-hand-side project panel view
		// TODO: improve appearance, check tree viewers
		_toggleProjectViewAction = new MyToggleProjectViewAction(_statusLineManager);
		_aciToggleProjectViewAction = new ActionContributionItem(_toggleProjectViewAction);

		// Message window at the bottom of the GUI
		// TODO: allow font settings
		_toggleMessageViewAction = new MyToggleMessageViewAction(_statusLineManager);
		_aciToggleMessageViewAction = new ActionContributionItem(_toggleMessageViewAction);

		// Tab panel with fuselage initiator view
		_toggleFuselageInitiatorViewAction = new MyToggleFuselageInitiatorViewAction(
				_statusLineManager);
		_aciToggleFuselageInitiatorViewAction = new ActionContributionItem(
				_toggleFuselageInitiatorViewAction);

		// Create new aircraft action
		_newAircraftAction = new MyNewAircraftAction(_statusLineManager);
		_aciNewAircraftAction = new ActionContributionItem(_newAircraftAction);
		
		_selectAircraftAction = new MySelectAircraftAction(_statusLineManager);
		_aciSelectAircraftAction = new ActionContributionItem(_selectAircraftAction);

		// Import aircraft action
		_importAircraftAction = new MyImportAircraftAction(_statusLineManager);
		_aciImportAircraftAction = new ActionContributionItem(_importAircraftAction);
		
		// Import aircraft from serialized xml action
		_importSerializedAircraftAction = new MyImportSerializedAircraftAction(_statusLineManager);
		_aciImportSerializedAircraftAction = new ActionContributionItem(_importSerializedAircraftAction);

		// Export aircraft action
		_exportAircraftAction = new MyExportAircraftAction(_statusLineManager);
		_aciExportAircraftAction = new ActionContributionItem(_exportAircraftAction);
		
		// Serialize aircraft action
		_serializeAircraftAction = new MySerializeAircraftAction(_statusLineManager);

		// Export CAD action
		_exportCADAction = new MyExportCadAction(_statusLineManager);
		_aciExportCADAction = new ActionContributionItem(_exportCADAction);

		// Create New analysis action
		_createAnalysisAction = new MyNewAnalysisAction(_statusLineManager);
		_aciCreateAnalysisAction = new ActionContributionItem(_createAnalysisAction);

		// Toggle 3D View action
		_3DViewAction = new My3DViewAction(_statusLineManager);
		_aci3DViewAction = new ActionContributionItem(_3DViewAction);

		// Help dialog
		_helpAction = new MyHelpAction(_statusLineManager);
		_aciHelpAction = new ActionContributionItem(_helpAction);

		// Exit action
		_exitAction = new Action("&Exit@Ctrl+Z") {
			@Override
			public void run() {
				close();
			}
		};
	}

	private ActionContributionItem addActionToMenuAndToolbar(Action act) {
		fileMenu.add(act);
		_toolBarManager.add(act);
		ActionContributionItem _aciAction = new ActionContributionItem(act);
		return _aciAction;
	}
	
	/**
	 * Create the menu manager.
	 * 
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {

		_mainMenuManager = new MenuManager(null);

		fileMenu.add(_newAircraftAction);
		fileMenu.add(_selectAircraftAction);
		fileMenu.add(_importAircraftAction);
		fileMenu.add(_importSerializedAircraftAction);
		fileMenu.add(_exportAircraftAction);
		fileMenu.add(_serializeAircraftAction);
		fileMenu.add(_exportCADAction);
		fileMenu.add(new Separator());
		fileMenu.add(_createAnalysisAction);
		fileMenu.add(new Separator());
		fileMenu.add(_statusAction);
		fileMenu.add(new Separator());
		fileMenu.add(_exitAction);

		MenuManager viewMenu = new MenuManager("&View");
		viewMenu.add(_toggleProjectViewAction);
		viewMenu.add(_toggleMessageViewAction);
		viewMenu.add(new Separator());
		viewMenu.add(_toggleFuselageInitiatorViewAction);
		// viewMenu.add(_xmlViewAction);
		viewMenu.add(_3DViewAction);


		MenuManager optionsMenu = new MenuManager("&Options");
		optionsMenu.add(_optionsAction);
		// optionsMenu.add(new Separator());
		optionsMenu.add(_workingDirectoryAction);

		MenuManager helpMenu = new MenuManager("&Help");
		helpMenu.add(_helpAction);

		_mainMenuManager.add(fileMenu);
		_mainMenuManager.add(viewMenu);
		_mainMenuManager.add(optionsMenu);
		_mainMenuManager.add(helpMenu);

		return _mainMenuManager;
	}

	/**
	 * Create the toolbar manager.
	 * 
	 * @return the toolbar manager
	 */
	@Override
	protected ToolBarManager createToolBarManager(int style) {
		
		_toolBarManager = new ToolBarManager(style);
		_toolBarManager.add(_statusAction);
		_toolBarManager.add(_newAircraftAction);
		_toolBarManager.add(_selectAircraftAction);
		_toolBarManager.add(_importAircraftAction);
		_toolBarManager.add(_importSerializedAircraftAction);
		_toolBarManager.add(_exportAircraftAction);
		_toolBarManager.add(_serializeAircraftAction);
		_toolBarManager.add(_exportCADAction);
		_toolBarManager.add(new Separator());
		_toolBarManager.add(_createAnalysisAction);
		// _toolBarManager.add(_xmlViewAction);
		_toolBarManager.add(_toggleProjectViewAction);
		_toolBarManager.add(_toggleMessageViewAction);
		_toolBarManager.add(_3DViewAction);
		_toolBarManager.add(_toggleFuselageInitiatorViewAction);
		_toolBarManager.add(_optionsAction);
		_toolBarManager.add(_workingDirectoryAction);
		_toolBarManager.add(_helpAction);
		return _toolBarManager;
	}

	/**
	 * Create the status line manager.
	 * 
	 * @return the StatusLineManager
	 */
	@Override
	public StatusLineManager createStatusLineManager() {
		// _statusLineManager = new StatusLineManager();
		return _statusLineManager;
	}


	/**
	 * LAUNCH THE APPLICATION.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		
		// +++++++++++++++++++++++++++++++++++++++++++++++
		// TODO: add command line parsing feature here
		// http://martin-thoma.com/how-to-parse-command-line-arguments-in-java/
		// https://weblogs.java.net/blog/2005/05/11/parsing-command-line-options-jdk-50-style-args4j
		theCmdLineOptions = new MyCommandLineOptions(args);
		theCmdLineParser = new CmdLineParser(theCmdLineOptions);

		try {
			theCmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.exit(1);
		}
		
		////////////////////////////
		// Initialize log
		////////////////////////////
		try {

			File logDir = new File(
					System.getProperty("user.dir") + File.separator + 
					"logs" + File.separator);
			if (logDir.exists()) {
				// Delete log file to avoid overwriting problems
				File logTemp = new File(
						System.getProperty("user.dir") + File.separator + 
						"logs" + File.separator + 
						"logFile.txt");
				if(logTemp.exists()) {
					logTemp.delete();
				}
			} else {
				// TODO: create a /logs dir??
				System.out.println("not using /logs dir.");
			}
			
			
			// This block configure the logger with handler and formatter
			GlobalData.log.setUseParentHandlers(false);
			
			System.out.println(System.getProperty("user.dir") + "/logs/logFile.txt");
			
			_fh = new FileHandler(System.getProperty("user.dir") + "/logs/logFile.txt");
			
		
			GlobalData.log.addHandler(_fh);
			SimpleFormatter formatter = new SimpleFormatter();
			_fh.setFormatter(formatter);

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// ------------------------------------------------------------------
		// Sand boxes ... BEFORE GUI calls

		// Lorenzo
		if (theCmdLineOptions.getUseSandboxLA()){
			
			ADOPT_GUI.useSandBox_LA = true;

			MySandbox_LA mySandbox_LA;
			mySandbox_LA = new MySandbox_LA();

		}

		// Agodemar
		if (theCmdLineOptions.getUseSandboxADM()) {
			
			ADOPT_GUI.useSandBox_ADM = true;
			MySandbox_ADM mySandbox_ADM = new MySandbox_ADM();
		}

		// ... end-of Sand box
		// ------------------------------------------------------------------------------

		// cmdLineParser.getArguments() ??

		if (theCmdLineOptions.getUseGUI()) {
			// User asked for the no-GUI mode
			_theApp.isGUIMode = false;
			System.out.println("No-GUI mode");

		} else {
			
			// User wanted to use the GUI
			System.out.println("GUI mode");
			_theApp.isGUIMode = true;
			
			Realm.runWithDefault(SWTObservables.getRealm(Display.getCurrent()), new Runnable() {
				public void run() {
					try {
						_theApp.setBlockOnOpen(true); // true -> enter the infinite loop
						_theApp.open(); // the GUI infinite loop
						Display.getCurrent().dispose();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

		}
	} // end of MAIN

	/**
	 * Configure the shell.
	 * 
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);

		_shell = newShell; 
		newShell.setText("ADOpTApplication");
		
		Image image = new Image(Display.getCurrent(), 
				MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/applicationIcon.png");
		
		_shell.setImage(image);
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {

		Rectangle monitorArea = getShell().getDisplay().getPrimaryMonitor().getBounds();
		int width = (int) ((double) monitorArea.width * 3.0 / 4.0);
		int height = (int) ((double) monitorArea.height * 4.0 / 5.0);
		return new Point(width, height);
	}

	public static void set_CurrentDirectory(File currentDirectory) {

		if (ADOPT_GUI.getApp().getTopLevelComposite().getMyTabbedPane().getInitiatorPaneFuselage() != null) {
			// TODO: ...

			// ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
			// "The ADOpT | current directory changed to: " +
			// _CurrentDirectory + "\n"
			// );

			// update data in InitiatorPaneFuselage object
			ADOPT_GUI.getApp()
			.getTopLevelComposite()
			.getMyTabbedPane()
			.getInitiatorPaneFuselage().setSerializationFilePath();

			String filePath =
					ADOPT_GUI.getApp()
					.getTopLevelComposite()
					.getMyTabbedPane()
					.getInitiatorPaneFuselage().getSerializationFilePath();

			//
			ADOPT_GUI.getApp()
			.getTopLevelComposite()
			.getMyTabbedPane()
			.getInitiatorPaneFuselage().getTextSerializeFile().setText(filePath);

		}
	}

	// see:
	// https://github.com/iee/iee/blob/master/org.eclipse.iee.sample.matrix/src/org/eclipse/iee/sample/matrix/pad/FormulaRenderer.java
	// see:
	// http://git.eclipse.org/c/platform/eclipse.platform.swt.git/tree/examples/org.eclipse.swt.snippets/src/org/eclipse/swt/snippets/Snippet156.java

	public static MyCommandLineOptions getTheCmdLineOptions() {
		return theCmdLineOptions;
	}

	public static Map<String, Image> getfCachedImages() {
		return fCachedImages;
	}

	public static void setfCachedImages(Map<String, Image> fCachedImages) {
		ADOPT_GUI.fCachedImages = fCachedImages;
	}

	public static Map<Object, IExternalFunctionsHandler> get_strategyHandlers() {
		return _strategyHandlers;
	}

	public MyProjectTreePane getTheProjectPane() {
		return _theProjectPanel;
	}

	public void setTheProjectPane(MyProjectTreePane p) {
		_theProjectPanel = p;
	}

	public ACAerodynamicsManager getTheCurrentCalculator() {
		return _theCalculator;
	}

	public void setTheTestCalculator(ACAerodynamicsManager calc) {
		_theCalculator = calc;
	}

	/**
	 * Gets/sets the pointer to the MyInitiatorPaneFuselage object
	 * 
	 * @return MyTopLevelComposite
	 */
	public MyFuselagePanel getInitiatorPaneFuselage() {
		return _theFuselagePanel;
	}

	public void setFuselagePanel(MyFuselagePanel ipf) {
		_theFuselagePanel = ipf;
	}

	public MyLiftingSurfacePanel getInitiatorPaneWing() {
		return _theWingPanel;
	}

	public void setInitiatorPaneWing(MyLiftingSurfacePanel ipw) {
		_theWingPanel = ipw;
	}

	public MyLiftingSurfacePanel getInitiatorPaneHTail() {
		return _theHTailPanel;
	}

	public void setInitiatorPaneHTail(MyLiftingSurfacePanel ipHt) {
		_theHTailPanel = ipHt;
	}

	public MyLiftingSurfacePanel getInitiatorPaneVTail() {
		return _theVTailPanel;
	}

	public void setInitiatorPaneVTail(MyLiftingSurfacePanel ipVt) {
		_theVTailPanel = ipVt;
	}

	public MyAnalysisPanel getInitiatorPaneAnalysis() {
		return _theAnalysisPanel;
	}

	public void setInitiatorPaneAnalysis(MyAnalysisPanel ipA) {
		_theAnalysisPanel = ipA;
	}

	/**
	 * Gets the application top-level composite
	 * 
	 * @return MyTopLevelComposite
	 */
	public MyTopLevelComposite getTopLevelComposite() {
		return _topLevelContainer;
	}

	/**
	 * Gets the application menu bar
	 * 
	 * @return MenuManager
	 */
	public MenuManager getMainMenuManager() {
		return _mainMenuManager;
	}

	/**
	 * Gets the application toolbar
	 * 
	 * @return ToolBarManager
	 */
	public ToolBarManager getToolBarManager() {
		return _toolBarManager;
	}

	/**
	 * Gets the application status line
	 * 
	 * @return StatusLineManager
	 */
	public StatusLineManager getStatusLineManager() {
		return _statusLineManager;
	}

	public static Shell get_shell() {
		return _shell;
	}

	public MyBalancePanel get_theBalancePanel() {
		return _theBalancePanel;
	}

	public void set_theBalancePanel(MyBalancePanel _theBalancePanel) {
		this._theBalancePanel = _theBalancePanel;
	}

	public MyWeightsPanel get_theWeightsPanel() {
		return _theWeightsPanel;
	}

	public void set_theWeightsPanel(MyWeightsPanel _theWeightsPanel) {
		this._theWeightsPanel = _theWeightsPanel;
	}

}// end of class definition

// MyAeroWing theWing = new MyAeroWing(
// "My test wing", // name
// "A toy wing to check the implementation", // description
// 4.0, 0.0, -1.5, // Wing apex (x,y,z)-coordinates in construction axes
// Math.toRadians(3.2) // rigging angle
// );

// ------------------------------------------------------------------------------
// FILE WING TEST
// String WING_FILE_NAME = "wing_test.xml";
// String wingFilePath = "test" + File.separator + WING_FILE_NAME;
// File wingFile = new File(wingFilePath);
// System.out.println("ADOPT_GUI :: reading file " +
// wingFile.getAbsolutePath());

// MyAeroWing theWing = new MyAeroWing(
// wingFile,
// "My test wing", // name
// "A toy wing to check the implementation", // description
// 4.0, 0.0, -1.5, // Wing apex (x,y,z)-coordinates in construction axes
// Math.toRadians(3.2) // rigging angle
// );
// ---------------------------------------------------------
