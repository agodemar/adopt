package it.unina.adopt.main;

import java.io.File;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class MyCommandLineOptions {
	
	@Option(name = "-ng", aliases = {"--no-gui" }, required = false,
			usage = "do not use GUI but use the program in batch mode")
	private boolean _useGUI = false;

	@Option(name = "-i", aliases = { "--input" }, required = false,
			usage = "input file with two matrices")
	private File _source;

	@Option(name = "-sbadm", aliases = {"--sandbox-adm" }, required = false,
			usage = "use Sandbox test code by Agostino De Marco")
	private boolean _useSandbox_ADM = false;

	@Option(name = "-sbla", aliases = {"--sandbox-la" }, required = false,
			usage = "use SandBox test code by LA")
	private boolean _useSandbox_LA = false;
	
	private boolean _errorFree = false;

	public MyCommandLineOptions(String... args) {
		CmdLineParser parser = new CmdLineParser(this);
		parser.setUsageWidth(80);
		try {
			parser.parseArgument(args);

//			if (!getSource().isFile()) {
//				throw new CmdLineException(parser,
//						"--input is no valid input file.");
//			}
			_errorFree = true;
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			parser.printUsage(System.err);
		}
	}

	/**
	 * Returns whether the parameters could be parsed without an
	 * error.
	 *
	 * @return true if no error occurred.
	 */
	public boolean isErrorFree() {
		return _errorFree;
	}

	/**
	 * Returns the source file.
	 *
	 * @return The source file.
	 */
	public File getSource() {
		return _source;
	}
	
	public boolean getUseGUI() {
		return _useGUI;
	}

	public boolean getUseSandboxADM() {
		return _useSandbox_ADM;
	}

	public boolean getUseSandboxLA() {
		return _useSandbox_LA;
	}

}// end-of class