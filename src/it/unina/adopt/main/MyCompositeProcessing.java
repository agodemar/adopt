package it.unina.adopt.main;

import java.awt.Frame;
import java.awt.Panel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;

import it.unina.adopt.test.igeoaddons.MyPAppletIGeo;
import processing.core.PApplet;

public class MyCompositeProcessing extends Composite {
	
	private PApplet _thePAppletContainer = null;

	public MyCompositeProcessing(Composite parent) {
		super(parent, SWT.NONE);
		
		// the outer grid layout
		GridLayout layoutCtr = new GridLayout(
				1,     // num. columns 
				true   // make columns equal
				);
		GridData gridDataCtr = new GridData(
				SWT.FILL, // horizontal alignment 
				SWT.FILL, // vertical alignment
				true,     // grab excess horizontal space
				false     // grab excess vertical space
				);
		
		// returns an array of monitors attached to device and 0 fetches first one.
		Monitor monitor = Display.getDefault().getMonitors()[0];
		// set x- and y- sizes as percentages of screen lengths
		gridDataCtr.widthHint  = (int)(0.75*monitor.getBounds().width); // 1000;
		gridDataCtr.heightHint = (int)(0.75*monitor.getBounds().height); // 600
		gridDataCtr.grabExcessHorizontalSpace = true;
		gridDataCtr.horizontalAlignment = GridData.FILL;
		gridDataCtr.grabExcessVerticalSpace = true;
		gridDataCtr.verticalAlignment = GridData.FILL;
		this.setLayout(layoutCtr);
		this.setLayoutData(gridDataCtr);

//		Button b1 = new Button(this,SWT.HORIZONTAL);
//		b1.setText("Pippo 1");
//		b1.setLayoutData(gridDataCtr);
//		Button b2 = new Button(this,SWT.BORDER_SOLID);
//		b2.setText("Pippo 3333");
//		b2.setLayoutData(gridDataCtr);
		
		// the Processing sketch
		
		final Composite compositeSketch = new Composite(this, SWT.EMBEDDED);
        final Frame frame = SWT_AWT.new_Frame(compositeSketch);
        //frame.setLayout((LayoutManager) new BorderLayout( ));

        // create the sketch as a MyPApplet object
        _thePAppletContainer = new MyPAppletIGeo(gridDataCtr.widthHint,gridDataCtr.heightHint);
        
        // associate the sketch to the application member variable theSketch
        ADOPT_GUI.getApp().theSketch = _thePAppletContainer;
        
        // put all into a Panel
        final Panel panel = new Panel();
/*
 * TODO: fix for Processing 3
 * 
        panel.add(_thePAppletContainer);
        frame.add(panel);
        
        // link the Frame to sketch.frame 
        _thePAppletContainer.frame = frame;
        _thePAppletContainer.frame.setResizable(true);
        
		// important to call this whenever embedding a PApplet.
		// It ensures that the animation thread is started and
		// that other internal variables are properly set.
        _thePAppletContainer.init();
		// Set the frame size based on the sketch size.
		frame.pack( );
		// And make the frame visible.
		frame.setVisible(true);

        compositeSketch.setLayoutData(gridDataCtr);
        
        // test resize
//        _thePAppletContainer.frame.setSize(200, 400);
//        _thePAppletContainer.redraw();
		
        // Composite pack (SWT)
		this.pack();
*/
		//----------------------------------------------------------
		// EVENTS
		
//		this.addListener(SWT.Resize,  new Listener () {
//			@Override
//			public void handleEvent(Event arg0) {
//		        Rectangle rect = getClientArea ();
//		        System.out.println(rect);
//				
//			}
//		    });
		
//		// http://processing.org/discourse/beta/num_1193490050.html
//		panel.addComponentListener(
//				new ComponentAdapter() { 
//					public void componentResized(ComponentEvent e) { 
//						if(e.getSource()==panel) { 
//							//redraw();
//							//							ADOPT_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
//							//									"The ADOpT | MyPApplet: resized.\n"
//							//									);
//							System.out.println("Rect: " + getClientArea ());
//						} 
//					} 
//				}
//				);
		
	} // end-of-constructor

	public PApplet get_thePAppletContainer() {
		return _thePAppletContainer;
	}

	public void release_thePAppletContainer() {
		_thePAppletContainer = null;
	}

	public boolean isThePAppletContainerReleased() {
		return (_thePAppletContainer == null);
	}
	
	
} // end-of-class
