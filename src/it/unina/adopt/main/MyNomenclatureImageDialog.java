package it.unina.adopt.main;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class MyNomenclatureImageDialog {
	
	// TO DO: see if a scrollable Composite is feasible
	// TO DO: see if a non-modal dialog is feasible
	
	public 	MyNomenclatureImageDialog(Shell parent, String NomenclatureFuselage){
		
				// Create the ScrolledComposite to scroll horizontally and vertically
				ScrolledComposite scrolledComposite = new ScrolledComposite(
						parent, SWT.H_SCROLL | SWT.V_SCROLL 
						);	 	
			
				// Create a child composite to hold the controls
//			    Composite container = new Composite(_parent, SWT.PUSH);
			    Composite container = new Composite(scrolledComposite, SWT.PUSH);
			    container.setLayout(new FillLayout());
			    
			    Image image = new Image(
//			    		_parent.getDisplay(),
			    		Display.getDefault(),
						this.getClass().getResourceAsStream(NomenclatureFuselage)
						);

				Label labelNomenclature = new Label(container,SWT.NONE);
				labelNomenclature.setImage(image);	
				
				  // Set the child as the scrolled content of the ScrolledComposite
			    scrolledComposite.setContent(container);

			    // Set the minimum size
			    // to make scroll bars visible when container is resized
				Monitor monitor = Display.getDefault().getMonitors()[0];
			    scrolledComposite.setMinSize(
			    		(int)( 0.55*monitor.getBounds().width  ), 
			    		(int)( 0.55*monitor.getBounds().height )
			    	);			    		
			    // Expand both horizontally and vertically
			    scrolledComposite.setExpandHorizontal(true);
			    scrolledComposite.setExpandVertical(true);
			    
			    container.pack();
			    scrolledComposite.pack();
				parent.pack();
			    parent.open();	 
			   
				while (!parent.isDisposed()) {
					if (!Display.getCurrent().readAndDispatch()) {
						Display.getCurrent().sleep();
					}
				}
		
	

	}


}// end-of class
