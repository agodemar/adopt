package it.unina.adopt.main;

import it.unina.adopt.GUI.tree.IMyProjectTreeDeltaListener;
import it.unina.adopt.GUI.tree.MyProjectTreeDeltaEvent;

public class MyNullAircraftDeltaListener implements IMyProjectTreeDeltaListener {
	
	protected static MyNullAircraftDeltaListener soleInstance = new MyNullAircraftDeltaListener();
	
	public static MyNullAircraftDeltaListener getSoleInstance() {
		return soleInstance;
	}
	
	/*
	 * @see IMyProjectTreeDeltaListener#add(MyProjectTreeDeltaEvent)
	 */
	public void add(MyProjectTreeDeltaEvent event) {}

	/*
	 * @see IMyProjectTreeDeltaListener#remove(MyProjectTreeDeltaEvent)
	 */
	public void remove(MyProjectTreeDeltaEvent event) {}
	
}
