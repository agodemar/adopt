package it.unina.adopt.test.igeoaddons;

import igeo.IG;
import processing.core.PApplet;

public class MyPAppletIGeo extends PApplet {

	private static final long serialVersionUID = 1L;
	private int _width, _height;
	private MyReferenceFrame _theReferenceFrame;
	
	public MyPAppletIGeo(int w, int h) {
		this._width  = w;
		this._height = h;
	} // end-of-constructor
	
	public void setup( ) {

		// size always goes first!

		size(
				this._width, this._height, 
				IG.GL
				);

		if (frame != null) {
			frame.setResizable(true);
		}

		initVariables( );

		// Prevent thread from starving everything else.
		noLoop( );
		redraw( );
	}
	
	public void draw( ) {
		// Drawing code goes here. (in continuous mode)
	}
	
	public void initVariables( ) {
		// TODO: initialize all variables properly 
		
		_theReferenceFrame = new MyReferenceFrame();
		
	}
	
	public void redraw( ) {
		// Drawing code goes here. (in non-continuous mode)
		
//		_theReferenceFrame.draw(null);
		
	}
	
} // end-of-class