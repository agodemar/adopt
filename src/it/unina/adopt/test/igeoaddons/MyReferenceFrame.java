package it.unina.adopt.test.igeoaddons;

import java.awt.Color;

import igeo.IEntityParameter;
import igeo.IParameter;
import igeo.IParameterObject;
import igeo.IText;
import igeo.IVec;

public class MyReferenceFrame extends IParameterObject implements IEntityParameter {
	
	private boolean visible = true;
	private float _lenX = (float) 1.0;
	private float _lenY = (float) 1.0;
	private float _lenZ = (float) 1.0;
	private String _labelX = "X";
	private String _labelY = "Y";
	private String _labelZ = "Z";
	private double _fontSize = 0.3;
	private IVec _vAxisX, _vAxisY, _vAxisZ;
	private IText _itextX, _itextY, _itextZ;

	public MyReferenceFrame (
			float lenx, float leny, float lenz, 
			String sx, String sy, String sz, 
			double fontsz
			) {
		super();
		_lenX = lenx; _lenY = leny; _lenZ = lenz;
		_labelX = sx; _labelY = sy; _labelZ = sz;
		_fontSize = fontsz;
		init();
	} // end-of-constructor
	public MyReferenceFrame() {
		super();
		init();
	} // end-of-constructor
	
	private void init() {
		
		_vAxisX = new IVec(_lenX,    0,    0);
		_vAxisY = new IVec(   0, _lenY,    0);
		_vAxisZ = new IVec(   0,    0, _lenZ);
		// Axes labels
		_itextX = new IText(
				_labelX,            // string 
				0.3,               // font size (double)
				1.02*_vAxisX.x, 0.0, 0.0,  // X, Y, Z (double)
				1.0, 0.0, 0.0,     // textDirX, textDirY, textDirZ
				0.0, 0.0, 1.0      // textUpDirX, textUpDirY, textUpDirZ
				);
		_itextY = new IText(
				_labelY,            // string 
				0.3,               // font size (double)
				0.0, 1.02*_vAxisY.y, 0.0,  // X, Y, Z (double)
				1.0, 0.0, 0.0,     // textDirX, textDirY, textDirZ
				0.0, 0.0, 1.0      // textUpDirX, textUpDirY, textUpDirZ
				);
		_itextZ = new IText(
				_labelZ,            // string 
				0.3,               // font size (double)
				0.0, 0.0, 1.02*_vAxisZ.z,  // X, Y, Z (double)
				1.0, 0.0, 0.0,     // textDirX, textDirY, textDirZ
				0.0, 0.0, 1.0      // textUpDirX, textUpDirY, textUpDirZ
				);

		// show basic objects
		if (_vAxisX != null) _vAxisX.show().clr(Color.RED).size(0.5).weight(5.0);
		if (_vAxisY != null) _vAxisY.show().clr(Color.GREEN).size(0.5).weight(5.0);
		if (_vAxisZ != null) _vAxisZ.show().clr(Color.BLUE).size(0.5).weight(5.0);
		if (_itextX != null) _itextX.show().clr(Color.RED);
		if (_itextY != null) _itextY.show().clr(Color.GREEN);
		if (_itextZ != null) _itextZ.show().clr(Color.BLUE);
	}
	@Override
	public IParameter get() {
		// TODO Auto-generated method stub
		return null;
	}

} // end-of-class