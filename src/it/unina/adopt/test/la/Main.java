package it.unina.adopt.test.la;

import java.util.Arrays;

import jmatrix.Matrix;
import standaloneutils.MyArrayUtils;

public class Main {

	public static void main(String[] args) {
		
		double[] a = MyArrayUtils.linspace(-10., -10., 10);
		double[] b = Matrix.linspace(-10., -10., 10).data;
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(b));
		
	}
	
}
