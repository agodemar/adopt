package it.unina.adopt.test.la;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;

import it.unina.adopt.GUI.tabpanels.MyPanel;

public class MySWTTest extends MyPanel {
	private Text text;
	private Combo combo;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MySWTTest(Composite parent, CTabItem tab, int style) {
		super(parent, tab, style);

	}

	public MySWTTest(Composite parent, int style) {
		super(parent, style);
		setLayout(new FormLayout());
		
		Label lblHdfGroupPath = new Label(this, SWT.NONE);
		FormData fd_lblHdfGroupPath = new FormData();
		fd_lblHdfGroupPath.right = new FormAttachment(0, 170);
		fd_lblHdfGroupPath.top = new FormAttachment(0, 26);
		fd_lblHdfGroupPath.left = new FormAttachment(0, 32);
		lblHdfGroupPath.setLayoutData(fd_lblHdfGroupPath);
		lblHdfGroupPath.setText("HDF group path");
		
		text = new Text(this, SWT.BORDER);
		FormData fd_text = new FormData();
		fd_text.right = new FormAttachment(0, 468);
		fd_text.top = new FormAttachment(0, 47);
		fd_text.left = new FormAttachment(0, 32);
		text.setLayoutData(fd_text);
		
		combo = new Combo(this, SWT.NONE);
		FormData fd_combo = new FormData();
		fd_combo.right = new FormAttachment(0, 123);
		fd_combo.top = new FormAttachment(0, 110);
		fd_combo.left = new FormAttachment(0, 32);
		combo.setLayoutData(fd_combo);
		combo.setItems(new String[] {"1D", "2D", "3D"});
		
		Label lblRank = new Label(this, SWT.NONE);
		FormData fd_lblRank = new FormData();
		fd_lblRank.right = new FormAttachment(0, 87);
		fd_lblRank.top = new FormAttachment(0, 89);
		fd_lblRank.left = new FormAttachment(0, 32);
		lblRank.setLayoutData(fd_lblRank);
		lblRank.setText("Rank");
		
		Button btnInterpolate = new Button(this, SWT.NONE);
		FormData fd_btnInterpolate = new FormData();
		fd_btnInterpolate.right = new FormAttachment(0, 107);
		fd_btnInterpolate.top = new FormAttachment(0, 156);
		fd_btnInterpolate.left = new FormAttachment(0, 32);
		btnInterpolate.setLayoutData(fd_btnInterpolate);
		btnInterpolate.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
			
		});
		btnInterpolate.setText("Interpolate");
		
		Button btnAircraft = new Button(this, SWT.RADIO);
		FormData fd_btnAircraft = new FormData();
		fd_btnAircraft.right = new FormAttachment(0, 90);
		fd_btnAircraft.top = new FormAttachment(0);
		fd_btnAircraft.left = new FormAttachment(0);
		btnAircraft.setLayoutData(fd_btnAircraft);
		btnAircraft.setText("Aircraft 1");
		
		TreeViewer treeViewer = new TreeViewer(this, SWT.BORDER);
		treeViewer.setColumnProperties(new String[] {"Fuselage", "Wing"});
		Tree tree = treeViewer.getTree();
		FormData fd_tree = new FormData();
		fd_tree.bottom = new FormAttachment(0, 201);
		fd_tree.right = new FormAttachment(0, 315);
		fd_tree.top = new FormAttachment(0, 74);
		fd_tree.left = new FormAttachment(0, 168);
		tree.setLayoutData(fd_tree);

	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	public Combo getCombo() {
		return combo;
	}
	public Text getText() {
		return text;
	}
}
