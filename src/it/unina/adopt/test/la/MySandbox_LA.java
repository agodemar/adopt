package it.unina.adopt.test.la;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import aircraft.OperatingConditions;
import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import configuration.enumerations.AnalysisTypeEnum;
import it.unina.adopt.core.GlobalData;
import it.unina.adopt.utilities.MyXMLReader;
import it.unina.adopt.utilities.write.MyChartWriter;
import it.unina.adopt.utilities.write.MyDataWriter;
import it.unina.adopt.utilities.write.MyWriteUtils;
import standaloneutils.MyXLSWriteUtils;


public class MySandbox_LA {

	private final String[] aircraftName = 
			//					{"ATR72",
			//						"F100",
			//						"A320",
			//					"B747_100B"};

			//		{"ATR72"};
		{"F100"};
	//		{"A320"};
	//		{"B747_100B"};

	private final String[] exportFileName = new String[aircraftName.length];
	private final String[] importFileName = new String[aircraftName.length];

	private final String comparisonFileName = "REPORT_ALL";
	private String exportFile, importFile, comparisonFile;

	private boolean initializeComparison;
	private int idxAircraft = 0;

	private ACAnalysisManager _theAnalysis;
	private MyDataWriter _theWriteUtilities;

	private long _stopTime, _elapsedTime, _startTime;

	private MyXMLReader _theReadUtilities;
	private MyChartWriter _theWriteCharts;

	public MySandbox_LA() {

		_startTime = System.currentTimeMillis();

		GlobalData.getLOG().info("Starting sandbox LA");

		System.out.println("---------------------------");
		System.out.println("MySandbox_LA ...");

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		System.out.println("Started at: " + dateFormat.format(Calendar.getInstance().getTime()));

		comparisonFile = MyConfiguration.runsDirectory + comparisonFileName;
		initializeComparison = false;

		// Analyze each aircraft
		for (int i=0; i < aircraftName.length; i++){

			System.out.println("------------- AIRCRAFT " + aircraftName[i] + " -------------");

			exportFileName[i] = "REPORT_" + aircraftName[i];
			importFileName[i] = "REPORT_" + aircraftName[i];
			importFile = MyConfiguration.inputDirectory + importFileName[i];
			exportFile = MyConfiguration.outputDirectory + exportFileName[i];

			importUsingCustomXML(aircraftName[i]);
			//			importUsingSerialization(AIRCRAFT_NAME[i]);

			///////////////////////////////////////////
			// Analysis cases
			///////////////////////////////////////////
			_theAnalysis = new ACAnalysisManager(
					GlobalData.getTheCurrentOperatingConditions(),
					GlobalData.getTheCurrentAircraft(),
					AnalysisTypeEnum.AERODYNAMIC, 
					AnalysisTypeEnum.BALANCE,
					AnalysisTypeEnum.WEIGHTS,
					AnalysisTypeEnum.PERFORMANCES,
					AnalysisTypeEnum.COSTS);

			_theAnalysis.doAnalysis(GlobalData.getTheCurrentAircraft(),
					AnalysisTypeEnum.AERODYNAMIC, 
					AnalysisTypeEnum.BALANCE,
					AnalysisTypeEnum.WEIGHTS,
					AnalysisTypeEnum.PERFORMANCES,
					AnalysisTypeEnum.COSTS);
			
			Aircraft aircraft = GlobalData.getTheCurrentAircraft();
			
//			double range2 = MyRangeCalc.calculateRangeBreguetJetSFCJ(0.7, 0.4, 
//					aircraft.get_wing().get_surface().doubleValue(SI.SQUARE_METRE), 0.45, 0.035, 
//					aircraft.get_weights().get_MTOW().doubleValue(SI.NEWTON), 
//					aircraft.get_weights().get_MZFW().doubleValue(SI.NEWTON));
//			
//			double range3 = MyRangeCalc.calculateRangeAtConstantSpeedAndAltitudeJet(
//					aircraft.get_weights().get_MTOW().doubleValue(SI.NEWTON), 
//					aircraft.get_weights().get_MZFW().doubleValue(SI.NEWTON), 
//					240.,//aircraft.get_performances().get_vOptimumCruise().doubleValue(SI.METERS_PER_SECOND), 
//					9500., 
//					aircraft.get_theAerodynamics().get_cD0().doubleValue(),
//					aircraft.get_theAerodynamics().get_oswald().doubleValue(), 
//					aircraft.get_wing().get_surface().doubleValue(SI.SQUARE_METRE), 
//					aircraft.get_wing().get_aspectRatio().doubleValue(), 
//					aircraft.get_wing().get_sweepHalfChordEq().doubleValue(SI.RADIAN), 
//					0.15,//aircraft.get_wing().get_thicknessMax(), 
//					aircraft.get_wing().get_theAirfoilsList().get(0).get_type(), 
//					61000., //aircraft.get_powerPlant().get_engineList().get(0).get_t0().doubleValue(SI.NEWTON), 
//					aircraft.get_powerPlant().get_engineNumber().intValue(),
//					0.9, 
//					3.,//aircraft.get_powerPlant().get_engineList().get(0).get_BPR(), 
//					FlightConditionEnum.CRUISE, true);
//
//			double range1 = MyRangeCalc.calculateRangeAtConstantSpeedAndLiftCoefficientJet(
//					aircraft.get_weights().get_MTOW().doubleValue(SI.NEWTON), 
//					aircraft.get_weights().get_MZFW().doubleValue(SI.NEWTON), 
//					240.,//aircraft.get_performances().get_vOptimumCruise().doubleValue(SI.METERS_PER_SECOND), 
//					aircraft.get_performances().get_cruiseCL().doubleValue(), 
//					aircraft.get_theAerodynamics().get_cD0().doubleValue(),
//					aircraft.get_theAerodynamics().get_oswald().doubleValue(), 
//					aircraft.get_wing().get_surface().doubleValue(SI.SQUARE_METRE), 
//					aircraft.get_wing().get_aspectRatio().doubleValue(), 
//					aircraft.get_wing().get_sweepHalfChordEq().doubleValue(SI.RADIAN), 
//					0.15,//aircraft.get_wing().get_thicknessMax(), 
//					aircraft.get_wing().get_theAirfoilsList().get(0).get_type(), 
//					61000.,//aircraft.get_powerPlant().get_engineList().get(0).get_t0().doubleValue(SI.NEWTON), 
//					aircraft.get_powerPlant().get_engineNumber().intValue(),
//					0.9, 
//					3.,//aircraft.get_powerPlant().get_engineList().get(0).get_BPR(), 
//					FlightConditionEnum.CRUISE, true);
//			
//			MyRangeCalc.calculateRangeAtConstantLiftCoefficientAndAltitudeJet(aircraft.get_weights().get_MTOW().doubleValue(SI.NEWTON), 
//					aircraft.get_weights().get_MZFW().doubleValue(SI.NEWTON), 
//					aircraft.get_performances().get_cruiseCL().doubleValue(),
//					9500.,
//					aircraft.get_theAerodynamics().get_cD0().doubleValue(),
//					aircraft.get_theAerodynamics().get_oswald().doubleValue(), 
//					aircraft.get_wing().get_surface().doubleValue(SI.SQUARE_METRE), 
//					aircraft.get_wing().get_aspectRatio().doubleValue(), 
//					aircraft.get_wing().get_sweepHalfChordEq().doubleValue(SI.RADIAN), 
//					0.15,//aircraft.get_wing().get_thicknessMax(), 
//					aircraft.get_wing().get_theAirfoilsList().get(0).get_type(), 
//					61000.,//aircraft.get_powerPlant().get_engineList().get(0).get_t0().doubleValue(SI.NEWTON), 
//					aircraft.get_powerPlant().get_engineNumber().intValue(),
//					0.9, 
//					3.,//aircraft.get_powerPlant().get_engineList().get(0).get_BPR(), 
//					FlightConditionEnum.CRUISE, true);
			
			GlobalData.setTheCurrentAnalysis(_theAnalysis);

			_theWriteUtilities = new MyDataWriter(
					GlobalData.getTheCurrentOperatingConditions(),
					GlobalData.getTheCurrentAircraft(), _theAnalysis);

			_theWriteCharts = new MyChartWriter(GlobalData.getTheCurrentAircraft());
			_theWriteCharts.createCharts();

			// +++++++++++++++++++++++++++++++++++++++++++++++
			// STATIC FUNCTIONS - TO BE CALLED BEFORE WRITING CUSTOM XML FILES
			MyWriteUtils.buildXmlTree();

			// Export everything to file
			_theWriteUtilities.exportToXMLfile(exportFile + ".xml");
			_theWriteUtilities.exportToXLSfile(exportFile + ".xls");
//			exportComparisonToXLS(exportFile, comparisonFile);
			//
			//			//			// Serialize aircraft and op. conditions
			//			MyWriteUtils.serializeObject(GlobalData.getTheCurrentAircraft(), AIRCRAFT_NAME[i]);
			//			MyWriteUtils.serializeObject(GlobalData.getTheCurrentOperatingConditions(), AIRCRAFT_NAME[i] + "ops");

			// Build aircraft CAD
			//			MyAircraftBuilder aircraftBuilder = new MyAircraftBuilder();
			//			aircraftBuilder.buildAndWriteCAD(
			//					GlobalData.getTheCurrentAircraft(), 
			//					MyStaticObjects.cadDirectory + GlobalData.getTheCurrentAircraft().get_name());

			idxAircraft = i;

		}

		//		System.out.println("---------------- TESTING REFERENCE ------------------");
		//		MyTest_LA_05 test = new MyTest_LA_05(GlobalData.getTheCurrentAircraft());
		//		test.printSomething();
		//		GlobalData.getTheCurrentAircraft().get_wing().set_aspectRatio(99.);
		//		test.printSomething();

		System.out.println("Finished at: " + dateFormat.format(Calendar.getInstance().getTime()));
		_stopTime = System.currentTimeMillis();
		_elapsedTime = _stopTime - _startTime;
		System.out.println("Wall time: " + (double) _elapsedTime/1000 + " s" );
		System.out.println("... end MySandbox_LA");

	}

	private void importUsingSerialization(String acName) {

		OperatingConditions operatingConditions = new OperatingConditions();

		///////////////////////////////////////////////////
		// Import parameters from file of some components
		///////////////////////////////////////////////////
		//					_theOperatingConditions = (MyOperatingConditions) MyReadUtils.deserializeObject(_theOperatingConditions, MyStaticObjects.dataDirectory + AIRCRAFT_NAME[i] + "ops");
		//FIXME change directory
//		MyAircraft aircraft = (MyAircraft) MyXMLReader.deserializeObject(new MyAircraft(), 
//				EngineDatabaseReader.getDatabaseDirectory() + acName);

//		aircraft.set_name(acName);
//
//		GlobalData.set_theCurrentOperatingConditions(operatingConditions);
//		GlobalData.setTheCurrentAircraftInMemory(aircraft);
//		GlobalData.get_theAircraftList().add(aircraft);

	}

	private void importUsingCustomXML(String acName) {

		System.out.println("\n ----- STARTED IMPORTING DATA FROM THE XML CUSTOM FILE -----\n");

		OperatingConditions operatingConditions = new OperatingConditions();
		GlobalData.set_theCurrentOperatingConditions(operatingConditions);

		// Initialize Aircraft with default parameters
		Aircraft aircraft = Aircraft.createDefaultAircraft();

		aircraft.set_name(acName);
		GlobalData.setTheCurrentAircraftInMemory(aircraft);
		GlobalData.get_theAircraftList().add(aircraft);

		// +++++++++++++++++++++++++++++++++++++++++++++++
		// STATIC FUNCTIONS - TO BE CALLED BEFORE EVERYTHING ELSE
		MyWriteUtils.buildXmlTree();

		MyXMLReader _theReadUtilities = new MyXMLReader(aircraft, operatingConditions, importFile);
		_theReadUtilities.importAircraftAndOperatingConditions(
				GlobalData.getTheCurrentAircraft(),
				GlobalData.getTheCurrentOperatingConditions(),
				importFile);

	}

	public void exportComparisonToXLS(String fileToBeAddedWithPathNoExt, String comparisonFileWithPathNoExt) {

		// Initialize comparison xls file with the first aircraft's xls file
		if (initializeComparison == false) {

			File f = new File(comparisonFileWithPathNoExt + ".xls");
			if(f.exists()) {
				f.delete();
			}

			try {
				Files.copy(new File(fileToBeAddedWithPathNoExt + ".xls").toPath(), 
						new File(comparisonFileWithPathNoExt + ".xls").toPath());
				initializeComparison = true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		MyXLSWriteUtils.compareXLSs(fileToBeAddedWithPathNoExt, comparisonFileWithPathNoExt, idxAircraft+2);
	}

	public ACAnalysisManager get_theAnalysis() {
		return _theAnalysis;
	}


	public void set_theAnalysis(ACAnalysisManager _theAnalysis) {
		this._theAnalysis = _theAnalysis;
	}

}


////////////////////////////////////////////////////////////////////////////////////////////

//		System.out.println("---------------------------");
//		System.out.println("testing jxls ... MyTest_LA_04 ...");
//		
//		try {
//			
//			MyTest_LA_04 myTest_LA_04 = new MyTest_LA_04();
//			
//		} catch (InvalidFormatException | IOException | SAXException e) {
//			e.printStackTrace();
//		}
//
//		System.out.println("... end MyTest_LA_04");


//System.out.println("---------------------------");
//System.out.println("testing MyTest_LA_01 ...");
//MyTest_LA_01 myTest_LA_01 = new MyTest_LA_01();
//myTest_LA_01.calculateCd0Fus(1.225, 200.0, 1.775*Math.pow(10, -5));
//System.out.println("... end MyTest_LA_01");

//// Test MyAeroCalculator0: aerodynamic calculations.
//System.out.println("---------------------------");
//System.out.println("testing MyAeroCalculator0 ...");
//// Try-catch commented out because if something goes wrong eclipse shows the line to check for errors.
//// With try-catch there are no hints.
////try {
//	MyAnalysis myAnalysis = new MyAnalysis();
//	myAnalysis.calculate();
//	myAnalysis.report();
//	System.out.println("Test MyAeroCalculator0 calculations done.");
//	
//	MyUtilities.logToGUI("FILE SAVED!");
//
//	
////}
////catch (Exception exc) {
////	System.out.println(exc);
////	System.out.println("Test ADM 03 went wrong!");
////}
//System.out.println("... end of MyAeroCalculator0 test");
//System.out.println("---------------------------");
