package it.unina.adopt.test.la;

import java.awt.Frame;
import java.awt.Panel;
import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;

import processing.core.PApplet;

public class MyTestComposite extends Composite {

//	private File _currentDir = new File(System.getProperty("user.dir"));
	
	private File _fileIn;
	
	private PApplet _theSketch = null;
	
	public MyTestComposite(Composite parent)
	{
		super(parent, SWT.NONE);
		
		// the outer grid layout
		GridLayout layoutCtr = new GridLayout(
				1,     // num. columns 
				true   // make columns equal
				);
		GridData gridDataCtr = new GridData(
				SWT.FILL, // horizontal alignment 
				SWT.FILL, // vertical alignment
				true,     // grab excess horizontal space
				false     // grab excess vertical space
				);
		
		// returns an array of monitors attached to device and 0 fetches first one.
		Monitor monitor = Display.getDefault().getMonitors()[0];
		// set x- and y- sizes as percentages of screen lengths
		gridDataCtr.widthHint  = (int)(0.75*monitor.getBounds().width); // 1000;
		gridDataCtr.heightHint = (int)(0.75*monitor.getBounds().height); // 600
		gridDataCtr.grabExcessHorizontalSpace = true;
		gridDataCtr.horizontalAlignment = GridData.FILL;
		gridDataCtr.grabExcessVerticalSpace = true;
		gridDataCtr.verticalAlignment = GridData.FILL;
		this.setLayout(layoutCtr);
		this.setLayoutData(gridDataCtr);

		// the Processing sketch
		
		final Composite compositeSketch = new Composite(this, SWT.EMBEDDED);
        final Frame frame = SWT_AWT.new_Frame(compositeSketch);
        //frame.setLayout((LayoutManager) new BorderLayout( ));

        // create the sketch as a MyPApplet object
//        _theSketch = new MyPAppletIGeo(gridDataCtr.widthHint,gridDataCtr.heightHint);
        
        // associate the sketch to the application member variable theSketch
//        ADOPT_GUI.getApp().theSketch = _theSketch;
        
        // put all into a Panel
        final Panel panel = new Panel();
/*
 * TODO: fix for Processing 3
 * 
        panel.add(_theSketch);
        frame.add(panel);
        
        // link the Frame to sketch.frame 
        _theSketch.frame = frame;
        _theSketch.frame.setResizable(true);
        
		// important to call this whenever embedding a PApplet.
		// It ensures that the animation thread is started and
		// that other internal variables are properly set.
        _theSketch.init();
		// Set the frame size based on the sketch size.
		frame.pack( );
		// And make the frame visible.
		frame.setVisible(true);

        compositeSketch.setLayoutData(gridDataCtr);
        
        // test resize
//        _theSketch.frame.setSize(200, 400);
//        _theSketch.redraw();
		
        // Composite pack (SWT)
		this.pack();
*/
	} // end-of-constructor

} // end-of class


//=================================================================
//_fileIn = new File(
//		System.getProperty("user.dir") + "/test/" + "REPORT_ATR72.xls"
//		);
//
///** Opening Excel File From Java **/
//try {
//	
//	// The Excel file will be opened with the desktop default
//	// application, e.g. MS Excel or LibreOffice
//    Desktop.getDesktop().open( _fileIn );
//    
//} catch (IOException e) {
//    e.printStackTrace();
//}
//OK opens an .xls with MS Excel
//=================================================================
