package it.unina.adopt.test.la;


//private 
public class MyTest_LA_01 {

//	private MyAeroFuselage _theFuselage;
//	private MyAeroWing _theWing;
//	private MyOperatingConditions _myOperatingConditions;
//
//	public MyTest_LA_01(){
//
//		_theFuselage = new MyAeroFuselage(
//				"My test fuselage", // name
//				"A toy fuselage to check the implementation", // description
//				0.0, 0.0, 0.0 // Fuselage apex (x,y,z)-coordinates in construction axes
//				);
//
//		_theWing = new MyAeroWing(
//				"My test wing", // name
//				"A toy wing to check the implementation", // description
//				4.0, 0.0, -1.5, // Wing apex (x,y,z)-coordinates in construction axes
//				Math.toRadians(3.2) // rigging angle
//				);
//
//		_myOperatingConditions = new MyOperatingConditions();
//		
//	}
//
//	// To compute base and upsweep drag insert h, l, db (in this order) after mu
//	public double calculateCd0Fus(double rho, double V, double mu, Double... doubles){
//
//		double re, db, h, l;
//		db = h = l = 0;
//		
//		Double db_h_l[] = doubles;
//		if (doubles.length != 0) {
//			h = db_h_l[0]; // as defined at page 67 Behind ADAS pdf
//			l = db_h_l[1]; // as defined at page 67 Behind ADAS pdf
//			db = db_h_l[2]; // base diameter of fuselage tailcone
//		}
//
//		double df = _theFuselage.get_sectionCylinderHeight().getEstimatedValue();
//		double lf = _theFuselage.get_len_F().getEstimatedValue();
//		double s = _theWing.get_surface().getEstimatedValue();
//		double kff = _theFuselage.get_formFactor();
//
//		re = _myOperatingConditions.calculateRe(lf);
//		
//		// page 409 torenbeek 2013
//		double sFront = (Math.PI/4) * Math.pow(_theFuselage.get_sectionWidth().getEstimatedValue(),2); // CANNOT FIND FUSELAGE HEIGHT in MyAeroFuselage!!
//		double sWet = sFront*4*(_theFuselage.get_lambda_F() - 1.30); 
//
//		// Behind ADAS pdf, beginning at page 63. 
//		// Assume turbulent flow over the fuselage
//		double xTransition = 0.0;
//
//		if (_myOperatingConditions.calculateReCutOff(lf, _theFuselage.get_roughness().getEstimatedValue()) < re) {
//			re = _myOperatingConditions.calculateReCutOff(lf,_theFuselage.get_roughness().getEstimatedValue());
//		}
//		
//		double cfLam = 1.328/Math.sqrt(re);
//		double cfTurb = 0.455/Math.pow((Math.log(re)/Math.log(10)),2.58);
//		double cf = cfLam*xTransition + cfTurb*(1-xTransition);
//		double cd0FusParasite = kff*cf*sWet/s;
//		System.out.println("################ re, width, sfront, swet, cf lam, cf turb, kff, cd0fuspar " +
//				re + " " + _theFuselage.get_sectionWidth().getEstimatedValue() + " " + sFront + " " 
//				+ sWet + " " + cfLam + " " + cfTurb + " " + kff + " " + cd0FusParasite);
//
//		if (doubles.length != 0){
//			// page 354 Nicolai pdf
//			double cd0FusBase = 0.029 * Math.pow(db/_theFuselage.get_sectionCylinderHeight().getEstimatedValue(),3)/Math.sqrt(cd0FusParasite);
//
//			// page 67 Behind ADAS
//			double cd0FusUpsweep = 0.075 * _theFuselage.get_sectionCylinderHeight().getEstimatedValue() * (Math.PI/s) * (h/l); 
//
//			double cd0Fus = cd0FusParasite + cd0FusBase + cd0FusUpsweep;
//			return cd0Fus;
//		}
//		else {
//			return cd0FusParasite;		
//		}
//
//	}


} // end of class
