package it.unina.adopt.test.la;

import aircraft.components.Aircraft;

public class MyTest_LA_05 {

	Aircraft ac;
	
	public MyTest_LA_05(Aircraft aircraft) {
		ac = aircraft;
	}
	
	public void printSomething() {
		System.out.println(ac.get_wing().get_aspectRatio());
	}
	
}
