package it.unina.adopt.test.la;

import javax.measure.quantity.Area;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jscience.physics.amount.Amount;

import aircraft.components.Aircraft;

@XmlRootElement(name = "ADOpT")
//@XStreamAlias("ADOpT")
//@XmlAccessorType(XmlAccessType.PROPERTY)
public class MyWriteClass {

//	@XStreamAlias("surface")
	 @XmlElement(name = "surface")
	private Amount<Area> _surface;
	 
	 Aircraft theAircraft;
	
	public MyWriteClass() {
	
	}
	
	public MyWriteClass(Aircraft aircraft) {
		theAircraft = aircraft;
		_surface = aircraft.get_wing().get_surface();
	}

	
	//	    @XmlElement(name = "prova")
	//	    public List<GameSetting> getGameSetting(){
	//	        if( gameSetting == null ){
	//	            gameSetting = new ArrayList<>();
	//	        }
	//	        return gameSetting;
	//	    }

//	@XmlElement(name = "surface")

	public void set_surface( Amount<Area> value ){
		this._surface = theAircraft.get_wing().get_surface();
	}

}
