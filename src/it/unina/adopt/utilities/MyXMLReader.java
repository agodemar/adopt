package it.unina.adopt.utilities;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import aircraft.OperatingConditions;
import aircraft.components.Aircraft;
import it.unina.adopt.core.GlobalData;
import standaloneutils.MyXMLReaderUtils;

/** 
 * Utilities for reading and importing xml file.
 * 
 * @author LA
 */
public class MyXMLReader {

	Aircraft _theAircraft;
	OperatingConditions _theOperatingConditions;

	private Document _importDoc;

	// Create XPathFactory object
	private XPathFactory _xpathFactoryImport;

	public String _xmlFileImport = "";
	public Document _parsedDoc;


	public MyXMLReader(Aircraft aircraft, OperatingConditions conditions,
			String importFileName) {
		
		_theAircraft = aircraft;
		_theOperatingConditions = conditions;
		//		_factoryImport.setSchema(null);

		_xpathFactoryImport = XPathFactory.newInstance();
		
		_xmlFileImport = importFileName;
		_importDoc = MyXMLReaderUtils.importDocument(importFileName);
	}

	public void readAllXml() {

	}

	// TODO
	/* Prototype methods for robust import
	private void readPerformances(MyPerformances performances) {

		getElementsByTagName(
				ADOPT_GUI.getTheXmlTree().getDescription(performances))
				.item(0));

		performances.set_machOptimumCruise(readNode("Optimum_Cruise_Mach_Number"));
		performances.set_machMaxCruise(readNode("Maximum_Cruise_Mach_Number"));
		performances.set_nLimit(readNode("Limit_load_factor"));
		performances.set_nLimitZFW(readNode("Limit_load_factor_at_MZFW"));
		performances.set_nUltimate(readNode("Ultimate_load_factor"));

	}

	private void readNode(String description) {

	}
	 */


	/** 
	 * This method reads all the XML file using recursion
	 * 
	 * @author LA
	 * @param doc
	 * @param xpath
	 * @param mainNode
	 * @param childNodeV
	 */
	//	public static void recursiveRead(Document doc, XPath xpath, Node mainNode, Node ... childNodeV) {
	//
	//		Node childNode;
	//
	//		if (childNodeV.length == 0) {
	//			childNodeV = new Node[1];
	//			childNodeV[0] = mainNode;
	//		}
	//
	//		for(childNode = childNodeV[0].getFirstChild();
	//				childNode != null;
	//				childNode = childNode.getNextSibling()) {
	//			if (!childNode.getFirstChild().hasChildNodes()){
	//				MyReadUtils.readAllBlocks(doc, 
	//						MyUtilities.get_objectsMap().get(mainNode.getNodeName()), 
	//						xpath, 
	//						MyReadUtils.getElementXpath((Element) childNode));
	//
	//			} else {
	//				recursiveRead(doc, xpath, mainNode, childNode);
	//			}
	//		}
	//	}


	/** 
	 * Import all REPORT.xml file using recursiveRead() method
	 * 
	 * @author LA
	 * @param xmlFile
	 */
	public void importXMLfile (String importFileName, Aircraft aircraft) {

		//		_importDoc = prepareImportDocument(IMPORT_FILE_NAME + ".xml");
		//
		//		// Create XPath object
		//		XPath xpath = _xpathFactoryImport.newXPath();
		//
		//		// Get all main nodes in xml document and put them in allNodes
		//		NodeList adopt = _importDoc.getDocumentElement().getChildNodes();
		//		Node[] allNodes = new Node[adopt.getLength()];
		//
		//		for (int i=0; i<adopt.getLength(); i++) {
		//			allNodes[i] = adopt.item(i);
		//		}
		//
		//		// (allNodes.length - 1) because Analysis should not be imported
		//		for (int i=0; i < allNodes.length-1; i++) { 
		//
		//			// TODO: rewrite this method
		//			//			MyReadUtils.recursiveRead(_importDoc, xpath, allNodes[i]);
		//
		//		}

		//		try {
		//            JAXBContext jc = JAXBContext.newInstance(MyAircraft.class);
		//            Unmarshaller unmarshaller = jc.createUnmarshaller();
		// 
		//            File file = new File(IMPORT_FILE_NAME + ".xml");
		//            aircraft = (MyAircraft) unmarshaller.unmarshal(file);
		// 
		//        } catch (JAXBException e) {
		//            e.printStackTrace();
		//        }

		System.out.println(importFileName + ".xml successfully read");

	}

	/** 
	 * Read component (e.g., theFuselage) from file and initialize it
	 * The component is recognized through its unique id.
	 * 
	 * @author LA
	 * @param object the component which has to be initialized
	 * @param xmlFile
	 */
	public void importItemFromXMLById(Object object, String importFilenameWithPath, Integer ... lev) {

		if (object != null) {

			// Create XPath object
			XPath xpath = _xpathFactoryImport.newXPath();
			Node node = null;
			//		Class<?> clazz = object.getClass();

			try {
				node = (Node) xpath.evaluate(
						"//*[@id='" + GlobalData.getTheXmlTree().getIdAsString(object) + "']",
						_importDoc, 
						XPathConstants.NODE);

			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}

			Integer level;
			if (lev.length == 0) level = 2;
			else level = lev[0];

			if (node != null)
				System.out.println("Importing " + node.getNodeName() + "...");

			MyXMLReaderUtils.recursiveRead(_importDoc, object, level, xpath, node);
			//				_importDoc.getElementById(ADOPT_GUI.getTheXmlTree().getIdAsString(object)));
		} else {
			System.out.println("The object to be read has not been initialized");
		}
	}

	/**
	 * @deprecated
	 * @param object
	 * @param importFileName
	 * @param lev
	 */
	public void importItemFromXMLByTag(Object object, String importFileName, Integer ... lev) {

		// Create XPath object
		XPath xpath = _xpathFactoryImport.newXPath();

		Integer level;
		if (lev.length == 0) level = 2;
		else level = lev[0];

		MyXMLReaderUtils.recursiveRead(
				_importDoc, object, level, xpath, 
				_importDoc.getElementsByTagName(
						GlobalData.getTheXmlTree().getDescription(object))
				.item(0));
	}

	/**
	 * Import the entire aircraft
	 * 
	 * @author LA
	 * @param aircraft
	 * @param importFile
	 */
	public void importAircraft(Aircraft aircraft, String importFile) {

		String importFileWithExt = importFile;
		if (!importFile.endsWith(".xml")) importFileWithExt = importFile + ".xml";

		importItemFromXMLById(aircraft.get_configuration(), importFileWithExt);
		importItemFromXMLById(aircraft.get_weights(), importFileWithExt);
		importItemFromXMLById(aircraft.get_performances(), importFileWithExt);
		importItemFromXMLById(aircraft.get_fuselage(), importFileWithExt);

		importItemFromXMLById(aircraft.get_wing(), importFileWithExt);
		for (int k=0; k < aircraft.get_wing().get_theAirfoilsList().size(); k++) {
			importItemFromXMLById(aircraft.get_wing().get_theAirfoilsList().get(k), importFileWithExt, 3);
			importItemFromXMLById(aircraft.get_wing().get_theAirfoilsList().get(k).getGeometry(), importFileWithExt, 4);
			importItemFromXMLById(aircraft.get_wing().get_theAirfoilsList().get(k).getAerodynamics(), importFileWithExt, 4);
		}

		importItemFromXMLById(aircraft.get_HTail(), importFileWithExt);
		for (int k=0; k < aircraft.get_HTail().get_theAirfoilsList().size(); k++) {
			importItemFromXMLById(aircraft.get_HTail().get_theAirfoilsList().get(k), importFileWithExt, 3);
			importItemFromXMLById(aircraft.get_HTail().get_theAirfoilsList().get(k).getGeometry(), importFileWithExt, 4);
			importItemFromXMLById(aircraft.get_HTail().get_theAirfoilsList().get(k).getAerodynamics(), importFileWithExt, 4);
		}

		importItemFromXMLById(aircraft.get_VTail(), importFileWithExt);
		for (int k=0; k < aircraft.get_VTail().get_theAirfoilsList().size(); k++) {
			importItemFromXMLById(aircraft.get_VTail().get_theAirfoilsList().get(k), importFileWithExt, 3);
			importItemFromXMLById(aircraft.get_VTail().get_theAirfoilsList().get(k).getGeometry(), importFileWithExt, 4);
			importItemFromXMLById(aircraft.get_VTail().get_theAirfoilsList().get(k).getAerodynamics(), importFileWithExt, 4);
		}

		importItemFromXMLById(aircraft.get_theNacelles(), importFileWithExt);
		for (int k=0; k < aircraft.get_theNacelles().get_nacellesNumber(); k++) {
			importItemFromXMLById(aircraft.get_theNacelles().get_nacellesList().get(k), importFileWithExt, 3);
		}

		importItemFromXMLById(aircraft.get_theFuelTank(), importFileWithExt);

		importItemFromXMLById(aircraft.get_powerPlant(), importFileWithExt);
		for (int k=0; k < aircraft.get_powerPlant().get_engineList().size(); k++) {
			importItemFromXMLById(aircraft.get_powerPlant().get_engineList().get(k), importFileWithExt, 3);
		}

		importItemFromXMLById(aircraft.get_landingGear(), importFileWithExt);
		importItemFromXMLById(aircraft.get_systems(), importFileWithExt);
	}

	/**
	 * @author LA
	 * @param aircraft
	 * @param conditions
	 * @param importFile
	 */
	public void importAircraftAndOperatingConditions(
			Aircraft aircraft, 
			OperatingConditions conditions,
			String importFile) {

		String ext = "";
		if (!importFile.endsWith(".xml")) ext = ".xml";

		importItemFromXMLById(conditions, importFile + ext);
		importAircraft(aircraft, importFile + ext);
	}


}
