package it.unina.adopt.utilities.cpacs;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import standaloneutils.MyXMLReaderUtils;

/*
 * see:
 * TIGL --> src/configuration/CCPACSConfiguration.h
 */

public class CPACSConfiguration {

	private DocumentBuilderFactory docFactory;
	private DocumentBuilder docBuilder;
	private Document doc;
	private Element rootElement;

	public CPACSConfiguration(File xmlFile) {

		// Create a document builder using DocumentBuilderFactory class
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		doc = null;

		// Once we have a document builder object, we use it to parse 
		// the XML file and create a document object.
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(xmlFile.toString());
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		MyXMLReaderUtils.removeWhitespaceAndCommentNodes(doc.getDocumentElement());
		System.out.println("File "+ xmlFile.getName() + " parsed.");

		// Once we have document object. We are ready to use XPath. Just create an xpath object using XPathFactory.
		// Create XPathFactory object
		XPathFactory xpathFactory = XPathFactory.newInstance();

		// Create XPath object
		XPath xpath = xpathFactory.newXPath();
		
		// Get all main nodes in xml document and put them in allNodes
		NodeList cpacs = doc.getDocumentElement().getChildNodes();
		Node[] allNodes = new Node[cpacs.getLength()];

		for (int i = 0; i < cpacs.getLength(); i++) {
			allNodes[i] = cpacs.item(i);
		}
		
		// TODO: manage CPACS file reading ...
		

	}
	
	void ReadCPACS(String configurationUID)
	{
		// TODO
		
	}
	
	Document getDocumentHandle()
	{
		// TODO
		
		return null;
		
	}
	
	public int getWingProfileCount()
	{
		// TODO
		
		return 0;
	}
	
    public CPACSWingProfile GetWingProfile(String uid)
    {
    	// TODO
    
		return null;    	
    }


}// end-of class

/*
 * 
https://code.google.com/p/tigl/source/browse/src/configuration/CCPACSConfiguration.h

#ifndef CCPACSCONFIGURATION_H
#define CCPACSCONFIGURATION_H

#include "tigl_internal.h"

#include "CTiglUIDManager.h"
#include "CTiglLogging.h"
#include "CCPACSHeader.h"
#include "CCPACSWings.h"
#include "CCPACSWingProfile.h"
#include "CCPACSFuselages.h"
#include "CCPACSFuselageProfile.h"
#include "CCPACSFarField.h"
#include "CCPACSGuideCurveProfiles.h"
#include "TopoDS_Compound.hxx"
#include "BRep_Builder.hxx"
#include "CTiglShapeCache.h"
#include "CTiglMemoryPool.h"
#include "CSharedPtr.h"

namespace tigl
{

class CTiglFusePlane;
typedef CSharedPtr<CTiglFusePlane> PTiglFusePlane;

class CCPACSConfiguration
{

public:
    // Constructor
    TIGL_EXPORT CCPACSConfiguration(TixiDocumentHandle tixiHandle);

    // Virtual Destructor
    TIGL_EXPORT virtual ~CCPACSConfiguration(void);

    // Invalidates the internal state of the configuration and forces
    // recalculation of wires, lofts etc.
    TIGL_EXPORT void Invalidate(void);

    // Read CPACS configuration
    TIGL_EXPORT void ReadCPACS(const char* configurationUID);

    // Returns the underlying tixi document handle used by a CPACS configuration
    TIGL_EXPORT TixiDocumentHandle GetTixiDocumentHandle(void) const;

    // Returns the total count of wing profiles in this configuration
    TIGL_EXPORT int GetWingProfileCount(void) const;

    // Returns the wing profile for a given index - TODO: depricated!
    TIGL_EXPORT CCPACSWingProfile& GetWingProfile(int index) const;

    // Returns the wing profile for a given uid.
    TIGL_EXPORT CCPACSWingProfile& GetWingProfile(std::string uid) const;

    // Returns the total count of wings in a configuration
    TIGL_EXPORT int GetWingCount(void) const;

    // Returns the wing for a given index.
    TIGL_EXPORT CCPACSWing& GetWing(int index) const;

    // Returns the wing for a given UID.
    TIGL_EXPORT CCPACSWing& GetWing(const std::string& UID) const;

    TIGL_EXPORT TopoDS_Shape GetParentLoft(const std::string& UID);

    // Returns the total count of fuselage profiles in this configuration
    TIGL_EXPORT int GetFuselageProfileCount(void) const;

    // Returns the fuselage profile for a given index.
    TIGL_EXPORT CCPACSFuselageProfile& GetFuselageProfile(int index) const;

    // Returns the fuselage profile for a given uid.
    TIGL_EXPORT CCPACSFuselageProfile& GetFuselageProfile(std::string uid) const;

    // Returns the total count of fuselages in a configuration
    TIGL_EXPORT int GetFuselageCount(void) const;

    // Returns the fuselage for a given index.
    TIGL_EXPORT CCPACSFuselage& GetFuselage(int index) const;

    // Returns the fuselage for a given UID.
    TIGL_EXPORT CCPACSFuselage& GetFuselage(std::string UID) const;

    TIGL_EXPORT CCPACSFarField& GetFarField();

    // Returns the guide curve profile for a given UID.
    TIGL_EXPORT CCPACSGuideCurveProfile& GetGuideCurveProfile(std::string UID) const;

    // Returns the uid manager
    TIGL_EXPORT CTiglUIDManager& GetUIDManager(void);

    // Returns the algorithm for fusing the aircraft
    TIGL_EXPORT PTiglFusePlane AircraftFusingAlgo(void);

    // Returns the length of the airplane
    TIGL_EXPORT double GetAirplaneLenth(void);

    // Returns the UID of the loaded configuration.
    TIGL_EXPORT const std::string& GetUID(void) const;

    TIGL_EXPORT CTiglShapeCache& GetShapeCache(void);

    TIGL_EXPORT CTiglMemoryPool& GetMemoryPool(void);

protected:
    // transform all components relative to their parents
    void transformAllComponents(CTiglAbstractPhysicalComponent* _parent);

private:
    // Copy constructor
    CCPACSConfiguration(const CCPACSConfiguration&);

    // Assignment operator
    void operator=(const CCPACSConfiguration&);

private:
    TixiDocumentHandle           tixiDocumentHandle;   // < Handle for internal TixiDocument
    CCPACSHeader                 header;               // < Configuration header element
    CCPACSWings                  wings;                // < Configuration wings element
    CCPACSFuselages              fuselages;            // < Configuration fuselages element
    CCPACSFarField               farField;             // < Far field configuration for CFD tools
    CCPACSGuideCurveProfiles     guideCurveProfiles;   // < Guide curve profiles
    CTiglUIDManager              uidManager;           // < Stores the unique ids of the components
    PTiglFusePlane               aircraftFuser;        // < The aircraft fusing algo
    std::string                  configUID;            // < UID of the opened configuration
    CTiglShapeCache              shapeCache;
    CTiglMemoryPool              memoryPool;
};

} // end namespace tigl

#endif // CCPACSCONFIGURATION_H

*/