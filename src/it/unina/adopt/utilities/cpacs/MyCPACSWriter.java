package it.unina.adopt.utilities.cpacs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import aircraft.components.Aircraft;
import configuration.MyConfiguration;
import configuration.enumerations.ComponentEnum;
import de.dlr.sc.tigl.CpacsConfiguration;
import de.dlr.sc.tigl.Tigl;
import de.dlr.sc.tigl.TiglException;
import standaloneutils.MyXMLReaderUtils;
import writers.JPADStaticWriteUtils;

public class MyCPACSWriter {
	static Document dom;

	private final static String OUTPUT_DIR =  
			MyConfiguration.currentDirectoryString 
			+ File.separator + "test" + File.separator + "cpacs";

	//-------------------------------------------------------------------------------
	public static void main(String[] args) {

		System.out.println("Testing CPACS writing...");

		Aircraft aircraft = new Aircraft(
				ComponentEnum.FUSELAGE, 
				ComponentEnum.WING,
				ComponentEnum.HORIZONTAL_TAIL,
				ComponentEnum.VERTICAL_TAIL,
				ComponentEnum.FUEL_TANK,
				ComponentEnum.POWER_PLANT,
				ComponentEnum.NACELLE,
				ComponentEnum.LANDING_GEAR,
				ComponentEnum.SYSTEMS
				);

		// MyAircraft aircraft = new MyAircraft("name.xml");

		aircraft.set_name("My nice little aircraft!");
		if (aircraft.get_fuselage() != null) {
			aircraft.get_fuselage().set_name("My nice little fuselage!");
		}

		System.out.println(
				"Aircraft: " +
						aircraft.get_name()
				);

		MyCPACSWriter cpacsWriter = new MyCPACSWriter(aircraft);

		cpacsWriter.write("aaa_pippo.xml");

		System.out.println("Current directory: " + MyCPACSWriter.OUTPUT_DIR);
		System.out.println(
				"File " +
						cpacsWriter.getFullFileName() +
						" written."
				);

	}// end-of-main
	//-------------------------------------------------------------------------------

	//###########################################################

	private Aircraft _theAircraft;
	private String _theFileName = "ADOpT_CPACS.xml";

	public MyCPACSWriter(Aircraft aircraft) {
		// super();
		JPADStaticWriteUtils.checkIfOutputDirExists(MyCPACSWriter.OUTPUT_DIR);
		this.setAircraft(aircraft);
	}

	//-----------------------------------------------------------
	// member functions

	public void write(String fileName) {
		_theFileName = fileName;
		write();
	}

	public void write() {
		// write an XML file according to CPACS format
		saveToXML(this.getFullFileName()); 
	} 

	public Aircraft getAircraft() {
		return _theAircraft;
	}

	public void setAircraft(Aircraft _theAircraft) {
		this._theAircraft = _theAircraft;
	}

	public void setFileName(String _theFileName) {
		this._theFileName = _theFileName;
	}

	public String getFileName() {
		return _theFileName;
	}

	public String getFullFileName() {
		return OUTPUT_DIR + File.separator + _theFileName;
	}

	private void saveToXML(String xml) {
		Element headerElement = null;
		Element vehiclesElement;
		Element aircraftElement;
		Element profilesElement;


		// instance of a DocumentBuilderFactory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			// use factory to get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();
			// create instance of DOM
			dom = db.newDocument();

			// create the root element
			Element rootElement = dom.createElement("cpacs");
			rootElement.setAttributeNS(
					"http://www.w3.org/2001/XMLSchema-instance",
					"xsi:noNamespaceSchemaLocation","http://cpacs.googlecode.com/files/CPACS_21_Schema.xsd"
					);
			// CPACS <-- HEADER
			headerElement = dom.createElement("header");
			//e.setAttribute("xsi:type","headerType");
			rootElement.appendChild(headerElement);

			// Populate Header
			Element timeStampElement;
			timeStampElement = dom.createElement("timestamp");
			timeStampElement.appendChild(dom.createTextNode(JPADStaticWriteUtils.getCurrentTimeStamp()));
			headerElement.appendChild(timeStampElement);

			Element versionElement;
			versionElement = dom.createElement("version");
			versionElement.appendChild(dom.createTextNode("0.0"));
			headerElement.appendChild(versionElement);

			Element cpacsVersionElement;
			cpacsVersionElement = dom.createElement("cpacsVersion");
			cpacsVersionElement.appendChild(dom.createTextNode("2.0"));
			headerElement.appendChild(cpacsVersionElement);


			// CPACS <-- VEHICLES
			vehiclesElement = dom.createElement("vehicles");
			rootElement.appendChild(vehiclesElement);

			//---------------------------------------------------------------------------------------------
			// VEHICLES <-- AIRCRAFT
			aircraftElement = dom.createElement("aircraft");
			aircraftElement.setAttribute("aircraftID", "ADOpT:01");
			populateAircraft(dom, aircraftElement);
			vehiclesElement.appendChild(aircraftElement);

			//---------------------------------------------------------------------------------------------
			// VEHICLES <-- PROFILES
			profilesElement = dom.createElement("profiles");
			populateProfiles(dom, profilesElement);
			vehiclesElement.appendChild(profilesElement);
			//---------------------------------------------------------------------------------------------

			// populate document
			dom.appendChild(rootElement);

			try {
				Transformer tr = TransformerFactory.newInstance().newTransformer();
				tr.setOutputProperty(OutputKeys.INDENT, "yes");
				tr.setOutputProperty(OutputKeys.METHOD, "xml");
				tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

				// dom.setXmlStandalone(true);				

				// send DOM to file
				tr.transform(new DOMSource(dom), 
						new StreamResult(new FileOutputStream(xml)));

			} catch (TransformerException te) {
				System.out.println(te.getMessage());
			} catch (IOException ioe) {
				System.out.println(ioe.getMessage());
			}
		} catch (ParserConfigurationException pce) {
			System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
		}
	}

	/* Populate 
	 *   <aircraft>
	 *     <model>
	 *     
	 *       <fuselages>
	 *   
	 *       </fuselages>
	 *       
	 *       <wings>
	 *   
	 *       </wings>
	 *   
	 *     </model>
	 *   </aircraft>
	 */
	private void populateAircraft(Document dom, Element aircraftElement) {

		Element modelElement = dom.createElement("model");
		modelElement.setAttribute("uID", "ADOpT:MODEL:01");

		Element nameModelElement = dom.createElement("model");
		nameModelElement.setAttribute("uID", "ADOpT:MODEL:01");

		// TODO: implementation ... to be continued

		//---------------------------------------------------------------------------------------------
		// MODEL <-- FUSELAGES

		if (_theAircraft != null) {

			Element fuselagesElement = dom.createElement("fuselages");

			//#########################################################################################
			// TODO:
			// Experimental: read a pre-saved fuselage from a CPACS file

			String filename = OUTPUT_DIR + File.separator + 
					"aaa_pippo_OK.xml";
			String fuselageuID = null;
			
			System.out.println("TiGL Version: " + Tigl.getVersion());
			System.out.println("Reading file: " + filename);

			try (CpacsConfiguration config = Tigl.openCPACSConfiguration(filename, "")) {

				int fuselageCount = config.getFuselageCount();
				System.out.println("fuselage count: " + fuselageCount);
				if (fuselageCount > 0 ) {

					System.out.println("fuselage[1] UID: " + config.fuselageGetUID(fuselageCount));
					fuselageuID = config.fuselageGetUID(fuselageCount);
//					/*
//					// TIGL/TIXI low level commands
//					// TiglReturnCode tiglRet = TiglReturnCode.TIGL_NULL_POINTER;
//					int tiglRet = -1;
//					IntByReference tixiHandlePtr = new IntByReference();
//
//					TiglNativeInterface.tiglGetCPACSTixiHandle(
//							config.getCPACSHandle(), tixiHandlePtr
//							);
//					System.out.println("tixiHandlePtr value: " + tixiHandlePtr.getValue());
//
//					IntByReference tiglHandlePtr = new IntByReference();
//					tiglRet = TiglNativeInterface
//							.tiglOpenCPACSConfiguration(
//									tixiHandlePtr.getValue(), 
//									"ADOpT:MODEL:01", // must be the model UID (under <aircraft>) // "ADOpT:FUSELAGE:01:01", 
//									tiglHandlePtr
//							);
//
//					System.out.println("tiglHandlePtr value: " + tiglHandlePtr.getValue());	
//					System.out.println("tiglOpenCPACSConfiguration return value: " + tiglRet);	
//					 */
//
//					// get the desired node via XPath
//					DocumentBuilderFactory factoryImport = DocumentBuilderFactory.newInstance();
//					factoryImport.setNamespaceAware(true);
//					factoryImport.setIgnoringComments(true);
//					DocumentBuilder builderImport;
//					Document parserDoc = null;
//					try {
//						builderImport = factoryImport.newDocumentBuilder();
//						parserDoc = builderImport.parse(filename);
//						MyReadUtils.removeWhitespaceAndCommentNodes(parserDoc.getDocumentElement());
//
//						System.out.println("File "+ filename + " parsed.");
//
//						// Create XPath object
//						XPathFactory xpathFactoryImport = XPathFactory.newInstance();
//						XPath xpath = xpathFactoryImport.newXPath();
//						Node node = null;
//						if (parserDoc != null) {
//							// search for a XML node with a given uID attribute
//							node = (Node) xpath.evaluate(
//									"//*[@uID='" + "ADOpT:FUSELAGE:01:01" + "']",
//									parserDoc, 
//									XPathConstants.NODE);
//							if (node != null) {
//								System.out.println("Fuselage node reached --> " + node.getNodeName() + "...");
//								// sending node contents to a different document, i.e. dom passed
//								// to this function, populateAircraft
//								Node newNode = dom.importNode(node, true);
//								fuselagesElement.appendChild(newNode);
//							}
//						}
//					} catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
//						e.printStackTrace();
//					}

				}// end-of-try-catch on CpacsConfiguration obj
			}
			catch(TiglException err) {
				System.out.println(err.getMessage());
				System.out.println(err.getErrorCode());
				return;
			}
			Node fuselageNode = copyNode(filename, fuselageuID);
			fuselagesElement.appendChild(fuselageNode);
			//#########################################################################################

			//			if (_theAircraft.get_fuselage() != null) {
			//				
			//				
			//				Element fusElement = dom.createElement("fuselage");
			//				fusElement.setAttribute("uID", "ADOpT:FUSELAGE:01:01");
			//
			//				Element nameFusElement = dom.createElement("name");
			//
			//				nameFusElement.appendChild(
			//						dom.createTextNode(
			//								_theAircraft.get_fuselage().get_name()
			//								)
			//						);
			//				fusElement.appendChild(nameFusElement);
			//
			//				fuselagesElement.appendChild(fusElement);
			//			}

			modelElement.appendChild(fuselagesElement);

			//---------------------------------------------------------------------------------------------
			// MODEL <-- WINGS

			Element wingsElement = dom.createElement("wings");

			if (_theAircraft.get_wing() != null) {
				Element wingElement = dom.createElement("wing");
				Element nameWingElement = dom.createElement("name");
				Element parentUIDWingElement = dom.createElement("parentUID");
				Element descriptionWingElement = dom.createElement("description");
				Element transformationWingElement = dom.createElement("transformation");

				writeTransformationChild(1.0, 0.5, 0.5, transformationWingElement, "scaling", null);
				writeTransformationChild(0.0, 0.0, 0.0, transformationWingElement, "rotation", null);
				writeTransformationChild(0.0, 0.0, 0.0, transformationWingElement, "translation", "absLocal");

				wingElement.setAttribute("uID", "Wing");
				wingElement.setAttribute("simmetry", "x-z-plane");
				//			translationWingElement.setAttribute("refType","absGlobal");				

				nameWingElement.appendChild(dom.createTextNode("Wing"));
				parentUIDWingElement.appendChild(dom.createTextNode("ADOpT:FUSELAGE:01:01"));
				descriptionWingElement.appendChild(dom.createTextNode("This is a wing-profiles test "));

				wingElement.appendChild(nameWingElement);
				wingElement.appendChild(parentUIDWingElement);
				wingElement.appendChild(descriptionWingElement);
				wingElement.appendChild(transformationWingElement);
				wingsElement.appendChild(wingElement);

				// Populate Sections				
				Element sectionsWingElement = dom.createElement("sections");
				Element section1Element = dom.createElement("section");				

				// Section 1
				// I elements
				Element nameSect1Element = dom.createElement("name");
				Element descriptionSect1Element = dom.createElement("description");
				Element trasnformationSect1Element = dom.createElement("transformation");
				Element elementsSect1Element = dom.createElement("elements");

				// Section 1 II elements
				Element scalingSect1Element = dom.createElement("scaling");
				Element rotationSect1Element = dom.createElement("rotation");
				Element translationSect1Element = dom.createElement("translation");
				Element elementSect1Element = dom.createElement("element");


				// Section 1 III elements
				Element xScaSect1Element = dom.createElement("x");
				Element yScaSect1Element = dom.createElement("y");
				Element zScaSect1Element = dom.createElement("z");

				Element xRotSect1Element = dom.createElement("x");
				Element yRotSect1Element = dom.createElement("y");
				Element zRotSect1Element = dom.createElement("z");

				Element xTraSect1Element = dom.createElement("x");
				Element yTraSect1Element = dom.createElement("y");
				Element zTraSect1Element = dom.createElement("z");

				Element nameElementSect1Element = dom.createElement("name");
				Element descriptionElementSect1Element = dom.createElement(
						"description");
				Element airfoilUIDElementSect1Element = dom.createElement(
						"airfoilUID");
				Element transformationElementSect1Element = dom.createElement(
						"transformation");

				// Section 1 IV elements
				Element scalingElementSect1Element = dom.createElement("scaling");
				Element rotationElementSect1Element = dom.createElement("rotation");
				Element translationElementSect1Element = dom.createElement("translation");

				// Section 1 V elements
				Element xScaElementSect1Element = dom.createElement("x");
				Element yScaElementSect1Element = dom.createElement("y");
				Element zScaElementSect1Element = dom.createElement("z");

				Element xRotElementSect1Element = dom.createElement("x");
				Element yRotElementSect1Element = dom.createElement("y");
				Element zRotElementSect1Element = dom.createElement("z");

				Element xTraElementSect1Element = dom.createElement("x");
				Element yTraElementSect1Element = dom.createElement("y");
				Element zTraElementSect1Element = dom.createElement("z");				
				//---------------------------------------------------------------------

				// Text nodes Section 1
				nameSect1Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 1"));
				descriptionSect1Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 1"));
				nameElementSect1Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 1 Main Element"));				
				descriptionElementSect1Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 1 Main Element"));				
				airfoilUIDElementSect1Element.appendChild(dom.createTextNode(
						"NACA0012"));				

				// Attributes Section 1
				section1Element.setAttribute("uID","Cpacs2Test_Wing_Sec1");
				translationSect1Element.setAttribute("refType","absLocal");				
				elementSect1Element.setAttribute("uID","Cpacs2Test_Wing_Sec1_El1");
				translationElementSect1Element.setAttribute("refType","absLocal");				

				// Scaling Section 1
				xScaSect1Element.appendChild(dom.createTextNode("1"));
				yScaSect1Element.appendChild(dom.createTextNode("1"));
				zScaSect1Element.appendChild(dom.createTextNode("1"));

				// Rotation Section 1
				xRotSect1Element.appendChild(dom.createTextNode("0"));
				yRotSect1Element.appendChild(dom.createTextNode("0"));
				zRotSect1Element.appendChild(dom.createTextNode("0"));

				// Translation Section 1
				xTraSect1Element.appendChild(dom.createTextNode("0"));
				yTraSect1Element.appendChild(dom.createTextNode("0"));
				zTraSect1Element.appendChild(dom.createTextNode("0"));

				// Scaling Element Section 1
				xScaElementSect1Element.appendChild(dom.createTextNode("1"));
				yScaElementSect1Element.appendChild(dom.createTextNode("1"));
				zScaElementSect1Element.appendChild(dom.createTextNode("1"));

				// Rotation Element Section 1
				xRotElementSect1Element.appendChild(dom.createTextNode("0"));
				yRotElementSect1Element.appendChild(dom.createTextNode("0"));
				zRotElementSect1Element.appendChild(dom.createTextNode("0"));

				// Translation Element Section 1
				xTraElementSect1Element.appendChild(dom.createTextNode("0"));
				yTraElementSect1Element.appendChild(dom.createTextNode("0"));
				zTraElementSect1Element.appendChild(dom.createTextNode("0"));

				scalingSect1Element.appendChild(xScaSect1Element);
				scalingSect1Element.appendChild(yScaSect1Element);
				scalingSect1Element.appendChild(zScaSect1Element);
				rotationSect1Element.appendChild(xRotSect1Element);
				rotationSect1Element.appendChild(yRotSect1Element);
				rotationSect1Element.appendChild(zRotSect1Element);
				translationSect1Element.appendChild(xTraSect1Element);
				translationSect1Element.appendChild(yTraSect1Element);
				translationSect1Element.appendChild(zTraSect1Element);
				trasnformationSect1Element.appendChild(scalingSect1Element);
				trasnformationSect1Element.appendChild(rotationSect1Element);
				trasnformationSect1Element.appendChild(translationSect1Element);
				scalingElementSect1Element.appendChild(xScaElementSect1Element);
				scalingElementSect1Element.appendChild(yScaElementSect1Element);
				scalingElementSect1Element.appendChild(zScaElementSect1Element);
				rotationElementSect1Element.appendChild(xRotElementSect1Element);
				rotationElementSect1Element.appendChild(yRotElementSect1Element);
				rotationElementSect1Element.appendChild(zRotElementSect1Element);
				translationElementSect1Element.appendChild(xTraElementSect1Element);
				translationElementSect1Element.appendChild(yTraElementSect1Element);
				translationElementSect1Element.appendChild(zTraElementSect1Element);
				transformationElementSect1Element.appendChild(scalingElementSect1Element);
				transformationElementSect1Element.appendChild(rotationElementSect1Element);
				transformationElementSect1Element.appendChild(translationElementSect1Element);
				elementSect1Element.appendChild(nameElementSect1Element);
				elementSect1Element.appendChild(descriptionElementSect1Element);
				elementSect1Element.appendChild(airfoilUIDElementSect1Element);
				elementSect1Element.appendChild(transformationElementSect1Element);
				elementsSect1Element.appendChild(elementSect1Element);

				section1Element.appendChild(nameSect1Element);
				section1Element.appendChild(descriptionSect1Element);
				section1Element.appendChild(trasnformationSect1Element);
				section1Element.appendChild(elementsSect1Element);

				// Section 2
				// I elements
				Element section2Element = dom.createElement("section");				
				Element nameSect2Element = dom.createElement("name");
				Element descriptionSect2Element = dom.createElement("description");
				Element trasnformationSect2Element = dom.createElement("transformation");
				Element elementsSect2Element = dom.createElement("elements");

				// Section 2 II elements
				Element scalingSect2Element = dom.createElement("scaling");
				Element rotationSect2Element = dom.createElement("rotation");
				Element translationSect2Element = dom.createElement("translation");
				Element elementSect2Element = dom.createElement("element");


				// Section 2 III elements
				Element xScaSect2Element = dom.createElement("x");
				Element yScaSect2Element = dom.createElement("y");
				Element zScaSect2Element = dom.createElement("z");

				Element xRotSect2Element = dom.createElement("x");
				Element yRotSect2Element = dom.createElement("y");
				Element zRotSect2Element = dom.createElement("z");

				Element xTraSect2Element = dom.createElement("x");
				Element yTraSect2Element = dom.createElement("y");
				Element zTraSect2Element = dom.createElement("z");

				Element nameElementSect2Element = dom.createElement("name");
				Element descriptionElementSect2Element = dom.createElement(
						"description");
				Element airfoilUIDElementSect2Element = dom.createElement(
						"airfoilUID");
				Element transformationElementSect2Element = dom.createElement(
						"transformation");

				// Section 2 IV elements
				Element scalingElementSect2Element = dom.createElement("scaling");
				Element rotationElementSect2Element = dom.createElement("rotation");
				Element translationElementSect2Element = dom.createElement("translation");

				// Section 2 V elements
				Element xScaElementSect2Element = dom.createElement("x");
				Element yScaElementSect2Element = dom.createElement("y");
				Element zScaElementSect2Element = dom.createElement("z");

				Element xRotElementSect2Element = dom.createElement("x");
				Element yRotElementSect2Element = dom.createElement("y");
				Element zRotElementSect2Element = dom.createElement("z");

				Element xTraElementSect2Element = dom.createElement("x");
				Element yTraElementSect2Element = dom.createElement("y");
				Element zTraElementSect2Element = dom.createElement("z");				
				//---------------------------------------------------------------------

				// Text nodes Section 2
				nameSect2Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 2"));
				descriptionSect2Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 2"));
				nameElementSect2Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 2 Main Element"));				
				descriptionElementSect2Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 2 Main Element"));				
				airfoilUIDElementSect2Element.appendChild(dom.createTextNode(
						"NACA0012"));				

				// Attributes Section 2
				section2Element.setAttribute("uID","Cpacs2Test_Wing_Sec2");
				translationSect2Element.setAttribute("refType","absLocal");				
				elementSect2Element.setAttribute("uID","Cpacs2Test_Wing_Sec2_El1");
				translationElementSect2Element.setAttribute("refType","absLocal");				

				// Scaling Section 2
				xScaSect2Element.appendChild(dom.createTextNode("1"));
				yScaSect2Element.appendChild(dom.createTextNode("1"));
				zScaSect2Element.appendChild(dom.createTextNode("1"));

				// Rotation Section 2
				xRotSect2Element.appendChild(dom.createTextNode("0"));
				yRotSect2Element.appendChild(dom.createTextNode("0"));
				zRotSect2Element.appendChild(dom.createTextNode("0"));

				// Translation Section 2
				xTraSect2Element.appendChild(dom.createTextNode("0"));
				yTraSect2Element.appendChild(dom.createTextNode("0"));
				zTraSect2Element.appendChild(dom.createTextNode("0"));

				// Scaling Element Section 2
				xScaElementSect2Element.appendChild(dom.createTextNode("1"));
				yScaElementSect2Element.appendChild(dom.createTextNode("1"));
				zScaElementSect2Element.appendChild(dom.createTextNode("1"));

				// Rotation Element Section 2
				xRotElementSect2Element.appendChild(dom.createTextNode("0"));
				yRotElementSect2Element.appendChild(dom.createTextNode("0"));
				zRotElementSect2Element.appendChild(dom.createTextNode("0"));

				// Translation Element Section 2
				xTraElementSect2Element.appendChild(dom.createTextNode("0"));
				yTraElementSect2Element.appendChild(dom.createTextNode("0"));
				zTraElementSect2Element.appendChild(dom.createTextNode("0"));

				scalingSect2Element.appendChild(xScaSect2Element);
				scalingSect2Element.appendChild(yScaSect2Element);
				scalingSect2Element.appendChild(zScaSect2Element);
				rotationSect2Element.appendChild(xRotSect2Element);
				rotationSect2Element.appendChild(yRotSect2Element);
				rotationSect2Element.appendChild(zRotSect2Element);
				translationSect2Element.appendChild(xTraSect2Element);
				translationSect2Element.appendChild(yTraSect2Element);
				translationSect2Element.appendChild(zTraSect2Element);
				trasnformationSect2Element.appendChild(scalingSect2Element);
				trasnformationSect2Element.appendChild(rotationSect2Element);
				trasnformationSect2Element.appendChild(translationSect2Element);
				scalingElementSect2Element.appendChild(xScaElementSect2Element);
				scalingElementSect2Element.appendChild(yScaElementSect2Element);
				scalingElementSect2Element.appendChild(zScaElementSect2Element);
				rotationElementSect2Element.appendChild(xRotElementSect2Element);
				rotationElementSect2Element.appendChild(yRotElementSect2Element);
				rotationElementSect2Element.appendChild(zRotElementSect2Element);
				translationElementSect2Element.appendChild(xTraElementSect2Element);
				translationElementSect2Element.appendChild(yTraElementSect2Element);
				translationElementSect2Element.appendChild(zTraElementSect2Element);
				transformationElementSect2Element.appendChild(scalingElementSect2Element);
				transformationElementSect2Element.appendChild(rotationElementSect2Element);
				transformationElementSect2Element.appendChild(translationElementSect2Element);
				elementSect2Element.appendChild(nameElementSect2Element);
				elementSect2Element.appendChild(descriptionElementSect2Element);
				elementSect2Element.appendChild(airfoilUIDElementSect2Element);
				elementSect2Element.appendChild(transformationElementSect2Element);
				elementsSect2Element.appendChild(elementSect2Element);

				section2Element.appendChild(nameSect2Element);
				section2Element.appendChild(descriptionSect2Element);
				section2Element.appendChild(trasnformationSect2Element);
				section2Element.appendChild(elementsSect2Element);

				// Section 3
				// I elements
				Element section3Element = dom.createElement("section");				
				Element nameSect3Element = dom.createElement("name");
				Element descriptionSect3Element = dom.createElement("description");
				Element trasnformationSect3Element = dom.createElement("transformation");
				Element elementsSect3Element = dom.createElement("elements");

				writeTransformationChild(1.0, 1.0, 1.0, trasnformationSect3Element, "scaling", null);
				writeTransformationChild(0.0, 0.0, 0.0, trasnformationSect3Element, "rotation", null);
				writeTransformationChild(0.0, 0.0, 0.0, trasnformationSect3Element, "translation", "absLocal");


				// Section 3 II elements
				Element elementSect3Element = dom.createElement("element");


				// Section 3 III elements
				Element nameElementSect3Element = dom.createElement("name");
				Element descriptionElementSect3Element = dom.createElement(
						"description");
				Element airfoilUIDElementSect3Element = dom.createElement(
						"airfoilUID");
				Element transformationElementSect3Element = dom.createElement(
						"transformation");

				// Section 3 IV elements
				Element scalingElementSect3Element = dom.createElement("scaling");
				Element rotationElementSect3Element = dom.createElement("rotation");
				Element translationElementSect3Element = dom.createElement("translation");

				// Section 3 V elements
				Element xScaElementSect3Element = dom.createElement("x");
				Element yScaElementSect3Element = dom.createElement("y");
				Element zScaElementSect3Element = dom.createElement("z");

				Element xRotElementSect3Element = dom.createElement("x");
				Element yRotElementSect3Element = dom.createElement("y");
				Element zRotElementSect3Element = dom.createElement("z");

				Element xTraElementSect3Element = dom.createElement("x");
				Element yTraElementSect3Element = dom.createElement("y");
				Element zTraElementSect3Element = dom.createElement("z");				
				//---------------------------------------------------------------------

				// Text nodes Section 3
				nameSect3Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 3"));
				descriptionSect3Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 3"));
				nameElementSect3Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 3 Main Element"));				
				descriptionElementSect3Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 3 Main Element"));				
				airfoilUIDElementSect3Element.appendChild(dom.createTextNode(
						"NACA0012"));				

				// Attributes Section 3
				section3Element.setAttribute("uID","Cpacs2Test_Wing_Sec3");
				//			translationSect3Element.setAttribute("refType","absLocal");				
				elementSect3Element.setAttribute("uID","Cpacs2Test_Wing_Sec3_El1");
				translationElementSect3Element.setAttribute("refType","absLocal");				


				// Scaling Element Section 3
				xScaElementSect3Element.appendChild(dom.createTextNode("0.5"));
				yScaElementSect3Element.appendChild(dom.createTextNode("0.5"));
				zScaElementSect3Element.appendChild(dom.createTextNode("0.5"));

				// Rotation Element Section 3
				xRotElementSect3Element.appendChild(dom.createTextNode("0"));
				yRotElementSect3Element.appendChild(dom.createTextNode("0"));
				zRotElementSect3Element.appendChild(dom.createTextNode("0"));

				// Translation Element Section 3
				xTraElementSect3Element.appendChild(dom.createTextNode("0.5"));
				yTraElementSect3Element.appendChild(dom.createTextNode("0"));
				zTraElementSect3Element.appendChild(dom.createTextNode("0"));

				scalingElementSect3Element.appendChild(xScaElementSect3Element);
				scalingElementSect3Element.appendChild(yScaElementSect3Element);
				scalingElementSect3Element.appendChild(zScaElementSect3Element);
				rotationElementSect3Element.appendChild(xRotElementSect3Element);
				rotationElementSect3Element.appendChild(yRotElementSect3Element);
				rotationElementSect3Element.appendChild(zRotElementSect3Element);
				translationElementSect3Element.appendChild(xTraElementSect3Element);
				translationElementSect3Element.appendChild(yTraElementSect3Element);
				translationElementSect3Element.appendChild(zTraElementSect3Element);
				transformationElementSect3Element.appendChild(scalingElementSect3Element);
				transformationElementSect3Element.appendChild(rotationElementSect3Element);
				transformationElementSect3Element.appendChild(translationElementSect3Element);
				elementSect3Element.appendChild(nameElementSect3Element);
				elementSect3Element.appendChild(descriptionElementSect3Element);
				elementSect3Element.appendChild(airfoilUIDElementSect3Element);
				elementSect3Element.appendChild(transformationElementSect3Element);
				elementsSect3Element.appendChild(elementSect3Element);

				section3Element.appendChild(nameSect3Element);
				section3Element.appendChild(descriptionSect3Element);
				section3Element.appendChild(trasnformationSect3Element);
				section3Element.appendChild(elementsSect3Element);

				sectionsWingElement.appendChild(section1Element);
				sectionsWingElement.appendChild(section2Element);
				sectionsWingElement.appendChild(section3Element);
				wingElement.appendChild(sectionsWingElement);
				wingsElement.appendChild(wingElement);
				//End Populate Sections

				// Populate Positionings
				Element positioningsWingsElement = dom.createElement("positionings");
				Element positioning1Element = dom.createElement("positioning");				
				Element positioning2Element = dom.createElement("positioning");	
				Element positioning3Element = dom.createElement("positioning");	

				// Section 1 (positioning)
				// I elements
				Element namePos1Element = dom.createElement("name");
				Element descriptionPos1Element = dom.createElement("description");
				Element lengthPos1Element = dom.createElement("length");
				Element sweepAnglePos1Element = dom.createElement("sweepAngle");
				Element dihedralAnglePos1Element = dom.createElement("dihedralAngle");
				Element toSectionUIDPos1Element = dom.createElement("toSectionUID");

				// Text nodes Section 1 (positioning)
				namePos1Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 1 Positioning"));
				descriptionPos1Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 1 Positioning"));
				lengthPos1Element.appendChild(dom.createTextNode("0"));
				sweepAnglePos1Element.appendChild(dom.createTextNode("0"));
				dihedralAnglePos1Element.appendChild(dom.createTextNode("0"));
				toSectionUIDPos1Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec1"));

				// Append child Section 1 (positioning)
				positioning1Element.appendChild(namePos1Element);
				positioning1Element.appendChild(descriptionPos1Element);
				positioning1Element.appendChild(lengthPos1Element);
				positioning1Element.appendChild(sweepAnglePos1Element);
				positioning1Element.appendChild(dihedralAnglePos1Element);
				positioning1Element.appendChild(toSectionUIDPos1Element);

				// Section 2 (positioning)
				// I elements
				Element namePos2Element = dom.createElement("name");
				Element descriptionPos2Element = dom.createElement("description");
				Element lengthPos2Element = dom.createElement("length");
				Element sweepAnglePos2Element = dom.createElement("sweepAngle");
				Element dihedralAnglePos2Element = dom.createElement("dihedralAngle");
				Element fromSectionUIDPos2Element = dom.createElement("fromSectionUID");
				Element toSectionUIDPos2Element = dom.createElement("toSectionUID");

				// Text nodes Section 2 (positioning)
				namePos2Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 2 Positioning"));
				descriptionPos2Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 2 Positioning"));
				lengthPos2Element.appendChild(dom.createTextNode("1"));
				sweepAnglePos2Element.appendChild(dom.createTextNode("0"));
				dihedralAnglePos2Element.appendChild(dom.createTextNode("0"));
				fromSectionUIDPos2Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec1"));
				toSectionUIDPos2Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec2"));

				// Append child Section 2 (positioning)
				positioning2Element.appendChild(namePos2Element);
				positioning2Element.appendChild(descriptionPos2Element);
				positioning2Element.appendChild(lengthPos2Element);
				positioning2Element.appendChild(sweepAnglePos2Element);
				positioning2Element.appendChild(dihedralAnglePos2Element);
				positioning2Element.appendChild(fromSectionUIDPos2Element);
				positioning2Element.appendChild(toSectionUIDPos2Element);				

				// Section 3 (positioning)
				// I elements
				Element namePos3Element = dom.createElement("name");
				Element descriptionPos3Element = dom.createElement("description");
				Element lengthPos3Element = dom.createElement("length");
				Element sweepAnglePos3Element = dom.createElement("sweepAngle");
				Element dihedralAnglePos3Element = dom.createElement("dihedralAngle");
				Element fromSectionUIDPos3Element = dom.createElement("fromSectionUID");
				Element toSectionUIDPos3Element = dom.createElement("toSectionUID");

				// Text nodes Section 3 (positioning)
				namePos3Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 3 Positioning"));
				descriptionPos3Element.appendChild(dom.createTextNode(
						"Cpacs2Test - Wing Section 3 Positioning"));
				lengthPos3Element.appendChild(dom.createTextNode("1"));
				sweepAnglePos3Element.appendChild(dom.createTextNode("0"));
				dihedralAnglePos3Element.appendChild(dom.createTextNode("0"));
				fromSectionUIDPos3Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec2"));
				toSectionUIDPos3Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec3"));

				// Append child Section 3 (positioning)
				positioning3Element.appendChild(namePos3Element);
				positioning3Element.appendChild(descriptionPos3Element);
				positioning3Element.appendChild(lengthPos3Element);
				positioning3Element.appendChild(sweepAnglePos3Element);
				positioning3Element.appendChild(dihedralAnglePos3Element);
				positioning3Element.appendChild(fromSectionUIDPos3Element);
				positioning3Element.appendChild(toSectionUIDPos3Element);

				positioningsWingsElement.appendChild(positioning1Element);
				positioningsWingsElement.appendChild(positioning2Element);
				positioningsWingsElement.appendChild(positioning3Element);
				wingElement.appendChild(positioningsWingsElement);
				wingsElement.appendChild(wingElement);
				// End populate Positionings

				// Populate segments
				Element segmentsWingsElement = dom.createElement("segments");
				Element segment12Element = dom.createElement("segment");				
				Element segment23Element = dom.createElement("segment");	

				// Section 1-2 (segment)
				// I elements
				Element nameSeg12Element = dom.createElement("name");
				Element descriptionSeg12Element = dom.createElement("description");
				Element fromElementUIDSeg12Element = dom.createElement("fromElementUID");
				Element toElementUIDSeg12Element = dom.createElement("toElementUID");

				// Text nodes Section 1-2 (segment)
				nameSeg12Element.appendChild(dom.createTextNode(
						"Fuselage Segment from Cpacs2Test - "
								+ "Wing Section 1 Main Element to Cpacs2Test - "
								+ "Wing Section 2 Main Element"));
				descriptionSeg12Element.appendChild(dom.createTextNode(
						"Fuselage Segment from Cpacs2Test -"
								+ " Wing Section 1 Main Element to Cpacs2Test -"
								+ " Wing Section 2 Main Element"));
				fromElementUIDSeg12Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec1_El1"));
				toElementUIDSeg12Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec2_El1"));

				// Attributes Section 1-2 (segment)
				segment12Element.setAttribute("uID", "Cpacs2Test_Wing_Seg_1_2");

				// Append child Section 1-2 (segment)
				segment12Element.appendChild(nameSeg12Element);
				segment12Element.appendChild(descriptionSeg12Element);
				segment12Element.appendChild(fromElementUIDSeg12Element);
				segment12Element.appendChild(toElementUIDSeg12Element);

				// Section 2-3 (segment)
				// I elements
				Element nameSeg23Element = dom.createElement("name");
				Element descriptionSeg23Element = dom.createElement("description");
				Element fromElementUIDSeg23Element = dom.createElement("fromElementUID");
				Element toElementUIDSeg23Element = dom.createElement("toElementUID");

				// Text nodes Section 2-3 (segment)
				nameSeg23Element.appendChild(dom.createTextNode(
						"Fuselage Segment from Cpacs2Test - "
								+ "Wing Section 2 Main Element to Cpacs2Test - "
								+ "Wing Section 3 Main Element"));
				descriptionSeg23Element.appendChild(dom.createTextNode(
						"Fuselage Segment from Cpacs2Test -"
								+ " Wing Section 2 Main Element to Cpacs2Test -"
								+ " Wing Section 3 Main Element"));
				fromElementUIDSeg23Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec2_El1"));
				toElementUIDSeg23Element.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec3_El1"));

				// Attributes Section 2-3 (segment)
				segment23Element.setAttribute("uID", "Cpacs2Test_Wing_Seg_2_3");

				// Append child Section 2-3 (segment)
				segment23Element.appendChild(nameSeg23Element);
				segment23Element.appendChild(descriptionSeg23Element);
				segment23Element.appendChild(fromElementUIDSeg23Element);
				segment23Element.appendChild(toElementUIDSeg23Element);

				segmentsWingsElement.appendChild(segment12Element);
				segmentsWingsElement.appendChild(segment23Element);
				wingElement.appendChild(segmentsWingsElement);
				wingsElement.appendChild(wingElement);
				// End populate segments


				// Populate componentSegments
				Element compSegmentsWingsElement = dom.createElement(
						"componentSegments");
				Element compSegElement = dom.createElement("componentSegment");				

				// I elements componentSegments
				Element nameCSElement = dom.createElement("name");
				Element fromElementUIDCSElement = dom.createElement("fromElementUID");
				Element toElementUIDCSElement = dom.createElement("toElementUID");
				Element structureCSElement = dom.createElement("structure");

				// II elements componentSegments
				Element upperShellStCSElement = dom.createElement("upperShell");

				// III elements componentSegments
				Element skinUpShStCSElement = dom.createElement("skin");
				Element cellsUpShStCSElement = dom.createElement("cells");

				// IV elements componentSegments
				Element materialSkinUpShStCSElement = dom.createElement("material");
				Element cellCellsUpShStCSElement = dom.createElement("cell");

				// V elements componentSegments
				Element materialUIDMatSkinUpShStCSElement = dom.createElement(
						"materialUID");
				Element thicknessMatSkinUpShStCSElement = dom.createElement(
						"thickness");
				Element skinCCsUpShStCSElement = dom.createElement("skin");
				Element posLECCsUpShStCSElement = dom.createElement(
						"positioningLeadingEdge");
				Element posTECCsUpShStCSElement = dom.createElement(
						"positioningTrailingEdge");
				Element posIBCCsUpShStCSElement = dom.createElement(
						"positioningInnerBorder");
				Element posOBCCsUpShStCSElement = dom.createElement(
						"positioningOuterBorder");

				// V elements componentSegments
				Element materialSkCCsUpShStCSElement = dom.createElement("material");
				Element xsi1PLECCsUpShStCSElement = dom.createElement(
						"xsi1");
				Element xsi2PLECCsUpShStCSElement = dom.createElement(
						"xsi2");				
				Element xsi1PTECCsUpShStCSElement = dom.createElement(
						"xsi1");
				Element xsi2PTECCsUpShStCSElement = dom.createElement(
						"xsi2");	
				Element eta1PIBCCsUpShStCSElement = dom.createElement(
						"eta1");
				Element eta2PIBCCsUpShStCSElement = dom.createElement(
						"eta2");				
				Element eta1POBCCsUpShStCSElement = dom.createElement(
						"eta1");
				Element eta2POBCCsUpShStCSElement = dom.createElement(
						"eta2");

				// VI elements componentSegments
				Element materialUIDMatSkCCsUpShStCSElement = dom.createElement(
						"materialUID");
				Element thicknessMatSkCCsUpShStCSElement = dom.createElement(
						"thickness");				

				// Text nodes componentSegment
				nameCSElement.appendChild(dom.createTextNode("Wing_CS1"));
				fromElementUIDCSElement.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec1_El1"));
				toElementUIDCSElement.appendChild(dom.createTextNode(
						"Cpacs2Test_Wing_Sec3_El1"));
				materialUIDMatSkinUpShStCSElement.appendChild(dom.createTextNode(
						"MySkinMat"));
				thicknessMatSkinUpShStCSElement.appendChild(dom.createTextNode(
						"0.0"));				
				xsi1PLECCsUpShStCSElement.appendChild(dom.createTextNode("0.8"));
				xsi2PLECCsUpShStCSElement.appendChild(dom.createTextNode("0.8"));
				xsi1PTECCsUpShStCSElement.appendChild(dom.createTextNode("1.0"));
				xsi2PTECCsUpShStCSElement.appendChild(dom.createTextNode("1.0"));
				eta1PIBCCsUpShStCSElement.appendChild(dom.createTextNode("0.0"));
				eta2PIBCCsUpShStCSElement.appendChild(dom.createTextNode("0.0"));
				eta1POBCCsUpShStCSElement.appendChild(dom.createTextNode("0.5"));
				eta2POBCCsUpShStCSElement.appendChild(dom.createTextNode("0.5"));
				materialUIDMatSkCCsUpShStCSElement.appendChild(dom.createTextNode(
						"MyCellMat"));
				thicknessMatSkCCsUpShStCSElement.appendChild(dom.createTextNode("0.0"));

				// Attributes componentSegment
				compSegElement.setAttribute("uID", "WING_CS1");
				cellCellsUpShStCSElement.setAttribute("uID", "WING_CS1_CELL1" );

				// Append child componentSegment
				materialSkCCsUpShStCSElement.appendChild(materialUIDMatSkCCsUpShStCSElement);
				materialSkCCsUpShStCSElement.appendChild(thicknessMatSkCCsUpShStCSElement);

				skinCCsUpShStCSElement.appendChild(materialSkCCsUpShStCSElement);
				posLECCsUpShStCSElement.appendChild(xsi1PLECCsUpShStCSElement);
				posLECCsUpShStCSElement.appendChild(xsi2PLECCsUpShStCSElement);
				posTECCsUpShStCSElement.appendChild(xsi1PTECCsUpShStCSElement);
				posTECCsUpShStCSElement.appendChild(xsi2PTECCsUpShStCSElement);
				posIBCCsUpShStCSElement.appendChild(eta1PIBCCsUpShStCSElement);
				posIBCCsUpShStCSElement.appendChild(eta2PIBCCsUpShStCSElement);
				posOBCCsUpShStCSElement.appendChild(eta1POBCCsUpShStCSElement);
				posOBCCsUpShStCSElement.appendChild(eta2POBCCsUpShStCSElement);

				materialSkinUpShStCSElement.appendChild(
						materialUIDMatSkinUpShStCSElement);
				materialSkinUpShStCSElement.appendChild(
						thicknessMatSkinUpShStCSElement);
				cellCellsUpShStCSElement.appendChild(
						skinCCsUpShStCSElement);
				cellCellsUpShStCSElement.appendChild(
						posLECCsUpShStCSElement);
				cellCellsUpShStCSElement.appendChild(
						posTECCsUpShStCSElement);
				cellCellsUpShStCSElement.appendChild(
						posIBCCsUpShStCSElement);
				cellCellsUpShStCSElement.appendChild(
						posOBCCsUpShStCSElement);

				skinUpShStCSElement.appendChild(materialSkinUpShStCSElement);
				cellsUpShStCSElement.appendChild(cellCellsUpShStCSElement);

				upperShellStCSElement.appendChild(skinUpShStCSElement);
				upperShellStCSElement.appendChild(cellsUpShStCSElement);

				structureCSElement.appendChild(upperShellStCSElement);

				compSegElement.appendChild(nameCSElement);
				compSegElement.appendChild(fromElementUIDCSElement);
				compSegElement.appendChild(toElementUIDCSElement);
				compSegElement.appendChild(structureCSElement);

				compSegmentsWingsElement.appendChild(compSegElement);
				wingElement.appendChild(compSegmentsWingsElement);
				wingsElement.appendChild(wingElement);				
				// End populate componentSegments

			}// end-if-wing-not-null

			modelElement.appendChild(wingsElement);

		}

		aircraftElement.appendChild(modelElement);
	}// end-of-populateAircraft

	private void populateProfiles(Document dom, Element profilesElement){
		// Populate fusolageProfiles
		// TODO
		// End populate fusolageProfiles		
		// Level 1
		Element wingAirfoilsElement = dom.createElement("wingAirfoils");

		// Level 2
		Element wingAirfoilElement = dom.createElement("wingAirfoil");

		// Level 3
		Element nameWingAirfoilElement = dom.createElement("name");
		Element descriptionWingAirfoilElement = dom.createElement("description");
		Element pointListWingAirfoilElement = dom.createElement("pointList");

		// Level 4
		Element xPointListWingAirfoilElement = dom.createElement("x");
		Element yPointListWingAirfoilElement = dom.createElement("y");
		Element zPointListWingAirfoilElement = dom.createElement("z");

		// Text nodes wingAirfoils
		nameWingAirfoilElement.appendChild(dom.createTextNode("NACA0.00.00.12"));
		descriptionWingAirfoilElement.appendChild(dom.createTextNode("NACA 4 Series Profile"));
		xPointListWingAirfoilElement.appendChild(dom.createTextNode("1.0;0.9875;0.975;0.9625;"
				+ "0.95;0.9375;0.925;"
				+ "0.9125;0.9;0.8875;0.875;0.8625;0.85;0.8375;0.825;0.8125;0.8;0.7875;0.775;"
				+ "0.7625;0.75;0.7375;0.725;0.7125;0.7;0.6875;0.675;0.6625;0.65;0.6375;0.625;"
				+ "0.6125;0.6;0.5875;0.575;0.5625;0.55;0.5375;0.525;0.5125;0.5;0.4875;0.475;"
				+ "0.4625;0.45;0.4375;0.425;0.4125;0.4;0.3875;0.375;0.3625;0.35;0.3375;0.325;"
				+ "0.3125;0.3;0.2875;0.275;0.2625;0.25;0.2375;0.225;0.2125;0.2;0.1875;0.175;"
				+ "0.1625;0.15;0.1375;0.125;0.1125;0.1;0.0875;0.075;0.0625;0.05;0.0375;0.025;"
				+ "0.0125;0.0;0.0125;0.025;0.0375;0.05;0.0625;0.075;0.0875;0.1;0.1125;0.125;"
				+ "0.1375;0.15;0.1625;0.175;0.1875;0.2;0.2125;0.225;0.2375;0.25;0.2625;0.275;"
				+ "0.2875;0.3;0.3125;0.325;0.3375;0.35;0.3625;0.375;0.3875;0.4;0.4125;0.425;"
				+ "0.4375;0.45;0.4625;0.475;0.4875;0.5;0.5125;0.525;0.5375;0.55;0.5625;0.575;"
				+ "0.5875;0.6;0.6125;0.625;0.6375;0.65;0.6625;0.675;0.6875;0.7;0.7125;0.725;"
				+ "0.7375;0.75;0.7625;0.775;0.7875;0.8;0.8125;0.825;0.8375;0.85;0.8625;0.875;"
				+ "0.8875;0.9;0.9125;0.925;0.9375;0.95;0.9625;0.975;0.9875;1.0"));
		yPointListWingAirfoilElement.appendChild(dom.createTextNode("0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;"
				+ "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0"));
		zPointListWingAirfoilElement.appendChild(dom.createTextNode("-0.00126;"
				+ "-0.0030004180415;-0.00471438572941;-0.00640256842113;-0.00806559133343;"
				+ "-0.00970403933653;-0.0113184567357;-0.0129093470398;-0.0144771727147;"
				+ "-0.0160223549226;-0.0175452732434;-0.0190462653789;-0.0205256268372;"
				+ "-0.0219836105968;-0.0234204267471;-0.024836242105;-0.0262311798047;"
				+ "-0.0276053188583;-0.0289586936852;-0.0302912936071;-0.0316030623052;"
				+ "-0.0328938972373;-0.0341636490097;-0.0354121207001;-0.0366390671268;"
				+ "-0.0378441940595;-0.0390271573644;-0.0401875620783;-0.0413249614032;"
				+ "-0.042438855614;-0.043528690869;-0.0445938579126;-0.0456336906587;"
				+ "-0.04664746464;-0.0476343953088;-0.0485936361694;-0.0495242767241;"
				+ "-0.0504253402064;-0.0512957810767;-0.0521344822472;-0.0529402520006;"
				+ "-0.0537118205596;-0.0544478362583;-0.0551468612564;-0.0558073667285;"
				+ "-0.0564277274483;-0.0570062156697;-0.0575409941929;-0.0580301084765;"
				+ "-0.0584714776309;-0.0588628840933;-0.059201961739;-0.0594861821311;"
				+ "-0.0597128385384;-0.059879027262;-0.0599816256958;-0.060017266394;"
				+ "-0.059982306219;-0.05987278938;-0.0596844028137;-0.059412421875;"
				+ "-0.059051643633;-0.0585963041308;-0.0580399746271;-0.0573754299024;"
				+ "-0.0565944788455;-0.0556877432118;-0.054644363746;-0.0534516022043;"
				+ "-0.0520942903127;-0.0505540468987;-0.0488081315259;-0.0468277042382;"
				+ "-0.0445750655553;-0.0419990347204;-0.0390266537476;-0.0355468568262;"
				+ "-0.0313738751622;-0.0261471986426;-0.0189390266528;0.0;0.0189390266528;"
				+ "0.0261471986426;0.0313738751622;0.0355468568262;0.0390266537476;"
				+ "0.0419990347204;0.0445750655553;0.0468277042382;0.0488081315259;"
				+ "0.0505540468987;0.0520942903127;0.0534516022043;0.054644363746;"
				+ "0.0556877432118;0.0565944788455;0.0573754299024;0.0580399746271;"
				+ "0.0585963041308;0.059051643633;0.059412421875;0.0596844028137;"
				+ "0.05987278938;0.059982306219;0.060017266394;0.0599816256958;"
				+ "0.059879027262;0.0597128385384;0.0594861821311;0.059201961739;"
				+ "0.0588628840933;0.0584714776309;0.0580301084765;0.0575409941929;"
				+ "0.0570062156697;0.0564277274483;0.0558073667285;0.0551468612564;"
				+ "0.0544478362583;0.0537118205596;0.0529402520006;0.0521344822472;"
				+ "0.0512957810767;0.0504253402064;0.0495242767241;0.0485936361694;"
				+ "0.0476343953088;0.04664746464;0.0456336906587;0.0445938579126;"
				+ "0.043528690869;0.042438855614;0.0413249614032;0.0401875620783;"
				+ "0.0390271573644;0.0378441940595;0.0366390671268;0.0354121207001;"
				+ "0.0341636490097;0.0328938972373;0.0316030623052;0.0302912936071;"
				+ "0.0289586936852;0.0276053188583;0.0262311798047;0.024836242105;"
				+ "0.0234204267471;0.0219836105968;0.0205256268372;0.0190462653789;"
				+ "0.0175452732434;0.0160223549226;0.0144771727147;0.0129093470398;"
				+ "0.0113184567357;0.00970403933653;0.00806559133343;0.00640256842113;"
				+ "0.00471438572941;0.0030004180415;0.00126"));

		// Set Attributes wingAirfoils
		wingAirfoilElement.setAttribute("uID", "NACA0012");
		xPointListWingAirfoilElement.setAttribute("mapType", "vector" );
		yPointListWingAirfoilElement.setAttribute("mapType", "vector" );
		zPointListWingAirfoilElement.setAttribute("mapType", "vector" );

		// Append Child wingAirfoils
		pointListWingAirfoilElement.appendChild(xPointListWingAirfoilElement);
		pointListWingAirfoilElement.appendChild(yPointListWingAirfoilElement);
		pointListWingAirfoilElement.appendChild(zPointListWingAirfoilElement);

		wingAirfoilElement.appendChild(nameWingAirfoilElement);
		wingAirfoilElement.appendChild(descriptionWingAirfoilElement);
		wingAirfoilElement.appendChild(pointListWingAirfoilElement);

		wingAirfoilsElement.appendChild(wingAirfoilElement);
		// End populate wingAirfoils


		profilesElement.appendChild(wingAirfoilsElement);
	}
	//	private void writeCPACSNode(Document dom, Element parent, String tagName, String textNode, 
	//			Map <String,String> attributeMap) throws ScriptException{
	//	    ScriptEngineManager manager = new ScriptEngineManager();
	//	    ScriptEngine engine = manager.getEngineByName("js"); 
	//	    
	//		Element element = dom.createElement("skin");
	//		nameWingAirfoilElement.appendChild(dom.createTextNode("NACA0.00.00.12"));
	//		wingAirfoilElement.setAttribute("uID", "NACA0012");
	//		wingAirfoilElement.appendChild(nameWingAirfoilElement);
	//
	//		if(name!=null)
	//		{engine.eval("Element " + name + parent + " " + dom + ".createElement(" + "" + name + "" + ");");
	//		}

	//    <transformation>
	//    <scaling>
	//      <x>1.0</x>
	//      <y>0.5</y>
	//      <z>0.5</z>
	//    </scaling>
	//    <rotation>
	//      <x>0.0</x>
	//      <y>0.0</y>
	//      <z>0.0</z>
	//    </rotation>
	//    <translation refType="absLocal">
	//      <x>0.0</x>
	//      <y>0.0</y>
	//      <z>0.0</z>
	//    </translation>
	//  </transformation>

	private void writeXYZ (Double x, Double y, Double z, Element parent) {

		Element xLocal = dom.createElement("x") ;
		Element yLocal = dom.createElement("y") ;
		Element zLocal = dom.createElement("z") ;

		xLocal.appendChild(dom.createTextNode(x.toString()));
		yLocal.appendChild(dom.createTextNode(y.toString()));
		zLocal.appendChild(dom.createTextNode(z.toString()));

		parent.appendChild(xLocal);
		parent.appendChild(yLocal);
		parent.appendChild(zLocal);
	}

	private void writeTransformationChild(Double x, Double y, Double z,
			Element parent,String elementTag, String attributeRefType){

		Element transformationChild = dom.createElement(elementTag);

		if(attributeRefType != null && attributeRefType != ""){
			transformationChild.setAttribute("refType", attributeRefType);
		}

		writeXYZ(x, y, z, transformationChild);

		parent.appendChild(transformationChild);
	}
	/**
	 * Experimental: copy a pre-saved node from a CPACS file.
	 *				In a later version I have to add a switch that allows the user to choose
	 * 				how seek the node (by name, by tag, by uID...). In this version the 
	 * 				seeking is performed via uID.
	 * @param filename is file complete directory path + file name, of CPACS file to 
	 * 			copy.
	 * @param seekedNode is the node's identifier (name/tag/uID) that the function 
	 *			has to copy.
	 * @return newNode is the copied node.
	 * @author AC
	 */

	public static Node copyNode(String filename,/* int seekingMethod,*/ String seekedNode){
		
		System.out.println("TiGL Version: " + Tigl.getVersion());
		System.out.println("Reading file: " + filename);

		try (CpacsConfiguration config = Tigl.openCPACSConfiguration(filename, "")) {

			if (config != null ) {

				// get the desired node via XPath
				DocumentBuilderFactory factoryImport = DocumentBuilderFactory.newInstance();
				factoryImport.setNamespaceAware(true);
				factoryImport.setIgnoringComments(true);
				DocumentBuilder builderImport;
				Document parserDoc = null;
				try {
					builderImport = factoryImport.newDocumentBuilder();
					parserDoc = builderImport.parse(filename);
					MyXMLReaderUtils.removeWhitespaceAndCommentNodes(parserDoc.getDocumentElement());

					System.out.println("File "+ filename + " parsed.");

					// Create XPath object
					XPathFactory xpathFactoryImport = XPathFactory.newInstance();
					XPath xpath = xpathFactoryImport.newXPath();
					Node node = null;
					if (parserDoc != null) {
						// search for a XML node with a given uID attribute
						node = (Node) xpath.evaluate(
								"//*[@uID='" + seekedNode + "']",
								parserDoc, 
								XPathConstants.NODE);
						if (node != null) {
							System.out.println("The node with uID= " + seekedNode +"was reached --> " + node.getNodeName() + "...");
							// sending node contents to a different document, i.e. dom passed
							// to this function, populateAircraft
							Node newNode = dom.importNode(node, true);
							return newNode;
						}
					}
				} catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
					e.printStackTrace();
				}

			}// end-of-try-catch on CpacsConfiguration obj

		}
		catch(TiglException err) {
			System.out.println(err.getMessage());
			System.out.println(err.getErrorCode());
		}
		return null;
	}

}

