package it.unina.adopt.utilities.gui;

import org.eclipse.nebula.visualization.xygraph.figures.Trace.PointStyle;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.TraceType;
import org.eclipse.nebula.visualization.xygraph.util.XYGraphMediaFactory;
import org.eclipse.swt.graphics.RGB;

/** 
 * A utility class to easily store all data and properties of a curve on a XYGraph canvas
 * 
 * @author ADM
 */
public class CurveOnXYGraph {
	
	private Double[] _x;
	private Double[] _y;
	private String _legend;
	
	private PointStyle _pointStyle = PointStyle.POINT;
	private int _pointSize = 4;
	private RGB _color = XYGraphMediaFactory.COLOR_BLUE;
	private int _lineWidth = 2;
	private TraceType _traceType = TraceType.SOLID_LINE;
	
	/**
	 * 
	 * @param x vector of x-data
	 * @param y vector of y-data
	 * @param legend
	 * @param pointStyle
	 * @param pointSize
	 * @param color
	 * @param lineWidth
	 */
	public CurveOnXYGraph(
			Double[] x, Double[] y, 
			String legend,
			PointStyle pointStyle,
			int pointSize,
			RGB color,
			int lineWidth,
			TraceType traceType) {
		set_x(x);
		set_y(y);
		set_legend(legend);
		set_pointStyle(pointStyle);
		set_pointSize(pointSize);
		set_color(color);
		set_lineWidth(lineWidth);
		set_traceType(traceType);
	}

	public Double[] get_x() {
		return _x;
	}

	public void set_x(Double[] _x) {
		this._x = _x;
	}

	public Double[] get_y() {
		return _y;
	}

	public void set_y(Double[] _y) {
		this._y = _y;
	}

	public String get_legend() {
		return _legend;
	}

	public void set_legend(String _legend) {
		this._legend = _legend;
	}

	public PointStyle get_pointStyle() {
		return _pointStyle;
	}

	public void set_pointStyle(PointStyle _pointStyle) {
		this._pointStyle = _pointStyle;
	}

	public int get_pointSize() {
		return _pointSize;
	}

	public void set_pointSize(int _pointSize) {
		this._pointSize = _pointSize;
	}

	public RGB get_color() {
		return _color;
	}

	public void set_color(RGB _color) {
		this._color = _color;
	}

	public int get_lineWidth() {
		return _lineWidth;
	}

	public void set_lineWidth(int _lineWidth) {
		this._lineWidth = _lineWidth;
	}

	public TraceType get_traceType() {
		return _traceType;
	}

	public void set_traceType(TraceType _traceType) {
		this._traceType = _traceType;
	}
	
	
}