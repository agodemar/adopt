// see: http://eclipsesource.com/blogs/2013/07/25/efficiently-dealing-with-swt-gridlayout-and-griddata/
// https://gist.github.com/mpost/6077907#file-gridlayoututil-java
/*
 * 
 * An efficient way to deal with SWT GridLayout and GridData configurations. 
 * Allows you to apply SWT GridLayout and GridData to Composites and Controls 
 * in a very compact form, while being more powerful at the same time. 
 * To apply a GridLayout and configure it to your use case the following 
 * expression can be used: 
 *   
 *   GridLayoutUtil.applyGridLayout( composite ).numColumns( 2 ); 
 *   
 * Similarly you can apply GridData on a Control: 
 * 
 *   GridDataUtil.applyGridData( control ).withHorizontalFill();
 * 
 * 
 */
package it.unina.adopt.utilities.gui;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.measure.unit.NonSI;
import javax.measure.unit.SI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;
import org.jscience.physics.amount.Amount;

import it.unina.adopt.main.ADOPT_GUI;

public class GridLayoutUtil {

	private final GridLayout gridLayout;

	private GridLayoutUtil( GridLayout gridLayout ) {
		this.gridLayout = gridLayout;
	}

	public static GridLayoutUtil applyGridLayout( Composite composite ) {
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.horizontalSpacing = 0;
		composite.setLayout( gridLayout );
		return new GridLayoutUtil( gridLayout );
	}

	public static GridLayoutUtil onGridLayout( Composite composite ) {
		Layout layout = composite.getLayout();
		if( layout instanceof GridLayout ) {
			return new GridLayoutUtil( ( GridLayout )layout );
		}
		throw new IllegalStateException( "Composite has to have a GridLayout. Has " + layout );
	}

	public GridLayoutUtil numColumns( int numColumns ) {
		gridLayout.numColumns = numColumns;
		return this;
	}

	public GridLayoutUtil columnsEqualWidth( boolean columnsEqualWidth ) {
		gridLayout.makeColumnsEqualWidth = columnsEqualWidth;
		return this;
	}

	public GridLayoutUtil horizontalSpacing( int horizontalSpacing ) {
		gridLayout.horizontalSpacing = horizontalSpacing;
		return this;
	}

	public GridLayoutUtil verticalSpacing( int verticalSpacing ) {
		gridLayout.verticalSpacing = verticalSpacing;
		return this;
	}

	public GridLayoutUtil marginWidth( int marginWidth ) {
		gridLayout.marginWidth = marginWidth;
		return this;
	}

	public GridLayoutUtil marginHeight( int marginHeight ) {
		gridLayout.marginHeight = marginHeight;
		return this;
	}

	public GridLayoutUtil marginTop( int marginTop ) {
		gridLayout.marginTop = marginTop;
		return this;
	}

	public GridLayoutUtil marginBottom( int marginBottom ) {
		gridLayout.marginBottom = marginBottom;
		return this;
	}

	public GridLayoutUtil marginLeft( int marginLeft ) {
		gridLayout.marginLeft = marginLeft;
		return this;
	}

	public GridLayoutUtil marginRight( int marginRight ) {
		gridLayout.marginRight = marginRight;
		return this;
	}

	/**
	 * 
	 * @param composite
	 * @param groupInput
	 * @param labelText
	 * @param latex_text
	 * @param objectToPrint
	 * @param rounding
	 * @param labelStyle
	 * @param horizontalAlignmentDescription
	 * @param verticalAlignmentDescription
	 * @param grabExcessHorizontalSpaceDescription
	 * @param grabExcessVerticalSpaceDescription
	 * @param horizontalSpanDescription
	 * @param verticalSpanDescription
	 * @param horizontalAlignmentValue
	 * @param verticalAlignmentValue
	 * @param grabExcessHorizontalSpaceValue
	 * @param grabExcessVerticalSpaceValue
	 * @param horizontalSpanValue
	 * @param verticalSpanValue
	 * @param TEXT_WIDTH_HINT
	 * @return
	 */
	public static Text putLabel(
			Composite composite,
			String labelText, 
			String latex_text,
			Object objectToPrint, 
			int rounding,
			int labelStyle,
			int TEXT_WIDTH_HINT){

		Amount<?> objectToPrintAmount;
		Double objectToPrintDouble;

		GridData gdDescription = new GridData(SWT.FILL, SWT.FILL, true, false);
		gdDescription.horizontalSpan = 3;
		Label labelDescription = new Label(composite, SWT.NONE);
		labelDescription.setText(labelText);
		labelDescription.setLayoutData(gdDescription);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Image texImage = MyGuiUtils.renderLatexFormula(composite,latex_text, 22);
		Label labelTex = new Label(composite, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text, texImage);
		labelTex.setImage (texImage);

		GridData gdLabelTex = new GridData(SWT.FILL, SWT.FILL, true, false);
		gdLabelTex.horizontalSpan = 1;
		gdLabelTex.widthHint = 25;
		labelTex.setLayoutData(gdLabelTex);
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++

		GridData gdValue = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gdValue.horizontalSpan = 1;
		gdValue.grabExcessHorizontalSpace = true;
		gdValue.horizontalAlignment = GridData.FILL;
		gdValue.widthHint = TEXT_WIDTH_HINT;

		Text value = new Text(composite, SWT.BORDER);

		if (objectToPrint instanceof Amount<?>) {
			objectToPrintAmount = (Amount<?>) objectToPrint;

			if (objectToPrintAmount.getUnit() != SI.RADIAN){
				BigDecimal bd = BigDecimal.valueOf( objectToPrintAmount.getEstimatedValue());
				bd = bd.setScale(rounding, RoundingMode.DOWN);
				value.setText( "  " + bd + " " + objectToPrintAmount.getUnit().toString());

			} else {
				BigDecimal bd = BigDecimal.valueOf(Math.toDegrees(objectToPrintAmount.getEstimatedValue()));
				bd = bd.setScale(rounding, RoundingMode.DOWN);
				value.setText( "  " + bd + NonSI.DEGREE_ANGLE.toString());
			}

			value.setLayoutData(gdValue);
			value.setEditable(true);

		} else {
			objectToPrintDouble = (Double) objectToPrint;
			BigDecimal bd = BigDecimal.valueOf(objectToPrintDouble);
			bd = bd.setScale(rounding, RoundingMode.DOWN);
			value.setText( "  " + bd);
			value.setLayoutData(gdValue);
			value.setEditable(true);
		}

		return value;
	}

	public static Slider createSlider(Composite composite, double value, double min, double max) {
		
		Slider slider = new Slider(composite, SWT.HORIZONTAL);
		slider.setBounds(0, 0, 40, 200);
		slider.setMaximum( (int) max );  // m
		slider.setMinimum( (int) min );  // m
		slider.setSelection( (int) value);    // m
		slider.setIncrement(1); // m
		slider.setPageIncrement(5);
		slider.setThumb(1);
		
		return slider;
	}

} 