package it.unina.adopt.utilities.gui;

import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.measure.unit.NonSI;
import javax.measure.unit.SI;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.nebula.visualization.xygraph.dataprovider.CircularBufferDataProvider;
import org.eclipse.nebula.visualization.xygraph.figures.ToolbarArmedXYGraph;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.PointStyle;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.TraceType;
import org.eclipse.nebula.visualization.xygraph.figures.XYGraph;
import org.eclipse.nebula.visualization.xygraph.util.XYGraphMediaFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.jscience.physics.amount.Amount;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;

import com.google.common.primitives.Doubles;

import aircraft.calculators.ACAerodynamicsManager;
import aircraft.calculators.ACAnalysisManager;
import aircraft.calculators.ACBalanceManager;
import aircraft.calculators.ACWeightsManager;
import aircraft.components.Aircraft;
import aircraft.components.FuelTank;
import aircraft.components.LandingGear;
import aircraft.components.Systems;
import aircraft.components.fuselage.Fuselage;
import aircraft.components.liftingSurface.Canard;
import aircraft.components.liftingSurface.HTail;
import aircraft.components.liftingSurface.LiftingSurface;
import aircraft.components.liftingSurface.VTail;
import aircraft.components.liftingSurface.Wing;
import aircraft.components.nacelles.Nacelle;
import aircraft.components.powerPlant.PowerPlant;
import configuration.MyConfiguration;
import it.unina.adopt.GUI.tabpanels.liftingSurface.MyLiftingSurfacePanel;
import it.unina.adopt.GUI.tree.MyProjectTree;
import it.unina.adopt.main.ADOPT_GUI;
import standaloneutils.MyArrayUtils;

public class MyGuiUtils {

	private MyLiftingSurfacePanel _liftingSurfaceInitiator;
	private final int TEXT_WIDTH_HINT;

	MyGuiUtils(MyLiftingSurfacePanel liftingInitiator){
		_liftingSurfaceInitiator = liftingInitiator;
		TEXT_WIDTH_HINT = 50;
	}

	/** 
	 * Create a new plot. 
	 * The method allows to add whatever number of traces.
	 * 
	 * @param x
	 * @param y
	 * @param legend
	 * @param xUnit
	 * @param yUnit
	 * @param _xyGraph
	 * @param _tracesXY
	 */
	public static void createXYGraph(
			List<Double[]> x,
			List<Double[]> y,
			String legend, 
			String title,
			String xLabel,
			String yLabel,
			String xUnit,
			String yUnit,
			XYGraph _xyGraph) {

		List<CircularBufferDataProvider> tdpXY = new ArrayList<CircularBufferDataProvider>();
		List<Trace> tracesXY = new ArrayList<Trace>();

		// MAKE TRACES
		// add a new trace data provider
		for (int i=0; i < y.size(); i++) {

			createSingleTraceXYGraph(
					x.get(i), y.get(i),
					legend,
					_xyGraph, tdpXY,
					tracesXY);
		}
		// NOTE: tracesXY is populated at the end of the cycle
		//       each call to createSingleTraceXYGraph adds a trace to the list

		setAxis(x, y,
				title,
				xLabel, yLabel,
				xUnit, yUnit, 
				_xyGraph);
	}

	/**
	 * Overloaded method: it allows to add
	 * a different label for each trace.
	 * 
	 * @param x list of vectors
	 * @param y list of vectors 
	 * @param legendList list of legends
	 * @param title
	 * @param xLabel
	 * @param yLabel
	 * @param xUnit
	 * @param yUnit
	 * @param _xyGraph
	 */
	public static void createXYGraph(
			List<Double[]> x,
			List<Double[]> y,
			List<String> legendList, 
			String title,
			String xLabel,
			String yLabel,
			String xUnit,
			String yUnit,
			XYGraph _xyGraph) {

		List<CircularBufferDataProvider> tdpXY = new ArrayList<CircularBufferDataProvider>();
		List<Trace> tracesXY = new ArrayList<Trace>();


		final List<PointStyle> LIST_POINT_STYLES = new ArrayList<PointStyle>();
		LIST_POINT_STYLES.add(PointStyle.POINT);
		LIST_POINT_STYLES.add(PointStyle.CROSS);
		LIST_POINT_STYLES.add(PointStyle.CIRCLE);
		LIST_POINT_STYLES.add(PointStyle.FILLED_DIAMOND);
		LIST_POINT_STYLES.add(PointStyle.FILLED_TRIANGLE);
		LIST_POINT_STYLES.add(PointStyle.FILLED_SQUARE);

		final List<TraceType> LIST_TRACE_TYPES = new ArrayList<TraceType>();
		LIST_TRACE_TYPES.add(TraceType.SOLID_LINE);
		LIST_TRACE_TYPES.add(TraceType.DASH_LINE);

		final List<RGB> LIST_CURVE_COLORS = new ArrayList<RGB>();
		LIST_CURVE_COLORS.add(XYGraphMediaFactory.COLOR_GREEN);
		LIST_CURVE_COLORS.add(XYGraphMediaFactory.COLOR_RED);
		LIST_CURVE_COLORS.add(XYGraphMediaFactory.COLOR_BLUE);
		LIST_CURVE_COLORS.add(XYGraphMediaFactory.COLOR_ORANGE);
		LIST_CURVE_COLORS.add(XYGraphMediaFactory.COLOR_BLACK);
		LIST_CURVE_COLORS.add(XYGraphMediaFactory.COLOR_PURPLE);
		LIST_CURVE_COLORS.add(XYGraphMediaFactory.COLOR_DARK_GRAY);


		// MAKE TRACES
		// add a new trace data provider
		for (int i=0; i < y.size(); i++) {

			// minVal = (a < b) ? a : b;
			int idx = (i < LIST_CURVE_COLORS.size()) 
					? i
							: i - Math.floorDiv(i, LIST_CURVE_COLORS.size()) 
							+ i % LIST_CURVE_COLORS.size();

			RGB curveColor = LIST_CURVE_COLORS.get(idx);

			CurveOnXYGraph curve = 
					new CurveOnXYGraph(
							x.get(i), y.get(i), legendList.get(i), 
							PointStyle.POINT, 
							4, // point size
							curveColor, 
							2, // line width
							TraceType.SOLID_LINE
							);

			createSingleTraceXYGraph(
					curve,
					_xyGraph, tdpXY,
					tracesXY);

			//			createSingleTraceXYGraph(
			//					x.get(i), y.get(i),
			//					legendList.get(i),
			//					_xyGraph, tdpXY,
			//					tracesXY);

		}
		// NOTE: tracesXY is populated at the end of the cycle
		//       each call to createSingleTraceXYGraph adds a trace to the list

		setAxis(x, y,
				title, xLabel, yLabel,
				xUnit, yUnit, 
				_xyGraph);

	}

	/** 
	 * Create a single trace in an XYGraph
	 * 
	 * @param x
	 * @param y
	 * @param legend
	 * @param _xyGraph
	 * @param tdpXY
	 * @param traceXY
	 */
	public static void createSingleTraceXYGraph(
			Double[] x, Double[] y, 
			String legend,
			XYGraph _xyGraph,
			List<CircularBufferDataProvider> tdpXY,
			List<Trace> traceXY
			) {

		tdpXY.add(new CircularBufferDataProvider(false));
		tdpXY.get(tdpXY.size()-1).setCurrentXDataArray(Doubles.toArray(Arrays.asList(x)));
		tdpXY.get(tdpXY.size()-1).setCurrentYDataArray(Doubles.toArray(Arrays.asList(y)));

		traceXY.add(new Trace(legend,					
				_xyGraph.primaryXAxis, _xyGraph.primaryYAxis, 
				tdpXY.get(tdpXY.size()-1)));		

		//set trace property
		traceXY.get(traceXY.size()-1).setPointStyle(PointStyle.POINT);
		traceXY.get(traceXY.size()-1).setPointSize(4);
		traceXY.get(traceXY.size()-1).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_BLUE)
				);
		traceXY.get(traceXY.size()-1).setLineWidth(2);

		//add the trace to xyGraph
		_xyGraph.addTrace( traceXY.get(traceXY.size()-1) );
	}

	/** 
	 * Create a single trace in an XYGraph
	 * 
	 * @param curve object with data and properties
	 * @param _xyGraph
	 * @param tdpXY
	 * @param traceXY
	 */
	public static void createSingleTraceXYGraph(
			CurveOnXYGraph curve,
			XYGraph _xyGraph,
			List<CircularBufferDataProvider> tdpXY,
			List<Trace> traceXY
			) {

		tdpXY.add(new CircularBufferDataProvider(false));
		tdpXY.get(tdpXY.size()-1).setCurrentXDataArray(Doubles.toArray(Arrays.asList(curve.get_x())));
		tdpXY.get(tdpXY.size()-1).setCurrentYDataArray(Doubles.toArray(Arrays.asList(curve.get_y())));

		traceXY.add(new Trace(curve.get_legend(),					
				_xyGraph.primaryXAxis, _xyGraph.primaryYAxis, 
				tdpXY.get(tdpXY.size()-1)));		

		//set trace property
		traceXY.get(traceXY.size()-1).setPointStyle(curve.get_pointStyle());
		traceXY.get(traceXY.size()-1).setPointSize(curve.get_pointSize());
		traceXY.get(traceXY.size()-1).setTraceColor(
				XYGraphMediaFactory.getInstance().getColor(curve.get_color())
				);
		traceXY.get(traceXY.size()-1).setLineWidth(curve.get_lineWidth());

		//add the trace to xyGraph
		_xyGraph.addTrace( traceXY.get(traceXY.size()-1) );
	}

	/** 
	 * Set axis properties
	 * 
	 * @param x
	 * @param y
	 * @param title
	 * @param xLabel
	 * @param yLabel
	 * @param xUnit
	 * @param yUnit
	 * @param _xyGraph
	 */
	public static void setAxis(
			List<Double[]> x, List<Double[]> y,
			String title,
			String xLabel, String yLabel,
			String xUnit, String yUnit, 
			XYGraph _xyGraph) {

		double xMin = MyArrayUtils.searchMin(x),
				xMax = MyArrayUtils.searchMax(x),
				yMin = MyArrayUtils.searchMin(y),
				yMax = MyArrayUtils.searchMax(y);

		_xyGraph.setTitle(title);
		_xyGraph.primaryXAxis.setTitle(xLabel + " (" + xUnit + ")");
		_xyGraph.primaryYAxis.setTitle(yLabel + " (" + yUnit + ")");
		_xyGraph.primaryXAxis.setRange(xMin*0.95, xMax*1.05);
		_xyGraph.primaryYAxis.setRange(yMin*0.95, yMax*1.05);
	}

	/**
	 *  Insert a new element in an initiator panes
	 *  
	 * @param groupInput
	 * @param labelText
	 * @param latex_text
	 * @param objectToPrint
	 * @param rounding
	 * @return
	 */
	public Text putLabel(Group groupInput, String labelText, String latex_text, Object objectToPrint, int rounding){
		Amount<?> objectToPrintAmount;
		Double objectToPrintDouble;
		Label label = new Label(groupInput, SWT.NONE);
		label.setText(labelText);
		label.setLayoutData( new GridData( SWT.FILL, SWT.FILL, false, false, 2, 2 ) );

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Image formulaImage = MyGuiUtils.renderLatexFormula(_liftingSurfaceInitiator,latex_text, 22);
		Label label_a = new Label(groupInput, SWT.NONE);
		ADOPT_GUI.getfCachedImages().put(latex_text, formulaImage);
		label_a.setImage (formulaImage);

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++
		GridData gd_text = new GridData(); // (SWT.FILL, SWT.FILL, true, false);
		gd_text.horizontalSpan = 1;
		gd_text.grabExcessHorizontalSpace = true;
		gd_text.horizontalAlignment = GridData.FILL;
		gd_text.widthHint = TEXT_WIDTH_HINT;
		Text text = new Text(groupInput, SWT.BORDER);
		if (objectToPrint instanceof Amount<?>) {
			objectToPrintAmount = (Amount<?>) objectToPrint;
			if (objectToPrintAmount.getUnit() != SI.RADIAN){
				BigDecimal bd = BigDecimal.valueOf( objectToPrintAmount.getEstimatedValue());
				bd = bd.setScale(rounding, RoundingMode.DOWN);
				text.setText( "  " + bd + " " + objectToPrintAmount.getUnit().toString());
			} else {
				BigDecimal bd = BigDecimal.valueOf(Math.toDegrees(objectToPrintAmount.getEstimatedValue()));
				bd = bd.setScale(rounding, RoundingMode.DOWN);
				text.setText( "  " + bd + NonSI.DEGREE_ANGLE.toString());
			}
			text.setLayoutData(gd_text);
			text.setEditable(true);
		} else {
			objectToPrintDouble = (Double) objectToPrint;
			BigDecimal bd = BigDecimal.valueOf(objectToPrintDouble);
			bd = bd.setScale(rounding, RoundingMode.DOWN);
			text.setText( "  " + bd);
			text.setLayoutData(gd_text);
			text.setEditable(true);
		}

		return text;
	}

	/** 
	 * Handle icons using a map
	 * 
	 * @author LA
	 */
	public static void populateTreeImagesMap(Map<Object, String> imagesMap) {

		imagesMap.put(MyProjectTree.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/TreeView_24x24.png");
		imagesMap.put(Aircraft.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_24x24.png");
		
		imagesMap.put(Fuselage.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/fuselage29x16.png");
		
		imagesMap.put(LiftingSurface.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_24x24.png");
		imagesMap.put(Wing.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_24x24.png");
		imagesMap.put(HTail.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_24x24.png");
		imagesMap.put(VTail.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_24x24.png");
		imagesMap.put(Canard.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_24x24.png");
		
		imagesMap.put(LandingGear.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/AirplanePaper_24x24.png");
		imagesMap.put(PowerPlant.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(FuelTank.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(Nacelle.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(Systems.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		
		imagesMap.put(ACAnalysisManager.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(ACAerodynamicsManager.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(ACWeightsManager.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(ACBalanceManager.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(String.class, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/eclipse24.png");
		imagesMap.put(null, MyConfiguration.currentDirectoryString 
				+ "/src/it/unina/adopt/images/null_16x16.png");
	}

	public static void createInnerTabWithGraph(
			CTabItem tabItemGraph, 
			FigureCanvas fc, 
			LightweightSystem lws, 
			XYGraph xyGraph, 
			String name) {

		tabItemGraph.setText(name);
		fc.setScrollBarVisibility(FigureCanvas.NEVER);
		lws.setContents(new ToolbarArmedXYGraph(xyGraph));		

		// create a new XY Graph
		xyGraph.primaryXAxis.setShowMajorGrid(true);
		xyGraph.primaryYAxis.setShowMajorGrid(true);
		//		_xyGraph_XZ.primaryYAxis.setRange(_xyGraph_XZ.primaryXAxis.getRange());
		lws.setControl(fc);
		tabItemGraph.setControl(fc);
	}

	/**
	 * 
	 * @param bufferedImage
	 * @return
	 */
	static public ImageData convertToSWT(BufferedImage bufferedImage) {
		if (bufferedImage.getColorModel() instanceof DirectColorModel) {
			DirectColorModel colorModel = (DirectColorModel) bufferedImage.getColorModel();
			PaletteData palette = new PaletteData(colorModel.getRedMask(),
					colorModel.getGreenMask(), colorModel.getBlueMask());
			ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
					colorModel.getPixelSize(), palette);
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					int rgb = bufferedImage.getRGB(x, y);
					int pixel = palette.getPixel(new RGB((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF,
							rgb & 0xFF));
					data.setPixel(x, y, pixel);
					if (colorModel.hasAlpha()) {
						data.setAlpha(x, y, (rgb >> 24) & 0xFF);
					}
				}
			}
			return data;
		} else if (bufferedImage.getColorModel() instanceof IndexColorModel) {
			IndexColorModel colorModel = (IndexColorModel) bufferedImage.getColorModel();
			int size = colorModel.getMapSize();
			byte[] reds = new byte[size];
			byte[] greens = new byte[size];
			byte[] blues = new byte[size];
			colorModel.getReds(reds);
			colorModel.getGreens(greens);
			colorModel.getBlues(blues);
			RGB[] rgbs = new RGB[size];
			for (int i = 0; i < rgbs.length; i++) {
				rgbs[i] = new RGB(reds[i] & 0xFF, greens[i] & 0xFF, blues[i] & 0xFF);
			}
			PaletteData palette = new PaletteData(rgbs);
			ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
					colorModel.getPixelSize(), palette);
			data.transparentPixel = colorModel.getTransparentPixel();
			WritableRaster raster = bufferedImage.getRaster();
			int[] pixelArray = new int[1];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					raster.getPixel(x, y, pixelArray);
					data.setPixel(x, y, pixelArray[0]);
				}
			}
			return data;
		}
		return null;
	}

	/**
	 * 
	 * @param parent
	 * @param latex_text
	 * @param size
	 * @return
	 */
	static public Image renderLatexFormula(Composite parent, String latex_text, int size) {
		Image formulaImage = null;
		// see:
		// https://github.com/iee/iee/blob/master/org.eclipse.iee.sample.matrix/src/org/eclipse/iee/sample/matrix/pad/FormulaRenderer.java
		try {
			/* Translating to Latex */
			java.awt.Image awtImage =
					TeXFormula.createBufferedImage(
							latex_text,
							TeXConstants.STYLE_TEXT,
							size,
							java.awt.Color.black,
							new java.awt.Color(
									parent.getBackground().getRGB().red,
									parent.getBackground().getRGB().green,
									parent.getBackground().getRGB().blue
									) // java.awt.Color.white
							);

			ImageData swtImageData = convertToSWT((BufferedImage) awtImage);

			if (swtImageData == null)
			{
				System.out.println("No conversion from AWT to SWT Image");
				return null;

			}
			formulaImage = new Image(Display.getCurrent(), swtImageData);

		} catch (Exception e) {
			System.out.println("getFormulaImage() failed");
			// e.printStackTrace();
		}
		return formulaImage;
	}

	public static void recursiveSetEnabled(Control control, boolean enabled) {

		if (control instanceof Composite) {
			Composite comp = (Composite) control;

			for (Control c : comp.getChildren())
				recursiveSetEnabled(c, enabled);
			
		} else {
			control.setEnabled(enabled);
		}
	}
	
	public static void recursiveSetEditable(Control control, boolean editable) {

		if (control instanceof Composite) {
			Composite comp = (Composite) control;

			for (Control c : comp.getChildren())
				recursiveSetEditable(c, editable);
			
		} else {
			if (control instanceof Text)
			((Text) control).setEditable(editable);
		}
	}


	/** 
	 * A utility class to easily create a plot in a new tab
	 * 
	 * @author LA
	 */
	public static class PlotFactory {

		private CTabItem _tabItemCanvas = null;
		private FigureCanvas _fc = null;
		private LightweightSystem _lws = null;
		private XYGraph _xyGraph = null;

		private List<CTabItem> _tabItemCanvasList = new ArrayList<CTabItem>();
		private List<FigureCanvas> _fcList = new ArrayList<FigureCanvas>();
		private List<LightweightSystem> _lwsList = new ArrayList<LightweightSystem>();
		private List<XYGraph> _xyGraphList = new ArrayList<XYGraph>();

		public PlotFactory() {

		}

		/** 
		 * Create a plot in a new tab.
		 * The plot can be made of multiple x-y traces
		 * (a sort of "hold on" in matlab)
		 * 
		 * @author LA
		 * @param xList the list which holds the arrays with the discrete independent variable
		 * @param yList the list which holds the arrays with the discrete dependent variable
		 * @param legend
		 * @param title the title of the graph
		 * @param xUnit
		 * @param yUnit
		 * @param _tabFolderPlots
		 */
		public void newTabPlot( 
				List<Double[]> xList,
				List<Double[]> yList,
				String legend, 
				String title,
				String xLabel,
				String yLabel,
				String xUnit,
				String yUnit,
				CTabFolder _tabFolderPlots) {

			_tabItemCanvas = new CTabItem(_tabFolderPlots, SWT.NULL);
			_fc = new FigureCanvas(_tabFolderPlots);
			_lws = new LightweightSystem(_fc);
			_xyGraph = new XYGraph();

			createInnerTabWithGraph(
					_tabItemCanvas, _fc, _lws, 
					_xyGraph, title);

			createXYGraph(xList, yList, 
					legend, title, 
					xLabel, yLabel,
					xUnit, yUnit,
					_xyGraph);

			_tabItemCanvasList.add(_tabItemCanvas);			
			_fcList.add(_fc);
			_lwsList.add(_lws);
			_xyGraphList.add(_xyGraph);

		}

		/**
		 * Overloaded method. It allows to add a 
		 * different legend for each trace
		 * 
		 * @see newTabPlot
		 */
		public void newTabPlot( 
				List<Double[]> xList,
				List<Double[]> yList,
				List<String> legendList, 
				String tabItemTitle, 
				String title,
				String xLabel,
				String yLabel,
				String xUnit,
				String yUnit,
				CTabFolder _tabFolderPlots) {

			_tabItemCanvas = new CTabItem(_tabFolderPlots, SWT.NULL);
			_fc = new FigureCanvas(_tabFolderPlots);
			_lws = new LightweightSystem(_fc);
			_xyGraph = new XYGraph();

			createInnerTabWithGraph(
					_tabItemCanvas, _fc, _lws, 
					_xyGraph, tabItemTitle);

			createXYGraph(
					xList, yList, legendList, 
					title, 
					xLabel, yLabel,
					xUnit, yUnit,
					_xyGraph);

			_tabItemCanvasList.add(_tabItemCanvas);			
			_fcList.add(_fc);
			_lwsList.add(_lws);
			_xyGraphList.add(_xyGraph);

		}

		public CTabItem get_tabItemCanvas(int i) {
			return _tabItemCanvasList.get(i);
		}

		public FigureCanvas get_figureCanvas(int i) {
			return _fcList.get(i);
		}

		public LightweightSystem get_lws(int i) {
			return _lwsList.get(i);
		}

		public XYGraph get_xyGraph(int i) {
			return _xyGraphList.get(i);
		}

		public int getItemCount() {
			return _tabItemCanvasList.size();
		}

	}

}

