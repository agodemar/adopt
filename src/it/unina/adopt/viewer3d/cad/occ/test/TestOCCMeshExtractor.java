package it.unina.adopt.viewer3d.cad.occ.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.jcae.opencascade.jni.BRep_Builder;
import org.jcae.opencascade.jni.TopoDS_Compound;
import org.jcae.opencascade.jni.TopoDS_Edge;
import org.jcae.opencascade.jni.TopoDS_Face;
import org.jcae.opencascade.jni.TopoDS_Shape;
import org.jcae.opencascade.jni.TopoDS_Vertex;
import org.jcae.opencascade.jni.TopoDS_Wire;

import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;

import aircraft.OperatingConditions;
import aircraft.calculators.ACAnalysisManager;
import aircraft.components.Aircraft;
import cad.aircraft.MyAircraftBuilder;
import cad.occ.OCCFXMeshExtractor;
import javafx.scene.shape.TriangleMesh;

public class TestOCCMeshExtractor {

	private List<TriangleMesh> _theTriangleMeshList = new ArrayList<TriangleMesh>();

	public List<TriangleMesh> getTriangleMeshList() {
		return _theTriangleMeshList;
	}

	private Aircraft _theAircraft;
	private OperatingConditions _theOperatingConditions = new OperatingConditions();
	// Gotta create this object
	private ACAnalysisManager _theAnalysis = new ACAnalysisManager(_theOperatingConditions);

	public TestOCCMeshExtractor(Aircraft aircraft) {

		final Logger LOG = Logger.getLogger("log");

		_theAircraft = aircraft;

		if (_theAircraft != null) {

			// CAD/OCC/OccJava-related stuff

			BRep_Builder theBuilder = new BRep_Builder();
			TopoDS_Compound theCompound = new TopoDS_Compound();
			theBuilder.makeCompound(theCompound);

			MyAircraftBuilder aircraftBuilder = new MyAircraftBuilder();
			aircraftBuilder.buildCAD(_theAircraft);
			theBuilder.add(theCompound, aircraftBuilder.getTheCurrentCompound());

			OCCFXMeshExtractor occMeshExtractor = 
					new OCCFXMeshExtractor((TopoDS_Shape)theCompound);

			System.out.println("############################################");
			System.out.println("### CAD stats");
			System.out.println("############################################");

			Collection<TopoDS_Face> faces = occMeshExtractor.getFaces();
			System.out.println("Faces: " + faces.size());

			Collection<TopoDS_Edge> edges = occMeshExtractor.getEdges();
			System.out.println("Edges: " + edges.size());

			Collection<TopoDS_Edge> freeEdges = occMeshExtractor.getFreeEdges();
			System.out.println("Free Edges: " + freeEdges.size());

			Collection<TopoDS_Vertex> vertices = occMeshExtractor.getVertices();
			System.out.println("Vertices: " + vertices.size());

			Collection<TopoDS_Wire> wires = occMeshExtractor.getWires();
			System.out.println("Wires: " + wires.size());

			Collection<TopoDS_Compound> compounds = occMeshExtractor.getCompounds();
			System.out.println("Compounds: " + compounds.size());

			System.out.println("############################################");

			// Faces are not zero
			// TODO: now we can work on OCCMeshExtractor and pass
			//       the CAD object to the 3D-view

			// transform the Collection into a List
			List<TopoDS_Face> faceList = new ArrayList<TopoDS_Face>(faces);

			// collect data
			if (faceList.size() > 0) {

				System.out.println("Now take faces via OCCMeshExtractor.FaceData ...");

				// loop on all faces in CAD obj
				for(TopoDS_Face face : faceList) {

					// int kFace = 0;
					OCCFXMeshExtractor.FaceData faceData = 
							new OCCFXMeshExtractor.FaceData(
									// faceList.get(kFace),
									face,
									false
									);
					// build triangulation
					faceData.load();
					System.out.println(
							"--------- Triangulation ...\n" +
									"NbrOfPolys: " + faceData.getNbrOfPolys() + "\n" +
									"Polys length: " + faceData.getPolys().length + "\n" +
									"Polys:\n" + Ints.asList(faceData.getPolys()) + "\n" + // Guava
									"Triangles length: " + faceData.getITriangles().length + "\n" +
									"Triangles:\n" + Ints.asList(faceData.getITriangles()) + "\n" + // Guava
									"NbrOfVertices: " + faceData.getNbrOfVertices() + "\n" +
									"Vertices: " + faceData.getVertices().length + "\n" +
									"NbrOfLins: " + faceData.getNbrOfLines() + "\n" +
									"Lines: " + faceData.getLines().length + "\n" +
									"Nodes: " + faceData.getNodes().length
							);
					List<Float> nodes = Floats.asList(faceData.getNodes()); // Guava
					System.out.println(
							"Nodes :: " + nodes
							);

					// create the mesh
					TriangleMesh mesh = new TriangleMesh();

					//				Points, i.e. nodes
					//				mesh.getPoints().addAll(faceData.getNodes());
					//				
					//				//for now we'll just make an empty texCoordinate group
					//				mesh.getTexCoords().addAll(0, 0);
					//				
					//				//Add the faces "winding" the points generally counter clock wise
					//				for (int i = 0; i < faceData.getITriangles().length; )
					//				{
					//					// System.out.println("i: " + i);
					//					mesh.getFaces().addAll(
					//							faceData.getITriangles()[i++],0,
					//							faceData.getITriangles()[i++],0,
					//							faceData.getITriangles()[i++],0
					//					);
					//				}

					mesh = faceData.getTriangleMesh();
					//				System.out.println(
					//						"TriangleMesh :: FaceElementSize: " + _theTriangleMesh.getFaceElementSize()
					//				);

					_theTriangleMeshList.add(mesh);

				} // end-of-for loop

			} // if faces != null

		}

	} // end-of-constructor

	//	public TestOCCMeshExtractor() {
	//		this(
	//			new MyAircraft(
	//				MyComponent.ComponentEnum.FUSELAGE, 
	//				MyComponent.ComponentEnum.WING,
	//				MyComponent.ComponentEnum.HORIZONTAL_TAIL,
	//				MyComponent.ComponentEnum.VERTICAL_TAIL,
	//				MyComponent.ComponentEnum.FUEL_TANK,
	//				MyComponent.ComponentEnum.POWER_PLANT,
	//				MyComponent.ComponentEnum.NACELLE,
	//				MyComponent.ComponentEnum.LANDING_GEAR,
	//				MyComponent.ComponentEnum.SYSTEMS
	//				)
	//		);
	//	}

	//	public static void main(String[] args)
	//	{
	//		TestOCCMeshExtractor test = new TestOCCMeshExtractor();
	//	}
}