package it.unina.adopt.viewer3d.cad.occ.test;


public class TestViewableCAD2 {
	
//	public TestViewableCAD2() {
//		
//	}
//	
//	public static void main(String[] args)
//	{
//		
//		Logger LOG = Logger.getLogger("log");
//
//		// Default operating conditions
//		MyOperatingConditions theOperatingConditions = new MyOperatingConditions();
//		
//		// Initialize Aircraft with default parameters
//		MyAircraft theAircraft = new MyAircraft(
//				MyComponent.ComponentEnum.FUSELAGE, 
//				MyComponent.ComponentEnum.WING,
//				MyComponent.ComponentEnum.HORIZONTAL_TAIL,
//				MyComponent.ComponentEnum.VERTICAL_TAIL,
//				MyComponent.ComponentEnum.FUEL_TANK,
//				MyComponent.ComponentEnum.POWER_PLANT,
//				MyComponent.ComponentEnum.NACELLE,
//				MyComponent.ComponentEnum.LANDING_GEAR,
//				MyComponent.ComponentEnum.SYSTEMS
//				);
//		
//		// Gotta create this object
//		MyAnalysis theAnalysis = new MyAnalysis(
//				theOperatingConditions,
//				theAircraft);
//		
//		// ... and calculate auxiliary geometrical data
//		theAnalysis.updateGeometry(theAircraft);
//		
//		// CAD/OCC/OccJava-related stuff
//		
//		BRep_Builder theBuilder = new BRep_Builder();
//		TopoDS_Compound theCompound = new TopoDS_Compound();
//		theBuilder.makeCompound(theCompound);
//		
//		MyAircraftBuilder aircraftBuilder = new MyAircraftBuilder();
//		aircraftBuilder.buildCAD(theAircraft);
//		theBuilder.add(theCompound, aircraftBuilder.getTheCurrentCompound());
//
//		OCCMeshExtractor occMeshExtractor = 
//				new OCCMeshExtractor((TopoDS_Shape)theCompound);
//		
//		System.out.println("############################################");
//		System.out.println("### CAD stats");
//		System.out.println("############################################");
//		
//		Collection<TopoDS_Face> faces = occMeshExtractor.getFaces();
//		System.out.println("Faces: " + faces.size());
//
//		Collection<TopoDS_Edge> edges = occMeshExtractor.getEdges();
//		System.out.println("Edges: " + edges.size());
//
//		Collection<TopoDS_Edge> freeEdges = occMeshExtractor.getFreeEdges();
//		System.out.println("Free Edges: " + freeEdges.size());
//
//		Collection<TopoDS_Vertex> vertices = occMeshExtractor.getVertices();
//		System.out.println("Vertices: " + vertices.size());
//
//		Collection<TopoDS_Wire> wires = occMeshExtractor.getWires();
//		System.out.println("Wires: " + wires.size());
//
//		Collection<TopoDS_Compound> compounds = occMeshExtractor.getCompounds();
//		System.out.println("Compounds: " + compounds.size());
//
//		System.out.println("############################################");
//		
//		// Faces are not zero
//		// TODO: now we can work on OCCMeshExtractor and pass
//		//       the CAD object to the 3D-view
//		
//		// transform the Collection into a List
//		List<TopoDS_Face> faceList = new ArrayList<TopoDS_Face>(faces);
//		
//		// collect data
//		if (faceList.size() > 0) {
//			
//			System.out.println("Now take a Face via OCCMeshExtractor.FaceData ...");
//			
//			int kFace = 0;
//			OCCMeshExtractor.FaceData faceData = 
//					new OCCMeshExtractor.FaceData(faceList.get(kFace),false);
//			// build triangulation
//			faceData.load();
//			System.out.println(
//				"--------- Triangulation ...\n" +
//				"NbrOfPolys: " + faceData.getNbrOfPolys() + "\n" +
//				"Polys: " + faceData.getPolys().length + "\n" +
//				Ints.asList(faceData.getPolys()) + "\n" + // Guava
//				"NbrOfVertices: " + faceData.getNbrOfVertices() + "\n" +
//				"Vertices: " + faceData.getVertices().length + "\n" +
//				"NbrOfLins: " + faceData.getNbrOfLines() + "\n" +
//				"Lines: " + faceData.getLines().length + "\n" +
//				"Nodes: " + faceData.getNodes().length
//			);
//			List<Float> nodes = Floats.asList(faceData.getNodes()); // Guava
//			System.out.println(
//					"Nodes :: " + nodes
//			);
//		}
//		
//	}

}