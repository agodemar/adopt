<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" encoding="UTF-8"/>

<xsl:template match="/mooAircraft_config">
	<html>
		<head>
			<script
				type="text/javascript" 
				src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
			</script>
            <!-- TO DO: add css style -->
			<title>
				<xsl:value-of select="@name"/>
			</title>
			<style>
					#adoptTable
					{
					font-family:Arial, Helvetica, sans-serif;
					border-collapse:collapse;
					}
					#adoptTable td, #adoptTable th 
					{
					font-size:1em;
					border:2px solid #224488;
					padding:5px 9px 4px 9px;
					}
					#adoptTable th 
					{
					font-size:1.1em;
					text-align:left;
					padding-top:7px;
					padding-bottom:6px;
					background-color:#224488;
					color:#ffffff;
					}
					#adoptTable tr.alt td 
					{
					color:#000000;
					background-color:#224488;
					}
			</style>
            </head>
            <body style="font-family:Arial;font-size:90%">
                <a name="top"/>
                <font face="Arial" size="3" color="224488">
                    <b>
                        <xsl:value-of select="@name"/>
                    </b>
                </font>
                <br/>
                <font face="Arial" size="2">Configuration File Version: <xsl:value-of
                        select="@version"/></font>
                <br/>
                <font face="Arial" size="2">
                    Release level: <xsl:value-of select="@release"/></font>
                <br/>
                <hr width="100%"/>

                <p/>

                <xsl:if test="fileheader">
                    <!-- FILEHEADER -->
                    <table 
                        width="80%" bgcolor="EEEEEE" cellpadding="0" cellspacing="0"
                        style="font-family:arial;font-size:90%">
                        <tr>
                            <td colspan="2">
                                <b>FILE INFORMATION</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="3">
                                <hr size="1"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Author[s]</b>
                            </td>
                            <td valign="top" align="left">
                                <xsl:for-each select="fileheader/author">
                                <xsl:value-of select="."/>, </xsl:for-each>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150">
                                <b>File created</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="fileheader/filecreationdate"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="fileheader/description"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150">
                                <b>Model version</b>
                            </td>
                            <td>
                                <xsl:value-of select="fileheader/version"/>
                            </td>
                        </tr>
                        <xsl:if test="fileheader/reference">
                            <tr>
                                <td width="150">
                                    <b>References:</b>
                                    <br/>
                                </td>
                                <td/>
                            </tr>
                            <xsl:for-each select="fileheader/reference">
                                <tr valign="top">
                                    <td>
                                        <xsl:value-of select="@refID"/>
                                    </td>
                                    <td>
                                        <i>
                                            <xsl:value-of select="@title"/>
                                        </i>
                                    </td>
                                    <td>
                                        <xsl:value-of select="@author"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="@date"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="fileheader/note">
                            <tr>
                                <td width="100">
                                    <b>Notes:</b>
                                    <br/>
                                </td>
                                <td/>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ol>
                                        <xsl:for-each select="fileheader/note">
                                            <li>
                                                <xsl:value-of select="."/>
                                            </li>
                                        </xsl:for-each>
                                    </ol>
                                </td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="fileheader/limitation">
                            <tr>
                                <td width="150">
                                    <b>Limitations:</b>
                                    <br/>
                                </td>
                                <td/>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ol>
                                        <xsl:for-each select="fileheader/limitation">
                                            <li>
                                                <xsl:value-of select="."/>
                                            </li>
                                        </xsl:for-each>
                                    </ol>
                                </td>
                            </tr>
                        </xsl:if>
                    </table>

                    <p>
                        <font face="Arial" size="2">[<a href="#top">Top</a>]</font>
                    </p>

                </xsl:if>

                <p/>

                <!-- Try some programming -->
                <table id="adoptTable" width="90%">
                    <tr><th>Quantity</th><th>Value</th><th>Tag</th></tr>
                    <xsl:apply-templates select="//Fuselage_Parameters"/>
                </table>

            </body>
        </html>
    </xsl:template>

    <xsl:template match="//Fuselage_Parameters">
        <tr>
            <td width="20%">
			
				<!-- \(<xsl:value-of select="./symbol"/>=\)<xsl:text> </xsl:text> -->
				\(M_{\mathrm{fus}}=\)<xsl:text> </xsl:text>
				<xsl:value-of select="./Mass"/>
                <xsl:choose>
                    <xsl:when test="./Mass/@unit">
                        <xsl:text> </xsl:text><xsl:value-of select="./Mass/@unit"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- TO DO: handle this with a case or a function -->
                        <!-- (non-dim.) -->
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td align="left">
                <!-- <strong><xsl:value-of select="../description"/></strong> -->
				
            </td>
            <td align="left">
                <strong>
                <xsl:value-of select="local-name(..)"/></strong>
            </td>
        </tr>
    </xsl:template>
    
</xsl:stylesheet>